﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Diagnostics;
using Oracle.ManagedDataAccess.Client;
namespace yz_monitoring_realtime.cls
{
    class Batch
    {
        #region Main Proccess
        OracleConnection con;

        public static string dbconnyz = yz_monitoring_realtime.Properties.Settings.Default.yzdb;
        #region Declare Demographics
        public static bool is_rekam_ktp = yz_monitoring_realtime.Properties.Settings.Default.is_rekam_ktp;
        public static bool is_cetak_ktp = yz_monitoring_realtime.Properties.Settings.Default.is_cetak_ktp;
        public static bool is_prr = yz_monitoring_realtime.Properties.Settings.Default.is_prr;
        public static bool is_sfe = yz_monitoring_realtime.Properties.Settings.Default.is_sfe;
        public static bool is_duplicate = yz_monitoring_realtime.Properties.Settings.Default.is_duplicate;
        public static bool is_sisa_suket = yz_monitoring_realtime.Properties.Settings.Default.is_sisa_suket;
        public static bool is_blangko_out = yz_monitoring_realtime.Properties.Settings.Default.is_blangko_out;
        public static bool is_blangko_sisa = yz_monitoring_realtime.Properties.Settings.Default.is_blangko_sisa;
        #endregion
        #region Declare Dafduk
        public static bool is_cetak_kk = yz_monitoring_realtime.Properties.Settings.Default.is_cetak_kk;
        public static bool is_cetak_kia = yz_monitoring_realtime.Properties.Settings.Default.is_cetak_kia;
        public static bool is_nik_baru = yz_monitoring_realtime.Properties.Settings.Default.is_nik_baru;
        public static bool is_pindah_antar_kab = yz_monitoring_realtime.Properties.Settings.Default.is_pindah_antar_kab;
        public static bool is_pindah_antar_kec = yz_monitoring_realtime.Properties.Settings.Default.is_pindah_antar_kec;
        public static bool is_pindah_dalam_kec = yz_monitoring_realtime.Properties.Settings.Default.is_pindah_dalam_kec;
        public static bool is_datang_antar_kab = yz_monitoring_realtime.Properties.Settings.Default.is_datang_antar_kab;
        public static bool is_datang_antar_kec = yz_monitoring_realtime.Properties.Settings.Default.is_datang_antar_kec;
        public static bool is_datang_dalam_kec = yz_monitoring_realtime.Properties.Settings.Default.is_datang_dalam_kec;
        #endregion
        #region Declare Capil
        public static bool is_akta_lhr_um = yz_monitoring_realtime.Properties.Settings.Default.is_akta_lhr_um;
        public static bool is_akta_lhr_lt = yz_monitoring_realtime.Properties.Settings.Default.is_akta_lhr_lt;
        public static bool is_akta_mt = yz_monitoring_realtime.Properties.Settings.Default.is_akta_mt;
        public static bool is_akta_kwn = yz_monitoring_realtime.Properties.Settings.Default.is_akta_kwn;
        public static bool is_akta_cry = yz_monitoring_realtime.Properties.Settings.Default.is_akta_cry;
        #endregion
        private string app_path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location.ToString());

        public void process()
        {
            Batch bct = new Batch();
            Console.WriteLine("Checking Connections");
            this.ReadSystemFile("Checking First Connections");
            Console.WriteLine(dbconnyz);
            #region Do Check Demographics
            if (is_rekam_ktp)
            {
                Console.WriteLine("Dashboard_perekaman");
                bct.Dashboard_perekaman();
            }
            if (is_cetak_ktp)
            {
                Console.WriteLine("Dashboard_pencetakan_ktp");
                bct.Dashboard_pencetakan_ktp();
            }
            if (is_prr)
            {
                Console.WriteLine("Dashboard_prr");
                bct.Dashboard_prr();
            }
            if (is_sfe)
            {
                Console.WriteLine("Dashboard_sfe");
                bct.Dashboard_sfe();
            }
            if (is_duplicate)
            {
                Console.WriteLine("Dashboard_duplicate");
                bct.Dashboard_duplicate();
            }
            if (is_sisa_suket)
            {
                Console.WriteLine("Dashboard_sisa_suket");
                bct.Dashboard_sisa_suket();
            }
            if (is_blangko_out)
            {
                Console.WriteLine("Dashboard_blangko_out");
                bct.Dashboard_blangko_out();
            }
            if (is_blangko_sisa)
            {
                Console.WriteLine("Dashboard_blangko_sisa");
                bct.Dashboard_blangko_sisa();
            }
            #endregion
            #region Do Check Dafduk
            if (is_cetak_kk)
            {
                Console.WriteLine("Dashboard_cetak_kk");
                bct.Dashboard_cetak_kk();
            }
            if (is_cetak_kia)
            {
                Console.WriteLine("Dashboard_cetak_kia");
                bct.Dashboard_cetak_kia();
            }
            if (is_nik_baru)
            {
                Console.WriteLine("Dashboard_input_nik");
                bct.Dashboard_input_nik();
            }
            if (is_pindah_antar_kab)
            {
                Console.WriteLine("Dashboard_pindah_antar_kab");
                bct.Dashboard_pindah_antar_kab();
            }
            if (is_pindah_antar_kab)
            {
                Console.WriteLine("Dashboard_pindah_antar_kab");
                bct.Dashboard_pindah_antar_kab();
            }
            if (is_pindah_antar_kec)
            {
                Console.WriteLine("Dashboard_pindah_antar_kec");
                bct.Dashboard_pindah_antar_kec();
            }
            if (is_pindah_dalam_kec)
            {
                Console.WriteLine("Dashboard_pindah_dalam_kec");
                bct.Dashboard_pindah_dalam_kec();
            }
            if (is_datang_antar_kab)
            {
                Console.WriteLine("Dashboard_datang_antar_kab");
                bct.Dashboard_datang_antar_kab();
            }
            if (is_datang_antar_kec)
            {
                Console.WriteLine("Dashboard_datang_antar_kec");
                bct.Dashboard_datang_antar_kec();
            }
            if (is_datang_dalam_kec)
            {
                Console.WriteLine("Dashboard_datang_dalam_kec");
                bct.Dashboard_datang_dalam_kec();
            }
            #endregion
            #region Do Check Capil
            if (is_akta_lhr_um)
            {
                Console.WriteLine("Dashboard_cetak_akta_lhr_um");
                bct.Dashboard_cetak_akta_lhr_um();
            }
            if (is_akta_lhr_lt)
            {
                Console.WriteLine("Dashboard_cetak_akta_lhr_lt");
                bct.Dashboard_cetak_akta_lhr_lt();
            }
            if (is_akta_mt)
            {
                Console.WriteLine("Dashboard_cetak_akta_mt");
                bct.Dashboard_cetak_akta_mt();
            }
            if (is_akta_kwn)
            {
                Console.WriteLine("Dashboard_cetak_akta_kwn");
                bct.Dashboard_cetak_akta_kwn();
            }
            if (is_akta_cry)
            {
                Console.WriteLine("Dashboard_cetak_akta_cry");
                bct.Dashboard_cetak_akta_cry();
            }
            #endregion
            Console.WriteLine("Progres Done For One Cycle");
            this.ReadSystemFile("Progres Done For One Cycle");
            bct.Closeall();
            Console.WriteLine("Close All Connections");
            this.ReadSystemFile("Close All Connections");
        }
        #region Set Dashboard Demographics
        void Dashboard_perekaman()
        {
            try
            {
                con = new OracleConnection();
                con.ConnectionString = dbconnyz;
                con.Open();
                Console.WriteLine("Connected to Oracle db yzdb get perekaman " + con.ServerVersion);
                this.ReadSystemFile("1. Connected to Oracle db yzdb get perekaman " + con.ServerVersion);
                get_data_rekam();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.WriteToFile("1. " + ex.Message + "Connected to Oracle db yzdb get perekaman : {0}");
            }

        }
        void Dashboard_pencetakan_ktp()
        {
            try
            {
                con = new OracleConnection();
                con.ConnectionString = dbconnyz;
                con.Open();
                Console.WriteLine("Connected to Oracle db yzdb get pencetakan ktp-el " + con.ServerVersion);
                this.ReadSystemFile("2. Connected to Oracle db yzdb get pencetakan ktp-el " + con.ServerVersion);
                get_data_cetak_ktp();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.WriteToFile("2. " + ex.Message + "Connected to Oracle db yzdb get pencetakan ktp-el : {0}");
            }

        }
        void Dashboard_prr()
        {
            try
            {
                con = new OracleConnection();
                con.ConnectionString = dbconnyz;
                con.Open();
                Console.WriteLine("Connected to Oracle db yzdb get prr " + con.ServerVersion);
                this.ReadSystemFile("3. Connected to Oracle db yzdb get prr " + con.ServerVersion);
                get_data_prr();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.WriteToFile("3. " + ex.Message + "Connected to Oracle db yzdb get prr : {0}");
            }

        }
        void Dashboard_sfe()
        {
            try
            {
                con = new OracleConnection();
                con.ConnectionString = dbconnyz;
                con.Open();
                Console.WriteLine("Connected to Oracle db yzdb get sfe " + con.ServerVersion);
                this.ReadSystemFile("4. Connected to Oracle db yzdb get sfe " + con.ServerVersion);
                get_data_sfe();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.WriteToFile("4. " + ex.Message + "Connected to Oracle db yzdb get sfe : {0}");
            }

        }
        void Dashboard_duplicate()
        {
            try
            {
                con = new OracleConnection();
                con.ConnectionString = dbconnyz;
                con.Open();
                Console.WriteLine("Connected to Oracle db yzdb get duplicate " + con.ServerVersion);
                this.ReadSystemFile("5. Connected to Oracle db yzdb get duplicate " + con.ServerVersion);
                get_data_duplicate();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.WriteToFile("5. " + ex.Message + "Connected to Oracle db yzdb get duplicate : {0}");
            }

        }
        void Dashboard_sisa_suket()
        {
            try
            {
                con = new OracleConnection();
                con.ConnectionString = dbconnyz;
                con.Open();
                Console.WriteLine("Connected to Oracle db yzdb get sisa suket " + con.ServerVersion);
                this.ReadSystemFile("6. Connected to Oracle db yzdb get sisa suket " + con.ServerVersion);
                get_data_sisa_suket();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.WriteToFile("6. " + ex.Message + "Connected to Oracle db yzdb get sisa suket : {0}");
            }

        }
        void Dashboard_blangko_out()
        {
            try
            {
                con = new OracleConnection();
                con.ConnectionString = dbconnyz;
                con.Open();
                Console.WriteLine("Connected to Oracle db yzdb get pengeluaran keping " + con.ServerVersion);
                this.ReadSystemFile("7. Connected to Oracle db yzdb get pengeluaran keping " + con.ServerVersion);
                get_data_blangko_out();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.WriteToFile("7. " + ex.Message + "Connected to Oracle db yzdb get pengeluaran keping : {0}");
            }

        }
        void Dashboard_blangko_sisa()
        {
            try
            {
                con = new OracleConnection();
                con.ConnectionString = dbconnyz;
                con.Open();
                Console.WriteLine("Connected to Oracle db yzdb get sisa keping " + con.ServerVersion);
                this.ReadSystemFile("8. Connected to Oracle db yzdb get sisa keping " + con.ServerVersion);
                get_data_blangko_sisa();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.WriteToFile("8. " + ex.Message + "Connected to Oracle db yzdb get sisa keping : {0}");
            }

        }
        #endregion
        #region Set Dashboard Dafduk
        void Dashboard_cetak_kk()
        {
            try
            {
                con = new OracleConnection();
                con.ConnectionString = dbconnyz;
                con.Open();
                Console.WriteLine("Connected to Oracle db yzdb get cetak kk " + con.ServerVersion);
                this.ReadSystemFile("9. Connected to Oracle db yzdb get cetak kk " + con.ServerVersion);
                get_data_cetak_kk();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.WriteToFile("9. " + ex.Message + "Connected to Oracle db yzdb get cetak kk : {0}");
            }

        }
        void Dashboard_cetak_kia()
        {
            try
            {
                con = new OracleConnection();
                con.ConnectionString = dbconnyz;
                con.Open();
                Console.WriteLine("Connected to Oracle db yzdb get cetak kia " + con.ServerVersion);
                this.ReadSystemFile("10. Connected to Oracle db yzdb get cetak kia " + con.ServerVersion);
                get_data_cetak_kia();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.WriteToFile("10. " + ex.Message + "Connected to Oracle db yzdb get cetak kia : {0}");
            }

        }
        void Dashboard_input_nik()
        {
            try
            {
                con = new OracleConnection();
                con.ConnectionString = dbconnyz;
                con.Open();
                Console.WriteLine("Connected to Oracle db yzdb get input nik " + con.ServerVersion);
                this.ReadSystemFile("11. Connected to Oracle db yzdb get input nik " + con.ServerVersion);
                get_data_nik_baru();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.WriteToFile("11. " + ex.Message + "Connected to Oracle db yzdb get input nik : {0}");
            }

        }
        void Dashboard_pindah_antar_kab()
        {
            try
            {
                con = new OracleConnection();
                con.ConnectionString = dbconnyz;
                con.Open();
                Console.WriteLine("Connected to Oracle db yzdb get pindah antar kab " + con.ServerVersion);
                this.ReadSystemFile("12. Connected to Oracle db yzdb get pindah antar kab " + con.ServerVersion);
                get_data_pindah_antar_kab();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.WriteToFile("12. " + ex.Message + "Connected to Oracle db yzdb get pindah antar kab : {0}");
            }

        }
        void Dashboard_pindah_antar_kec()
        {
            try
            {
                con = new OracleConnection();
                con.ConnectionString = dbconnyz;
                con.Open();
                Console.WriteLine("Connected to Oracle db yzdb get pindah antar kec " + con.ServerVersion);
                this.ReadSystemFile("13. Connected to Oracle db yzdb get pindah antar kec " + con.ServerVersion);
                get_data_pindah_antar_kec();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.WriteToFile("13. " + ex.Message + "Connected to Oracle db yzdb get pindah antar kec : {0}");
            }

        }
        void Dashboard_pindah_dalam_kec()
        {
            try
            {
                con = new OracleConnection();
                con.ConnectionString = dbconnyz;
                con.Open();
                Console.WriteLine("Connected to Oracle db yzdb get pindah dalam kec " + con.ServerVersion);
                this.ReadSystemFile("14. Connected to Oracle db yzdb get pindah dalam kec " + con.ServerVersion);
                get_data_pindah_dalam_kec();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.WriteToFile("14. " + ex.Message + "Connected to Oracle db yzdb get pindah dalam kec : {0}");
            }

        }
        void Dashboard_datang_antar_kab()
        {
            try
            {
                con = new OracleConnection();
                con.ConnectionString = dbconnyz;
                con.Open();
                Console.WriteLine("Connected to Oracle db yzdb get datang antar kab " + con.ServerVersion);
                this.ReadSystemFile("12. Connected to Oracle db yzdb get datang antar kab " + con.ServerVersion);
                get_data_datang_antar_kab();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.WriteToFile("12. " + ex.Message + "Connected to Oracle db yzdb get datang antar kab : {0}");
            }

        }
        void Dashboard_datang_antar_kec()
        {
            try
            {
                con = new OracleConnection();
                con.ConnectionString = dbconnyz;
                con.Open();
                Console.WriteLine("Connected to Oracle db yzdb get datang antar kec " + con.ServerVersion);
                this.ReadSystemFile("13. Connected to Oracle db yzdb get datang antar kec " + con.ServerVersion);
                get_data_datang_antar_kec();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.WriteToFile("13. " + ex.Message + "Connected to Oracle db yzdb get datang antar kec : {0}");
            }

        }
        void Dashboard_datang_dalam_kec()
        {
            try
            {
                con = new OracleConnection();
                con.ConnectionString = dbconnyz;
                con.Open();
                Console.WriteLine("Connected to Oracle db yzdb get datang dalam kec " + con.ServerVersion);
                this.ReadSystemFile("14. Connected to Oracle db yzdb get datang dalam kec " + con.ServerVersion);
                get_data_datang_dalam_kec();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.WriteToFile("14. " + ex.Message + "Connected to Oracle db yzdb get datang dalam kec : {0}");
            }

        }

        #endregion
        #region Set Dashboard Capil
        void Dashboard_cetak_akta_lhr_um()
        {
            try
            {
                con = new OracleConnection();
                con.ConnectionString = dbconnyz;
                con.Open();
                Console.WriteLine("Connected to Oracle db yzdb get akta lahir umum " + con.ServerVersion);
                this.ReadSystemFile("15. Connected to Oracle db yzdb get akta lahir umum " + con.ServerVersion);
                get_data_akta_lhr_um();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.WriteToFile("15. " + ex.Message + "Connected to Oracle db yzdb get akta lahir umum : {0}");
            }

        }
        void Dashboard_cetak_akta_lhr_lt()
        {
            try
            {
                con = new OracleConnection();
                con.ConnectionString = dbconnyz;
                con.Open();
                Console.WriteLine("Connected to Oracle db yzdb get akta lahir terlambat " + con.ServerVersion);
                this.ReadSystemFile("16. Connected to Oracle db yzdb get akta lahir terlambat " + con.ServerVersion);
                get_data_akta_lhr_lt();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.WriteToFile("16. " + ex.Message + "Connected to Oracle db yzdb get akta lahir terlambat : {0}");
            }

        }
        void Dashboard_cetak_akta_mt()
        {
            try
            {
                con = new OracleConnection();
                con.ConnectionString = dbconnyz;
                con.Open();
                Console.WriteLine("Connected to Oracle db yzdb get akta kematian " + con.ServerVersion);
                this.ReadSystemFile("17. Connected to Oracle db yzdb get akta kematian " + con.ServerVersion);
                get_data_akta_mt();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.WriteToFile("17. " + ex.Message + "Connected to Oracle db yzdb get akta kematian : {0}");
            }

        }
        void Dashboard_cetak_akta_kwn()
        {
            try
            {
                con = new OracleConnection();
                con.ConnectionString = dbconnyz;
                con.Open();
                Console.WriteLine("Connected to Oracle db yzdb get akta perkawinan " + con.ServerVersion);
                this.ReadSystemFile("18. Connected to Oracle db yzdb get akta perkawinan " + con.ServerVersion);
                get_data_akta_kwn();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.WriteToFile("18. " + ex.Message + "Connected to Oracle db yzdb get akta perkawinan : {0}");
            }

        }
        void Dashboard_cetak_akta_cry()
        {
            try
            {
                con = new OracleConnection();
                con.ConnectionString = dbconnyz;
                con.Open();
                Console.WriteLine("Connected to Oracle db yzdb get akta perceraian " + con.ServerVersion);
                this.ReadSystemFile("19. Connected to Oracle db yzdb get akta perceraian " + con.ServerVersion);
                get_data_akta_cry();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.WriteToFile("19. " + ex.Message + "Connected to Oracle db yzdb get akta perceraian : {0}");
            }

        }
        #endregion
        #region State Dashboard Demographics
        #region State Get Data Perekaman
        public void get_data_rekam()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    Console.WriteLine("1. State Get Data Perekaman {0}", connection.State);
                    this.WriteToFile("1. State Get Data Perekaman " + connection.State);
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT COUNT(1) AS CNT FROM SIAK_DASHBOARD WHERE ID = 'GET_PEREKAMAN' AND TO_CHAR(CREATED_DT,'DD-MM-YYYY') = TO_CHAR(SYSDATE,'DD-MM-YYYY')";
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        decimal cnt = (decimal)reader["CNT"];
                        Console.WriteLine("count data: {0}", cnt);
                        if (cnt > 0)
                        {
                            update_data_rekam();
                        }
                        else
                        {
                            this.WriteToFile("1. Proccess Starting insert Data Perekaman {0}");
                            insert_data_rekam();
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("1. " + ex.Message + "Proccess Get Data Perekaman {0}");
                }
                finally
                {
                    connection.Close();
                }

            }
        }
        public void insert_data_rekam()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO INSERT DASHBOARD DATA REKAM");
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "INSERT INTO SIAK_DASHBOARD C (C.ID,C.CREATED_DT, C.VAL)  SELECT A.ID, A.CREATED_DT, A.VAL FROM (SELECT 'GET_PEREKAMAN' ID, SYSDATE CREATED_DT, COUNT(1) VAL FROM DEMOGRAPHICS@DB221 A WHERE EXISTS(SELECT 1 FROM FACES@DB221 B WHERE A.NIK = B.NIK) AND TO_CHAR(A.CREATED,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY')) A WHERE NOT EXISTS (SELECT 1 FROM SIAK_DASHBOARD C WHERE A.ID = C.ID AND TO_CHAR(A.CREATED_DT,'DD/MM/YYYY') = TO_CHAR(C.CREATED_DT,'DD/MM/YYYY'))";
                    Console.WriteLine("INSERT DASHBOARD DATA REKAM");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("Proccess Insert Data Rekam " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void update_data_rekam()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO UPDATE DASHBOARD DATA REKAM");
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "UPDATE SIAK_DASHBOARD C SET (C.CREATED_DT, C.VAL) = (SELECT SYSDATE CREATED_DT, COUNT(1) VAL FROM DEMOGRAPHICS@DB221 A WHERE EXISTS(SELECT 1 FROM FACES@DB221 B WHERE A.NIK = B.NIK) AND TO_CHAR(A.CREATED,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY')) WHERE C.ID = 'GET_PEREKAMAN' AND TO_CHAR(SYSDATE,'DD/MM/YYYY') = TO_CHAR(C.CREATED_DT,'DD/MM/YYYY')";
                    Console.WriteLine("UPDATE DASHBOARD DATA REKAM");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("Proccess Update Data Rekam " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        #endregion
        #region State Get Data Pencetakan KTP-EL
        public void get_data_cetak_ktp()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    Console.WriteLine("2. State Get Data Pencetakan Ktp-el {0}", connection.State);
                    this.WriteToFile("2. State Get Data Pencetakan Ktp-el " + connection.State);
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT COUNT(1) AS CNT FROM SIAK_DASHBOARD WHERE ID = 'GET_PENCETAKAN_KTP' AND TO_CHAR(CREATED_DT,'DD-MM-YYYY') = TO_CHAR(SYSDATE,'DD-MM-YYYY')";
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        decimal cnt = (decimal)reader["CNT"];
                        Console.WriteLine("count data: {0}", cnt);
                        if (cnt > 0)
                        {
                            update_data_cetak_ktp();
                        }
                        else
                        {
                            this.WriteToFile("2. Proccess Starting insert Data Pencetakan Ktp-el {0}");
                            insert_data_cetak_ktp();
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("2. " + ex.Message + "Proccess Get Data Perekaman {0}");
                }
                finally
                {
                    connection.Close();
                }

            }
        }
        public void insert_data_cetak_ktp()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO INSERT DASHBOARD DATA PENCETAKAN KTP-EL");
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "INSERT INTO SIAK_DASHBOARD C (C.ID,C.CREATED_DT, C.VAL) SELECT A.ID, A.CREATED_DT, A.VAL FROM (SELECT 'GET_PENCETAKAN_KTP' ID, SYSDATE CREATED_DT, COUNT(1) AS VAL FROM CARD_MANAGEMENT@DB2 A WHERE CASE WHEN TO_CHAR(LAST_UPDATE,'DD/MM/YYYY') IS NULL THEN TO_CHAR(PERSONALIZED_DATE,'DD/MM/YYYY') ELSE TO_CHAR(LAST_UPDATE,'DD/MM/YYYY') END =  TO_CHAR(SYSDATE,'DD/MM/YYYY') AND CASE WHEN UPPER(A.LAST_UPDATED_USERNAME) IS NULL THEN UPPER(A.CREATED_USERNAME) ELSE UPPER(A.LAST_UPDATED_USERNAME) END NOT IN ('32730000YANZEN','32730000DNS','32730000AZI')) A WHERE NOT EXISTS (SELECT 1 FROM SIAK_DASHBOARD C WHERE A.ID = C.ID AND TO_CHAR(A.CREATED_DT,'DD/MM/YYYY') = TO_CHAR(C.CREATED_DT,'DD/MM/YYYY'))";
                    Console.WriteLine("INSERT DASHBOARD DATA PENCETAKAN KTP-EL");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("Proccess Insert Data Pencetakan Ktp-El " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void update_data_cetak_ktp()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO UPDATE DASHBOARD DATA PENCETAKAN KTP-EL");
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "UPDATE SIAK_DASHBOARD C SET (C.CREATED_DT, C.VAL) = (SELECT SYSDATE CREATED_DT, COUNT(1) AS VAL FROM CARD_MANAGEMENT@DB2 A WHERE CASE WHEN TO_CHAR(LAST_UPDATE,'DD/MM/YYYY') IS NULL THEN TO_CHAR(PERSONALIZED_DATE,'DD/MM/YYYY') ELSE TO_CHAR(LAST_UPDATE,'DD/MM/YYYY') END =  TO_CHAR(SYSDATE,'DD/MM/YYYY') AND CASE WHEN UPPER(A.LAST_UPDATED_USERNAME) IS NULL THEN UPPER(A.CREATED_USERNAME) ELSE UPPER(A.LAST_UPDATED_USERNAME) END NOT IN ('32730000YANZEN','32730000DNS','32730000AZI')) WHERE C.ID = 'GET_PENCETAKAN_KTP' AND TO_CHAR(SYSDATE,'DD/MM/YYYY') = TO_CHAR(C.CREATED_DT,'DD/MM/YYYY')";
                    Console.WriteLine("UPDATE DASHBOARD DATA PENCETAKAN KTP-EL");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("Proccess Update Data Pencetakan Ktp-El " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        #endregion
        #region State Get Data Print Ready Record
        public void get_data_prr()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    Console.WriteLine("3. State Get Data Print Ready Record {0}", connection.State);
                    this.WriteToFile("3. State Get Data Print Ready Record " + connection.State);
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT COUNT(1) AS CNT FROM SIAK_DASHBOARD WHERE ID = 'GET_PRR' AND TO_CHAR(CREATED_DT,'DD-MM-YYYY') = TO_CHAR(SYSDATE,'DD-MM-YYYY')";
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        decimal cnt = (decimal)reader["CNT"];
                        Console.WriteLine("count data: {0}", cnt);
                        if (cnt > 0)
                        {
                            update_data_prr();
                        }
                        else
                        {
                            this.WriteToFile("3. Proccess Starting insert Data Print Ready Record {0}");
                            insert_data_prr();
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("3. " + ex.Message + "Proccess Get Data Print Ready Record {0}");
                }
                finally
                {
                    connection.Close();
                }

            }
        }
        public void insert_data_prr()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO INSERT DASHBOARD DATA PRINT READY RECORD");
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "INSERT INTO SIAK_DASHBOARD C (C.ID,C.CREATED_DT, C.VAL) SELECT A.ID, A.CREATED_DT, A.VAL FROM (SELECT 'GET_PRR' ID, SYSDATE CREATED_DT, COUNT(1) AS VAL FROM DEMOGRAPHICS@DB221 A  INNER JOIN BIODATA_WNI@DB222 B ON A.NIK = B.NIK LEFT JOIN DATA_KELUARGA@DB222 C ON B.NO_KK = C.NO_KK WHERE A.CURRENT_STATUS_CODE = 'PRINT_READY_RECORD' AND (B.TGL_LHR<=ADD_MONTHS(SYSDATE,-12*17) OR B.STAT_KWN<>1) AND (B.FLAG_STATUS = '0' OR (B.FLAG_STATUS = '2' AND B.FLAG_PINDAH IN(1,2,3))) AND B.NO_PROP = 32 AND B.NO_KAB =73  AND NOT EXISTS(SELECT 1 FROM CARD_MANAGEMENT@DB2 D WHERE A.NIK = D.NIK) AND NOT EXISTS(SELECT 1 FROM DEMOGRAPHICS@DB2 D WHERE A.NIK = D.NIK AND D.CURRENT_STATUS_CODE LIKE '%CARD%')) A WHERE NOT EXISTS (SELECT 1 FROM SIAK_DASHBOARD C WHERE A.ID = C.ID AND TO_CHAR(A.CREATED_DT,'DD/MM/YYYY') = TO_CHAR(C.CREATED_DT,'DD/MM/YYYY'))";
                    Console.WriteLine("INSERT DASHBOARD DATA PRINT READY RECORD");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("Proccess Insert Data Print Ready Record " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void update_data_prr()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO UPDATE DASHBOARD DATA PRINT READY RECORD");
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "UPDATE SIAK_DASHBOARD C SET (C.CREATED_DT, C.VAL) = (SELECT SYSDATE CREATED_DT, COUNT(1) AS VAL FROM DEMOGRAPHICS@DB221 A INNER JOIN BIODATA_WNI@DB222 B ON A.NIK = B.NIK LEFT JOIN DATA_KELUARGA@DB222 C ON B.NO_KK = C.NO_KK WHERE A.CURRENT_STATUS_CODE = 'PRINT_READY_RECORD' AND (B.TGL_LHR<=ADD_MONTHS(SYSDATE,-12*17) OR B.STAT_KWN<>1) AND (B.FLAG_STATUS = '0' OR (B.FLAG_STATUS = '2' AND B.FLAG_PINDAH IN(1,2,3))) AND B.NO_PROP = 32 AND B.NO_KAB =73  AND NOT EXISTS(SELECT 1 FROM CARD_MANAGEMENT@DB2 D WHERE A.NIK = D.NIK) AND NOT EXISTS(SELECT 1 FROM DEMOGRAPHICS@DB2 D WHERE A.NIK = D.NIK AND D.CURRENT_STATUS_CODE LIKE '%CARD%')) WHERE C.ID = 'GET_PRR' AND TO_CHAR(SYSDATE,'DD/MM/YYYY') = TO_CHAR(C.CREATED_DT,'DD/MM/YYYY')";
                    Console.WriteLine("UPDATE DASHBOARD DATA PRINT READY RECORD");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("Proccess Update Data Print Ready Record " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        #endregion
        #region State Get Data Sent For Enrolment
        public void get_data_sfe()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    Console.WriteLine("4. State Get Data Sent For Enrolment {0}", connection.State);
                    this.WriteToFile("4. State Get Data Sent For Enrolment " + connection.State);
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT COUNT(1) AS CNT FROM SIAK_DASHBOARD WHERE ID = 'GET_SFE' AND TO_CHAR(CREATED_DT,'DD-MM-YYYY') = TO_CHAR(SYSDATE,'DD-MM-YYYY')";
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        decimal cnt = (decimal)reader["CNT"];
                        Console.WriteLine("count data: {0}", cnt);
                        if (cnt > 0)
                        {
                            update_data_sfe();
                        }
                        else
                        {
                            this.WriteToFile("4. Proccess Starting insert Data Sent For Enrolment {0}");
                            insert_data_sfe();
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("4. " + ex.Message + "Proccess Get Data Sent For Enrolment {0}");
                }
                finally
                {
                    connection.Close();
                }

            }
        }
        public void insert_data_sfe()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO INSERT DASHBOARD DATA SENT FOR ENROLMENT");
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "INSERT INTO SIAK_DASHBOARD C (C.ID,C.CREATED_DT, C.VAL) SELECT A.ID, A.CREATED_DT, A.VAL FROM (SELECT 'GET_SFE' ID, SYSDATE CREATED_DT, COUNT(1) AS VAL FROM DEMOGRAPHICS@DB221 A  INNER JOIN BIODATA_WNI@DB222 B ON A.NIK = B.NIK LEFT JOIN DATA_KELUARGA@DB222 C ON B.NO_KK = C.NO_KK WHERE A.CURRENT_STATUS_CODE = 'SENT_FOR_ENROLLMENT' AND (B.TGL_LHR<=ADD_MONTHS(SYSDATE,-12*17) OR B.STAT_KWN<>1) AND (B.FLAG_STATUS = '0' OR (B.FLAG_STATUS = '2' AND B.FLAG_PINDAH IN(1,2,3))) AND B.NO_PROP = 32 AND B.NO_KAB =73  AND NOT EXISTS(SELECT 1 FROM CARD_MANAGEMENT@DB2 D WHERE A.NIK = D.NIK) AND NOT EXISTS(SELECT 1 FROM DEMOGRAPHICS@DB2 D WHERE A.NIK = D.NIK AND D.CURRENT_STATUS_CODE LIKE '%CARD%')) A WHERE NOT EXISTS (SELECT 1 FROM SIAK_DASHBOARD C WHERE A.ID = C.ID AND TO_CHAR(A.CREATED_DT,'DD/MM/YYYY') = TO_CHAR(C.CREATED_DT,'DD/MM/YYYY'))";
                    Console.WriteLine("INSERT DASHBOARD DATA SENT FOR ENROLMENT");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("Proccess Insert Data Sent For Enrolment " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void update_data_sfe()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO UPDATE DASHBOARD DATA SENT FOR ENROLMENT");
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "UPDATE SIAK_DASHBOARD C SET (C.CREATED_DT, C.VAL) = (SELECT SYSDATE CREATED_DT, COUNT(1) AS VAL FROM DEMOGRAPHICS@DB221 A INNER JOIN BIODATA_WNI@DB222 B ON A.NIK = B.NIK LEFT JOIN DATA_KELUARGA@DB222 C ON B.NO_KK = C.NO_KK WHERE A.CURRENT_STATUS_CODE = 'SENT_FOR_ENROLLMENT' AND (B.TGL_LHR<=ADD_MONTHS(SYSDATE,-12*17) OR B.STAT_KWN<>1) AND (B.FLAG_STATUS = '0' OR (B.FLAG_STATUS = '2' AND B.FLAG_PINDAH IN(1,2,3))) AND B.NO_PROP = 32 AND B.NO_KAB =73  AND NOT EXISTS(SELECT 1 FROM CARD_MANAGEMENT@DB2 D WHERE A.NIK = D.NIK) AND NOT EXISTS(SELECT 1 FROM DEMOGRAPHICS@DB2 D WHERE A.NIK = D.NIK AND D.CURRENT_STATUS_CODE LIKE '%CARD%')) WHERE C.ID = 'GET_SFE' AND TO_CHAR(SYSDATE,'DD/MM/YYYY') = TO_CHAR(C.CREATED_DT,'DD/MM/YYYY')";
                    Console.WriteLine("UPDATE DASHBOARD DATA SENT FOR ENROLMENT");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("Proccess Update Data Sent For Enrolment " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        #endregion
        #region State Get Data Duplicate
        public void get_data_duplicate()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    Console.WriteLine("5. State Get Data Duplicate {0}", connection.State);
                    this.WriteToFile("5. State Get Data Duplicate " + connection.State);
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT COUNT(1) AS CNT FROM SIAK_DASHBOARD WHERE ID = 'GET_DUPLICATE' AND TO_CHAR(CREATED_DT,'DD-MM-YYYY') = TO_CHAR(SYSDATE,'DD-MM-YYYY')";
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        decimal cnt = (decimal)reader["CNT"];
                        Console.WriteLine("count data: {0}", cnt);
                        if (cnt > 0)
                        {
                            update_data_duplicate();
                        }
                        else
                        {
                            this.WriteToFile("5. Proccess Starting insert Data Duplicate {0}");
                            insert_data_duplicate();
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("5. " + ex.Message + "Proccess Get Data Duplicate {0}");
                }
                finally
                {
                    connection.Close();
                }

            }
        }
        public void insert_data_duplicate()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO INSERT DASHBOARD DATA DUPLICATE");
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "INSERT INTO SIAK_DASHBOARD C (C.ID,C.CREATED_DT, C.VAL) SELECT A.ID, A.CREATED_DT, A.VAL FROM (SELECT 'GET_DUPLICATE' ID, SYSDATE CREATED_DT, COUNT(1) AS VAL FROM DEMOGRAPHICS@DB221 A  INNER JOIN BIODATA_WNI@DB222 B ON A.NIK = B.NIK LEFT JOIN DATA_KELUARGA@DB222 C ON B.NO_KK = C.NO_KK WHERE A.CURRENT_STATUS_CODE = 'DUPLICATE_RECORD' AND (B.TGL_LHR<=ADD_MONTHS(SYSDATE,-12*17) OR B.STAT_KWN<>1) AND (B.FLAG_STATUS = '0' OR (B.FLAG_STATUS = '2' AND B.FLAG_PINDAH IN(1,2,3))) AND B.NO_PROP = 32 AND B.NO_KAB =73  AND NOT EXISTS(SELECT 1 FROM CARD_MANAGEMENT@DB2 D WHERE A.NIK = D.NIK) AND NOT EXISTS(SELECT 1 FROM DEMOGRAPHICS@DB2 D WHERE A.NIK = D.NIK AND D.CURRENT_STATUS_CODE LIKE '%CARD%')) A WHERE NOT EXISTS (SELECT 1 FROM SIAK_DASHBOARD C WHERE A.ID = C.ID AND TO_CHAR(A.CREATED_DT,'DD/MM/YYYY') = TO_CHAR(C.CREATED_DT,'DD/MM/YYYY'))";
                    Console.WriteLine("INSERT DASHBOARD DATA DUPLICATE");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("Proccess Insert Data Duplicate " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void update_data_duplicate()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO UPDATE DASHBOARD DATA DUPLICATE");
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "UPDATE SIAK_DASHBOARD C SET (C.CREATED_DT, C.VAL) = (SELECT SYSDATE CREATED_DT, COUNT(1) AS VAL FROM DEMOGRAPHICS@DB221 A INNER JOIN BIODATA_WNI@DB222 B ON A.NIK = B.NIK LEFT JOIN DATA_KELUARGA@DB222 C ON B.NO_KK = C.NO_KK WHERE A.CURRENT_STATUS_CODE = 'DUPLICATE_RECORD' AND (B.TGL_LHR<=ADD_MONTHS(SYSDATE,-12*17) OR B.STAT_KWN<>1) AND (B.FLAG_STATUS = '0' OR (B.FLAG_STATUS = '2' AND B.FLAG_PINDAH IN(1,2,3))) AND B.NO_PROP = 32 AND B.NO_KAB =73  AND NOT EXISTS(SELECT 1 FROM CARD_MANAGEMENT@DB2 D WHERE A.NIK = D.NIK) AND NOT EXISTS(SELECT 1 FROM DEMOGRAPHICS@DB2 D WHERE A.NIK = D.NIK AND D.CURRENT_STATUS_CODE LIKE '%CARD%')) WHERE C.ID = 'GET_DUPLICATE' AND TO_CHAR(SYSDATE,'DD/MM/YYYY') = TO_CHAR(C.CREATED_DT,'DD/MM/YYYY')";
                    Console.WriteLine("UPDATE DASHBOARD DATA DUPLICATE");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("Proccess Update Data Duplicate " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        #endregion
        #region State Get Sisa Suket
        public void get_data_sisa_suket()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    Console.WriteLine("6. State Get Data Sisa Suket {0}", connection.State);
                    this.WriteToFile("6. State Get Data Sisa Suket " + connection.State);
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT COUNT(1) AS CNT FROM SIAK_DASHBOARD WHERE ID = 'GET_SISA_SUKET' AND TO_CHAR(CREATED_DT,'DD-MM-YYYY') = TO_CHAR(SYSDATE,'DD-MM-YYYY')";
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        decimal cnt = (decimal)reader["CNT"];
                        Console.WriteLine("count data: {0}", cnt);
                        if (cnt > 0)
                        {
                            update_data_sisa_suket();
                        }
                        else
                        {
                            this.WriteToFile("6. Proccess Starting insert Data Sisa Suket {0}");
                            insert_data_sisa_suket();
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("6. " + ex.Message + "Proccess Get Data Sisa Suket {0}");
                }
                finally
                {
                    connection.Close();
                }

            }
        }
        public void insert_data_sisa_suket()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO INSERT DASHBOARD DATA SISA SUKET");
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "INSERT INTO SIAK_DASHBOARD C (C.ID,C.CREATED_DT, C.VAL) SELECT A.ID, A.CREATED_DT, A.VAL FROM (SELECT 'GET_SISA_SUKET' ID, SYSDATE CREATED_DT, COUNT(1) AS VAL FROM BIODATA_WNI@DB222 A INNER JOIN (SELECT NIK, PRINTED_DATE,PRINTED_BY,NO_DOKUMEN  FROM (SELECT NIK,PRINTED_DATE,PRINTED_BY,NO_DOKUMEN, RANK() OVER (PARTITION BY NIK ORDER BY PRINTED_DATE,NO_DOKUMEN DESC) RNK FROM T7_HIST_SUKET@DB222 WHERE TIPE = 1) WHERE RNK = 1) B ON A.NIK = B.NIK LEFT JOIN (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME,LAST_UPDATE FROM (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME,LAST_UPDATE, RANK() OVER (PARTITION BY NIK ORDER BY LAST_UPDATE DESC,PERSONALIZED_DATE DESC) RNK FROM CARD_MANAGEMENT@DB2) WHERE RNK = 1) C ON A.NIK =C.NIK LEFT JOIN DEMOGRAPHICS_ALL@DB2 D ON A.NIK = D.NIK WHERE C.NIK IS NULL AND (D.CURRENT_STATUS_CODE LIKE '%CARD%' OR D.CURRENT_STATUS_CODE = 'PRINT_READY_RECORD') AND  (A.TGL_LHR<=ADD_MONTHS(SYSDATE,-12*17) OR A.STAT_KWN<>1) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3))) AND A.NO_PROP = 32 AND A.NO_KAB =73) A WHERE NOT EXISTS (SELECT 1 FROM SIAK_DASHBOARD C WHERE A.ID = C.ID AND TO_CHAR(A.CREATED_DT,'DD/MM/YYYY') = TO_CHAR(C.CREATED_DT,'DD/MM/YYYY'))";
                    Console.WriteLine("INSERT DASHBOARD DATA SISA SUKET");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("Proccess Insert Data Sisa Suket " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void update_data_sisa_suket()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO UPDATE DASHBOARD DATA SISA SUKET");
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "UPDATE SIAK_DASHBOARD C SET (C.CREATED_DT, C.VAL) = (SELECT SYSDATE CREATED_DT, COUNT(1) AS VAL FROM BIODATA_WNI@DB222 A INNER JOIN (SELECT NIK, PRINTED_DATE,PRINTED_BY,NO_DOKUMEN  FROM (SELECT NIK,PRINTED_DATE,PRINTED_BY,NO_DOKUMEN, RANK() OVER (PARTITION BY NIK ORDER BY PRINTED_DATE,NO_DOKUMEN DESC) RNK FROM T7_HIST_SUKET@DB222 WHERE TIPE = 1) WHERE RNK = 1) B ON A.NIK = B.NIK LEFT JOIN (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME,LAST_UPDATE FROM (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME,LAST_UPDATE, RANK() OVER (PARTITION BY NIK ORDER BY LAST_UPDATE DESC,PERSONALIZED_DATE DESC) RNK FROM CARD_MANAGEMENT@DB2) WHERE RNK = 1) C ON A.NIK =C.NIK LEFT JOIN DEMOGRAPHICS_ALL@DB2 D ON A.NIK = D.NIK WHERE C.NIK IS NULL AND (D.CURRENT_STATUS_CODE LIKE '%CARD%' OR D.CURRENT_STATUS_CODE = 'PRINT_READY_RECORD') AND  (A.TGL_LHR<=ADD_MONTHS(SYSDATE,-12*17) OR A.STAT_KWN<>1) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3))) AND A.NO_PROP = 32 AND A.NO_KAB =73) WHERE C.ID = 'GET_SISA_SUKET' AND TO_CHAR(SYSDATE,'DD/MM/YYYY') = TO_CHAR(C.CREATED_DT,'DD/MM/YYYY')";
                    Console.WriteLine("UPDATE DASHBOARD DATA SISA SUKET");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("Proccess Update Data Sisa Suket " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        #endregion
        #region State Get Pengeluaran Keping
        public void get_data_blangko_out()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    Console.WriteLine("7. State Get Data Pengeluaran Keping {0}", connection.State);
                    this.WriteToFile("7. State Get Data Pengeluaran Keping " + connection.State);
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT COUNT(1) AS CNT FROM SIAK_DASHBOARD WHERE ID = 'GET_BLANGKO_OUT' AND TO_CHAR(CREATED_DT,'DD-MM-YYYY') = TO_CHAR(SYSDATE,'DD-MM-YYYY')";
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        decimal cnt = (decimal)reader["CNT"];
                        Console.WriteLine("count data: {0}", cnt);
                        if (cnt > 0)
                        {
                            update_data_blangko_out();
                        }
                        else
                        {
                            this.WriteToFile("7. Proccess Starting insert Data Pengeluaran Keping {0}");
                            insert_data_blangko_out();
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("7. " + ex.Message + "Proccess Get Data Pengeluaran Keping {0}");
                }
                finally
                {
                    connection.Close();
                }

            }
        }
        public void insert_data_blangko_out()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO INSERT DASHBOARD DATA PENGELUARAN KEPING");
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "INSERT INTO SIAK_DASHBOARD C (C.ID,C.CREATED_DT, C.VAL) SELECT A.ID, A.CREATED_DT, A.VAL FROM (SELECT 'GET_BLANGKO_OUT' ID, SYSDATE CREATED_DT, (SELECT SUM(JUMLAH) FROM SIAK_BLANGKO) - (SELECT SUM(AMBIL) - SUM(PENGEMBALIAN) AS JUMLAH FROM SIAK_MONEV_BLANGKO) AS VAL FROM DUAL A ) A WHERE NOT EXISTS (SELECT 1 FROM SIAK_DASHBOARD C WHERE A.ID = C.ID AND TO_CHAR(A.CREATED_DT,'DD/MM/YYYY') = TO_CHAR(C.CREATED_DT,'DD/MM/YYYY'))";
                    Console.WriteLine("INSERT DASHBOARD DATA PENGELUARAN KEPING");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("Proccess Insert Data Pengeluaran Keping " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void update_data_blangko_out()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO UPDATE DASHBOARD DATA PENGELUARAN KEPING");
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "UPDATE SIAK_DASHBOARD C SET (C.CREATED_DT, C.VAL) = (SELECT SYSDATE CREATED_DT, (SELECT SUM(JUMLAH) FROM SIAK_BLANGKO) - (SELECT SUM(AMBIL) - SUM(PENGEMBALIAN) AS JUMLAH FROM SIAK_MONEV_BLANGKO) AS VAL FROM DUAL A ) WHERE C.ID = 'GET_BLANGKO_OUT' AND TO_CHAR(SYSDATE,'DD/MM/YYYY') = TO_CHAR(C.CREATED_DT,'DD/MM/YYYY')";
                    Console.WriteLine("UPDATE DASHBOARD DATA PENGELUARAN KEPING");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("Proccess Update Data Pengeluaran Keping " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        #endregion
        #region State Get Sisa Keping
        public void get_data_blangko_sisa()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    Console.WriteLine("8. State Get Data Sisa Keping {0}", connection.State);
                    this.WriteToFile("8. State Get Data Sisa Keping " + connection.State);
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT COUNT(1) AS CNT FROM SIAK_DASHBOARD WHERE ID = 'GET_BLANGKO_SISA' AND TO_CHAR(CREATED_DT,'DD-MM-YYYY') = TO_CHAR(SYSDATE,'DD-MM-YYYY')";
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        decimal cnt = (decimal)reader["CNT"];
                        Console.WriteLine("count data: {0}", cnt);
                        if (cnt > 0)
                        {
                            update_data_blangko_sisa();
                        }
                        else
                        {
                            this.WriteToFile("8. Proccess Starting insert Data Sisa Keping {0}");
                            insert_data_blangko_sisa();
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("8. " + ex.Message + "Proccess Get Data Sisa Keping {0}");
                }
                finally
                {
                    connection.Close();
                }

            }
        }
        public void insert_data_blangko_sisa()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO INSERT DASHBOARD DATA SISA KEPING");
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "INSERT INTO SIAK_DASHBOARD C (C.ID,C.CREATED_DT, C.VAL) SELECT A.ID, A.CREATED_DT, A.VAL FROM (SELECT 'GET_BLANGKO_SISA' ID, SYSDATE CREATED_DT, CASE WHEN SUM(A.AMBIL) - (SUM(A.PENGEMBALIAN)) IS NULL THEN 0 ELSE SUM(A.AMBIL) - (SUM(A.PENGEMBALIAN)) END AS VAL FROM SIAK_MONEV_BLANGKO A WHERE TO_CHAR(A.TANGGAL,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY')) A WHERE NOT EXISTS (SELECT 1 FROM SIAK_DASHBOARD C WHERE A.ID = C.ID AND TO_CHAR(A.CREATED_DT,'DD/MM/YYYY') = TO_CHAR(C.CREATED_DT,'DD/MM/YYYY'))";
                    Console.WriteLine("INSERT DASHBOARD DATA SISA KEPING");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("Proccess Insert Data Sisa Keping " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void update_data_blangko_sisa()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO UPDATE DASHBOARD DATA SISA KEPING");
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "UPDATE SIAK_DASHBOARD C SET (C.CREATED_DT, C.VAL) = (SELECT SYSDATE CREATED_DT, CASE WHEN SUM(A.AMBIL) - (SUM(A.PENGEMBALIAN)) IS NULL THEN 0 ELSE SUM(A.AMBIL) - (SUM(A.PENGEMBALIAN)) END AS VAL FROM SIAK_MONEV_BLANGKO A WHERE TO_CHAR(A.TANGGAL,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY')) WHERE C.ID = 'GET_BLANGKO_SISA' AND TO_CHAR(SYSDATE,'DD/MM/YYYY') = TO_CHAR(C.CREATED_DT,'DD/MM/YYYY')";
                    Console.WriteLine("UPDATE DASHBOARD DATA SISA KEPING");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("Proccess Update Data Sisa Keping " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        #endregion
        #endregion
        #region State Dashboard Dafduk
        #region State Get Cetak KK
        public void get_data_cetak_kk()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    Console.WriteLine("9. State Get Data Cetak KK {0}", connection.State);
                    this.WriteToFile("9. State Get Data Cetak KK " + connection.State);
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT COUNT(1) AS CNT FROM SIAK_DASHBOARD WHERE ID = 'GET_CETAK_KK' AND TO_CHAR(CREATED_DT,'DD-MM-YYYY') = TO_CHAR(SYSDATE,'DD-MM-YYYY')";
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        decimal cnt = (decimal)reader["CNT"];
                        Console.WriteLine("count data: {0}", cnt);
                        if (cnt > 0)
                        {
                            update_data_cetak_kk();
                        }
                        else
                        {
                            this.WriteToFile("9. Proccess Starting insert Data Cetak KK {0}");
                            insert_data_cetak_kk();
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("9. " + ex.Message + "Proccess Get Data Cetak KK {0}");
                }
                finally
                {
                    connection.Close();
                }

            }
        }
        public void insert_data_cetak_kk()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO INSERT DASHBOARD DATA CETAK KK");
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "INSERT INTO SIAK_DASHBOARD C (C.ID,C.CREATED_DT, C.VAL) SELECT A.ID, A.CREATED_DT, A.VAL FROM (SELECT 'GET_CETAK_KK' ID, SYSDATE CREATED_DT, COUNT(1) VAL FROM T5_SEQN_PRINT_KK@DB222 B WHERE B.PRINT_TYPE =0 AND TO_CHAR(B.PRINTED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND B.NO_PROP = 32 AND B.NO_KAB = 73) A WHERE NOT EXISTS (SELECT 1 FROM SIAK_DASHBOARD C WHERE A.ID = C.ID AND TO_CHAR(A.CREATED_DT,'DD/MM/YYYY') = TO_CHAR(C.CREATED_DT,'DD/MM/YYYY'))";
                    Console.WriteLine("INSERT DASHBOARD DATA CETAK KK");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("Proccess Insert Data Cetak KK " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void update_data_cetak_kk()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO UPDATE DASHBOARD DATA CETAK KK");
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "UPDATE SIAK_DASHBOARD C SET (C.CREATED_DT, C.VAL) = (SELECT SYSDATE CREATED_DT, COUNT(1) VAL FROM T5_SEQN_PRINT_KK@DB222 B WHERE B.PRINT_TYPE =0 AND TO_CHAR(B.PRINTED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND B.NO_PROP = 32 AND B.NO_KAB = 73) WHERE C.ID = 'GET_CETAK_KK' AND TO_CHAR(SYSDATE,'DD/MM/YYYY') = TO_CHAR(C.CREATED_DT,'DD/MM/YYYY')";
                    Console.WriteLine("UPDATE DASHBOARD DATA CETAK KK");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("Proccess Update Data Cetak KK " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        #endregion
        #region State Get Cetak KIA
        public void get_data_cetak_kia()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    Console.WriteLine("10. State Get Data Cetak KIA {0}", connection.State);
                    this.WriteToFile("10. State Get Data Cetak KIA " + connection.State);
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT COUNT(1) AS CNT FROM SIAK_DASHBOARD WHERE ID = 'GET_CETAK_KIA' AND TO_CHAR(CREATED_DT,'DD-MM-YYYY') = TO_CHAR(SYSDATE,'DD-MM-YYYY')";
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        decimal cnt = (decimal)reader["CNT"];
                        Console.WriteLine("count data: {0}", cnt);
                        if (cnt > 0)
                        {
                            update_data_cetak_kia();
                        }
                        else
                        {
                            this.WriteToFile("10. Proccess Starting insert Data Cetak KIA {0}");
                            insert_data_cetak_kia();
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("10. " + ex.Message + "Proccess Get Data Cetak KIA {0}");
                }
                finally
                {
                    connection.Close();
                }

            }
        }
        public void insert_data_cetak_kia()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO INSERT DASHBOARD DATA CETAK KIA");
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "INSERT INTO SIAK_DASHBOARD C (C.ID,C.CREATED_DT, C.VAL) SELECT A.ID, A.CREATED_DT, A.VAL FROM (SELECT 'GET_CETAK_KIA' ID, SYSDATE CREATED_DT, COUNT(1) VAL FROM T5_SEQN_KIA_PRINT@DB222 B WHERE B.PRINT_TYPE =0 AND TO_CHAR(B.PRINTED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND B.NO_PROP = 32 AND B.NO_KAB = 73) A WHERE NOT EXISTS (SELECT 1 FROM SIAK_DASHBOARD C WHERE A.ID = C.ID AND TO_CHAR(A.CREATED_DT,'DD/MM/YYYY') = TO_CHAR(C.CREATED_DT,'DD/MM/YYYY'))";
                    Console.WriteLine("INSERT DASHBOARD DATA CETAK KIA");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("Proccess Insert Data Cetak KIA " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void update_data_cetak_kia()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO UPDATE DASHBOARD DATA CETAK KIA");
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "UPDATE SIAK_DASHBOARD C SET (C.CREATED_DT, C.VAL) = (SELECT SYSDATE CREATED_DT, COUNT(1) VAL FROM T5_SEQN_KIA_PRINT@DB222 B WHERE B.PRINT_TYPE =0 AND TO_CHAR(B.PRINTED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND B.NO_PROP = 32 AND B.NO_KAB = 73) WHERE C.ID = 'GET_CETAK_KIA' AND TO_CHAR(SYSDATE,'DD/MM/YYYY') = TO_CHAR(C.CREATED_DT,'DD/MM/YYYY')";
                    Console.WriteLine("UPDATE DASHBOARD DATA CETAK KIA");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("Proccess Update Data Cetak KIA " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        #endregion
        #region State Get Input Nik Baru
        public void get_data_nik_baru()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    Console.WriteLine("11. State Get Data NIK Baru {0}", connection.State);
                    this.WriteToFile("11. State Get Data NIK Baru " + connection.State);
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT COUNT(1) AS CNT FROM SIAK_DASHBOARD WHERE ID = 'GET_NIK_BARU' AND TO_CHAR(CREATED_DT,'DD-MM-YYYY') = TO_CHAR(SYSDATE,'DD-MM-YYYY')";
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        decimal cnt = (decimal)reader["CNT"];
                        Console.WriteLine("count data: {0}", cnt);
                        if (cnt > 0)
                        {
                            update_data_nik_baru();
                        }
                        else
                        {
                            this.WriteToFile("11. Proccess Starting insert Data NIK Baru {0}");
                            insert_data_nik_baru();
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("11. " + ex.Message + "Proccess Get Data NIK Baru {0}");
                }
                finally
                {
                    connection.Close();
                }

            }
        }
        public void insert_data_nik_baru()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO INSERT DASHBOARD DATA NIK BARU");
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "INSERT INTO SIAK_DASHBOARD C (C.ID,C.CREATED_DT, C.VAL) SELECT A.ID, A.CREATED_DT, A.VAL FROM (SELECT 'GET_NIK_BARU' ID, SYSDATE CREATED_DT, COUNT(1) AS VAL from BIODATA_WNI@DB222 A WHERE A.FLAG_STATUS = 0 AND TO_CHAR(A.TGL_ENTRI,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.NO_PROP = 32 AND A.NO_KAB = 73) A WHERE NOT EXISTS (SELECT 1 FROM SIAK_DASHBOARD C WHERE A.ID = C.ID AND TO_CHAR(A.CREATED_DT,'DD/MM/YYYY') = TO_CHAR(C.CREATED_DT,'DD/MM/YYYY'))";
                    Console.WriteLine("INSERT DASHBOARD DATA NIK BARU");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("Proccess Insert Data NIK Baru" + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void update_data_nik_baru()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO UPDATE DASHBOARD DATA NIK BARU");
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "UPDATE SIAK_DASHBOARD C SET (C.CREATED_DT, C.VAL) = (SELECT SYSDATE CREATED_DT, COUNT(1) AS VAL from BIODATA_WNI@DB222 A WHERE A.FLAG_STATUS = 0 AND TO_CHAR(A.TGL_ENTRI,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.NO_PROP = 32 AND A.NO_KAB = 73) WHERE C.ID = 'GET_NIK_BARU' AND TO_CHAR(SYSDATE,'DD/MM/YYYY') = TO_CHAR(C.CREATED_DT,'DD/MM/YYYY')";
                    Console.WriteLine("UPDATE DASHBOARD DATA NIK BARU");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("Proccess Update Data NIK Baru" + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        #endregion
        #region State Get Pindah Antar Kab
        public void get_data_pindah_antar_kab()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    Console.WriteLine("12. State Get Data Pindah Antar Kab {0}", connection.State);
                    this.WriteToFile("12. State Get Data Pindah Antar Kab " + connection.State);
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT COUNT(1) AS CNT FROM SIAK_DASHBOARD WHERE ID = 'GET_PINDAH_ANTAR_KAB' AND TO_CHAR(CREATED_DT,'DD-MM-YYYY') = TO_CHAR(SYSDATE,'DD-MM-YYYY')";
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        decimal cnt = (decimal)reader["CNT"];
                        Console.WriteLine("count data: {0}", cnt);
                        if (cnt > 0)
                        {
                            update_data_pindah_antar_kab();
                        }
                        else
                        {
                            this.WriteToFile("12. Proccess Starting insert Data Pindah Antar Kab {0}");
                            insert_data_pindah_antar_kab();
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("12. " + ex.Message + "Proccess Get Data Pindah Antar Kab {0}");
                }
                finally
                {
                    connection.Close();
                }

            }
        }
        public void insert_data_pindah_antar_kab()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO INSERT DASHBOARD DATA PINDAH ANTAR KAB");
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "INSERT INTO SIAK_DASHBOARD C (C.ID,C.CREATED_DT, C.VAL) SELECT A.ID, A.CREATED_DT, A.VAL FROM (SELECT 'GET_PINDAH_ANTAR_KAB' ID, SYSDATE CREATED_DT, COUNT(DISTINCT(A.NO_PINDAH)) AS VAL FROM PINDAH_HEADER@DB222 A INNER JOIN PINDAH_DETAIL@DB222 B ON A.NO_PINDAH = B.NO_PINDAH WHERE 1=1 AND A.KLASIFIKASI_PINDAH >3 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY')  = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.FROM_NO_PROP = 32 AND A.FROM_NO_KAB = 73) A WHERE NOT EXISTS (SELECT 1 FROM SIAK_DASHBOARD C WHERE A.ID = C.ID AND TO_CHAR(A.CREATED_DT,'DD/MM/YYYY') = TO_CHAR(C.CREATED_DT,'DD/MM/YYYY'))";
                    Console.WriteLine("INSERT DASHBOARD DATA PINDAH ANTAR KAB");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("Proccess Insert Data Pindah Antar Kab" + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void update_data_pindah_antar_kab()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO UPDATE DASHBOARD DATA PINDAH ANTAR KAB");
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "UPDATE SIAK_DASHBOARD C SET (C.CREATED_DT, C.VAL) = (SELECT SYSDATE CREATED_DT, COUNT(DISTINCT(A.NO_PINDAH)) AS VAL FROM PINDAH_HEADER@DB222 A INNER JOIN PINDAH_DETAIL@DB222 B ON A.NO_PINDAH = B.NO_PINDAH WHERE 1=1 AND A.KLASIFIKASI_PINDAH >3 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY')  = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.FROM_NO_PROP = 32 AND A.FROM_NO_KAB = 73) WHERE C.ID = 'GET_PINDAH_ANTAR_KAB' AND TO_CHAR(SYSDATE,'DD/MM/YYYY') = TO_CHAR(C.CREATED_DT,'DD/MM/YYYY')";
                    Console.WriteLine("UPDATE DASHBOARD DATA PINDAH ANTAR KAB");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("Proccess Update Data Pindah Antar Kab" + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        #endregion
        #region State Get Pindah Antar Kec
        public void get_data_pindah_antar_kec()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    Console.WriteLine("13. State Get Data Pindah Antar Kec {0}", connection.State);
                    this.WriteToFile("13. State Get Data Pindah Antar Kec " + connection.State);
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT COUNT(1) AS CNT FROM SIAK_DASHBOARD WHERE ID = 'GET_PINDAH_ANTAR_KEC' AND TO_CHAR(CREATED_DT,'DD-MM-YYYY') = TO_CHAR(SYSDATE,'DD-MM-YYYY')";
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        decimal cnt = (decimal)reader["CNT"];
                        Console.WriteLine("count data: {0}", cnt);
                        if (cnt > 0)
                        {
                            update_data_pindah_antar_kec();
                        }
                        else
                        {
                            this.WriteToFile("13. Proccess Starting insert Data Pindah Antar Kec {0}");
                            insert_data_pindah_antar_kec();
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("13. " + ex.Message + "Proccess Get Data Pindah Antar Kec {0}");
                }
                finally
                {
                    connection.Close();
                }

            }
        }
        public void insert_data_pindah_antar_kec()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO INSERT DASHBOARD DATA PINDAH ANTAR KEC");
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "INSERT INTO SIAK_DASHBOARD C (C.ID,C.CREATED_DT, C.VAL) SELECT A.ID, A.CREATED_DT, A.VAL FROM (SELECT 'GET_PINDAH_ANTAR_KEC' ID, SYSDATE CREATED_DT, COUNT(DISTINCT(A.NO_PINDAH)) AS VAL FROM PINDAH_HEADER@DB222 A INNER JOIN PINDAH_DETAIL@DB222 B ON A.NO_PINDAH = B.NO_PINDAH WHERE 1=1 AND A.KLASIFIKASI_PINDAH =3 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY')  = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.FROM_NO_PROP = 32 AND A.FROM_NO_KAB = 73) A WHERE NOT EXISTS (SELECT 1 FROM SIAK_DASHBOARD C WHERE A.ID = C.ID AND TO_CHAR(A.CREATED_DT,'DD/MM/YYYY') = TO_CHAR(C.CREATED_DT,'DD/MM/YYYY'))";
                    Console.WriteLine("INSERT DASHBOARD DATA PINDAH ANTAR KEC");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("Proccess Insert Data Pindah Antar Kec" + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void update_data_pindah_antar_kec()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO UPDATE DASHBOARD DATA PINDAH ANTAR KEC");
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "UPDATE SIAK_DASHBOARD C SET (C.CREATED_DT, C.VAL) = (SELECT SYSDATE CREATED_DT, COUNT(DISTINCT(A.NO_PINDAH)) AS VAL FROM PINDAH_HEADER@DB222 A INNER JOIN PINDAH_DETAIL@DB222 B ON A.NO_PINDAH = B.NO_PINDAH WHERE 1=1 AND A.KLASIFIKASI_PINDAH =3 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY')  = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.FROM_NO_PROP = 32 AND A.FROM_NO_KAB = 73) WHERE C.ID = 'GET_PINDAH_ANTAR_KEC' AND TO_CHAR(SYSDATE,'DD/MM/YYYY') = TO_CHAR(C.CREATED_DT,'DD/MM/YYYY')";
                    Console.WriteLine("UPDATE DASHBOARD DATA PINDAH ANTAR KEC");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("Proccess Update Data Pindah Antar Kec" + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        #endregion
        #region State Get Pindah Dalam Kec
        public void get_data_pindah_dalam_kec()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    Console.WriteLine("14. State Get Data Pindah Dalam Kec {0}", connection.State);
                    this.WriteToFile("14. State Get Data Pindah Dalam Kec " + connection.State);
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT COUNT(1) AS CNT FROM SIAK_DASHBOARD WHERE ID = 'GET_PINDAH_DALAM_KEC' AND TO_CHAR(CREATED_DT,'DD-MM-YYYY') = TO_CHAR(SYSDATE,'DD-MM-YYYY')";
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        decimal cnt = (decimal)reader["CNT"];
                        Console.WriteLine("count data: {0}", cnt);
                        if (cnt > 0)
                        {
                            update_data_pindah_dalam_kec();
                        }
                        else
                        {
                            this.WriteToFile("14. Proccess Starting insert Data Pindah Dalam Kec {0}");
                            insert_data_pindah_dalam_kec();
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("14. " + ex.Message + "Proccess Get Data Pindah Dalam Kec {0}");
                }
                finally
                {
                    connection.Close();
                }

            }
        }
        public void insert_data_pindah_dalam_kec()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO INSERT DASHBOARD DATA PINDAH DALAM KEC");
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "INSERT INTO SIAK_DASHBOARD C (C.ID,C.CREATED_DT, C.VAL) SELECT A.ID, A.CREATED_DT, A.VAL FROM (SELECT 'GET_PINDAH_DALAM_KEC' ID, SYSDATE CREATED_DT, COUNT(DISTINCT(A.NO_PINDAH)) AS VAL FROM PINDAH_HEADER@DB222 A INNER JOIN PINDAH_DETAIL@DB222 B ON A.NO_PINDAH = B.NO_PINDAH WHERE 1=1 AND A.KLASIFIKASI_PINDAH <3 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY')  = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.FROM_NO_PROP = 32 AND A.FROM_NO_KAB = 73) A WHERE NOT EXISTS (SELECT 1 FROM SIAK_DASHBOARD C WHERE A.ID = C.ID AND TO_CHAR(A.CREATED_DT,'DD/MM/YYYY') = TO_CHAR(C.CREATED_DT,'DD/MM/YYYY'))";
                    Console.WriteLine("INSERT DASHBOARD DATA PINDAH DALAM KEC");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("Proccess Insert Data Pindah Dalam Kec" + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void update_data_pindah_dalam_kec()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO UPDATE DASHBOARD DATA PINDAH DALAM KEC");
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "UPDATE SIAK_DASHBOARD C SET (C.CREATED_DT, C.VAL) = (SELECT SYSDATE CREATED_DT, COUNT(DISTINCT(A.NO_PINDAH)) AS VAL FROM PINDAH_HEADER@DB222 A INNER JOIN PINDAH_DETAIL@DB222 B ON A.NO_PINDAH = B.NO_PINDAH WHERE 1=1 AND A.KLASIFIKASI_PINDAH <3 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY')  = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.FROM_NO_PROP = 32 AND A.FROM_NO_KAB = 73) WHERE C.ID = 'GET_PINDAH_DALAM_KEC' AND TO_CHAR(SYSDATE,'DD/MM/YYYY') = TO_CHAR(C.CREATED_DT,'DD/MM/YYYY')";
                    Console.WriteLine("UPDATE DASHBOARD DATA PINDAH DALAM KEC");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("Proccess Update Data Pindah Dalam Kec" + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        #endregion
        #region State Get Datang Antar Kab
        public void get_data_datang_antar_kab()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    Console.WriteLine("12. State Get Data Datang Antar Kab {0}", connection.State);
                    this.WriteToFile("12. State Get Data Datang Antar Kab " + connection.State);
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT COUNT(1) AS CNT FROM SIAK_DASHBOARD WHERE ID = 'GET_DATANG_ANTAR_KAB' AND TO_CHAR(CREATED_DT,'DD-MM-YYYY') = TO_CHAR(SYSDATE,'DD-MM-YYYY')";
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        decimal cnt = (decimal)reader["CNT"];
                        Console.WriteLine("count data: {0}", cnt);
                        if (cnt > 0)
                        {
                            update_data_datang_antar_kab();
                        }
                        else
                        {
                            this.WriteToFile("12. Proccess Starting insert Data Datang Antar Kab {0}");
                            insert_data_datang_antar_kab();
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("12. " + ex.Message + "Proccess Get Data Datang Antar Kab {0}");
                }
                finally
                {
                    connection.Close();
                }

            }
        }
        public void insert_data_datang_antar_kab()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO INSERT DASHBOARD DATA DATANG ANTAR KAB");
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "INSERT INTO SIAK_DASHBOARD C (C.ID,C.CREATED_DT, C.VAL) SELECT A.ID, A.CREATED_DT, A.VAL FROM (SELECT 'GET_DATANG_ANTAR_KAB' ID, SYSDATE CREATED_DT, COUNT(DISTINCT(A.NO_DATANG)) AS VAL FROM DATANG_HEADER@DB222 A INNER JOIN DATANG_DETAIL@DB222 B ON A.NO_DATANG = B.NO_DATANG WHERE 1=1 AND A.KLASIFIKASI_PINDAH >3 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY')  = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.NO_PROP = 32 AND A.NO_KAB = 73) A WHERE NOT EXISTS (SELECT 1 FROM SIAK_DASHBOARD C WHERE A.ID = C.ID AND TO_CHAR(A.CREATED_DT,'DD/MM/YYYY') = TO_CHAR(C.CREATED_DT,'DD/MM/YYYY'))";
                    Console.WriteLine("INSERT DASHBOARD DATA DATANG ANTAR KAB");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("Proccess Insert Data Datang Antar Kab" + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void update_data_datang_antar_kab()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO UPDATE DASHBOARD DATA DATANG ANTAR KAB");
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "UPDATE SIAK_DASHBOARD C SET (C.CREATED_DT, C.VAL) = (SELECT SYSDATE CREATED_DT, COUNT(DISTINCT(A.NO_DATANG)) AS VAL FROM DATANG_HEADER@DB222 A INNER JOIN DATANG_DETAIL@DB222 B ON A.NO_DATANG = B.NO_DATANG WHERE 1=1 AND A.KLASIFIKASI_PINDAH >3 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY')  = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.NO_PROP = 32 AND A.NO_KAB = 73) WHERE C.ID = 'GET_DATANG_ANTAR_KAB' AND TO_CHAR(SYSDATE,'DD/MM/YYYY') = TO_CHAR(C.CREATED_DT,'DD/MM/YYYY')";
                    Console.WriteLine("UPDATE DASHBOARD DATA DATANG ANTAR KAB");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("Proccess Update Data Datang Antar Kab" + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        #endregion
        #region State Get Datang Antar Kec
        public void get_data_datang_antar_kec()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    Console.WriteLine("13. State Get Data Datang Antar Kec {0}", connection.State);
                    this.WriteToFile("13. State Get Data Datang Antar Kec " + connection.State);
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT COUNT(1) AS CNT FROM SIAK_DASHBOARD WHERE ID = 'GET_DATANG_ANTAR_KEC' AND TO_CHAR(CREATED_DT,'DD-MM-YYYY') = TO_CHAR(SYSDATE,'DD-MM-YYYY')";
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        decimal cnt = (decimal)reader["CNT"];
                        Console.WriteLine("count data: {0}", cnt);
                        if (cnt > 0)
                        {
                            update_data_datang_antar_kec();
                        }
                        else
                        {
                            this.WriteToFile("13. Proccess Starting insert Data Datang Antar Kec {0}");
                            insert_data_datang_antar_kec();
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("13. " + ex.Message + "Proccess Get Data Datang Antar Kec {0}");
                }
                finally
                {
                    connection.Close();
                }

            }
        }
        public void insert_data_datang_antar_kec()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO INSERT DASHBOARD DATA DATANG ANTAR KEC");
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "INSERT INTO SIAK_DASHBOARD C (C.ID,C.CREATED_DT, C.VAL) SELECT A.ID, A.CREATED_DT, A.VAL FROM (SELECT 'GET_DATANG_ANTAR_KEC' ID, SYSDATE CREATED_DT, COUNT(DISTINCT(A.NO_DATANG)) AS VAL FROM DATANG_HEADER@DB222 A INNER JOIN DATANG_DETAIL@DB222 B ON A.NO_DATANG = B.NO_DATANG WHERE 1=1 AND A.KLASIFIKASI_PINDAH =3 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY')  = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.NO_PROP = 32 AND A.NO_KAB = 73) A WHERE NOT EXISTS (SELECT 1 FROM SIAK_DASHBOARD C WHERE A.ID = C.ID AND TO_CHAR(A.CREATED_DT,'DD/MM/YYYY') = TO_CHAR(C.CREATED_DT,'DD/MM/YYYY'))";
                    Console.WriteLine("INSERT DASHBOARD DATA DATANG ANTAR KEC");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("Proccess Insert Data Datang Antar Kec" + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void update_data_datang_antar_kec()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO UPDATE DASHBOARD DATA DATANG ANTAR KEC");
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "UPDATE SIAK_DASHBOARD C SET (C.CREATED_DT, C.VAL) = (SELECT SYSDATE CREATED_DT, COUNT(DISTINCT(A.NO_DATANG)) AS VAL FROM DATANG_HEADER@DB222 A INNER JOIN DATANG_DETAIL@DB222 B ON A.NO_DATANG = B.NO_DATANG WHERE 1=1 AND A.KLASIFIKASI_PINDAH =3 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY')  = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.NO_PROP = 32 AND A.NO_KAB = 73) WHERE C.ID = 'GET_DATANG_ANTAR_KEC' AND TO_CHAR(SYSDATE,'DD/MM/YYYY') = TO_CHAR(C.CREATED_DT,'DD/MM/YYYY')";
                    Console.WriteLine("UPDATE DASHBOARD DATA DATANG ANTAR KEC");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("Proccess Update Data Datang Antar Kec" + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        #endregion
        #region State Get Datang Dalam Kec
        public void get_data_datang_dalam_kec()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    Console.WriteLine("14. State Get Data Datang Dalam Kec {0}", connection.State);
                    this.WriteToFile("14. State Get Data Datang Dalam Kec " + connection.State);
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT COUNT(1) AS CNT FROM SIAK_DASHBOARD WHERE ID = 'GET_DATANG_DALAM_KEC' AND TO_CHAR(CREATED_DT,'DD-MM-YYYY') = TO_CHAR(SYSDATE,'DD-MM-YYYY')";
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        decimal cnt = (decimal)reader["CNT"];
                        Console.WriteLine("count data: {0}", cnt);
                        if (cnt > 0)
                        {
                            update_data_datang_dalam_kec();
                        }
                        else
                        {
                            this.WriteToFile("14. Proccess Starting insert Data Datang Dalam Kec {0}");
                            insert_data_datang_dalam_kec();
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("14. " + ex.Message + "Proccess Get Data Datang Dalam Kec {0}");
                }
                finally
                {
                    connection.Close();
                }

            }
        }
        public void insert_data_datang_dalam_kec()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO INSERT DASHBOARD DATA DATANG DALAM KEC");
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "INSERT INTO SIAK_DASHBOARD C (C.ID,C.CREATED_DT, C.VAL) SELECT A.ID, A.CREATED_DT, A.VAL FROM (SELECT 'GET_DATANG_DALAM_KEC' ID, SYSDATE CREATED_DT, COUNT(DISTINCT(A.NO_DATANG)) AS VAL FROM DATANG_HEADER@DB222 A INNER JOIN DATANG_DETAIL@DB222 B ON A.NO_DATANG = B.NO_DATANG WHERE 1=1 AND A.KLASIFIKASI_PINDAH <3 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY')  = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.NO_PROP = 32 AND A.NO_KAB = 73) A WHERE NOT EXISTS (SELECT 1 FROM SIAK_DASHBOARD C WHERE A.ID = C.ID AND TO_CHAR(A.CREATED_DT,'DD/MM/YYYY') = TO_CHAR(C.CREATED_DT,'DD/MM/YYYY'))";
                    Console.WriteLine("INSERT DASHBOARD DATA DATANG DALAM KEC");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("Proccess Insert Data Datang Dalam Kec" + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void update_data_datang_dalam_kec()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO UPDATE DASHBOARD DATA DATANG DALAM KEC");
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "UPDATE SIAK_DASHBOARD C SET (C.CREATED_DT, C.VAL) = (SELECT SYSDATE CREATED_DT, COUNT(DISTINCT(A.NO_DATANG)) AS VAL FROM DATANG_HEADER@DB222 A INNER JOIN DATANG_DETAIL@DB222 B ON A.NO_DATANG = B.NO_DATANG WHERE 1=1 AND A.KLASIFIKASI_PINDAH <3 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY')  = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.NO_PROP = 32 AND A.NO_KAB = 73) WHERE C.ID = 'GET_DATANG_DALAM_KEC' AND TO_CHAR(SYSDATE,'DD/MM/YYYY') = TO_CHAR(C.CREATED_DT,'DD/MM/YYYY')";
                    Console.WriteLine("UPDATE DASHBOARD DATA DATANG DALAM KEC");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("Proccess Update Data Datang Dalam Kec" + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        #endregion
        #endregion
        #region State Dashboard Capil
        #region State Get Cetak Akta Lahir Umum
        public void get_data_akta_lhr_um()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    Console.WriteLine("15. State Get Data Cetak Akta Lahir Umum {0}", connection.State);
                    this.WriteToFile("15. State Get Data Cetak Akta Lahir Umum " + connection.State);
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT COUNT(1) AS CNT FROM SIAK_DASHBOARD WHERE ID = 'GET_AKTA_LHR_UM' AND TO_CHAR(CREATED_DT,'DD-MM-YYYY') = TO_CHAR(SYSDATE,'DD-MM-YYYY')";
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        decimal cnt = (decimal)reader["CNT"];
                        Console.WriteLine("count data: {0}", cnt);
                        if (cnt > 0)
                        {
                            update_data_akta_lhr_um();
                        }
                        else
                        {
                            this.WriteToFile("15. Proccess Starting insert Data Cetak Akta Lahir Umum {0}");
                            insert_data_akta_lhr_um();
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("15. " + ex.Message + "Proccess Get Data Cetak Akta Lahir Umum {0}");
                }
                finally
                {
                    connection.Close();
                }

            }
        }
        public void insert_data_akta_lhr_um()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO INSERT DASHBOARD DATA CETAK AKTA LAHIR UMUM");
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "INSERT INTO SIAK_DASHBOARD C (C.ID,C.CREATED_DT, C.VAL) SELECT A.ID, A.CREATED_DT, A.VAL FROM (SELECT 'GET_AKTA_LHR_UM' ID, SYSDATE CREATED_DT, COUNT(DISTINCT(A.BAYI_NO)) VAL FROM CAPIL_LAHIR A WHERE 1=1  AND A.ADM_NO_PROV=32 AND A.ADM_NO_KAB=73 AND A.BAYI_ORG_ASING = 'T' AND A.BAYI_DOM_LAHIR = 'D' AND A.ADM_AKTA_NO LIKE '%LU%' AND TO_CHAR(A.PLPR_TGL_LAPOR,'DD/MM/YYYY')  = TO_CHAR(SYSDATE,'DD/MM/YYYY')) A WHERE NOT EXISTS (SELECT 1 FROM SIAK_DASHBOARD C WHERE A.ID = C.ID AND TO_CHAR(A.CREATED_DT,'DD/MM/YYYY') = TO_CHAR(C.CREATED_DT,'DD/MM/YYYY'))";
                    Console.WriteLine("INSERT DASHBOARD DATA CETAK AKTA LAHIR UMUM");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("Proccess Insert Data Cetak Akta Lahir Umum " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void update_data_akta_lhr_um()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO UPDATE DASHBOARD DATA CETAK AKTA LAHIR UMUM");
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "UPDATE SIAK_DASHBOARD C SET (C.CREATED_DT, C.VAL) = (SELECT SYSDATE CREATED_DT, COUNT(DISTINCT(A.BAYI_NO)) VAL FROM CAPIL_LAHIR A WHERE 1=1  AND A.ADM_NO_PROV=32 AND A.ADM_NO_KAB=73 AND A.BAYI_ORG_ASING = 'T' AND A.BAYI_DOM_LAHIR = 'D' AND A.ADM_AKTA_NO LIKE '%LU%' AND TO_CHAR(A.PLPR_TGL_LAPOR,'DD/MM/YYYY')  = TO_CHAR(SYSDATE,'DD/MM/YYYY')) WHERE C.ID = 'GET_AKTA_LHR_UM' AND TO_CHAR(SYSDATE,'DD/MM/YYYY') = TO_CHAR(C.CREATED_DT,'DD/MM/YYYY')";
                    Console.WriteLine("UPDATE DASHBOARD DATA CETAK AKTA LAHIR UMUM");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("Proccess Update Data Cetak Akta Lahir Umum " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        #endregion
        #region State Get Cetak Akta Lahir Terlambat
        public void get_data_akta_lhr_lt()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    Console.WriteLine("16. State Get Data Cetak Akta Lahir Terlambat {0}", connection.State);
                    this.WriteToFile("16. State Get Data Cetak Akta Lahir Terlambat " + connection.State);
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT COUNT(1) AS CNT FROM SIAK_DASHBOARD WHERE ID = 'GET_AKTA_LHR_LT' AND TO_CHAR(CREATED_DT,'DD-MM-YYYY') = TO_CHAR(SYSDATE,'DD-MM-YYYY')";
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        decimal cnt = (decimal)reader["CNT"];
                        Console.WriteLine("count data: {0}", cnt);
                        if (cnt > 0)
                        {
                            update_data_akta_lhr_lt();
                        }
                        else
                        {
                            this.WriteToFile("16. Proccess Starting insert Data Cetak Akta Lahir Terlambat {0}");
                            insert_data_akta_lhr_lt();
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("16. " + ex.Message + "Proccess Get Data Cetak Akta Lahir Terlambat {0}");
                }
                finally
                {
                    connection.Close();
                }

            }
        }
        public void insert_data_akta_lhr_lt()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO INSERT DASHBOARD DATA CETAK AKTA LAHIR TERLAMBAT");
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "INSERT INTO SIAK_DASHBOARD C (C.ID,C.CREATED_DT, C.VAL) SELECT A.ID, A.CREATED_DT, A.VAL FROM (SELECT 'GET_AKTA_LHR_LT' ID, SYSDATE CREATED_DT, COUNT(DISTINCT(A.BAYI_NO)) VAL FROM CAPIL_LAHIR A WHERE 1=1  AND A.ADM_NO_PROV=32 AND A.ADM_NO_KAB=73 AND A.BAYI_ORG_ASING = 'T' AND A.BAYI_DOM_LAHIR = 'D' AND A.ADM_AKTA_NO LIKE '%LT%' AND TO_CHAR(A.PLPR_TGL_LAPOR,'DD/MM/YYYY')  = TO_CHAR(SYSDATE,'DD/MM/YYYY')) A WHERE NOT EXISTS (SELECT 1 FROM SIAK_DASHBOARD C WHERE A.ID = C.ID AND TO_CHAR(A.CREATED_DT,'DD/MM/YYYY') = TO_CHAR(C.CREATED_DT,'DD/MM/YYYY'))";
                    Console.WriteLine("INSERT DASHBOARD DATA CETAK AKTA LAHIR TERLAMBAT");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("Proccess Insert Data Cetak Akta Lahir Terlambat " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void update_data_akta_lhr_lt()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO UPDATE DASHBOARD DATA CETAK AKTA LAHIR TERLAMBAT");
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "UPDATE SIAK_DASHBOARD C SET (C.CREATED_DT, C.VAL) = (SELECT SYSDATE CREATED_DT, COUNT(DISTINCT(A.BAYI_NO)) VAL FROM CAPIL_LAHIR A WHERE 1=1  AND A.ADM_NO_PROV=32 AND A.ADM_NO_KAB=73 AND A.BAYI_ORG_ASING = 'T' AND A.BAYI_DOM_LAHIR = 'D' AND A.ADM_AKTA_NO LIKE '%LT%' AND TO_CHAR(A.PLPR_TGL_LAPOR,'DD/MM/YYYY')  = TO_CHAR(SYSDATE,'DD/MM/YYYY')) WHERE C.ID = 'GET_AKTA_LHR_LT' AND TO_CHAR(SYSDATE,'DD/MM/YYYY') = TO_CHAR(C.CREATED_DT,'DD/MM/YYYY')";
                    Console.WriteLine("UPDATE DASHBOARD DATA CETAK AKTA LAHIR TERLAMBAT");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("Proccess Update Data Cetak Akta Lahir Terlambat " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        #endregion
        #region State Get Cetak Akta Kematian
        public void get_data_akta_mt()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    Console.WriteLine("17. State Get Data Cetak Akta Kematian {0}", connection.State);
                    this.WriteToFile("17. State Get Data Cetak Akta Kematian " + connection.State);
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT COUNT(1) AS CNT FROM SIAK_DASHBOARD WHERE ID = 'GET_AKTA_MT' AND TO_CHAR(CREATED_DT,'DD-MM-YYYY') = TO_CHAR(SYSDATE,'DD-MM-YYYY')";
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        decimal cnt = (decimal)reader["CNT"];
                        Console.WriteLine("count data: {0}", cnt);
                        if (cnt > 0)
                        {
                            update_data_akta_mt();
                        }
                        else
                        {
                            this.WriteToFile("17. Proccess Starting insert Data Cetak Akta Kematian {0}");
                            insert_data_akta_mt();
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("17. " + ex.Message + "Proccess Get Data Cetak Akta Kematian {0}");
                }
                finally
                {
                    connection.Close();
                }

            }
        }
        public void insert_data_akta_mt()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO INSERT DASHBOARD DATA CETAK AKTA KEMATIAN");
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "INSERT INTO SIAK_DASHBOARD C (C.ID,C.CREATED_DT, C.VAL) SELECT A.ID, A.CREATED_DT, A.VAL FROM (SELECT 'GET_AKTA_MT' ID, SYSDATE CREATED_DT, COUNT(DISTINCT(A.MATI_NO)) VAL FROM CAPIL_MATI@DB222 A WHERE 1=1  AND A.ADM_NO_PROV=32 AND A.ADM_NO_KAB=73 AND A.MATI_LUAR_NEGERI = 'T' AND A.MATI_DOM_MATI = 'D' AND TO_CHAR(A.PLPR_TGL_LAPOR,'DD/MM/YYYY')  = TO_CHAR(SYSDATE,'DD/MM/YYYY')) A WHERE NOT EXISTS (SELECT 1 FROM SIAK_DASHBOARD C WHERE A.ID = C.ID AND TO_CHAR(A.CREATED_DT,'DD/MM/YYYY') = TO_CHAR(C.CREATED_DT,'DD/MM/YYYY'))";
                    Console.WriteLine("INSERT DASHBOARD DATA CETAK AKTA KEMATIAN");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("Proccess Insert Data Cetak Akta Kematian " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void update_data_akta_mt()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO UPDATE DASHBOARD DATA CETAK AKTA KEMATIAN");
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "UPDATE SIAK_DASHBOARD C SET (C.CREATED_DT, C.VAL) = (SELECT SYSDATE CREATED_DT, COUNT(DISTINCT(A.MATI_NO)) VAL FROM CAPIL_MATI@DB222 A WHERE 1=1  AND A.ADM_NO_PROV=32 AND A.ADM_NO_KAB=73 AND A.MATI_LUAR_NEGERI = 'T' AND A.MATI_DOM_MATI = 'D' AND TO_CHAR(A.PLPR_TGL_LAPOR,'DD/MM/YYYY')  = TO_CHAR(SYSDATE,'DD/MM/YYYY')) WHERE C.ID = 'GET_AKTA_MT' AND TO_CHAR(SYSDATE,'DD/MM/YYYY') = TO_CHAR(C.CREATED_DT,'DD/MM/YYYY')";
                    Console.WriteLine("UPDATE DASHBOARD DATA CETAK AKTA KEMATIAN");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("Proccess Update Data Cetak Akta Kematian " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        #endregion
        #region State Get Cetak Akta Perkawinan
        public void get_data_akta_kwn()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    Console.WriteLine("18. State Get Data Cetak Akta Perkawinan {0}", connection.State);
                    this.WriteToFile("18. State Get Data Cetak Akta Perkawinan " + connection.State);
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT COUNT(1) AS CNT FROM SIAK_DASHBOARD WHERE ID = 'GET_AKTA_KWN' AND TO_CHAR(CREATED_DT,'DD-MM-YYYY') = TO_CHAR(SYSDATE,'DD-MM-YYYY')";
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        decimal cnt = (decimal)reader["CNT"];
                        Console.WriteLine("count data: {0}", cnt);
                        if (cnt > 0)
                        {
                            update_data_akta_kwn();
                        }
                        else
                        {
                            this.WriteToFile("18. Proccess Starting insert Data Cetak Akta Perkawinan {0}");
                            insert_data_akta_kwn();
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("18. " + ex.Message + "Proccess Get Data Cetak Akta Perkawinan {0}");
                }
                finally
                {
                    connection.Close();
                }

            }
        }
        public void insert_data_akta_kwn()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO INSERT DASHBOARD DATA CETAK AKTA PERKAWINAN");
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "INSERT INTO SIAK_DASHBOARD C (C.ID,C.CREATED_DT, C.VAL) SELECT A.ID, A.CREATED_DT, A.VAL FROM (SELECT 'GET_AKTA_KWN' ID, SYSDATE CREATED_DT, COUNT(DISTINCT(A.KAWIN_NO)) VAL FROM CAPIL_KAWIN@DB222 A WHERE 1=1  AND A.ADM_NO_PROV=32 AND A.ADM_NO_KAB=73 AND TO_CHAR(A.KAWIN_TGL_LAPOR,'DD/MM/YYYY')  = TO_CHAR(SYSDATE,'DD/MM/YYYY')) A WHERE NOT EXISTS (SELECT 1 FROM SIAK_DASHBOARD C WHERE A.ID = C.ID AND TO_CHAR(A.CREATED_DT,'DD/MM/YYYY') = TO_CHAR(C.CREATED_DT,'DD/MM/YYYY'))";
                    Console.WriteLine("INSERT DASHBOARD DATA CETAK AKTA PERKAWINAN");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("Proccess Insert Data Cetak Akta Perkawinan " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void update_data_akta_kwn()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO UPDATE DASHBOARD DATA CETAK AKTA PERKAWINAN");
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "UPDATE SIAK_DASHBOARD C SET (C.CREATED_DT, C.VAL) = (SELECT SYSDATE CREATED_DT, COUNT(DISTINCT(A.KAWIN_NO)) VAL FROM CAPIL_KAWIN@DB222 A WHERE 1=1  AND A.ADM_NO_PROV=32 AND A.ADM_NO_KAB=73 AND TO_CHAR(A.KAWIN_TGL_LAPOR,'DD/MM/YYYY')  = TO_CHAR(SYSDATE,'DD/MM/YYYY')) WHERE C.ID = 'GET_AKTA_KWN' AND TO_CHAR(SYSDATE,'DD/MM/YYYY') = TO_CHAR(C.CREATED_DT,'DD/MM/YYYY')";
                    Console.WriteLine("UPDATE DASHBOARD DATA CETAK AKTA PERKAWINAN");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("Proccess Update Data Cetak Akta Perkawinan " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        #endregion
        #region State Get Cetak Akta Perceraian
        public void get_data_akta_cry()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    Console.WriteLine("19. State Get Data Cetak Akta Perceraian {0}", connection.State);
                    this.WriteToFile("19. State Get Data Cetak Akta Perceraian " + connection.State);
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT COUNT(1) AS CNT FROM SIAK_DASHBOARD WHERE ID = 'GET_AKTA_CRY' AND TO_CHAR(CREATED_DT,'DD-MM-YYYY') = TO_CHAR(SYSDATE,'DD-MM-YYYY')";
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        decimal cnt = (decimal)reader["CNT"];
                        Console.WriteLine("count data: {0}", cnt);
                        if (cnt > 0)
                        {
                            update_data_akta_cry();
                        }
                        else
                        {
                            this.WriteToFile("19. Proccess Starting insert Data Cetak Akta Perceraian {0}");
                            insert_data_akta_cry();
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("19. " + ex.Message + "Proccess Get Data Cetak Akta Perceraian {0}");
                }
                finally
                {
                    connection.Close();
                }

            }
        }
        public void insert_data_akta_cry()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO INSERT DASHBOARD DATA CETAK AKTA PERCERAIAN");
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "INSERT INTO SIAK_DASHBOARD C (C.ID,C.CREATED_DT, C.VAL) SELECT A.ID, A.CREATED_DT, A.VAL FROM (SELECT 'GET_AKTA_CRY' ID, SYSDATE CREATED_DT, COUNT(DISTINCT(A.CERAI_NO)) VAL FROM CAPIL_CERAI@DB222 A WHERE 1=1  AND A.ADM_NO_PROV=32 AND A.ADM_NO_KAB=73 AND TO_CHAR(A.CERAI_TGL_LAPOR,'DD/MM/YYYY')  = TO_CHAR(SYSDATE,'DD/MM/YYYY')) A WHERE NOT EXISTS (SELECT 1 FROM SIAK_DASHBOARD C WHERE A.ID = C.ID AND TO_CHAR(A.CREATED_DT,'DD/MM/YYYY') = TO_CHAR(C.CREATED_DT,'DD/MM/YYYY'))";
                    Console.WriteLine("INSERT DASHBOARD DATA CETAK AKTA PERCERAIAN");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("Proccess Insert Data Cetak Akta Perceraian " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void update_data_akta_cry()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO UPDATE DASHBOARD DATA CETAK AKTA PERCERAIAN");
                    connection.ConnectionString = dbconnyz;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "UPDATE SIAK_DASHBOARD C SET (C.CREATED_DT, C.VAL) = (SELECT SYSDATE CREATED_DT, COUNT(DISTINCT(A.CERAI_NO)) VAL FROM CAPIL_CERAI@DB222 A WHERE 1=1  AND A.ADM_NO_PROV=32 AND A.ADM_NO_KAB=73 AND TO_CHAR(A.CERAI_TGL_LAPOR,'DD/MM/YYYY')  = TO_CHAR(SYSDATE,'DD/MM/YYYY')) WHERE C.ID = 'GET_AKTA_CRY' AND TO_CHAR(SYSDATE,'DD/MM/YYYY') = TO_CHAR(C.CREATED_DT,'DD/MM/YYYY')";
                    Console.WriteLine("UPDATE DASHBOARD DATA CETAK AKTA PERCERAIAN");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("Proccess Update Data Cetak Akta Perceraian " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        #endregion
        #endregion
        void Closeall()
        {
            con.Close();
            con.Dispose();
        }
        #endregion
        private void WriteToFile(string text)
        {
            try
            {
                Directory.CreateDirectory(app_path + "\\logs\\" + DateTime.Now.ToString("yyyyMM") + "\\" + DateTime.Now.ToString("yyyyMMdd") + "\\");
                string path = app_path + "\\logs\\" + DateTime.Now.ToString("yyyyMM") + "\\" + DateTime.Now.ToString("yyyyMMdd") + "\\ProcessLog_" + DateTime.Now.ToString("yyyyMMdd") + ".txt";
                using (StreamWriter writer = new StreamWriter(path, true))
                {
                    writer.WriteLine(string.Format(text, DateTime.Now.ToString("yyyy-MM-dd H:mm:ss fffffff")));
                    writer.WriteLine(string.Format(""));
                    writer.Close();
                }
            }
            catch (Exception e)
            {

            }

        }
        private void ReadSystemFile(string text)
        {
            try
            {
                Directory.CreateDirectory(app_path + "\\logs\\" + DateTime.Now.ToString("yyyyMM") + "\\" + DateTime.Now.ToString("yyyyMMdd") + "\\");
                string path = app_path + "\\logs\\" + DateTime.Now.ToString("yyyyMM") + "\\" + DateTime.Now.ToString("yyyyMMdd") + "\\StatusLog_" + DateTime.Now.ToString("yyyyMMdd") + ".txt";
                using (StreamWriter writer = new StreamWriter(path, true))
                {
                    writer.WriteLine(string.Format(text, DateTime.Now.ToString("yyyy-MM-dd H:mm:ss fffffff")));
                    writer.WriteLine(string.Format(""));
                    writer.Close();
                }
            }
            catch (Exception e)
            {

            }

        }
    }
}
