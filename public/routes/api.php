<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => 'auth:api'], function(){
	Route::post('details', 'EpuntenController@webdetails');

	// Process Business
	Route::post('epunten','EpuntenController@store');
	Route::post('epunten_update','EpuntenController@restore');
	Route::post('cek_pengajuan','EpuntenController@cek_pengajuan');
	Route::post('update_pengajuan','EpuntenController@update_pengajuan');
	Route::post('cek_notif','EpuntenController@cek_notif');
	Route::post('read_notif','EpuntenController@read_notif');

	Route::post('cek_user_petugas','EpuntenController@cek_user_petugas');
	Route::post('update_pengajuan','EpuntenController@update_pengajuan');
	Route::post('get_kk_epunten','EpuntenController@get_kk_epunten');
});

Route::get('/404', function () {
    return view('error_api.main');
})->name('404Notfound');

Route::get('dummy', 'EpuntenController@dummy');
Route::post('total_user', 'EpuntenController@total_user');


Route::post('login', 'EpuntenController@weblogin');
Route::post('logout', 'EpuntenController@weblogout');
Route::post('register', 'EpuntenController@webregister');

// Master 
Route::post('cek-nik','AndroidApiController@cek_nik');
Route::post('get-kk','AndroidApiController@get_data_kk');
Route::post('get-nik','AndroidApiController@get_data_nik');
Route::post('get_warga_bandung','AndroidApiController@get_data_suket');
Route::post('mst_prop','SharedController@get_webdb_provinsi');
Route::post('mst_kab','SharedController@get_webdb_kabupaten');
Route::post('mst_kec','SharedController@get_webdb_kecamatan');
Route::post('mst_kel','SharedController@get_webdb_kelurahan');
Route::post('mst_wna','EpuntenController@mst_wna');
Route::post('cek_negara','EpuntenController@cek_negara');

// Epunten Combobox
Route::post('ektp','EpuntenController@get_master_combobox');
Route::post('ektp-kab','EpuntenController@get_master_combobox_kab');
Route::post('ektp-kec','EpuntenController@get_master_combobox_kec');
Route::post('ektp-kel','EpuntenController@get_master_combobox_kel');
Route::post('ektp-dest-kel','EpuntenController@get_master_combobox_dest_kel');









