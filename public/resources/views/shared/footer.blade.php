    
    <script src="{{ url('/') }}/assets/plugins/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="{{ url('/') }}/assets/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
    <script src="{{ url('/') }}/assets/js/jquery.slimscroll.js"></script>
    <script src="{{ url('/') }}/assets/js/canvasjs.js"></script>
    <script src="{{ url('/') }}/assets/js/waves.js"></script>
    <script src="{{ url('/') }}/assets/js/jquery.blockUI.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/waypoints/lib/jquery.waypoints.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/counterup/jquery.counterup.min.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup.min.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup-init.js"></script>
    <script src="{{ url('/') }}/assets/js/custom.min.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/toast-master/js/jquery.toast.js"></script>
    <script src="{{ url('/') }}/assets/js/toastr.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/sweetalert/sweetalert.min.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/datatables/jquery.dataTables.min.js"></script>
    <script src="{{ url('/') }}/assets/datatable/dataTables.buttons.min.js"></script>
    <script src="{{ url('/') }}/assets/datatable/buttons.flash.min.js"></script>
    <script src="{{ url('/') }}/assets/datatable/jszip.min.js"></script>
    <script src="{{ url('/') }}/assets/datatable/pdfmake.min.js"></script>
    <script src="{{ url('/') }}/assets/datatable/vfs_fonts.js"></script>
    <script src="{{ url('/') }}/assets/datatable/buttons.html5.min.js"></script>
    <script src="{{ url('/') }}/assets/datatable/buttons.print.min.js"></script>
    <script type="text/javascript" src="{{ url('/') }}/assets/plugins/bower_components/multiselect/js/jquery.multi-select.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="{{ url('/') }}/assets/plugins/bower_components/custom-select/custom-select.min.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/moment/moment.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/jquery-asColorPicker-master/libs/jquery-asColor.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/jquery-asColorPicker-master/libs/jquery-asGradient.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/jquery-asColorPicker-master/dist/jquery-asColorPicker.min.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/timepicker/bootstrap-timepicker.min.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
    <script src="{{ url('/') }}/assets/js/mask.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/Chart.js/Chart.min.js"></script>
    <script src="{{ url('/') }}/assets/js/jquery.PrintArea.js" type="text/JavaScript"></script>
    <script type="text/javascript">
        var init_kec = {{ $user_no_kec = (!empty($user_no_kec)) ? $user_no_kec : 0 }};
        var init_level = {{ $user_level = (!empty($user_level)) ? $user_level : 0 }};
        var BASE_URL = "{{ url('/') }}/";

        $.ajaxSetup({
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function on_menu(){
            $.blockUI({ css: { 
                    border: 'none', 
                    padding: '15px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5, 
                    color: '#fff'
                },
                baseZ: 2000
            }); 
        }

    </script>
