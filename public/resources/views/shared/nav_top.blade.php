<nav class="navbar navbar-default navbar-static-top m-b-0">
            <div class="navbar-header" style="background: #41b3f9 !important; color: #fff !important;">
                <div class="top-left-part">
                    <a class="logo" href="{{ url('/') }}/">
                     <b>
                        <img src="{{ url('/') }}/assets/plugins/images/pemkot.png" alt="home" class="dark-logo"  style="width: 33px!important;" />
                        <img src="{{ url('/') }}/assets/plugins/images/pemkot.png" alt="home" class="light-logo"  style="width: 33px!important;" />
                     </b>
                        <span class="hidden-xs">
                        <img src="{{ url('/') }}/assets/plugins/images/LogoDIsduk1.png" alt="home" class="dark-logo"  style="width: 139px!important;" />
                        <img src="{{ url('/') }}/assets/plugins/images/LogoDIsduk1.png" alt="home" class="light-logo" style="width: 139px!important;" />
                     </span> </a>
                </div>
                <ul class="nav navbar-top-links navbar-left">
                    <li><a href="javascript:void(0)" class="open-close waves-effect waves-light"><i class="ti-menu"></i></a></li>
                </ul>
                 <ul class="nav navbar-top-links navbar-right pull-right">
                    <li class="dropdown">
                        <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#">
                            @php $filename = 'assets/upload/pp/'.$user_nik.'.jpg' @endphp
                                @if (file_exists($filename))
                                <img src="{{ url('/') }}/assets/upload/pp/{{$user_nik}}.jpg"  alt="user-img" width="36" class="img-circle">
                                @else
                                 <img src="{{ url('/') }}/assets/plugins/images/calming-cat.gif" alt="user-img" width="36" class="img-circle">
                                @endif
                           <b class="hidden-xs">{{ $user_id }}</b><span class="caret"></span> </a>
                        <ul class="dropdown-menu dropdown-user animated flipInY">
                            <li>
                                <div class="dw-user-box">
                                    <div class="u-img">
                                          @php $filename = 'assets/upload/pp/'.$user_nik.'.jpg' @endphp
                                            @if (file_exists($filename))
                                            <img src="{{ url('/') }}/assets/upload/pp/{{$user_nik}}.jpg"  alt="user-img">
                                            @else
                                            <img src="{{ url('/') }}/assets/plugins/images/calming-cat.gif" alt="user-img">
                                            @endif
                                    </div>
                                    <div class="u-text">
                                        <h4>{{$user_nama_dpn}}</h4>
                                        <p class="text-muted">{{$user_nik}}</p></div>
                                </div>
                            </li>
                            <li role="separator" class="divider"></li>
                            <li><a href="{{ url('/') }}/Activity/setting"><i class="ti-settings"></i> Account Setting</a>
                            </li>
                            <li role="separator" class="divider"></li>
                            <li><a href="{{ url('/') }}/login/dologout"><i class="fa fa-power-off"></i> Logout</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>