<script src="{{ url('/') }}/assets/plugins/bower_components/jquery/dist/jquery.min.js"></script>
<script src="{{ url('/') }}/assets/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="{{ url('/') }}/assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<script src="{{ url('/') }}/assets/js/jquery.slimscroll.js"></script>
<script src="{{ url('/') }}/assets/js/waves.js"></script>
<script src="{{ url('/') }}/assets/js/custom.min.js"></script>
<script src="{{ url('/') }}/assets/js/toastr.js"></script>
<script src="{{ url('/') }}/assets/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
<script type="text/javascript">
	    var BASE_URL = "{{ url('/') }}/";
</script>