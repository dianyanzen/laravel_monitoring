
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ url('/') }}/assets/plugins/images/pemkot.png">
    <title>{{ (!empty($stabletitle)) ? $stabletitle : $stitle }}</title>
    <link href="{{ url('/') }}/assets/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/css/animate.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/plugins/bower_components/morrisjs/morris.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/plugins/bower_components/css-chart/css-chart.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/plugins/bower_components/chartist-js/dist/chartist.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/plugins/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/plugins/bower_components/owl.carousel/owl.carousel.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ url('/') }}/assets/plugins/bower_components/owl.carousel/owl.theme.default.css" rel="stylesheet" type="text/css" />
    <link href="{{ url('/') }}/assets/plugins/bower_components/Minimal-Gauge-chart/css/cmGauge.css" rel="stylesheet" type="text/css" />
    <link href="{{ url('/') }}/assets/plugins/bower_components/calendar/dist/fullcalendar.css" rel="stylesheet" />
    <link href="{{ url('/') }}/assets/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ url('/') }}/assets/plugins/bower_components/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/plugins/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ url('/') }}/assets/datatable/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ url('/') }}/assets/css/toastr.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
    <link href="{{ url('/') }}/assets/plugins/bower_components/custom-select/custom-select.css" rel="stylesheet" type="text/css" />
    <link href="{{ url('/') }}/assets/plugins/bower_components/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
    <link href="{{ url('/') }}/assets/plugins/bower_components/multiselect/css/multi-select.css" rel="stylesheet" type="text/css" />
    <link href="{{ url('/') }}/assets/plugins/bower_components/switchery/dist/switchery.min.css" rel="stylesheet" />
    <link href="{{ url('/') }}/assets/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
    <link href="{{ url('/') }}/assets/plugins/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
    <link href="{{ url('/') }}/assets/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/plugins/bower_components/jquery-asColorPicker-master/css/asColorPicker.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ url('/') }}/assets/plugins/bower_components/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/plugins/bower_components/jquery-asColorPicker-master/css/asColorPicker.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ url('/') }}/assets/plugins/bower_components/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/css/animate.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/css/style.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/css/colors/default.css" id="theme" rel="stylesheet">
</head>
