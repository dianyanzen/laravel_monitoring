     <script>
        function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 44 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
    
        $(document).ready(function() {
            $('#antrian-list').DataTable({
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                "pageLength" : 10,
                "ajax": {
                 "url": BASE_URL+"Antrian/get_antrian",
                "type": "post",
                "data": {
                "_token": "{{ csrf_token() }}",
                "loket":  $('select[name="kdloket"]').val()
                }
                }
            });
        $.ajax({
                    type: "post",
                    url: BASE_URL+"Antrian/get_loket",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                    },
                    beforeSend:
                    function () {
                        $('select[name="kdloket"]').attr("disabled",true);
                    },
                    success: function (data) {
                        console.log(data);
                       $('select[name="kdloket"]').empty();
                       $('select[name="kdloket"]').append('<option value="0">-- Pilih Loket --</option>');
                        $.each(data, function(key, value) {
                            $('select[name="kdloket"]').append('<option value="'+ value.kode_pelayanan +'">LOKET '+ value.kode_pelayanan +'</option>');
                        });
                        $('select[name="kdloket"]').val("0").trigger("change");
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                        $('select[name="kdloket"]').attr("disabled",false);
                    }
                });
        $('select[name="kdloket"]').on('change', function() {

            var kdloket = $(this).val();
            $('#loket_name').html('');
            $('#antrian_today').html('0');
            $('#antrian_tomorrow').html('0');
            get_table(kdloket);
            if(kdloket != 0) {
                console.log(kdloket);
                 $('#loket_name').html('LOKET '+kdloket);
                 get_antrian_today(kdloket);
                 get_antrian_tomorrow(kdloket);
                 
            }

        });

    });
    function get_antrian_today(kdloket) {
       $.ajax({
                type: "post",
                url: BASE_URL+"Antrian/get_antrian_today",
                data: {
                    "_token": "{{ csrf_token() }}",
                    loket : kdloket
                },
                dataType: "json",
                beforeSend:
                function () {
                    // $('#antrian_today').html('0');
                },
                success: function (data) {
                    $('#antrian_today').html(data.jumlah);
                },
                error:
                function (data) {
                    // $('#antrian_today').html('0');
                },
                complete:
                function (response) {

                }
            });
        }
        function get_antrian_tomorrow(kdloket) {
       $.ajax({
                type: "post",
                url: BASE_URL+"Antrian/get_antrian_tomorrow",
                data: {
                    "_token": "{{ csrf_token() }}",
                    loket : kdloket
                },
                dataType: "json",
                beforeSend:
                function () {
                    // $('#antrian_tomorrow').html('0');
                },
                success: function (data) {
                    console.log(data);
                    $('#antrian_tomorrow').html(data.jumlah);
                },
                error:
                function (data) {
                    // $('#antrian_tomorrow').html('0');
                },
                complete:
                function (response) {

                }
            });
        }
    function get_table(kdloket) {
        console.log(kdloket);
        $('#antrian-list').DataTable({
                destroy: true,
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                "pageLength" : 50,
                "ajax": {
                 "url": BASE_URL+"Antrian/get_antrian",
                "type": "post",
                "data": {
                "_token": "{{ csrf_token() }}",
                "loket":  kdloket
                }
                }
            });
    }
    
    jQuery(document).ready(function() {
        $(".select2").select2();
        $('#tanggal').val("<?php echo date('d-m-Y');?>");
        });
    </script>