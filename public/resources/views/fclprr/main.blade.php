<!DOCTYPE html>
<html lang="en">
@include('shared.head')
<body class="fix-header">
    <div id="wrapper">
            @include('shared.nav_top')
            @include('shared/nav_side')
            @include('fclprr/content')
    </div>
     
    @include('shared/footer')
    
    @include('fclprr/javascript')
</body>
</html>