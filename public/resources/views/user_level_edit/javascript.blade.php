    <script src="{{ url('/') }}/assets/js/gijgo.min.js"></script>
    <link href="{{ url('/') }}/assets/css/gijgo.min.css" rel="stylesheet">
    
     <script>
        function on_back() {
            block_screen();
            var url = "{{ url('/') }}/";
            url += "Setting/UserLevel";
            var win = window.location.replace(url);
            win.focus();
        }
        
      $(document).ready(function () {
      			var user_level = $('#code_wil').val();
                var tree = $('#user_tree').tree({
                    primaryKey : 'id',
                    uiLibrary: 'bootstrap4',
                    dataSource: BASE_URL+"Setting/edit_tree_one?user_level="+<?php if (!empty($chead)){echo $chead[0]->user_level; }?>,
                    checkboxes: true,
                    cascadeCheck: false
                });
                tree.on('dataBound', function() {
                    tree.expandAll();
                });
                $('#btnSave').on('click', function () {
                    var checkedids = tree.getCheckedNodes();
                    $.ajax({
                    type: "post",
                    url: BASE_URL+"Setting/do_akses",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        checkedids : checkedids,
                        lvl_cd :<?php if (!empty($chead)){echo $chead[0]->user_level; }?>

                    },
                    beforeSend:
                    function () {
                         block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                        swal("Success!", data.message, "success");
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                        on_back();
                    }
                })
                    .fail(function () {
                            swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 
                        });
                        
                });
            });
        function block_screen(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    function unblock_screen(){
       $.unblockUI();
    }
    </script>