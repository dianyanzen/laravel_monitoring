     <script>
        function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 44 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
    
    $(document).ready(function() {
        console.log(init_kec);
        console.log(init_level);
         $.ajax({
                    type: "post",
                    url: BASE_URL+"Shared_api/get_capil_blk_opt",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                    },
                    beforeSend:
                    function () {
                        $('select[name="no_jenis"]').attr("disabled",true);
                    },
                    success: function (data) {
                        console.log(data);
                       $('select[name="no_jenis"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="no_jenis"]').append('<option value="'+ value.id +'">'+ value.jenis_blangko +'</option>');
                        });
                        $('select[name="no_jenis"]').val(0).trigger("change");
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                        $('select[name="no_jenis"]').attr("disabled",false);
                        get_all_data();
                        
                    }
                });
            $('select[name="no_jenis"]').on('change', function() {
                get_all_data();
            });
    });
    function get_all_data(){
        $.ajax({
                    type: "post",
                    url: BASE_URL+"BlangkoCapil/get_allin_capil_data",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        jenis : $("#no_jenis").val()
                    },
                    beforeSend:
                    function () {
                       
                    },
                    success: function (data) {
                        console.log(data);
                        $('#lbl_blangko').text(data.jenis_blangko);
                        $('#txt_jumlah').val(data.masuk);
                        $('#lbl_masuk').text(data.masuk);
                        $('#lbl_keluar').text(data.keluar);
                        $('#lbl_rusak').text(data.rusak);
                        $('#lbl_sisa').text(data.sisa);
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {

                        // $('select[name="no_kec"]').attr("disabled",false);
                    }
                });
    }
    function on_clear() {
        $('#txt_jumlah').val("0");
        $('#txt_ket').val("");
        $('#tanggal').val("<?php echo date('d-m-Y');?>");
        get_all_data();
    }
     function on_save(){
        if (validationdaily()){
          do_save();
        }
  
        
    }
    function validationdaily() {
        var blangko_masuk = $("#txt_jumlah");
        var jenis_blangko = $("#no_jenis");
        var txt_jenis = $("#lbl_blangko");
        console.log(blangko_masuk);
            if (blangko_masuk.val().length == 0) {                
                  swal("Warning!", "Input Blangko Tidak Boleh Kosong !", "warning");  
                 return false;
            }
            if (jenis_blangko.val() == 0) {                
                  swal("Warning!", "Jenis Blangko Tidak Boleh Kosong !", "warning");  
                 return false;
            }
            if (txt_jenis.text() == '-') {                
                  swal("Warning!", "Jenis Blangko Tidak Boleh Kosong !", "warning");  
                 return false;
            }
            if (parseInt(blangko_masuk.val()) == 0) {                
                  swal("Warning!", "Input Blangko Tidak Boleh 0 !", "warning");  
                 return false;
            }
           
            if (init_kec != 0){
             console.log(init_kec);
             swal("Error!", "Hanya Operator Dinas Saja Yang Boleh Menginput Kendali Blangko", "error"); 
              return false;    
            }
            return true;
       
        }
        function do_save(){
            $.ajax({
                    type: "post",
                    url: BASE_URL+"BlangkoCapil/doin_save_capil",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        blangko_masuk : $("#txt_jumlah").val(),
                        tanggal : $("#tanggal").val(),
                        jenis_blangko : $("#no_jenis").val(),
                        keterangan : $("#txt_ket").val(),
                        user_name : '<?php $user_id = (!empty($user_id)) ? $user_id : '-'; echo $user_id;?>'
                    },
                    beforeSend:
                    function () {
                         block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                        swal("Success!", data.message, "success");
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                        on_clear();
                    }
                });
        }
    function block_screen(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    function unblock_screen(){
       $.unblockUI();
    }
    jQuery(document).ready(function() {
            $(".select2").select2();
            $('.input-daterange-datepicker').daterangepicker({
                buttonClasses: ['btn', 'btn-sm'],
                applyClass: 'btn-info',
                cancelClass: 'btn-info',
                locale: {
                    format: 'DD-MM-YYYY'
                },
                startDate: '<?php echo date('d-m-Y');?>',
                endDate: '<?php echo date('d-m-Y');?>'
            });
             $('#mytable').DataTable({
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
        $('#txt_jumlah').val("0");
        $('#txt_ket').val("");
        $('#tanggal').val("<?php echo date('d-m-Y');?>");
        });
    </script>
   