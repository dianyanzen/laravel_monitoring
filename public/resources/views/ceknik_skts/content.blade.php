 <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                            <li class="active"><?php echo $mtitle; ?></li>
                        </ol>
                    </div>
                </div>
                 <div class="row">
                <form name ="get_form" action="<?php echo $my_url; ?>" method="post">
                    @csrf
                    <div class="col-sm-12">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-lg-3">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_kec" type="checkbox" checked disabled="true">
                                            <label for="cb_kec"> Kecamatan</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">         
                                    <div class="form-group" <?php if (!empty($is_colnotwil)){echo 'style="display: none!important;"'; }?>>
                                        <select class="form-control select2" name="no_kec" id="no_kec">
                                         <option  value="0">-- Pilih Kecamatan --</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-3">         
                                    <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_kel" type="checkbox" checked disabled="true">
                                            <label for="cb_kel"> Kelurahan</label>
                                        </div>
                                </div>
                                <div class="col-lg-3">         
                                    <div class="form-group" <?php if (!empty($is_colnotwil)){echo 'style="display: none!important;"'; }?>>
                                        <select class="form-control select2" name="no_kel" id="no_kel">
                                        <option value="0">-- Pilih Kelurahan --</option>  
                                    </select>
                           
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-3">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_tgl" type="checkbox" checked disabled="true">
                                            <label for="cb_tgl"> Tanggal Verivikasi</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">         
                                    <div class="form-group">
                                        <input class="form-control input-daterange-datepicker" type="text" id="tanggal" name="tanggal" /></div>
                                </div>
                            </div>
                            <div class="row">
                                <button type="submit" class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btn-filter" onclick="on_serach();" >Search <i class="mdi  mdi-magnify fa-fw"></i></button>
                                  <button type="button" class="btn btn-invert waves-effect waves-light m-r-10 pull-right" id="btn-reset" onclick="on_clear();">Clear <i class="mdi  mdi-delete fa-fw"></i></button>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
                <div class="row">
                <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title"><?php echo $mtitle; ?></h3>
                            <div class="scrollable">
                                <div class="table-responsive">
                                    <table id="mytable" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                    
                                        <tr>
                                            <th width="10%" style="text-align: center;">No Skts</th>
                                            <th width="10%" style="text-align: center;">Nik</th>
                                            <th width="20%" style="text-align: center;">Nama Lengkap</th>
                                            <th width="20%" style="text-align: center;">Tempat Lahir</th>
                                            <th width="20%" style="text-align: center;">Tanggal Lahir</th>
                                            <th width="20%" style="text-align: center;">Umur</th>
                                            <th width="20%" style="text-align: center;">Jenis Kelamin</th>
                                            <th width="20%" style="text-align: center;">Agama</th>
                                            <th width="10%" style="text-align: center;">Pekerjaan</th>
                                            <th width="10%" style="text-align: center;">Pendidikan</th>
                                            <th width="10%" style="text-align: center;">Status Kawin</th>
                                            <th width="10%" style="text-align: center;">Golongan Darah</th>
                                            <th width="10%" style="text-align: center;">Provinsi Asal</th>
                                            <th width="10%" style="text-align: center;">Kabupaten Asal</th>
                                            <th width="10%" style="text-align: center;">Kecamatan Asal</th>

                                            <th width="10%" style="text-align: center;">Kelurahan Asal</th>
                                            <th width="10%" style="text-align: center;">Alamat Asal</th>
                                            <th width="10%" style="text-align: center;">Rw Asal</th>
                                            <th width="10%" style="text-align: center;">Rt Asal</th>
                                            <th width="10%" style="text-align: center;">Provinsi Tujuan</th>
                                            <th width="10%" style="text-align: center;">Kabupaten Tujuan</th>
                                            <th width="10%" style="text-align: center;">Kecamatan Tujuan</th>
                                            <th width="10%" style="text-align: center;">Kelurahan Tujuan</th>
                                            <th width="10%" style="text-align: center;">Alamat Tujuan</th>
                                            <th width="10%" style="text-align: center;">Rw Tujuan</th>
                                            <th width="10%" style="text-align: center;">Rt Tujuan</th>
                                            <th width="10%" style="text-align: center;">Telepon</th>
                                            <th width="10%" style="text-align: center;">Email</th>
                                            <th width="10%" style="text-align: center;">Tanggal Verifikasi</th>
                                            <th width="10%" style="text-align: center;">Tanggal Berlaku</th>

                                        </tr>
                                       
                                    </thead>
                                    <tbody id="show_data">
                                       <?php
                                        if (!empty($data)){
                                           foreach($data as $row){?>
                                             <tr>
                                                <td width="10%" style="text-align: center;"><?php echo $row->no_skts ;?></td>
                                                <td width="10%" style="text-align: center;"><span style="color: #fff">,</span><?php echo $row->nik ;?></td>
                                                <td width="20%" style="text-align: center;"><?php echo $row->nama_lgkp ;?></td>
                                                <td width="20%" style="text-align: center;"><?php echo $row->tmpt_lhr ;?></td>
                                                <td width="20%" style="text-align: center;"><?php echo $row->tgl_lhr ;?></td>
                                                <td width="20%" style="text-align: center;"><?php echo $row->umur ;?></td>
                                                <td width="20%" style="text-align: center;"><?php echo $row->jenis_klmin ;?></td>
                                                <td width="20%" style="text-align: center;"><?php echo $row->agama ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->pekerjaan ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->pendidikan ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->stat_kwn ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->gol_drh ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->src_nama_prop ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->src_nama_kab ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->src_nama_kec ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->src_nama_kel ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->src_alamat ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->src_rw ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->src_rt ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->nama_prop ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->nama_kab ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->nama_kec ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->nama_kel ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->alamat ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->rw ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->rt ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->telepon ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->email ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->tgl_verifikasi ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->tgl_berlaku ;?></td>
                                            </tr>
                                          <?php } ?>
                                          <?php } ?>
                                       
                                        
                                    </tbody>
                                    
                                        <tfoot id="my_foot">
                                            <tr>
                                            <?php if (!empty($jumlah)){ ?>
                                            <th width="80%" colspan="29" style="text-align: center;">Jumlah</th>
                                            <th width="20%"  style="text-align: left;"><?php echo number_format(htmlentities($jumlah),0,',','.');?></th>
                                            <?php } ?>
                                        </tr>

                                            </tfoot>
                                </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                 </div>
       @include('shared.footer_detail')