 <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                            <li class="text-info"><?php echo $stitle; ?></li>
                        </ol>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-lg-12">
                        <div class="white-box p-b-0">
                            <div class="row">
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12" >
                                    <h2 class="font-medium m-t-0">Dashboard Ktp-El</h2>
                                    <h5 class="text-muted m-t-0">Tanggal <span id="date-part">{{ date("d-m-Y")}}</span> <span id="time-part">{{ date("H:i:s")}}</span></h5>
                                    <h5 class="m-t-0" id="is_services" style="color: red !important; display: none">Service Dashboard Is Offline <span id="date-diff"></span>, Please Check The Service !</h5>
                                </div>
                            </div>
                           
                            <div class="row minus-margin">
                                 <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 b-t b-r b-b b-l">
                                    <ul class="expense-box">
                                        <li><i class="ti-headphone-alt text-info"></i>
                                            <div>
                                                <a href ="#" onclick="dsb_rekam()">
                                                    <h2><span id="perekaman_today">0</span></h2>
                                                    <h4>Perekaman KTP-EL</h4>
                                                </a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12  b-t b-r b-b b-l">
                                    <ul class="expense-box">
                                        <li><i class="ti-home text-info"></i>
                                            <div>
                                                <a href ="#" onclick="dsb_cetak()">
                                                    <h2><span id="pencetakan_today">0</span></h2>
                                                    <h4>Pencetakan KTP-EL</h4>
                                                </a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12  b-t b-r b-b b-l">
                                    <ul class="expense-box">
                                        <li><i class="fa fa-credit-card text-info"></i>
                                            <div>
                                                <a href ="#" onclick="dsb_blangko_sisa()">
                                                    <h2><span id="sisa_blangko">0</span></h2>
                                                    <h4>Pengeluaran Blangko</h4>
                                                </a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12  b-t b-r b-b b-l">
                                    <ul class="expense-box">
                                        <li><i class="ti-unlink text-info"></i>
                                            <div>
                                                <a href ="#" onclick="dsb_blanko_keluar()">
                                                    <h2><span id="blangko_out">0</span></h2>
                                                    <h4>Sisa Blangko E-Ktp</h4>
                                                </a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12  b-t b-r b-b b-l">
                                    <ul class="expense-box">
                                        <li><i class="fa fa-paper-plane-o text-info"></i>
                                            <div>
                                                <a href ="#" onclick="dsb_prr()">
                                                    <h2><span id="sisa_prr">0</span></h2>
                                                    <h4>Print Ready Record</h4>
                                                </a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12  b-t b-r b-b b-l">
                                    <ul class="expense-box">
                                        <li><i class="ti-shopping-cart text-info"></i>
                                            <div>
                                                <a href ="#" onclick="dsb_sfe()">
                                                    <h2><span id="sisa_sfe">0</span></h2>
                                                    <h4>Sent For Enrollment</h4>
                                                </a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12  b-t b-r b-b b-l">
                                    <ul class="expense-box">
                                        <li><i class="ti-drupal text-info"></i>
                                            <div>
                                                <a href ="#" onclick="dsb_duplicate()">
                                                    <h2><span id="sisa_duplicate">0</span></h2>
                                                    <h4>Duplicate Record</h4>
                                                </a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12  b-t b-r b-b b-l">
                                    <ul class="expense-box">
                                        <li><i class="fa fa-print text-info"></i>
                                            <div>
                                                <a href ="#" onclick="dsb_sisa_suket()">
                                                    <h2><span id="sisa_suket">0</span></h2>
                                                    <h4>PR Suket</h4>
                                                </a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                
                                
                            </div>
                            <div class="row" style="margin-top: 10px !important">
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12" >
                                    <h2 class="font-medium m-t-0">Dashboard Dafduk</h2>
                                </div>
                            </div>
                           
                            <div class="row minus-margin">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 b-t b-r b-b b-l">
                                    <ul class="expense-box">
                                        <li><i class="ti-book text-info"></i>
                                            <div>
                                                <a href ="#" onclick="dsb_cetak_kk()">
                                                    <h2><span id="pen_kk">0</span></h2>
                                                    <h4>Pencetakan KK</h4>
                                                </a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  b-t b-r b-b b-l">
                                    <ul class="expense-box">
                                        <li><i class="fa fa-book text-info"></i>
                                            <div>
                                                <a href ="#" onclick="dsb_cetak_kia()">
                                                    <h2><span id="pen_kia">0</span></h2>
                                                    <h4>Pencetakan Kia</h4>
                                                </a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  b-t b-r b-b b-l">
                                    <ul class="expense-box">
                                        <li><i class="fa fa-user text-info"></i>
                                            <div>
                                                <a href ="#" onclick="dsb_nik_baru()">
                                                    <h2><span id="pen_nik_baru">0</span></h2>
                                                    <h4>Nik Baru</h4>
                                                </a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  b-t b-r b-b b-l">
                                    <ul class="expense-box">
                                        <li><i class="ti-truck text-info"></i>
                                            <div>
                                                <a href ="#" onclick="dsb_mobilitas()">
                                                    <h2><span id="pen_pindah_akab">0</span></h2>
                                                    <h4>Kepindahan Antar Kab</h4>
                                                </a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                               <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  b-t b-r b-b b-l">
                                    <ul class="expense-box">
                                        <li><i class="ti-truck text-info"></i>
                                            <div>
                                                <a href ="#" onclick="dsb_mobilitas()">
                                                    <h2><span id="pen_pindah_akec">0</span></h2>
                                                    <h4>Kepindahan Antar Kec</h4>
                                                </a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  b-t b-r b-b b-l">
                                    <ul class="expense-box">
                                        <li><i class="ti-truck text-info"></i>
                                            <div>
                                                <a href ="#" onclick="dsb_mobilitas()">
                                                    <h2><span id="pen_pindah_dkec">0</span></h2>
                                                    <h4>Kepindahan Dalam Kec</h4>
                                                </a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  b-t b-r b-b b-l">
                                    <ul class="expense-box">
                                        <li><i class="ti-car text-info"></i>
                                            <div>
                                                <a href ="#" onclick="dsb_mobilitas()">
                                                    <h2><span id="pen_datang_akab">0</span></h2>
                                                    <h4>Kedatangan Antar Kab</h4>
                                                </a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  b-t b-r b-b b-l">
                                    <ul class="expense-box">
                                        <li><i class="ti-car text-info"></i>
                                            <div>
                                                <a href ="#" onclick="dsb_mobilitas()">
                                                    <h2><span id="pen_datang_akec">0</span></h2>
                                                    <h4>Kedatangan Antar Kec</h4>
                                                </a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  b-t b-r b-b b-l">
                                    <ul class="expense-box">
                                        <li><i class="ti-car text-info"></i>
                                            <div>
                                                <a href ="#" onclick="dsb_mobilitas()">
                                                    <h2><span id="pen_datang_dkec">0</span></h2>
                                                    <h4>Kedatangan Dalam Kec</h4>
                                                </a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                </div>
                            <div class="row" style="margin-top: 10px !important">
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12" >
                                    <h2 class="font-medium m-t-0">Dashboard Capil</h2>
                                </div>
                            </div>
                           
                            <div class="row minus-margin">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12  b-t b-r b-b b-l">
                                    <ul class="expense-box">
                                        <li><i class="mdi mdi-baby-buggy text-info"></i>
                                            <div>
                                                <a href ="#" onclick="dsb_lahir_lu()">
                                                    <h2><span id="pen_lahir_lu">0</span></h2>
                                                    <h4>Akta Kelahiran Umum</h4>
                                                </a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>

                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12  b-t b-r b-b b-l">
                                    <ul class="expense-box">
                                        <li><i class="mdi mdi-baby-buggy text-info"></i>
                                            <div>
                                                <a href ="#" onclick="dsb_lahir_lt()">
                                                    <h2><span id="pen_lahir_lt">0</span></h2>
                                                    <h4>Akta Kelahiran Terlambat</h4>
                                                </a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  b-t b-r b-b b-l">
                                    <ul class="expense-box">
                                        <li><i class="mdi mdi-hospital-building text-info"></i>
                                            <div>
                                                <a href ="#" onclick="dsb_mati()">
                                                    <h2><span id="pen_mati">0</span></h2>
                                                    <h4>Akta Kematian</h4>
                                                </a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  b-t b-r b-b b-l">
                                    <ul class="expense-box">
                                        <li><i class="mdi mdi-heart text-info"></i>
                                            <div>
                                                <a href ="#" onclick="dsb_kawin()">
                                                    <h2><span id="pen_kawin">0</span></h2>
                                                    <h4>Akta Perkawinan</h4>
                                                </a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  b-t b-r b-b b-l">
                                    <ul class="expense-box">
                                        <li><i class="mdi mdi-heart-broken text-info"></i>
                                            <div>
                                                <a href ="#" onclick="dsb_cerai()">
                                                    <h2><span id="pen_cerai">0</span></h2>
                                                    <h4>Akta Perceraian</h4>
                                                </a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                
                                
                            </div>
                        </div>
                    </div>
                 
                </div>
        
                 </div>
       @include('shared.footer_detail')