<!DOCTYPE html>
<html lang="en">
@include('shared.head')
<body class="fix-header">
    <div id="wrapper">
            @include('shared.nav_top')
            @include('shared/nav_side')
            @include('Antrian_akta_insert/content')
            @include('Antrian_akta_insert/modal')
    </div>
     
    @include('shared/footer')
    
    @include('Antrian_akta_insert/javascript')
</body>
</html>