<!DOCTYPE html>
<html lang="en">
@include('shared.head')
<body class="fix-header">
    <div id="wrapper">
            @include('shared.nav_top')
            @include('shared/nav_side')
            @include('dashboard_capil/content')
    </div>
     
    @include('shared/footer')
    
    @include('dashboard_capil/javascript')
</body>
</html>