<!DOCTYPE html>
<html lang="en">
@include('shared.head')
<body class="fix-header">
    <div id="wrapper">
            @include('shared.nav_top')
            @include('shared/nav_side')
            @include('salaman_pembatalan/content')
            @include('salaman_pembatalan/modal')
    </div>
     
    @include('shared/footer')
    
    @include('salaman_pembatalan/javascript')
</body>
</html>