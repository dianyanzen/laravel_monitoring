<!DOCTYPE html>
<html lang="en">
@include('shared.head')
<body class="fix-header">
    <div id="wrapper">
            @include('shared.nav_top')
            @include('shared/nav_side')
            @include('ceklistfailure/content')
    </div>
     
    @include('shared/footer')
    
    @include('ceklistfailure/javascript')
</body>
</html>