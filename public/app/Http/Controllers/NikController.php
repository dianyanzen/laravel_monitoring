<?php

namespace App\Http\Controllers;

use DB;
use Session;
use DateTime;
use PDF;
use Excel;
use App\Biodatawni;
use App\Exports\BiodatawniExport;
use Illuminate\Http\Request;
use App\Http\Controllers\SharedController as Shr;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class NikController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

     public function Nik_duplicate(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 77;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->nik != null || $request->no_kec != null){
			if($request->nik != null){
			$nik = $request->nik;
			if (substr($nik, 0, 1) === ','){
				$nik = ltrim($nik, ',');
			}
				$d = $this->cek_data_duplicate($nik);
				$r = [];
			}else{
				$no_kec = $request->no_kec;
				$no_kel = $request->no_kel;
				$d = [];
				$r = $this->prr_get_list_duplicate($no_kec,$no_kel);
			}
			
			
			$data = array(
		 		"stitle"=>'Ktp-el Cek Duplicate Monitoring',
		 		"mtitle"=>'Ktp-el Cek Duplicate Monitoring',
		 		"my_url"=>'Duplicate',
		 		"type_tgl"=>'Rekam',
		 		"data"=>$r,
		 		"data_duplicate"=>$d,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'Ktp-el Cek Duplicate Monitoring',
		 		"mtitle"=>'Ktp-el Cek Duplicate Monitoring',
		 		"my_url"=>'Duplicate',
		 		"type_tgl"=>'Rekam',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('Ceklistduplicate/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Nik_cetak(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
		  	$menu_id = 75;
	        $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
		  if($request->nik != null || $request->no_kk != null || $request->nama_lgkp != null || $request->tgl_lhr != null){
			$nik = $request->nik;
				if (substr($nik, 0, 1) === ','){
					$nik = ltrim($nik, ',');
				}
	      if (substr($nik, -1, 1) === ','){
	        $nik = rtrim($nik, ',');
	      }
				$no_kk = $request->no_kk;
				if (substr($no_kk, 0, 1) === ','){
					$no_kk = ltrim($no_kk, ',');
				}
	      if (substr($no_kk, -1, 1) === ','){
	        $no_kk = rtrim($no_kk, ',');
	      }
	      
				$nama_lgkp = $request->nama_lgkp;
		        $tgl_lhr = $request->tgl_lhr;
		        $no_kec = $request->no_kec;
		        $no_kel = $request->no_kel;
				$r = $this->scr_cek_data_rekam($nik,$no_kk,$nama_lgkp,$tgl_lhr,$no_kec,$no_kel);
				$data = array(
			 		"stitle"=>'Ktp-el Cek Status',
			 		"mtitle"=>'Ktp-el Cek Status',
			 		"my_url"=>'Ktpel',
			 		"type_tgl"=>'Cetak',
			 		"data"=>$r,
			 		"jumlah"=>count($r),
			 		"menu"=>$menu,
	            	"akses_kec"=>$isakses_kec,
	            	"akses_kel"=>$isakses_kel,
			 		"user_id"=>$request->session()->get('S_USER_ID'),
			 		"user_nik"=>$request->session()->get('S_NIK'),
			 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
			 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
			 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
			 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
			 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
			 		"user_level"=>$request->session()->get('S_USER_LEVEL')
	    		);
				}else{
				$data = array(
			 		"stitle"=>'Ktp-el Cek Status',
			 		"mtitle"=>'Ktp-el Cek Status',
			 		"my_url"=>'Ktpel',
			 		"type_tgl"=>'Cetak',
			 		"menu"=>$menu,
	            	"akses_kec"=>$isakses_kec,
	            	"akses_kel"=>$isakses_kel,
			 		"user_id"=>$request->session()->get('S_USER_ID'),
			 		"user_nik"=>$request->session()->get('S_NIK'),
			 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
			 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
			 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
			 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
			 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
			 		"user_level"=>$request->session()->get('S_USER_LEVEL')
	    		);
    		}
			return view('ceknik/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Nikwna_cetak(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
		  	$menu_id = 282;
	        $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
		  if($request->nik != null || $request->no_kk != null || $request->nama_lgkp != null || $request->tgl_lhr != null){
			$nik = $request->nik;
				if (substr($nik, 0, 1) === ','){
					$nik = ltrim($nik, ',');
				}
	      if (substr($nik, -1, 1) === ','){
	        $nik = rtrim($nik, ',');
	      }
				$no_kk = $request->no_kk;
				if (substr($no_kk, 0, 1) === ','){
					$no_kk = ltrim($no_kk, ',');
				}
	      if (substr($no_kk, -1, 1) === ','){
	        $no_kk = rtrim($no_kk, ',');
	      }
	      
				$nama_lgkp = $request->nama_lgkp;
		        $tgl_lhr = $request->tgl_lhr;
		        $no_kec = $request->no_kec;
		        $no_kel = $request->no_kel;
				$r = $this->scr_cek_data_rekam_wna($nik,$no_kk,$nama_lgkp,$tgl_lhr,$no_kec,$no_kel);
				$data = array(
			 		"stitle"=>'Ktp-el Cek Status (WNA)',
			 		"mtitle"=>'Ktp-el Cek Status (WNA)',
			 		"my_url"=>'KtpelWna',
			 		"type_tgl"=>'Cetak',
			 		"data"=>$r,
			 		"jumlah"=>count($r),
			 		"menu"=>$menu,
	            	"akses_kec"=>$isakses_kec,
	            	"akses_kel"=>$isakses_kel,
			 		"user_id"=>$request->session()->get('S_USER_ID'),
			 		"user_nik"=>$request->session()->get('S_NIK'),
			 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
			 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
			 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
			 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
			 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
			 		"user_level"=>$request->session()->get('S_USER_LEVEL')
	    		);
				}else{
				$data = array(
			 		"stitle"=>'Ktp-el Cek Status (WNA)',
			 		"mtitle"=>'Ktp-el Cek Status (WNA)',
			 		"my_url"=>'KtpelWna',
			 		"type_tgl"=>'Cetak',
			 		"menu"=>$menu,
	            	"akses_kec"=>$isakses_kec,
	            	"akses_kel"=>$isakses_kel,
			 		"user_id"=>$request->session()->get('S_USER_ID'),
			 		"user_nik"=>$request->session()->get('S_NIK'),
			 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
			 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
			 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
			 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
			 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
			 		"user_level"=>$request->session()->get('S_USER_LEVEL')
	    		);
    		}
			return view('ceknik/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}

	
	public function Nik_kia(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 80;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kec != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$r = $this->prr_get_list_kia($tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'List Kia',
		 		"mtitle"=>'List Kia '.$tgl,
		 		"my_url"=>'Kia',
		 		"type_tgl"=>'Tanggal Cetak',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'List Kia',
		 		"mtitle"=>'List Kia',
		 		"my_url"=>'Kia',
		 		"type_tgl"=>'Tanggal Cetak',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('Ceknik_kia/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}

	
	public function Nik_skts(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 270;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kec != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$r = $this->prr_get_list_skts($tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'List Skts',
		 		"mtitle"=>'List Skts '.$tgl,
		 		"my_url"=>'Skts',
		 		"type_tgl"=>'Tanggal Verivikasi',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'List Skts',
		 		"mtitle"=>'List Skts',
		 		"my_url"=>'Skts',
		 		"type_tgl"=>'Tanggal Verivikasi',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('Ceknik_skts/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function do_push(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
		  $menu_id = 75;
	        $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->nik != null){
			$nik = $request->nik;
			if (substr($nik, 0, 1) === ','){
				$nik = ltrim($nik, ',');
			}
			$this->scr_push_demogh221($nik);
			$this->scr_push_demogh2($nik);
			$this->scr_push_demoghall($nik);
			$r = $this->scr_cek_data_rekam($nik);
			$data = array(
		 		"stitle"=>'Re-Push Status Ktp-el',
		 		"mtitle"=>'Re-Push Status Ktp-el',
		 		"my_url"=>'Push',
		 		"type_tgl"=>'Cetak',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
            	"akses_kec"=>$isakses_kec,
            	"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'Re-Push Status Ktp-el',
		 		"mtitle"=>'Re-Push Status Ktp-el',
		 		"my_url"=>'Push',
		 		"type_tgl"=>'Cetak',
		 		"menu"=>$menu,
            	"akses_kec"=>$isakses_kec,
            	"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('ceknik_dopush/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Nik_history(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 76;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->nik != null){
			$nik = $request->nik;
			if (substr($nik, 0, 1) === ','){
				$nik = ltrim($nik, ',');
			}
			
			$r = $this->scr_cek_data_history($nik,$request->session()->get('S_NO_KEC'));
			$data = array(
		 		"stitle"=>'Ktp-el Cek History Cetak',
		 		"mtitle"=>'Ktp-el Cek History Cetak',
		 		"my_url"=>'History',
		 		"type_tgl"=>'Cetak',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'Ktp-el Cek History Cetak',
		 		"mtitle"=>'Ktp-el Cek History Cetak',
		 		"my_url"=>'History',
		 		"type_tgl"=>'Cetak',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('cekhistory/main',$data);
		}else{
			return redirect()->route('logout');
		}
		
	}
	public function Nik_kk(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 129;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kk != null){
			$no_kk = $request->no_kk;
			if (substr($no_kk, 0, 1) === ','){
				$no_kk = ltrim($no_kk, ',');
			}
	        if (substr($no_kk, -1, 1) === ','){
	            $no_kk = rtrim($no_kk, ',');
	        }
			$r = $this->scr_cek_kk($no_kk,$request->session()->get('S_NO_KEC'));
			$data = array(
		 		"stitle"=>'Cek Status KK',
		 		"mtitle"=>'Cek Status KK',
		 		"my_url"=>'KK',
		 		"type_tgl"=>'Cetak',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'Cek Status KK',
		 		"mtitle"=>'Cek Status KK',
		 		"my_url"=>'KK',
		 		"type_tgl"=>'Cetak',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('cekkk/main',$data);
		}else{
			return redirect()->route('logout');
		}
		
	}
	public function kkdo_push(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 129;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kk != null){
			$no_kk = $request->no_kk;
			if (substr($no_kk, 0, 1) === ','){
				$no_kk = ltrim($no_kk, ',');
			}
		      $this->scr_push_data_kel($no_kk);
		      $this->scr_push_data_bsre($no_kk);
		      $r = $this->scr_cek_kk($no_kk);
			$data = array(
		 		"stitle"=>'Repush Status KK',
		 		"mtitle"=>'Repush Status KK',
		 		"my_url"=>'KK',
		 		"type_tgl"=>'Cetak',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'Repush Status KK',
		 		"mtitle"=>'Repush Status KK',
		 		"my_url"=>'KK',
		 		"type_tgl"=>'Cetak',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('cekkk_dopush/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Nik_kkroket(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 131;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kec != null){
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$r = $this->prr_get_list_kkroket($no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'List KK Roket',
		 		"mtitle"=>'List KK Roket',
		 		"my_url"=>'KKRoket',
		 		"type_tgl"=>'Tanggal Pengajuan',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'List KK Roket',
		 		"mtitle"=>'List KK Roket',
		 		"my_url"=>'KKRoket',
		 		"type_tgl"=>'Tanggal Pengajuan',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('Cekkkroket/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Nik_prr(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 78;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kec != null){
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$r = $this->prr_get_list_prr($no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'List PRR',
		 		"mtitle"=>'List PRR',
		 		"my_url"=>'Prr',
		 		"type_tgl"=>'Tanggal Rekam',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'List PRR',
		 		"mtitle"=>'List PRR',
		 		"my_url"=>'Prr',
		 		"type_tgl"=>'Tanggal Rekam',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('Cekprr/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Nik_srekam(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 80;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kec != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$r = $this->prr_get_list_perekaman($tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'List Perekaman',
		 		"mtitle"=>'List Perekaman '.$tgl,
		 		"my_url"=>'Rekam',
		 		"type_tgl"=>'Tanggal Rekam',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'List Perekaman',
		 		"mtitle"=>'List Perekaman',
		 		"my_url"=>'Rekam',
		 		"type_tgl"=>'Tanggal Rekam',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('Ceksperekaman/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Cetak_kk(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 177;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kec != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$r = $this->prr_get_list_cetak_kk($tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'List Cetak KK',
		 		"mtitle"=>'List Cetak KK '.$tgl,
		 		"my_url"=>'Cetak_kk',
		 		"type_tgl"=>'Tanggal Cetak KK',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'List Cetak KK',
		 		"mtitle"=>'List Cetak KK',
		 		"my_url"=>'Cetak_kk',
		 		"type_tgl"=>'Tanggal Cetak KK',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('Cekscetakkk/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Nik_baru(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 114;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kec != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$r = $this->prr_get_list_nik_baru($tgl_start,$tgl_end,$no_kec,$no_kel);
			$r2 = $this->prr_get_group_nik_baru($tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'List Nik Baru',
		 		"mtitle"=>'List Nik Baru '.$tgl,
		 		"my_url"=>'Nik_baru',
		 		"type_tgl"=>'Tanggal Nik Baru',
		 		"data"=>$r,
		 		"data2"=>$r2,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'List Nik Baru',
		 		"mtitle"=>'List Nik Baru',
		 		"my_url"=>'Nik_baru',
		 		"type_tgl"=>'Tanggal Nik Baru',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('Ceksnikbaru/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Nik_scetak(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 81;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kec != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$user = $request->user;
			$r = $this->prr_get_list_sudah_cetak($tgl_start,$tgl_end,$no_kec,$no_kel,$user);
			$data = array(
		 		"stitle"=>'List Sudah Cetak '.$tgl,
		 		"mtitle"=>'List Sudah Cetak '.$tgl,
		 		"my_url"=>'Cetak',
		 		"type_tgl"=>'Tanggal Cetak',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'List Sudah Cetak',
		 		"mtitle"=>'List Sudah Cetak',
		 		"my_url"=>'Cetak',
		 		"type_tgl"=>'Tanggal Cetak',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('Cekscetak/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Nik_list_sfe(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 83;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kec != null){
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$r = $this->prr_get_list_sfe($no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'List Sent For Enrollment',
		 		"mtitle"=>'List Sent For Enrollment',
		 		"my_url"=>'Sfe',
		 		"type_tgl"=>'Tanggal Rekam',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'List Sent For Enrollment',
		 		"mtitle"=>'List Sent For Enrollment',
		 		"my_url"=>'Sfe',
		 		"type_tgl"=>'Tanggal Rekam',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('Ceklistsfe/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Nik_list_cetak_suket(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 82;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kec != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$nik = rtrim($request->nik, ',');
			$r = $this->prr_get_list_cetak_suket($tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'List Cetak Suket',
		 		"mtitle"=>'List Cetak Suket',
		 		"my_url"=>'ListCetakSuket',
		 		"type_tgl"=>'Tanggal Cetak',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'List Cetak Suket',
		 		"mtitle"=>'List Cetak Suket',
		 		"my_url"=>'ListCetakSuket',
		 		"type_tgl"=>'Tanggal Cetak',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('Cekcetaksuket/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Nik_list_failure(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 74;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kec != null){
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$r = $this->prr_get_list_failure($no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'List Failure',
		 		"mtitle"=>'List Failure',
		 		"my_url"=>'Failure',
		 		"type_tgl"=>'Tanggal Rekam',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'List Failure',
		 		"mtitle"=>'List Failure',
		 		"my_url"=>'Failure',
		 		"type_tgl"=>'Tanggal Rekam',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('Ceklistfailure/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Nik_belum_rekam(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 121;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kec != null){
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$r = $this->prr_get_list_belum_rekam($no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Target Perekaman',
		 		"mtitle"=>'Target Perekaman',
		 		"my_url"=>'NotRecord',
		 		"type_tgl"=>'Tanggal Lahir',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'Target Perekaman',
		 		"mtitle"=>'Target Perekaman',
		 		"my_url"=>'NotRecord',
		 		"type_tgl"=>'Tanggal Lahir',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('Cekbelumrekam/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Nik_prsuket(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 79;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kec != null){
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$r = $this->prr_get_list_pr_suket($no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'List PR Suket',
		 		"mtitle"=>'List PR Suket',
		 		"my_url"=>'Suket',
		 		"type_tgl"=>'Tanggal Rekam',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'List PR Suket',
		 		"mtitle"=>'List PR Suket',
		 		"my_url"=>'Suket',
		 		"type_tgl"=>'Tanggal Rekam',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('Cekprsuket/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Delete_suket(Request $request)
	{
		header('Content-type: application/json');
		$no_dokumen = $request->no_dokumen;
		$this->del_suket($no_dokumen);
		$output = array(
			"message_type"=>1,
			"message"=>"Suket Berhasil Dihapus."
		);
		return $output;
	}
	public function Nik_pemula(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 118;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$r = $this->prr_nik_pemula($tgl_start,$tgl_end);
			$data = array(
		 		"stitle"=>'Nik Pemula',
		 		"mtitle"=>'Nik Pemula',
		 		"my_url"=>'Beginner',
		 		"type_tgl"=>'Tanggal',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'Nik Pemula',
		 		"mtitle"=>'Nik Pemula',
		 		"my_url"=>'Beginner',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('Nikpemula/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Fcl_prr(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 71;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kec != null){
			$no_kec = $request->no_kec;
			$no_kel = 0;
			$r = $this->prr_get_list_prr_fcl($no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'List PRR Gerai',
		 		"mtitle"=>'List PRR Gerai',
		 		"my_url"=>'FclPrr',
		 		"type_tgl"=>'Tanggal Rekam',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'List PRR Gerai',
		 		"mtitle"=>'List PRR Gerai',
		 		"my_url"=>'FclPrr',
		 		"type_tgl"=>'Tanggal Rekam',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('Fclprr/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Fcl_scetak(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 73;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = 0;
			$r = $this->prr_get_list_sudah_cetak_fcl($tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'List Sudah Cetak Gerai',
		 		"mtitle"=>'List Sudah Cetak Gerai'.$tgl,
		 		"my_url"=>'FclCetak',
		 		"type_tgl"=>'Tanggal Cetak',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'List Sudah Cetak Gerai',
		 		"mtitle"=>'List Sudah Cetak Gerai',
		 		"my_url"=>'FclCetak',
		 		"type_tgl"=>'Tanggal Cetak',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('fclscetak/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Fcl_srekam(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 71;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = 0;
			$r = $this->prr_get_list_perekaman_fcl($tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'List Perekaman Gerai',
		 		"mtitle"=>'List Perekaman Gerai'.$tgl,
		 		"my_url"=>'FclRekam',
		 		"type_tgl"=>'Tanggal Rekam',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'List Perekaman Gerai',
		 		"mtitle"=>'List Perekaman Gerai',
		 		"my_url"=>'FclRekam',
		 		"type_tgl"=>'Tanggal Rekam',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('fclsperekaman/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function input_lintas_batas(Request $request) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 251;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Input Lintas Batas',
		 		"mtitle"=>'Input Lintas Batas',
		 		"my_url"=>'InputLintasBatas',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('lintas_batas/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function cek_lintas_batas(Request $request) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 252;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->nik != null OR $request->nama != null){
			$nik = $request->nik;
			if (substr($nik, 0, 1) === ','){
				$nik = ltrim($nik, ',');
			}
			$nama = $request->nama;
			$r = $this->plb_cek_rekap_lintas_batas($nik,$nama);
			$data = array(
		 		"stitle"=>'Pencarian Lintas Batas',
		 		"mtitle"=>'Pencarian Lintas Batas',
		 		"my_url"=>'CekLintasBatas',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'Pencarian Lintas Batas',
		 		"mtitle"=>'Pencarian Lintas Batas',
		 		"my_url"=>'CekLintasBatas',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('lintas_batas_cek/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function cek_lintas_batas_lihat(Request $request, $id) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 252;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
		    if($id != null){
		    $nik = $id;
		    $data = $this->plb_cek_rekap_lintas_batas($nik,'');
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Pencarian Lintas Batas',
		 		"mtitle"=>'Pencarian Lintas Batas',
		 		"my_url"=>'CekLintasBatas',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"data"=>$data,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
    		);
			return view('lintas_batas_lihat/main',$data);
			}else{
				return redirect()->route('logout');
			}
			}else{
			return redirect()->route('logout');
		}
	}
	public function cek_lintas_batas_edit(Request $request, $id) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 252;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
		    if($id != null){
		    $nik = $id;
		    $data = $this->plb_cek_rekap_lintas_batas($nik,'');
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Pencarian Lintas Batas',
		 		"mtitle"=>'Pencarian Lintas Batas',
		 		"my_url"=>'CekLintasBatas',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"data"=>$data,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
    		);
			return view('lintas_batas_edit/main',$data);
			}else{
				return redirect()->route('logout');
			}
			}else{
			return redirect()->route('logout');
		}
	}
	public function plb_get_nik(Request $request) 
	{
		if($request->nik != null){
			$nik = $request->nik;
			$j = $this->do_plb_get_nik($nik);
			if($j > 0){
				$r = $this->do_plb_get_data_nik($nik);
				$data["success"] = TRUE;
				$data["is_exists"] = 1;
        		$data["nama"] = $r[0]->nama_lgkp;
        		$data["tmpt_lhr"] = $r[0]->tmpt_lhr;
        		$data["tgl_lhr"] = $r[0]->tgl_lhr;
        		$data["umur"] = $r[0]->umur;
        		$data["jenis_klmin"] = $r[0]->jenis_klmin;
        		$data["jenis_pkrjn"] = $r[0]->pekerjaan;
        		$data["alamat"] = $r[0]->alamat;
        		$data["rt"] = $r[0]->rt;
        		$data["rw"] = $r[0]->rw;
        		$data["no_prop"] = $r[0]->no_prop;
        		$data["nama_prop"] = $r[0]->nama_prop;
        		$data["no_kab"] = $r[0]->no_kab;
        		$data["nama_kab"] = $r[0]->nama_kab;
        		$data["no_kec"] = $r[0]->no_kec;
        		$data["nama_kec"] = $r[0]->nama_kec;
        		$data["no_kel"] = $r[0]->no_kel;
        		$data["nama_kel"] = $r[0]->nama_kel;
        		return $data;
			}else{
				$data["success"] = TRUE;
				$data["is_exists"] = 0;
        		return $data;
			}
		}else{
			return redirect()->route('logout');
		}
	}
	public function plb_do_save(Request $request) 
	{
		if($request->plb != null){
			$plb = $request->plb;
			$nik = $request->nik;
			$nama = str_replace('\'', '',$request->nama);
			$umur = $request->umur;
			$tgl_lhr = $request->tgl_lhr;
			$tmpt_lhr = $request->tmpt_lhr;
			$jenis_klmin = $request->jenis_klmin;
			$jenis_pkrjn = $request->jenis_pkrjn;
			$no_prop = $request->no_prop;
			$no_kab = $request->no_kab;
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$alamat = $request->alamat;
			$rt = $request->rt;
			$rw = $request->rw;
			$j = $this->do_plb_get_data($nik);
			if($j > 0){
			$data["success"] = FALSE;
			$data["message"] = "Nik Sudah Terdaftar PLB !";
       		return $data;
			}else{
			$this->plb_save($plb,$nik,$nama,$umur,$tgl_lhr,$tmpt_lhr,$jenis_klmin,$jenis_pkrjn,$no_prop,$no_kab,$no_kec,$no_kel,$alamat,$rt,$rw,$request->session()->get('S_USER_ID'));
			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Simpan";
       		return $data;
       		}
		}else{
			return redirect()->route('logout');
		}
	}
	public function plb_do_edit(Request $request) 
	{
		if($request->plb != null){
			$plb = $request->plb;
			$nik = $request->nik;
			
			$this->plb_edit($plb,$nik,$request->session()->get('S_USER_ID'));
			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Ubah";
       		return $data;
		}else{
			return redirect()->route('logout');
		}
	}
	public function plb_do_delete(Request $request) 
	{
		if($request->nik != null){
			$nik = $request->nik;
			$this->plb_delete($nik);
			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Hapus";
       		return $data;
		}else{
			return redirect()->route('logout');
		}
	}
	public function input_petugas_khusus(Request $request) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 253;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Input Petugas Khusus',
		 		"mtitle"=>'Input Petugas Khusus',
		 		"my_url"=>'InputLintasBatas',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('petugas_khusus/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function cek_petugas_khusus(Request $request) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 254;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->nik != null OR $request->nama != null){
			$nik = $request->nik;
			if (substr($nik, 0, 1) === ','){
				$nik = ltrim($nik, ',');
			}
			$nama = $request->nama;
			$r = $this->ph_cek_rekap_petugas_khusus($nik,$nama);
			$data = array(
		 		"stitle"=>'Pencarian Petugas Khusus',
		 		"mtitle"=>'Pencarian Petugas Khusus',
		 		"my_url"=>'CekPetugasKhusus',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'Pencarian Petugas Khusus',
		 		"mtitle"=>'Pencarian Petugas Khusus',
		 		"my_url"=>'CekPetugasKhusus',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('petugas_khusus_cek/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function cek_petugas_khusus_lihat(Request $request, $id) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 254;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
		    if($id != null){
		    $nik = $id;
		    $data = $this->ph_cek_rekap_petugas_khusus($nik,'');
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Pencarian Petugas Khusus',
		 		"mtitle"=>'Pencarian Petugas Khusus',
		 		"my_url"=>'CekPetugasKhusus',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"data"=>$data,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
    		);
			return view('petugas_khusus_lihat/main',$data);
			}else{
				return redirect()->route('logout');
			}
			}else{
			return redirect()->route('logout');
		}
	}
	public function cek_petugas_khusus_edit(Request $request, $id) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 254;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
		    if($id != null){
		    $nik = $id;
		    $data = $this->ph_cek_rekap_petugas_khusus($nik,'');
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Pencarian Petugas Khusus',
		 		"mtitle"=>'Pencarian Petugas Khusus',
		 		"my_url"=>'CekPetugasKhusus',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"data"=>$data,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
    		);
			return view('petugas_khusus_edit/main',$data);
			}else{
				return redirect()->route('logout');
			}
			}else{
			return redirect()->route('logout');
		}
	}
	public function ph_get_nik(Request $request) 
	{
		if($request->nik != null){
			$nik = $request->nik;
			$j = $this->do_ph_get_nik($nik);
			if($j > 0){
				$r = $this->do_ph_get_data_nik($nik);
				$data["success"] = TRUE;
				$data["is_exists"] = 1;
        		$data["nama"] = $r[0]->nama_lgkp;
        		$data["tmpt_lhr"] = $r[0]->tmpt_lhr;
        		$data["tgl_lhr"] = $r[0]->tgl_lhr;
        		$data["umur"] = $r[0]->umur;
        		$data["jenis_klmin"] = $r[0]->jenis_klmin;
        		$data["jenis_pkrjn"] = $r[0]->pekerjaan;
        		$data["alamat"] = $r[0]->alamat;
        		$data["rt"] = $r[0]->rt;
        		$data["rw"] = $r[0]->rw;
        		$data["no_prop"] = $r[0]->no_prop;
        		$data["nama_prop"] = $r[0]->nama_prop;
        		$data["no_kab"] = $r[0]->no_kab;
        		$data["nama_kab"] = $r[0]->nama_kab;
        		$data["no_kec"] = $r[0]->no_kec;
        		$data["nama_kec"] = $r[0]->nama_kec;
        		$data["no_kel"] = $r[0]->no_kel;
        		$data["nama_kel"] = $r[0]->nama_kel;
        		return $data;
			}else{
				$data["success"] = TRUE;
				$data["is_exists"] = 0;
        		return $data;
			}
		}else{
			return redirect()->route('logout');
		}
	}
	public function ph_do_save(Request $request) 
	{
		if($request->ph != null){
			$ph = $request->ph;
			$nik = $request->nik;
			$nama = str_replace('\'', '',$request->nama);
			$umur = $request->umur;
			$tgl_lhr = $request->tgl_lhr;
			$tmpt_lhr = $request->tmpt_lhr;
			$jenis_klmin = $request->jenis_klmin;
			$jenis_pkrjn = $request->jenis_pkrjn;
			$no_prop = $request->no_prop;
			$no_kab = $request->no_kab;
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$alamat = $request->alamat;
			$rt = $request->rt;
			$rw = $request->rw;
			$j = $this->do_ph_get_data($nik);
			if($j > 0){
			$data["success"] = FALSE;
			$data["message"] = "Nik Sudah Terdaftar ph !";
       		return $data;
			}else{
			$this->ph_save($ph,$nik,$nama,$umur,$tgl_lhr,$tmpt_lhr,$jenis_klmin,$jenis_pkrjn,$no_prop,$no_kab,$no_kec,$no_kel,$alamat,$rt,$rw,$request->session()->get('S_USER_ID'));
			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Simpan";
       		return $data;
       		}
		}else{
			return redirect()->route('logout');
		}
	}
	public function ph_do_edit(Request $request) 
	{
		if($request->ph != null){
			$ph = $request->ph;
			$nik = $request->nik;
			
			$this->ph_edit($ph,$nik,$request->session()->get('S_USER_ID'));
			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Ubah";
       		return $data;
		}else{
			return redirect()->route('logout');
		}
	}
	public function ph_do_delete(Request $request) 
	{
		if($request->nik != null){
			$nik = $request->nik;
			$this->ph_delete($nik);
			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Hapus";
       		return $data;
		}else{
			return redirect()->route('logout');
		}
	}

	public function nik_export(Request $request) {
		if($request->niks != null){

			$nik = $request->niks;
			if (substr($nik, 0, 1) === ','){
				$nik = ltrim($nik, ',');
			}
		$data_rekam =  $this->scr_export_data_nik($nik);
		$export = new BiodatawniExport($data_rekam);
    	 return Excel::download($export, 'Biodatawni.xlsx');
		 } 
        else 
        {
           redirect('/','refresh');
        }

    }
    
	public function nik_pdf(Request $request) {
		if($request->niks != null){
			$nik = $request->niks;
			if (substr($nik, 0, 1) === ','){
				$nik = ltrim($nik, ',');
			}
        $fileName = strtoupper($request->session()->get('S_USER_ID')).'_DATA_REKAM_'.time().'.xlsx';  
		// $this->load->library('pdf');
		$data_rekam =  $this->scr_export_data_rekam($nik);
        // set document information
        PDF::SetCreator('Dian Yanzen');
		PDF::SetAuthor('Dian Yanzen');
		PDF::SetTitle('PDF Monitoring');
        PDF::SetSubject('YZ PDF');
        PDF::SetKeywords('YZ, PDF, Monitoring, DisdukCapil, Kota Bandung');
		// set auto page breaks
		PDF::SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		PDF::setImageScale(PDF_IMAGE_SCALE_RATIO);

		

// ---------------------------------------------------------

// set font

        // set default header data
        PDF::SetMargins(PDF_MARGIN_LEFT, 40, PDF_MARGIN_RIGHT);
		PDF::SetHeaderMargin(PDF_MARGIN_HEADER);
		PDF::SetFooterMargin(PDF_MARGIN_FOOTER);
        // set font
        PDF::SetFont('times', '', 12);
        
        // add a page
        foreach ($data_rekam as $val) 
        {
        PDF::AddPage();
        // new style
$style = array('border' => 0, 'vpadding' => 'auto', 'hpadding' => 'auto', 'fgcolor' => array(0, 0, 0), 'bgcolor' => false, 'module_width' => 1, 'module_height' => 1);
// QRCODE,H : QR-CODE Best error correction

        
        // print a line using Cell()
        // PDF::Cell(0, 12, strtoupper($request->session()->get('S_USER_ID')).'_DATA_REKAM_'.time(), 0, 1, 'C');
        $html = '
<!-- EXAMPLE OF CSS STYLE -->
<style>

    table.first {
    	padding: 0px;    
    	margin: 0px;    
        font-family: helvetica;
        font-size: 8pt;
        
    }
</style>
<table class="first" cellpadding="0" cellspacing="5" border="0">
 <tr>
  <td width="50" align="left">Nomor</td>
  <td width="20" align="center">:</td>
  <td width="140" align="left">'.time().'-Disdukcapil</td>
  <td width="80" align="center"></td>
  <td width="60" align="center"></td>
  <td width="20" align="center"></td>
  <td width="220" align="left">Bandung,'.date('d-m-Y').'</td>
 </tr>
 <tr>
  <td width="50" align="left">Lampiran</td>
  <td width="20" align="center">:</td>
  <td width="140" align="left">-</td>
  <td width="80" align="center"></td>
  <td width="60" align="center"></td>
  <td width="20" align="center"></td>
  <td width="220" align="left">Kepada</td>
 </tr>
 <tr>
  <td width="50" align="left">Perihal</td>
  <td width="20" align="center">:</td>
  <td width="210" align="left">Resi Pengajuan Ktp-El</td>
  <td width="10" align="center"></td>
  <td width="60" align="right">Yth.</td>
  <td width="20" align="center">:</td>
  <td width="220" align="left"><B>'.$val->nama_lgkp.'</B></td>
 </tr>
</table>
<br>
<br>
<br>
<table class="first" cellpadding="0" cellspacing="5" border="0">
 <tr>
  <td colspan = "7" align="center"><h2>RESI PENGAJUAN PENCETAKAN E-KTP</h2></td>
 </tr>
 <tr>
  <td colspan = "7" align="center"><br></td>
 </tr>
 <tr>
  <td width="120" align="left">NIK</td>
  <td width="20" align="center">:</td>
  <td width="210" align="left"><B>'.$val->nik.'</B></td>
  <td width="80" align="center"></td>
  <td width="60" align="center" colspan ="3"></td>
  
 </tr>
 <tr>
  <td width="120" align="left">Nama Lengkap</td>
  <td width="20" align="center">:</td>
  <td width="210" align="left"><B>'.$val->nama_lgkp.'</B></td>
  <td width="80" align="center"></td>
  <td width="60" align="center"></td>
  <td width="20" align="center"></td>
  <td width="50" align="left"></td>
 </tr>
 <tr>
  <td width="120" align="left">Tempat/Tanggal Lahir</td>
  <td width="20" align="center">:</td>
  <td width="210" align="left"><B>'.$val->tmpt_lhr.', '.$val->tgl_lhr.'</B></td>
  <td width="80" align="center"></td>
  <td width="60" align="center"></td>
  <td width="20" align="center"></td>
  <td width="50" align="left"></td>
 </tr>
 <tr>
  <td width="120" align="left">Jenis Kelamin</td>
  <td width="20" align="center">:</td>
  <td width="210" align="left"><B>'.$val->jenis_klmin.'</B></td>
  <td width="80" align="center"></td>
  <td width="60" align="center"></td>
  <td width="20" align="center"></td>
  <td width="50" align="left"></td>
 </tr>
 <tr>
  <td width="120" align="left">Alamat</td>
  <td width="20" align="center">:</td>
  <td width="210" align="left"><B>'.$val->alamat.'</B></td>
  <td width="80" align="center"></td>
  <td width="60" align="center"></td>
  <td width="20" align="center"></td>
  <td width="50" align="left"></td>
 </tr>
 <tr>
  <td width="120" align="left">&#160;&#160;&#160;&#160;&#160;&#160;RT/RW</td>
  <td width="20" align="center">:</td>
  <td width="210" align="left"><B>'.$val->rt.'/'.$val->rw.'</B></td>
  <td width="80" align="center"></td>
  <td width="60" align="center"></td>
  <td width="20" align="center"></td>
  <td width="50" align="left"></td>
 </tr>
 <tr>
  <td width="120" align="left">&#160;&#160;&#160;&#160;&#160;&#160;Kelurahan</td>
  <td width="20" align="center">:</td>
  <td width="210" align="left"><B>'.$val->nama_kel.'</B></td>
  <td width="80" align="center"></td>
  <td width="60" align="center"></td>
  <td width="20" align="center"></td>
  <td width="50" align="left"></td>
 </tr>
 <tr>
  <td width="120" align="left">&#160;&#160;&#160;&#160;&#160;&#160;Kecamatan</td>
  <td width="20" align="center">:</td>
  <td width="210" align="left"><B>'.$val->nama_kec.'</B></td>
  <td width="80" align="center"></td>
  <td width="60" align="center"></td>
  <td width="20" align="center"></td>
  <td width="50" align="left"></td>
 </tr>
 <tr>
  <td width="120" align="left">Status E-Ktp</td>
  <td width="20" align="center">:</td>
  <td width="210" align="left"><B>'.$val->current_status_code.'</B></td>
  <td width="80" align="center"></td>
  <td width="60" align="center"></td>
  <td width="20" align="center"></td>
  <td width="50" align="left"></td>
 </tr>
 <tr>
  <td width="120" align="left">Tanggal Pengajuan E-Ktp</td>
  <td width="20" align="center">:</td>
  <td width="210" align="left"><B>'.$val->req_date.'</B></td>
  <td width="80" align="center"></td>
  <td width="60" align="center"></td>
  <td width="20" align="center"></td>
  <td width="50" align="left"></td>
 </tr>
 <tr>
  <td width="120" align="left">Diajukan E-Ktp Oleh</td>
  <td width="20" align="center">:</td>
  <td width="210" align="left"><B>'.$val->req_by.'</B></td>
  <td width="80" align="center"></td>
  <td width="60" align="center"></td>
  <td width="20" align="center"></td>
  <td width="50" align="left"></td>
 </tr>
</table>
<br>
<br>
<br>
<table class="first" cellpadding="0" cellspacing="5" border="0">
 <tr>
  <td colspan = "7" align="left"><p>Keterangan Pengajuan Pencetakan E-ktp Ini Dikeluarkan Oleh '.$request->session()->get('S_NAMA_LGKP').' Selaku Operator Dinas Kependudukan Dan Pencatatan Sipil Kota Bandung Di-'.$request->session()->get('S_NAMA_KANTOR').', Melalui Aplikasi Monitoring Dinas Kependudukan Dan Pencatatan Sipil Kota Bandung</p><br><p>Resi Ini Kami Buat Sebagai Bukti Bahwa Telah Mengajukan Pencetakan KTP-el, Dan <b>Tidak Dapat</b> Dipergunakan Untuk Kepentingan Pemilu, Pemilukada, Pilkades, Perbankan, Imigrasi, Kepolisian, Asuransi, BPJS, Pernikahan, dan lain-lain. Kepada yang berkepentingan agar menjadi maklum.</p></td>
 </tr>
</table>
<table class="first" cellpadding="0" cellspacing="5" border="0">
	<tr>
			<td colspan="3" height="33"></td>
	</tr>
        <tr>
            <td width="30%"></td>	
            <td width="25%"></td>											
            <td width="45%" align="center">KOTA BANDUNG, '.date('d-m-Y').'</td>
		</tr>
		<tr>
          <td></td>
          <td></td>
          <td align="center">Dikeluarkan Oleh : </td>
        </tr>
        <tr>

          <td></td>
          
			  <td rowspan="5" align="center" valign="bottom">
			   
			          
		</td>
          <td align="center">a.n. KEPALA DINAS KEPENDUDUKAN DAN PENCATATAN SIPIL</td>
        </tr>
        <tr>
          <td height="60" valign="top" align="center">Pemohon</td>
          <td height="60" valign="top" align="center">Kepala Bidang Pelayanan Pendaftaran Penduduk</td>
        </tr>        
        <tr>
            <td height="20"></td>
            <td height="20"></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td align="center"><u>'.$val->nama_lgkp.'</u></td>
            <td align="center"><u>Hj. SITTI WAHYUNI, SH.</u></td>
        </tr>
        <tr style="line-height:7px">
            <td align="center">NIK. '.$val->nik.'</td>
            <td></td>
            <td align="center">NIP. 196205261986032006</td>
        </tr>
    </table>
';

// output the HTML content
$filename = 'assets/upload/pp/'.$request->session()->get('S_NIK').'.jpg';
if (file_exists($filename)) {
$mask = PDF::Image(K_PATH_IMAGES.'example.png', 50, 140, 100, '', '', '', '', false, 300, '', true);
$image_file_bg = K_PATH_IMAGES.'example.png';
PDF::Image($image_file_bg, 20, 50, 160, '', '', '', '', false, false, '', false, $mask);
} else {
$mask = PDF::Image(K_PATH_IMAGES.'example.png', 50, 140, 100, '', '', '', '', false, 300, '', true);
$image_file_bg = K_PATH_IMAGES.'example.png';
PDF::Image($image_file_bg, 20, 50, 160, '', '', '', '', false, false, '', false, $mask);
}	
PDF::writeHTML($html, true, false, true, false, '');
PDF::write2DBarcode($val->nik, 'QRCODE,Q',150 ,80,25,25, $style, 'N');


// embed image, masked with previously embedded mask

// PDF::Image(base_url('assets/upload/pp/'.$request->session()->get('S_NIK').'.jpg'), 50, 50, 100, '', '', 'http://www.tcpdf.org', '', false, 300);

// PDF::Image($image_file_bg, 10, 120, 180, '', '', '', '', false, 300, '', false, 300);
}
        
        //Close and output PDF document
        PDF::Output(strtoupper($request->session()->get('S_USER_ID')).'_DATA_REKAM_'.time().'.pdf', 'I');
         } 
        else 
        {
           redirect('/','refresh');
        }
    }

	public function cek_data_duplicate($NIK){
			$sql = "SELECT  A.NIK
					, A.NO_PROP
					, A.NAMA_PROP
					, A.NO_KAB
					, A.NAMA_KAB
					, A.NO_KEC
					, A.NAMA_KEC
					, A.NO_KEL
					, A.NAMA_KEL
					, A.NAMA_LGKP
					, A.ALAMAT
					, A.CURRENT_STATUS_CODE
					, A.TGL_REKAM
					, A.NIK_SINGLE
					, A.NO_PROP_SINGLE
					, A.NAMA_PROP_SINGLE
					, A.NO_KAB_SINGLE
					, A.NAMA_KAB_SINGLE
					, A.NO_KEC_SINGLE
					, A.NAMA_KEC_SINGLE
					, A.NO_KEL_SINGLE
					, A.NAMA_KEL_SINGLE
					, A.NAMA_LGKP_SINGLE
					, A.ALAMAT_SINGLE
					, A.CURRENT_STATUS_CODE_SINGLE
					, A.TGL_REKAM_SINGLE FROM (SELECT  A.NIK
					, A.NO_PROP
					, F5_GET_NAMA_PROVINSI(A.NO_PROP) NAMA_PROP
					, A.NO_KAB
					, F5_GET_NAMA_KABUPATEN(A.NO_PROP,A.NO_KAB) NAMA_KAB
					, A.NO_KEC
					, F5_GET_NAMA_KECAMATAN(A.NO_PROP,A.NO_KAB,A.NO_KEC) NAMA_KEC
					, '0' NO_KEL
					, '-' NAMA_KEL
					, A.NAMA_LGKP
					, '-' ALAMAT
					, A.CURRENT_STATUS_CODE
					, TO_CHAR(A.TGL_REKAM,'DD-MM-YYYY') TGL_REKAM
					, TO_CHAR(A.NIK_SINGLE) NIK_SINGLE
					, TO_CHAR(A.NO_PROP_SINGLE) NO_PROP_SINGLE
					, (SELECT C.NAMA_PROP FROM SETUP_PROP C WHERE A.NO_PROP_SINGLE = C.NO_PROP) NAMA_PROP_SINGLE
					, TO_CHAR(A.NO_KAB_SINGLE) NO_KAB_SINGLE
					, (SELECT C.NAMA_KAB FROM SETUP_KAB C WHERE A.NO_PROP_SINGLE = C.NO_PROP AND A.NO_KAB_SINGLE = C.NO_KAB) NAMA_KAB_SINGLE
					, TO_CHAR(A.NO_KEC_SINGLE) NO_KEC_SINGLE
					, (SELECT C.NAMA_KEC FROM SETUP_KEC C WHERE A.NO_PROP_SINGLE = C.NO_PROP AND A.NO_KAB_SINGLE = C.NO_KAB AND A.NO_KEC_SINGLE = C.NO_KEC) NAMA_KEC_SINGLE
					, '0' NO_KEL_SINGLE
					, '-' NAMA_KEL_SINGLE
					, A.NAMA_LGKP_SINGLE
					, '-' ALAMAT_SINGLE
					, A.CURRENT_STATUS_CODE_SINGLE
					, TO_CHAR(A.TGL_REKAM_SINGLE,'DD-MM-YYYY') TGL_REKAM_SINGLE
          			FROM SIAK_DUPLICATE_FULL A
                UNION ALL
                SELECT  B.NIK
					, B.NO_PROP
					, B.NAMA_PROP
					, B.NO_KAB
					, B.NAMA_KAB
					, B.NO_KEC
					, B.NAMA_KEC
					, B.NO_KEL
					, B.NAMA_KEL
					, B.NAMA_LGKP
					, B.ALAMAT
					, B.CURRENT_STATUS_CODE
					, B.TGL_REKAM
					, B.NIK_SINGLE
					, B.NO_PROP_SINGLE
					, B.NAMA_PROP_SINGLE
					, B.NO_KAB_SINGLE
					, B.NAMA_KAB_SINGLE
					, B.NO_KEC_SINGLE
					, B.NAMA_KEC_SINGLE
					, B.NO_KEL_SINGLE
					, B.NAMA_KEL_SINGLE
					, B.NAMA_LGKP_SINGLE
					, B.ALAMAT_SINGLE
					, B.CURRENT_STATUS_CODE_SINGLE
					, B.TGL_REKAM_SINGLE FROM (SELECT  B.NIK
					, B.NO_PROP
					, F5_GET_NAMA_PROVINSI(B.NO_PROP) NAMA_PROP
					, B.NO_KAB
					,F5_GET_NAMA_KABUPATEN(B.NO_PROP,B.NO_KAB) NAMA_KAB
					, B.NO_KEC
					, F5_GET_NAMA_KECAMATAN(B.NO_PROP,B.NO_KAB,B.NO_KEC) NAMA_KEC
					, '0' NO_KEL
					, '-' NAMA_KEL
					, B.NAMA_LGKP
					, '-' ALAMAT
					, B.CURRENT_STATUS_CODE
					, TO_CHAR(B.CREATED,'DD-MM-YYYY') TGL_REKAM
					, TO_CHAR(C.DUPLICATE_NIK) NIK_SINGLE
					, '0' NO_PROP_SINGLE
					, '-' NAMA_PROP_SINGLE
					, '0' NO_KAB_SINGLE
					, '-' NAMA_KAB_SINGLE
					, '0' NO_KEC_SINGLE
					, '-' NAMA_KEC_SINGLE
					, '0' NO_KEL_SINGLE
					, '-' NAMA_KEL_SINGLE
					, '-' NAMA_LGKP_SINGLE
					, '-' ALAMAT_SINGLE
					, '-' CURRENT_STATUS_CODE_SINGLE
					, '-' TGL_REKAM_SINGLE 
          FROM DEMOGRAPHICS@DB221 B INNER JOIN  DUPLICATE_RESULTS@DB221 C ON B.NIK = C.NIK WHERE 
          (B.NIK IN ($NIK)) AND NOT EXISTS (SELECT 1 FROM SIAK_DUPLICATE_FULL A WHERE A.NIK = B.NIK)) B
                ) A
                WHERE A.NIK IN ($NIK) OR A.NIK_SINGLE IN ($NIK)";
			$r = DB::select($sql);
			return $r;
		}
		public function scr_cek_data_rekam($nik = '',$no_kk = '',$nama_lgkp = '',$tgl_lhr = '',$no_kec = 0,$no_kel = 0){
			$sql = "";
			$sql .= " SELECT 
            B.NIK
            , B.NO_KK
            , B.NAMA_LGKP
            , B.TMPT_LHR
            , TO_CHAR(B.TGL_LHR,'DD-MM-YYYY') AS TGL_LHR
            , UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(B.JENIS_KLMIN,7), 801)) JENIS_KLMIN
            , D.ALAMAT
            , LPAD(TO_CHAR(D.NO_RT), 3, '0') AS RT
            , LPAD(TO_CHAR(D.NO_RW), 3, '0') AS RW
            , CASE WHEN D.KODE_POS IS NULL THEN '-' ELSE TO_CHAR(D.KODE_POS) END KODE_POS
            ,(SELECT COUNT(1) AS JML FROM CARD_MANAGEMENT@DB2 WHERE NIK = B.NIK ) AS JUMLAH_CETAK
            , F5_GET_NAMA_KECAMATAN(B.NO_PROP,B.NO_KAB,B.NO_KEC) NAMA_KEC
            , F5_GET_NAMA_KELURAHAN(B.NO_PROP,B.NO_KAB,B.NO_KEC,B.NO_KEL) NAMA_KEL
            , UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(B.AGAMA,7), 501)) AGAMA
            , UPPER(F5_GET_REF_WNI(B.JENIS_PKRJN, 201)) JENIS_PKRJN
            , UPPER(F5_GET_REF_WNI(B.STAT_KWN, 601)) STAT_KWN
            , CASE WHEN A.CURRENT_STATUS_CODE IS NULL THEN '-' ELSE A.CURRENT_STATUS_CODE END AS CURRENT_STATUS_CODE
            , CASE WHEN F.PRINTED_BY IS NULL THEN '-' ELSE F.PRINTED_BY END AS SUKET_BY 
            , CASE WHEN TO_CHAR(F.PRINTED_DATE,'DD-MM-YYYY') IS NULL THEN '-' ELSE TO_CHAR(F.PRINTED_DATE,'DD-MM-YYYY') END AS SUKET_DT
            , CASE WHEN  TO_CHAR(G.LAST_UPDATE,'DD-MM-YYYY') IS NULL AND TO_CHAR(G.PERSONALIZED_DATE,'DD-MM-YYYY') IS NULL THEN '-' WHEN TO_CHAR(G.LAST_UPDATE,'DD-MM-YYYY') IS NULL AND TO_CHAR(G.PERSONALIZED_DATE,'DD-MM-YYYY') IS NOT NULL THEN TO_CHAR(G.PERSONALIZED_DATE,'DD-MM-YYYY') ELSE  TO_CHAR(G.LAST_UPDATE,'DD-MM-YYYY') END AS KTP_DT
            , CASE WHEN TO_CHAR(G.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(G.CREATED_USERNAME) IS NULL  THEN '-' WHEN TO_CHAR(G.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(G.CREATED_USERNAME) IS NOT NULL THEN TO_CHAR(G.CREATED_USERNAME) ELSE TO_CHAR(G.LAST_UPDATED_USERNAME) END AS KTP_BY
            , E.PATH
            , CASE WHEN TO_CHAR(H.REQ_DATE,'DD-MM-YYYY') IS NULL THEN '-' ELSE TO_CHAR(H.REQ_DATE,'DD-MM-YYYY') END REQ_DATE
            , CASE WHEN H.REQ_BY IS NULL THEN '-' ELSE H.REQ_BY END REQ_BY
            , CASE WHEN H.ALASAN_PENGAJUAN IS NULL THEN '-' ELSE H.ALASAN_PENGAJUAN END PROC_BY
            , CASE WHEN H.STEP_PROC IS NULL THEN '-' WHEN H.STEP_PROC = 'T' THEN 'Pengajuan Ditolak' WHEN H.STEP_PROC = 'A' THEN 'Masih Dalam Proses' WHEN H.STEP_PROC = 'C' THEN 'Sudah Tercetak' ELSE  H.STEP_PROC END STEP_PROC 
            FROM BIODATA_WNI B 
            LEFT JOIN DATA_KELUARGA D ON B.NO_KK = D.NO_KK 
            LEFT JOIN DEMOGRAPHICS_ALL@DB2 A  ON A.NIK = B.NIK  
            LEFT JOIN T5_FOTO E ON A.NIK = E.NIK 
            LEFT JOIN (SELECT NIK, PRINTED_DATE,PRINTED_BY,NO_DOKUMEN  FROM (SELECT NIK,PRINTED_DATE,PRINTED_BY,NO_DOKUMEN, RANK() OVER (PARTITION BY NIK ORDER BY PRINTED_DATE DESC,NO_DOKUMEN DESC) RNK FROM T7_HIST_SUKET)  WHERE RNK = 1) F ON F.NIK = B.NIK 
            LEFT JOIN (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME,LAST_UPDATE,CREATED FROM (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME,LAST_UPDATE,CREATED, RANK() OVER (PARTITION BY NIK ORDER BY  PERSONALIZED_DATE DESC, LAST_UPDATE DESC, CHIP_ID DESC) RNK FROM CARD_MANAGEMENT@DB2) WHERE RNK = 1) G ON G.NIK =B.NIK LEFT JOIN (SELECT NIK,REQ_DATE,REQ_BY,PROC_DATE,PROC_BY,STEP_PROC,ALASAN_PENGAJUAN FROM (SELECT NIK,REQ_DATE,REQ_BY,PROC_DATE,PROC_BY,STEP_PROC,ALASAN_PENGAJUAN, RANK() OVER (PARTITION BY NIK ORDER BY REQ_DATE DESC,ID_CETAK DESC) RNK FROM SIAK_REQ_CETAK_KTP@YZDB) WHERE RNK = 1) H ON H.NIK =B.NIK WHERE 1=1 
				AND (B.FLAG_STATUS = '0' OR (B.FLAG_STATUS = '2' AND B.FLAG_PINDAH IN(1,2,3))) ";
			if($no_kec != 0 ){ 
				$sql .= " AND B.NO_KEC = $no_kec ";
			} 
			if($no_kel != 0 ){ 
				$sql .= " AND B.NO_KEL = $no_kel ";
			} 
			if($nik != '' || $nik != null || !empty($nik)){ 
				if(strlen($nik) <= 17){
				$sql .= " AND B.NIK = $nik ";	
				}else{
				$sql .= " AND B.NIK IN ($nik) ";	
				}
				
			} 
			if($no_kk != '' || $no_kk != null || !empty($no_kk)){ 
				if(strlen($no_kk) <= 17){
				$sql .= " AND B.NO_KK = $no_kk ";
				}else{
				$sql .= " AND B.NO_KK IN ($no_kk) ";
				}
			}
			if($nama_lgkp != '' || $nama_lgkp != null || !empty($nama_lgkp)){ 
				$sql .= " AND B.NAMA_LGKP LIKE UPPER('$nama_lgkp') ";
			}
			if($tgl_lhr != '' || $tgl_lhr != null || !empty($tgl_lhr)){ 
				$sql .= " AND TO_CHAR(B.TGL_LHR,'DD-MM-YYYY') = '$tgl_lhr' ";
			} 
			$sql .= " ORDER BY B.NO_KK, B.STAT_HBKEL ";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}
		public function scr_cek_data_rekam_wna($nik = '',$no_kk = '',$nama_lgkp = '',$tgl_lhr = '',$no_kec = 0,$no_kel = 0){
			$sql = "";
			$sql .= " SELECT 
            B.NIK
            , B.NO_KK
            , CONCAT(B.NAMA_PERTMA,CONCAT(' ',B.NAMA_KLRGA)) NAMA_LGKP
            , B.TMPT_LHR
            , TO_CHAR(B.TGL_LHR,'DD-MM-YYYY') AS TGL_LHR
            , UPPER(F5_GET_REF_WNA(F5_TO_NUMBER(B.JENIS_KLMIN,7), 801)) JENIS_KLMIN
            , D.ALAMAT
            , LPAD(TO_CHAR(D.NO_RT), 3, '0') AS RT
            , LPAD(TO_CHAR(D.NO_RW), 3, '0') AS RW
            , CASE WHEN D.KODE_POS IS NULL THEN '-' ELSE TO_CHAR(D.KODE_POS) END KODE_POS
            ,(SELECT COUNT(1) AS JML FROM CARD_MANAGEMENT@DB2 WHERE NIK = B.NIK ) AS JUMLAH_CETAK
            , F5_GET_NAMA_KECAMATAN(B.NO_PROP,B.NO_KAB,B.NO_KEC) NAMA_KEC
            , F5_GET_NAMA_KELURAHAN(B.NO_PROP,B.NO_KAB,B.NO_KEC,B.NO_KEL) NAMA_KEL
            , UPPER(F5_GET_REF_WNA(F5_TO_NUMBER(B.AGAMA,7), 901)) AGAMA
            , UPPER(F5_GET_REF_WNA(B.JENIS_PKRJN, 201)) JENIS_PKRJN
            , UPPER(F5_GET_REF_WNA(B.STAT_KWN, 701)) STAT_KWN
            , CASE WHEN A.CURRENT_STATUS_CODE IS NULL THEN '-' ELSE A.CURRENT_STATUS_CODE END AS CURRENT_STATUS_CODE
            , CASE WHEN  TO_CHAR(G.LAST_UPDATE,'DD-MM-YYYY') IS NULL AND TO_CHAR(G.PERSONALIZED_DATE,'DD-MM-YYYY') IS NULL THEN '-' WHEN TO_CHAR(G.LAST_UPDATE,'DD-MM-YYYY') IS NULL AND TO_CHAR(G.PERSONALIZED_DATE,'DD-MM-YYYY') IS NOT NULL THEN TO_CHAR(G.PERSONALIZED_DATE,'DD-MM-YYYY') ELSE  TO_CHAR(G.LAST_UPDATE,'DD-MM-YYYY') END AS KTP_DT
            , CASE WHEN TO_CHAR(G.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(G.CREATED_USERNAME) IS NULL  THEN '-' WHEN TO_CHAR(G.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(G.CREATED_USERNAME) IS NOT NULL THEN TO_CHAR(G.CREATED_USERNAME) ELSE TO_CHAR(G.LAST_UPDATED_USERNAME) END AS KTP_BY
            , E.PATH
            , '' SUKET_BY
            , '' SUKET_DT
            , '' REQ_DATE
            , '' REQ_BY
            , '' PROC_BY
            , '' STEP_PROC

            FROM BIODATA_WNA B 
            LEFT JOIN DATA_KELUARGA D ON B.NO_KK = D.NO_KK 
            LEFT JOIN DEMOGRAPHICS_ALL@DB2 A  ON A.NIK = B.NIK  
            LEFT JOIN T5_FOTO E ON A.NIK = E.NIK 
            LEFT JOIN (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME,LAST_UPDATE,CREATED FROM (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME,LAST_UPDATE,CREATED, RANK() OVER (PARTITION BY NIK ORDER BY  PERSONALIZED_DATE DESC, LAST_UPDATE DESC, CHIP_ID DESC) RNK FROM CARD_MANAGEMENT@DB2) WHERE RNK = 1) G ON G.NIK =B.NIK  WHERE 1=1 
				AND (B.FLAG_STATUS = '0' OR (B.FLAG_STATUS = '2' AND B.FLAG_PINDAH IN(1,2,3))) ";
			if($no_kec != 0 ){ 
				$sql .= " AND B.NO_KEC = $no_kec ";
			} 
			if($no_kel != 0 ){ 
				$sql .= " AND B.NO_KEL = $no_kel ";
			} 
			if($nik != '' || $nik != null || !empty($nik)){ 
				if(strlen($nik) <= 17){
				$sql .= " AND B.NIK = $nik ";	
				}else{
				$sql .= " AND B.NIK IN ($nik) ";	
				}
				
			} 
			if($no_kk != '' || $no_kk != null || !empty($no_kk)){ 
				if(strlen($no_kk) <= 17){
				$sql .= " AND B.NO_KK = $no_kk ";
				}else{
				$sql .= " AND B.NO_KK IN ($no_kk) ";
				}
			}
			if($nama_lgkp != '' || $nama_lgkp != null || !empty($nama_lgkp)){ 
				$sql .= " AND B.NAMA_LGKP LIKE UPPER('$nama_lgkp') ";
			}
			if($tgl_lhr != '' || $tgl_lhr != null || !empty($tgl_lhr)){ 
				$sql .= " AND TO_CHAR(B.TGL_LHR,'DD-MM-YYYY') = '$tgl_lhr' ";
			} 
			$sql .= " ORDER BY B.NO_KK, B.STAT_HBKEL ";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}
		public function scr_cek_kk($no_kk = '',$no_kec = 0){
			$sql = "";
			$sql .= " SELECT 
						  A.NO_KK
						  , A.NAMA_KEP
						  , A.ALAMAT
						  , A.NO_PROP
						  , A.NO_KAB
						  , A.NO_KEC
						  , A.NO_KEL
						  , F5_GET_BSRE_KK_JML(A.NO_KK) JML_PENGAJUAN
						  , F5_GET_NAMA_KECAMATAN(A.NO_PROP,A.NO_KAB,A.NO_KEC) NAMA_KEC
            			  , F5_GET_NAMA_KELURAHAN(A.NO_PROP,A.NO_KAB,A.NO_KEC,A.NO_KEL) NAMA_KEL
						  , LPAD(TO_CHAR(A.NO_RT), 3, '0') AS NO_RT
            			  , LPAD(TO_CHAR(A.NO_RW), 3, '0') AS NO_RW
						  , CASE WHEN A.KODE_POS IS NULL THEN '-' ELSE TO_CHAR(A.KODE_POS) END KODE_POS
						  , A.COUNT_KK
						  , CASE WHEN B.CERT_STATUS IS NULL THEN '-'  ELSE TO_CHAR(B.CERT_STATUS) END CERT_STATUS
						  , CASE WHEN B.REQ_DATE IS NULL THEN '-'  ELSE TO_CHAR(B.REQ_DATE,'DD-MM-YYYY') END REQ_DATE
						  , CASE WHEN B.REQ_BY IS NULL THEN '-'  ELSE TO_CHAR(B.REQ_BY) END REQ_BY
						  , CASE WHEN B.URL_DOKUMEN IS NULL THEN '-'  ELSE TO_CHAR(B.URL_DOKUMEN) END URL_DOKUMEN
						  , CASE WHEN B.URL_DOKUMEN IS NULL THEN '-'  ELSE SUBSTR(B.URL_DOKUMEN,20) END URL_KK
						  , CASE WHEN B.URL_DOKUMEN IS NULL THEN '-'  ELSE SUBSTR(B.URL_DOKUMEN,11,8) END URL_DATE
						  , CASE WHEN B.URL_DOKUMEN IS NULL THEN '-'  ELSE CONCAT(CONCAT(CONCAT(CONCAT('https://10.32.73.222/Siak/cetak/main/view_pdf/',SUBSTR(B.URL_DOKUMEN,20)),'/'),SUBSTR(B.URL_DOKUMEN,11,8)),'/cetak_kartu_keluarga.pdf') END URL_DOWNLOAD 
						  , CASE WHEN B.PEJABAT_PROCESS_DATE IS NULL THEN '-'  ELSE TO_CHAR(B.PEJABAT_PROCESS_DATE,'DD-MM-YYYY') END APROVE_DATE
						  , CASE WHEN C.PRINTED_DATE IS NULL THEN '-'  ELSE TO_CHAR(C.PRINTED_DATE,'DD-MM-YYYY') END PRINTED_DATE
						  , CASE WHEN C.PRINTED_BY IS NULL THEN '-'  ELSE TO_CHAR(C.PRINTED_BY) END PRINTED_BY
						FROM DATA_KELUARGA A 
						LEFT JOIN 
						  (
						    SELECT 
						    NO_DOC
						    , CERT_STATUS
						    , REQ_DATE
						    , REQ_BY
						    , SEQN_ID
						    , URL_DOKUMEN
						    , PEJABAT_PROCCESS_BY
						    , PEJABAT_PROCESS_DATE  
						  FROM 
						  (
						    SELECT 
						    NO_DOC
						    , CERT_STATUS
						    , REQ_DATE
						    , REQ_BY
						    , SEQN_ID
						    , URL_DOKUMEN
						    , PEJABAT_PROCCESS_BY
						    , PEJABAT_PROCESS_DATE 
						    , RANK() OVER (PARTITION BY NO_DOC ORDER BY REQ_DATE DESC,SEQN_ID DESC) RNK FROM BSRE_KARTU_KELUARGA) 
						    WHERE RNK = 1) B ON A.NO_KK = B.NO_DOC
						LEFT JOIN 
						   (
						     SELECT 
						     NO_KK
						     , PRINTED_DATE
						     , PRINTED_BY
						     , SEQN_ID  
						FROM 
						  (
						    SELECT 
						    NO_KK
						    , PRINTED_DATE
						    , PRINTED_BY
						    , SEQN_ID
						    , RANK() OVER (PARTITION BY NO_KK ORDER BY PRINTED_DATE DESC,SEQN_ID DESC) RNK FROM T5_SEQN_PRINT_KK) 
						    WHERE RNK = 1) C ON A.NO_KK = C.NO_KK 
						    WHERE 1=1 
   						 ";
					if($no_kec != 0 ){ 
						$sql .= " AND A.NO_KEC = $no_kec ";
					} 
					if($no_kk != '' || $no_kk != null || !empty($no_kk)){ 
						if(strlen($no_kk) <= 17){
						$sql .= " AND A.NO_KK = $no_kk ";
						}else{
						$sql .= " AND A.NO_KK IN ($no_kk) ";
						}
					}
					$sql .= " ORDER BY A.NO_KEC, A.NO_KEL, A.NO_RW, A.NO_RT,A.NO_KK ";
					$r = DB::connection('db222')->select($sql);
					return $r;
		}

		public function scr_export_data_rekam($nik = ''){
			$sql = "";
			$sql .= "SELECT B.NIK,B.NO_KK, B.NAMA_LGKP, B.TMPT_LHR,TO_CHAR(B.TGL_LHR,'DD-MM-YYYY') AS TGL_LHR,CASE WHEN B.JENIS_KLMIN = 1 THEN 'LAKI-LAKI' ELSE 'PEREMPUAN' END AS JENIS_KLMIN, D.ALAMAT,  LPAD(TO_CHAR(D.NO_RT), 3, '0') AS RT,LPAD(TO_CHAR(D.NO_RW), 3, '0') AS RW,(SELECT COUNT(1) AS JML FROM CARD_MANAGEMENT@DB2 WHERE NIK = B.NIK ) AS JUMLAH_CETAK, F5_GET_NAMA_KECAMATAN(B.NO_PROP,B.NO_KAB,B.NO_KEC) AS NAMA_KEC, F5_GET_NAMA_KELURAHAN(B.NO_PROP,B.NO_KAB,B.NO_KEC,B.NO_KEL) AS NAMA_KEL, UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(B.AGAMA,7), 501)) AGAMA, UPPER(F5_GET_REF_WNI(B.JENIS_PKRJN, 201)) JENIS_PKRJN, UPPER(F5_GET_REF_WNI(B.STAT_KWN, 601)) STAT_KWN, CASE WHEN A.CURRENT_STATUS_CODE IS NULL THEN '-' ELSE A.CURRENT_STATUS_CODE END AS CURRENT_STATUS_CODE, CASE WHEN F.PRINTED_BY IS NULL THEN '-' ELSE F.PRINTED_BY END AS SUKET_BY ,CASE WHEN TO_CHAR(F.PRINTED_DATE,'DD-MM-YYYY') IS NULL THEN '-' ELSE TO_CHAR(F.PRINTED_DATE,'DD-MM-YYYY') END AS SUKET_DT,CASE WHEN  TO_CHAR(G.LAST_UPDATE,'DD-MM-YYYY') IS NULL AND TO_CHAR(G.PERSONALIZED_DATE,'DD-MM-YYYY') IS NULL THEN '-' WHEN TO_CHAR(G.LAST_UPDATE,'DD-MM-YYYY') IS NULL AND TO_CHAR(G.PERSONALIZED_DATE,'DD-MM-YYYY') IS NOT NULL THEN TO_CHAR(G.PERSONALIZED_DATE,'DD-MM-YYYY') ELSE  TO_CHAR(G.LAST_UPDATE,'DD-MM-YYYY') END AS KTP_DT, CASE WHEN TO_CHAR(G.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(G.CREATED_USERNAME) IS NULL  THEN '-' WHEN TO_CHAR(G.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(G.CREATED_USERNAME) IS NOT NULL THEN TO_CHAR(G.CREATED_USERNAME) ELSE TO_CHAR(G.LAST_UPDATED_USERNAME) END AS KTP_BY, E.PATH, CASE WHEN TO_CHAR(H.REQ_DATE,'DD-MM-YYYY') IS NULL THEN '-' ELSE TO_CHAR(H.REQ_DATE,'DD-MM-YYYY') END REQ_DATE, CASE WHEN H.REQ_BY IS NULL THEN '-' ELSE H.REQ_BY END REQ_BY,CASE WHEN H.PROC_BY IS NULL THEN '-' ELSE H.PROC_BY END PROC_BY, CASE WHEN H.STEP_PROC IS NULL THEN '-' WHEN H.STEP_PROC = 'T' THEN 'Pengajuan Ditolak' WHEN H.STEP_PROC = 'A' THEN 'Masih Dalam Proses' WHEN H.STEP_PROC = 'C' THEN 'Sudah Tercetak' ELSE  H.STEP_PROC END STEP_PROC FROM BIODATA_WNI B LEFT JOIN DATA_KELUARGA D ON B.NO_KK = D.NO_KK LEFT JOIN DEMOGRAPHICS_ALL@DB2 A  ON A.NIK = B.NIK  LEFT JOIN T5_FOTO E ON A.NIK = E.NIK LEFT JOIN (SELECT NIK, PRINTED_DATE,PRINTED_BY,NO_DOKUMEN  FROM (SELECT NIK,PRINTED_DATE,PRINTED_BY,NO_DOKUMEN, RANK() OVER (PARTITION BY NIK ORDER BY PRINTED_DATE DESC,NO_DOKUMEN DESC) RNK FROM T7_HIST_SUKET)  WHERE RNK = 1) F ON F.NIK = B.NIK LEFT JOIN (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME,LAST_UPDATE,CREATED FROM (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME,LAST_UPDATE,CREATED, RANK() OVER (PARTITION BY NIK ORDER BY  PERSONALIZED_DATE DESC, LAST_UPDATE DESC, CHIP_ID DESC) RNK FROM CARD_MANAGEMENT@DB2) WHERE RNK = 1) G ON G.NIK =B.NIK LEFT JOIN (SELECT NIK,REQ_DATE,REQ_BY,PROC_DATE,PROC_BY,STEP_PROC FROM (SELECT NIK,REQ_DATE,REQ_BY,PROC_DATE,PROC_BY,STEP_PROC, RANK() OVER (PARTITION BY NIK ORDER BY REQ_DATE DESC,ID_CETAK DESC) RNK FROM SIAK_REQ_CETAK_KTP@YZDB) WHERE RNK = 1) H ON H.NIK =B.NIK WHERE 1=1 
				AND (B.FLAG_STATUS = '0' OR (B.FLAG_STATUS = '2' AND B.FLAG_PINDAH IN(1,2,3))) ";
			if($nik != '' || $nik != null || !empty($nik)){ 
				$sql .= " AND B.NIK IN ($nik) ";
			} 
			$sql .= " ORDER BY B.NO_KEC, B.NO_KEL, B.NO_KK, D.NO_RW, D.NO_RT, B.STAT_HBKEL ";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}
		public function scr_export_data_nik($nik = ''){
			$sql = "";
			$sql .= "SELECT CONCAT('''',B.NIK) NIK,CONCAT('''',B.NO_KK) NO_KK, B.NAMA_LGKP, TRUNC(MONTHS_BETWEEN(SYSDATE,B.TGL_LHR)/12) UMUR, B.TMPT_LHR,TO_CHAR(B.TGL_LHR,'DD-MM-YYYY') AS TGL_LHR,CASE WHEN B.JENIS_KLMIN = 1 THEN 'LAKI-LAKI' ELSE 'PEREMPUAN' END AS JENIS_KLMIN, D.ALAMAT,  LPAD(TO_CHAR(D.NO_RT), 3, '0') AS RT,LPAD(TO_CHAR(D.NO_RW), 3, '0') AS RW, F5_GET_NAMA_KECAMATAN(B.NO_PROP,B.NO_KAB,B.NO_KEC) AS NAMA_KEC, F5_GET_NAMA_KELURAHAN(B.NO_PROP,B.NO_KAB,B.NO_KEC,B.NO_KEL) AS NAMA_KEL, UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(B.AGAMA,7), 501)) AGAMA, UPPER(F5_GET_REF_WNI(B.JENIS_PKRJN, 201)) JENIS_PKRJN, UPPER(F5_GET_REF_WNI(B.STAT_KWN, 601)) STAT_KWN, UPPER(F5_GET_REF_WNI(B.STAT_HBKEL, 301)) STAT_HBKEL, CASE WHEN D.KODE_POS IS NULL THEN '-' ELSE TO_CHAR(D.KODE_POS) END KODE_POS FROM BIODATA_WNI B LEFT JOIN DATA_KELUARGA D ON B.NO_KK = D.NO_KK  WHERE 1=1 
				AND (B.FLAG_STATUS = '0' OR (B.FLAG_STATUS = '2' AND B.FLAG_PINDAH IN(1,2,3))) ";
			if($nik != '' || $nik != null || !empty($nik)){ 
				$sql .= " AND B.NIK IN ($nik) ";
			} 
			$sql .= " ORDER BY B.NO_KEC, B.NO_KEL, B.NO_KK, D.NO_RW, D.NO_RT, B.STAT_HBKEL ";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}
		public function scr_cek_data_history($nik = '',$no_kec= 0){
			$sql = "";
			$sql .= "SELECT A.NIK, A.NO_KK, A.NAMA_LGKP, B.CHIP_ID, TO_CHAR(B.PERSONALIZED_DATE,'DD-MM-YYYY') AS KTP_DT, B.CREATED_USERNAME AS KTP_BY
			, CASE WHEN TO_CHAR(B.LAST_UPDATE,'DD-MM-YYYY') IS NULL THEN '-' ELSE TO_CHAR(B.LAST_UPDATE,'DD-MM-YYYY') END AS UPDATED_DT
			, CASE WHEN B.LAST_UPDATED_USERNAME IS NULL THEN NULL ELSE B.LAST_UPDATED_USERNAME END AS UPDATED_BY
			,F5_GET_NAMA_KECAMATAN(32,73,A.NO_KEC) NAMA_KEC, F5_GET_NAMA_KELURAHAN(32,73,A.NO_KEC,A.NO_KEL) NAMA_KEL FROM BIODATA_WNI A INNER JOIN CARD_MANAGEMENT@DB2 B ON A.NIK = B.NIK WHERE 1=1 AND (A.TGL_LHR<=ADD_MONTHS(SYSDATE,-12*17) OR A.STAT_KWN<>1) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3))) ";
			if($no_kec != 0 ){ 
				$sql .= " AND A.NO_KEC = $no_kec ";
			} 
			$sql .= " AND A.NIK IN ($nik) ORDER BY B.PERSONALIZED_DATE DESC";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}
		public function scr_push_demogh221($NIK){
			$sql = "UPDATE DEMOGRAPHICS A SET A.CURRENT_STATUS_CODE = 'BIO_CAPTURED' WHERE A.CURRENT_STATUS_CODE IN (SELECT CODE FROM STATUS_ID WHERE ID_STATUS IN (3,4,5,6,7,8,9,10,11,12)) AND A.NIK IN ($NIK) AND EXISTS (SELECT 1 FROM BIODATA_WNI@DB222 B WHERE A.NIK = B.NIK)";
			$r = DB::connection('db221')->update($sql);
			return $r;
		}
		public function scr_push_demogh2($NIK){
			$sql = "UPDATE DEMOGRAPHICS A SET A.CURRENT_STATUS_CODE = 'BIO_CAPTURED' WHERE A.CURRENT_STATUS_CODE IN (SELECT CODE FROM STATUS_ID WHERE ID_STATUS IN (3,4,5,6,7,8,9,10,11,12)) AND A.NIK IN ($NIK) AND EXISTS (SELECT 1 FROM BIODATA_WNI@DB222 B WHERE A.NIK = B.NIK)";
			$r = DB::connection('db2')->update($sql);
			return $r;
		}
		public function scr_push_demoghall($NIK){
			$sql = "UPDATE DEMOGRAPHICS_ALL A SET A.CURRENT_STATUS_CODE = 'BIO_CAPTURED' WHERE A.CURRENT_STATUS_CODE IN (SELECT CODE FROM STATUS_ID WHERE ID_STATUS IN (3,4,5,6,7,8,9,10,11,12)) AND A.NIK IN ($NIK) AND EXISTS (SELECT 1 FROM BIODATA_WNI@DB222 B WHERE A.NIK = B.NIK)";
			$r = DB::connection('db2')->update($sql);
			return $r;
		}
		public function scr_push_data_kel($no_kk){
			$sql = "UPDATE DATA_KELUARGA A SET  A.CERT_STATUS = 1 WHERE EXISTS (SELECT 1 FROM BSRE_KARTU_KELUARGA B WHERE A.NO_KK = B.NO_DOC AND B.CERT_STATUS IN (2,3) AND A.NO_KK = $no_kk)";
			$r = DB::connection('db222')->update($sql);
			return $r;
		}
		public function scr_push_data_bsre($no_kk){
			$sql = "UPDATE BSRE_KARTU_KELUARGA SET 
					REQ_DATE = SYSDATE
					, PEJABAT_PROCESS_DATE = NULL
					, URL_DOKUMEN = null  
					, COUNT_PRINT = 0 
					, SENT_STATUS = 0
					, SENT_DATE = null
					, ACTIVE_STATUS = 0
					, ACTIVE_DATE = null
					, PEJABAT_PROCCESS_BY = null
					, CERT_STATUS = 1
					WHERE CERT_STATUS IN (2,3) AND NO_DOC = $no_kk";
			$r = DB::connection('db222')->update($sql);
			return $r;
		}	

		public function scr_del_req($nik,$no_kec){
			$sql = "";
			$sql .= "SELECT 
					X.NO_KEC
					,X.NIK
					,X.NAMA_LGKP AS NAMA_LENGKAP, Y.ALAMAT, CASE WHEN TO_CHAR(Y.KODE_POS) IS NULL THEN '-' ELSE TO_CHAR(Y.KODE_POS) END KODE_POS, LPAD(TO_CHAR(Y.NO_RT), 3, '0') AS RT, LPAD(TO_CHAR(Y.NO_RW), 3, '0') AS RW,CASE WHEN B.CURRENT_STATUS_CODE IS NULL THEN '-' ELSE B.CURRENT_STATUS_CODE END AS CURRENT_STATUS_CODE
					,TO_CHAR(A.REQ_DATE,'YYYY-MM-DD') AS REQ_DATE
					,A.REQ_BY
					, CASE WHEN A.STEP_PROC = 'T' THEN 'DITOLAK' WHEN A.STEP_PROC = 'C' THEN 'DICETAK' ELSE 'PROSSESS' END AS STATUS_PENGAJUAN
					, CASE WHEN A.STEP_PROC = 'T' THEN UPPER(A.PROC_BY) WHEN A.STEP_PROC = 'C' THEN 'SUDAH DICATAK' ELSE '-' END AS KETERANGAN_STATUS
					, TO_CHAR(C.PERSONALIZED_DATE,'YYYY-MM-DD') AS PRINTED_DATE
					, CASE WHEN TO_CHAR(C.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(C.CREATED_USERNAME) IS NULL  THEN '-' WHEN TO_CHAR(C.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(C.CREATED_USERNAME) IS NOT NULL THEN TO_CHAR(C.CREATED_USERNAME) ELSE TO_CHAR(C.LAST_UPDATED_USERNAME) END AS PRINTED_BY
					, F5_GET_NAMA_KECAMATAN(X.NO_PROP,X.NO_KAB,X.NO_KEC) NAMA_KEC
					, F5_GET_NAMA_KELURAHAN(X.NO_PROP,X.NO_KAB,X.NO_KEC,X.NO_KEL) NAMA_KEL
					, UPPER(F5_GET_REF_WNI(X.JENIS_PKRJN, 201)) JENIS_PKRJN
					, UPPER(F5_GET_REF_WNI(X.STAT_KWN, 601)) STAT_KWN
					FROM BIODATA_WNI X LEFT JOIN DATA_KELUARGA Y ON X.NO_KK = Y.NO_KK INNER JOIN (SELECT ID_CETAK,NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY,STEP_PROC,PROC_BY FROM 
					(SELECT  ID_CETAK, NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY,STEP_PROC,PROC_BY, RANK() OVER (PARTITION BY NIK ORDER BY REQ_DATE DESC, ID_CETAK DESC) RNK FROM SIAK_REQ_CETAK_KTP@YZDB WHERE STEP_PROC <> 'C') WHERE RNK = 1) A ON A.NIK =X.NIK
					LEFT JOIN DEMOGRAPHICS_ALL@DB2 B ON A.NIK = B.NIK LEFT JOIN (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME FROM (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME, RANK() OVER (PARTITION BY NIK ORDER BY  PERSONALIZED_DATE DESC, LAST_UPDATE DESC, CHIP_ID DESC) RNK FROM CARD_MANAGEMENT@DB2) WHERE RNK = 1) C ON A.NIK = C.NIK  WHERE 1=1 AND (TO_DATE(A.REQ_DATE+1,'DD-MM-YYYY') > TO_DATE(C.PERSONALIZED_DATE,'DD-MM-YYYY') OR C.PERSONALIZED_DATE IS NULL) ";
			if($no_kec != 0){
				$sql .= " AND X.NO_KEC = $no_kec";
			}
			if($nik != ""){
				$sql .= " AND X.NIK IN ($nik)";
			}
			$sql .= "ORDER BY C.PERSONALIZED_DATE";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}
		public function scr_do_delete($nik){
			$sql = "DELETE FROM SIAK_REQ_CETAK_KTP@YZDB WHERE STEP_PROC <> 'C' AND NIK IN ($nik) ";
			$r = DB::connection('db222')->delete($sql);
		}
		public function scr_cek_req_data_uktp($nik,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			$sql .= "SELECT 
					X.NO_KEC
					,X.NIK
					,X.NAMA_LGKP AS NAMA_LENGKAP, Y.ALAMAT, CASE WHEN TO_CHAR(Y.KODE_POS) IS NULL THEN '-' ELSE TO_CHAR(Y.KODE_POS) END KODE_POS, LPAD(TO_CHAR(Y.NO_RT), 3, '0') AS RT, LPAD(TO_CHAR(Y.NO_RW), 3, '0') AS RW,CASE WHEN B.CURRENT_STATUS_CODE IS NULL THEN '-' ELSE B.CURRENT_STATUS_CODE END AS CURRENT_STATUS_CODE
					,TO_CHAR(A.REQ_DATE,'YYYY-MM-DD') AS REQ_DATE
					,A.REQ_BY
					, CASE WHEN A.STEP_PROC = 'T' THEN 'DITOLAK' WHEN A.STEP_PROC = 'C' THEN 'DICETAK' ELSE 'PROSSESS' END AS STATUS_PENGAJUAN
					, CASE WHEN A.STEP_PROC = 'T' THEN UPPER(A.PROC_BY) WHEN A.STEP_PROC = 'C' THEN 'SUDAH DICATAK' ELSE '-' END AS KETERANGAN_STATUS
					, TO_CHAR(C.PERSONALIZED_DATE,'YYYY-MM-DD') AS PRINTED_DATE
					, CASE WHEN TO_CHAR(C.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(C.CREATED_USERNAME) IS NULL  THEN '-' WHEN TO_CHAR(C.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(C.CREATED_USERNAME) IS NOT NULL THEN TO_CHAR(C.CREATED_USERNAME) ELSE TO_CHAR(C.LAST_UPDATED_USERNAME) END AS PRINTED_BY
					, F5_GET_NAMA_KECAMATAN(X.NO_PROP,X.NO_KAB,X.NO_KEC) NAMA_KEC
					, F5_GET_NAMA_KELURAHAN(X.NO_PROP,X.NO_KAB,X.NO_KEC,X.NO_KEL) NAMA_KEL
					, UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(X.AGAMA,7), 501)) AGAMA
					, UPPER(F5_GET_REF_WNI(X.JENIS_PKRJN, 201)) JENIS_PKRJN
					, UPPER(F5_GET_REF_WNI(X.STAT_KWN, 601)) STAT_KWN
					FROM BIODATA_WNI X LEFT JOIN DATA_KELUARGA Y ON X.NO_KK = Y.NO_KK INNER JOIN (SELECT ID_CETAK,NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY,STEP_PROC,PROC_BY FROM 
					(SELECT  ID_CETAK, NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY,STEP_PROC,PROC_BY, RANK() OVER (PARTITION BY NIK ORDER BY REQ_DATE DESC, ID_CETAK DESC) RNK FROM SIAK_REQ_CETAK_KTP@YZDB WHERE STEP_PROC <> 'C') WHERE RNK = 1) A ON A.NIK =X.NIK
					LEFT JOIN DEMOGRAPHICS_ALL@DB2 B ON A.NIK = B.NIK LEFT JOIN (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME FROM (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME, RANK() OVER (PARTITION BY NIK ORDER BY  PERSONALIZED_DATE DESC, LAST_UPDATE DESC, CHIP_ID DESC) RNK FROM CARD_MANAGEMENT@DB2) WHERE RNK = 1) C ON A.NIK = C.NIK  WHERE 1=1 AND (TO_DATE(A.REQ_DATE,'DD-MM-YYYY') > TO_DATE(C.PERSONALIZED_DATE,'DD-MM-YYYY') OR C.PERSONALIZED_DATE IS NULL) ";
			if($no_kec != 0){
				$sql .= " AND X.NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND X.NO_KEL = $no_kel";
			} 
			if($nik != ""){
				$sql .= " AND X.NIK IN ($nik)";
			}
			$sql .= " AND A.REQ_DATE >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.REQ_DATE < TO_DATE('$tgl_end','DD-MM-YYYY') +1 ORDER BY C.PERSONALIZED_DATE";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}

		public function scr_cek_req_data_dktp($nik,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			$sql .= "SELECT X.NO_KEC,X.NIK,X.NAMA_LGKP AS NAMA_LENGKAP, Y.ALAMAT, CASE WHEN TO_CHAR(Y.KODE_POS) IS NULL THEN '-' ELSE TO_CHAR(Y.KODE_POS) END KODE_POS, LPAD(TO_CHAR(Y.NO_RT), 3, '0') AS RT, LPAD(TO_CHAR(Y.NO_RW), 3, '0') AS RW, B.CURRENT_STATUS_CODE,TO_CHAR(A.REQ_DATE,'DD-MM-YYYY') AS REQ_DATE,A.REQ_BY, TO_CHAR(C.PERSONALIZED_DATE,'DD-MM-YYYY') AS PRINTED_DATE,TO_CHAR(C.PERSONALIZED_DATE,'DD-MM-YYYY') AS PRINTED_DATE,CASE WHEN TO_CHAR(C.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(C.CREATED_USERNAME) IS NULL  THEN '-' WHEN TO_CHAR(C.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(C.CREATED_USERNAME) IS NOT NULL THEN TO_CHAR(C.CREATED_USERNAME) ELSE TO_CHAR(C.LAST_UPDATED_USERNAME) END AS PRINTED_BY, F5_GET_NAMA_KECAMATAN(32,73,A.NO_KEC) NAMA_KEC, F5_GET_NAMA_KELURAHAN(32,73,A.NO_KEC,A.NO_KEL) NAMA_KEL, UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(X.AGAMA,7), 501)) AGAMA, UPPER(F5_GET_REF_WNI(X.JENIS_PKRJN, 201)) JENIS_PKRJN, UPPER(F5_GET_REF_WNI(X.STAT_KWN, 601)) STAT_KWN FROM BIODATA_WNI X LEFT JOIN DATA_KELUARGA Y ON X.NO_KK = Y.NO_KK INNER JOIN (SELECT ID_CETAK,NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY FROM (SELECT  ID_CETAK, NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY, RANK() OVER (PARTITION BY NIK ORDER BY REQ_DATE DESC, ID_CETAK DESC) RNK FROM SIAK_REQ_CETAK_KTP@YZDB  WHERE STEP_PROC = 'A') WHERE RNK = 1) A ON A.NIK = X.NIK LEFT JOIN DEMOGRAPHICS_ALL@DB2 B ON A.NIK = B.NIK LEFT JOIN (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME FROM (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME, RANK() OVER (PARTITION BY NIK ORDER BY  PERSONALIZED_DATE DESC, LAST_UPDATE DESC, CHIP_ID DESC) RNK FROM CARD_MANAGEMENT@DB2) WHERE RNK = 1) C ON A.NIK = C.NIK  WHERE 1=1  AND (A.REQ_DATE < C.PERSONALIZED_DATE) ";
			if($no_kec != 0){
				$sql .= " AND X.NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND X.NO_KEL = $no_kel";
			} 
			if($nik != ""){
				$sql .= " AND X.NIK IN ($nik)";
			}
			$sql .= " AND A.REQ_DATE >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.REQ_DATE < TO_DATE('$tgl_end','DD-MM-YYYY') +1";
	
			$r = DB::connection('db222')->select($sql);
			return $r;
		}

		public function scr_cek_req_data_uktp_f($nik,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			$sql .= "SELECT X.NO_KEC,X.NIK,X.NAMA_LGKP AS NAMA_LENGKAP, Y.ALAMAT, CASE WHEN TO_CHAR(Y.KODE_POS) IS NULL THEN '-' ELSE TO_CHAR(Y.KODE_POS) END KODE_POS, LPAD(TO_CHAR(Y.NO_RT), 3, '0') AS RT, LPAD(TO_CHAR(Y.NO_RW), 3, '0') AS RW, CASE WHEN B.CURRENT_STATUS_CODE IS NULL THEN '-' ELSE B.CURRENT_STATUS_CODE END AS CURRENT_STATUS_CODE,TO_CHAR(A.REQ_DATE,'YYYY-MM-DD') AS REQ_DATE,A.REQ_BY, TO_CHAR(C.PERSONALIZED_DATE,'YYYY-MM-DD') AS PRINTED_DATE,CASE WHEN TO_CHAR(C.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(C.CREATED_USERNAME) IS NULL  THEN '-' WHEN TO_CHAR(C.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(C.CREATED_USERNAME) IS NOT NULL THEN TO_CHAR(C.CREATED_USERNAME) ELSE TO_CHAR(C.LAST_UPDATED_USERNAME) END AS PRINTED_BY, F5_GET_NAMA_KECAMATAN(X.NO_PROP,X.NO_KAB,X.NO_KEC) NAMA_KEC, F5_GET_NAMA_KELURAHAN(X.NO_PROP,X.NO_KAB,X.NO_KEC,X.NO_KEL) NAMA_KEL, UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(X.AGAMA,7), 501)) AGAMA, UPPER(F5_GET_REF_WNI(X.JENIS_PKRJN, 201)) JENIS_PKRJN, UPPER(F5_GET_REF_WNI(X.STAT_KWN, 601)) STAT_KWN FROM BIODATA_WNI X LEFT JOIN DATA_KELUARGA Y ON X.NO_KK = Y.NO_KK INNER JOIN (SELECT ID_CETAK,NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY FROM (SELECT  ID_CETAK, NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY, RANK() OVER (PARTITION BY NIK ORDER BY REQ_DATE DESC, ID_CETAK DESC) RNK FROM SIAK_REQ_CETAK_KTP@YZDB  WHERE STEP_PROC = 'A') WHERE RNK = 1) A ON A.NIK = X.NIK LEFT JOIN DEMOGRAPHICS_ALL@DB2 B ON A.NIK = B.NIK LEFT JOIN (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME FROM (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME, RANK() OVER (PARTITION BY NIK ORDER BY  PERSONALIZED_DATE DESC, LAST_UPDATE DESC, CHIP_ID DESC) RNK FROM CARD_MANAGEMENT@DB2) WHERE RNK = 1) C ON A.NIK = C.NIK  WHERE 1=1 AND (B.CURRENT_STATUS_CODE LIKE '%CARD%' OR B.CURRENT_STATUS_CODE LIKE '%PRINT%') AND (TO_DATE(A.REQ_DATE,'DD-MM-YYYY') > TO_DATE(C.PERSONALIZED_DATE,'DD-MM-YYYY') OR C.PERSONALIZED_DATE IS NULL)";
			if($no_kec != 0){
				$sql .= " AND X.NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND X.NO_KEL = $no_kel";
			} 
			if($nik != ""){
				$sql .= " AND X.NIK IN ($nik)";
			}
			$sql .= " AND A.REQ_DATE >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.REQ_DATE < TO_DATE('$tgl_end','DD-MM-YYYY') +1";
		
			$r = DB::connection('db222')->select($sql);
			return $r;
		}

		public function scr_cek_req_data_dktp_f($nik,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			$sql .= "SELECT X.NO_KEC,X.NIK,X.NAMA_LGKP AS NAMA_LENGKAP, Y.ALAMAT, CASE WHEN TO_CHAR(Y.KODE_POS) IS NULL THEN '-' ELSE TO_CHAR(Y.KODE_POS) END KODE_POS, LPAD(TO_CHAR(Y.NO_RT), 3, '0') AS RT, LPAD(TO_CHAR(Y.NO_RW), 3, '0') AS RW, B.CURRENT_STATUS_CODE,TO_CHAR(A.REQ_DATE,'DD-MM-YYYY') AS REQ_DATE,A.REQ_BY, TO_CHAR(C.PERSONALIZED_DATE,'DD-MM-YYYY') AS PRINTED_DATE,TO_CHAR(C.PERSONALIZED_DATE,'DD-MM-YYYY') AS PRINTED_DATE,CASE WHEN TO_CHAR(C.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(C.CREATED_USERNAME) IS NULL  THEN '-' WHEN TO_CHAR(C.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(C.CREATED_USERNAME) IS NOT NULL THEN TO_CHAR(C.CREATED_USERNAME) ELSE TO_CHAR(C.LAST_UPDATED_USERNAME) END AS PRINTED_BY, F5_GET_NAMA_KECAMATAN(X.NO_PROP,X.NO_KAB,X.NO_KEC) NAMA_KEC, F5_GET_NAMA_KELURAHAN(X.NO_PROP,X.NO_KAB,X.NO_KEC,X.NO_KEL) NAMA_KEL, UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(X.AGAMA,7), 501)) AGAMA, UPPER(F5_GET_REF_WNI(X.JENIS_PKRJN, 201)) JENIS_PKRJN, UPPER(F5_GET_REF_WNI(X.STAT_KWN, 601)) STAT_KWN FROM BIODATA_WNI X LEFT JOIN DATA_KELUARGA Y ON X.NO_KK = Y.NO_KK INNER JOIN (SELECT ID_CETAK,NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY FROM (SELECT  ID_CETAK, NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY, RANK() OVER (PARTITION BY NIK ORDER BY REQ_DATE DESC, ID_CETAK DESC) RNK FROM SIAK_REQ_CETAK_KTP@YZDB  WHERE STEP_PROC = 'A') WHERE RNK = 1) A ON A.NIK = X.NIK LEFT JOIN DEMOGRAPHICS_ALL@DB2 B ON A.NIK = B.NIK LEFT JOIN (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME FROM (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME, RANK() OVER (PARTITION BY NIK ORDER BY  PERSONALIZED_DATE DESC, LAST_UPDATE DESC, CHIP_ID DESC) RNK FROM CARD_MANAGEMENT@DB2) WHERE RNK = 1) C ON A.NIK = C.NIK  WHERE 1=1  AND (B.CURRENT_STATUS_CODE LIKE '%CARD%' OR B.CURRENT_STATUS_CODE LIKE '%PRINT%') AND (A.REQ_DATE < C.PERSONALIZED_DATE)";
			if($no_kec != 0){
				$sql .= " AND X.NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND X.NO_KEL = $no_kel";
			} 
			if($nik != ""){
				$sql .= " AND X.NIK IN ($nik)";
			}
			$sql .= " AND A.REQ_DATE >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.REQ_DATE < TO_DATE('$tgl_end','DD-MM-YYYY') +1";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}

		public function scr_cek_req_data_usuket($nik,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			$sql .= "SELECT X.NO_KEC,X.NIK,X.NAMA_LGKP AS NAMA_LENGKAP, Y.ALAMAT,UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(X.AGAMA,7), 501)) AGAMA, CASE WHEN TO_CHAR(Y.KODE_POS) IS NULL THEN '-' ELSE TO_CHAR(Y.KODE_POS) END KODE_POS, LPAD(TO_CHAR(Y.NO_RT), 3, '0') AS RT, LPAD(TO_CHAR(Y.NO_RW), 3, '0') AS RW, B.CURRENT_STATUS_CODE,TO_CHAR(A.REQ_DATE,'DD-MM-YYYY') AS REQ_DATE,A.REQ_BY, TO_CHAR(C.PRINTED_DATE,'DD-MM-YYYY') AS PRINTED_DATE,C.PRINTED_BY, F5_GET_NAMA_KECAMATAN(32,73,A.NO_KEC) NAMA_KEC, F5_GET_NAMA_KELURAHAN(32,73,A.NO_KEC,A.NO_KEL) NAMA_KEL, UPPER(F5_GET_REF_WNI(X.JENIS_PKRJN, 201)) JENIS_PKRJN, UPPER(F5_GET_REF_WNI(X.STAT_KWN, 601)) STAT_KWN FROM BIODATA_WNI X LEFT JOIN DATA_KELUARGA Y ON X.NO_KK = Y.NO_KK INNER JOIN (SELECT ID_CETAK,NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY FROM (SELECT  ID_CETAK, NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY, RANK() OVER (PARTITION BY NIK ORDER BY REQ_DATE DESC, ID_CETAK DESC) RNK FROM SIAK_REQ_CETAK_KTP@YZDB WHERE STEP_PROC = 'A') WHERE RNK = 1) A ON A.NIK = X.NIK INNER JOIN DEMOGRAPHICS_ALL@DB2 B ON A.NIK = B.NIK LEFT JOIN (SELECT NIK, PRINTED_DATE,PRINTED_BY FROM (SELECT NIK,PRINTED_DATE,PRINTED_BY, RANK() OVER (PARTITION BY NIK ORDER BY PRINTED_DATE DESC) RNK FROM T7_HIST_SUKET) WHERE RNK = 1) C ON A.NIK = C.NIK  WHERE 1=1 AND (B.CURRENT_STATUS_CODE NOT LIKE '%CARD%' AND B.CURRENT_STATUS_CODE NOT LIKE '%PRINT%') AND (A.REQ_DATE > C.PRINTED_DATE OR C.PRINTED_DATE IS NULL)";
			if($no_kec != 0){
				$sql .= " AND X.NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND X.NO_KEL = $no_kel";
			} 
			if($nik != ""){
				$sql .= " AND X.NIK IN ($nik)";
			}
			$sql .= " AND A.REQ_DATE >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.REQ_DATE < TO_DATE('$tgl_end','DD-MM-YYYY') +1";

			$r = DB::connection('db222')->select($sql);
			return $r;
		}

		public function scr_cek_req_data_dsuket($nik,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			$sql .= "SELECT X.NO_KEC,X.NIK,X.NAMA_LGKP AS NAMA_LENGKAP, Y.ALAMAT,UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(X.AGAMA,7), 501)) AGAMA, CASE WHEN TO_CHAR(Y.KODE_POS) IS NULL THEN '-' ELSE TO_CHAR(Y.KODE_POS) END KODE_POS, LPAD(TO_CHAR(Y.NO_RT), 3, '0') AS RT, LPAD(TO_CHAR(Y.NO_RW), 3, '0') AS RW, B.CURRENT_STATUS_CODE,TO_CHAR(A.REQ_DATE,'DD-MM-YYYY') AS REQ_DATE,A.REQ_BY, TO_CHAR(C.PRINTED_DATE,'DD-MM-YYYY') AS PRINTED_DATE,C.PRINTED_BY, F5_GET_NAMA_KECAMATAN(32,73,A.NO_KEC) NAMA_KEC, F5_GET_NAMA_KELURAHAN(32,73,A.NO_KEC,A.NO_KEL) NAMA_KEL, UPPER(F5_GET_REF_WNI(X.JENIS_PKRJN, 201)) JENIS_PKRJN, UPPER(F5_GET_REF_WNI(X.STAT_KWN, 601)) STAT_KWN FROM BIODATA_WNI X LEFT JOIN DATA_KELUARGA Y ON X.NO_KK = Y.NO_KK INNER JOIN (SELECT ID_CETAK,NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY FROM (SELECT  ID_CETAK, NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY, RANK() OVER (PARTITION BY NIK ORDER BY REQ_DATE DESC, ID_CETAK DESC) RNK FROM SIAK_REQ_CETAK_KTP@YZDB  WHERE STEP_PROC = 'A') WHERE RNK = 1) A ON A.NIK = X.NIK INNER JOIN DEMOGRAPHICS_ALL@DB2 B ON A.NIK = B.NIK LEFT JOIN (SELECT NIK, PRINTED_DATE,PRINTED_BY FROM (SELECT NIK,PRINTED_DATE,PRINTED_BY, RANK() OVER (PARTITION BY NIK ORDER BY PRINTED_DATE DESC) RNK FROM T7_HIST_SUKET) WHERE RNK = 1) C ON A.NIK = C.NIK  WHERE 1=1 AND (B.CURRENT_STATUS_CODE NOT LIKE '%CARD%' AND B.CURRENT_STATUS_CODE NOT LIKE '%PRINT%') AND (A.REQ_DATE < C.PRINTED_DATE)";
			if($no_kec != 0){
				$sql .= " AND X.NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND X.NO_KEL = $no_kel";
			} 
			if($nik != ""){
				$sql .= " AND X.NIK IN ($nik)";
			}
			$sql .= " AND A.REQ_DATE >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.REQ_DATE < TO_DATE('$tgl_end','DD-MM-YYYY') +1";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}
		public function scr_cek_req_data_uttelhr($is_user,$is_lahir,$tgl_start,$tgl_end){
			$sql = "";
			$sql .= "SELECT 
					  D.ADM_AKTA_NO
					  , D.BAYI_NAMA_LGKP
            		  , A.NO_KEC
					  , F5_GET_NAMA_KECAMATAN(A.NO_PROV,A.NO_KAB,A.NO_KEC) NAMA_KEC
					  , A.NO_KEL
					  , F5_GET_NAMA_KELURAHAN(A.NO_PROV,A.NO_KAB,A.NO_KEC,A.NO_KEL) NAMA_KEL
					  , CASE WHEN TRUNC(A.REQ_DATE) > TRUNC(B.PRINTED_DATE) OR B.PRINTED_DATE  IS NULL THEN 'BELUM CETAK' ELSE 'SUDAH CETAK' END STATUS_CETAK
					  , A.REQ_BY
					  , TO_CHAR(A.REQ_DATE,'DD-MM-YYYY') REQ_DATE
					  , CASE WHEN B.PRINTED_DATE IS NULL THEN '-' ELSE TO_CHAR(B.PRINTED_DATE,'DD-MM-YYYY') END TANGGAL_CETAK
					  , CASE WHEN A.CERT_STATUS = 1 THEN 'TELAH DIAJUKAN' WHEN A.CERT_STATUS = 2 THEN 'PROSES PENERBITAN TTE' WHEN A.CERT_STATUS = 3 THEN 'SUDAH TERSERTIFIKASI'  WHEN A.CERT_STATUS = 9 THEN 'BELUM DI VERIVIKASI' ELSE 'DOKUMEN TELAH DIPUBLISH' END STATUS_TTE
					  FROM (SELECT NO_PROV, NO_KAB, NO_KEC, NO_KEL,NO_DOKUMEN,PEJABAT_PROCESS_DATE, CERT_STATUS, REQ_DATE,REQ_BY FROM (SELECT  NO_PROV, NO_KAB, NO_KEC, NO_KEL,NO_DOKUMEN, CERT_STATUS,PEJABAT_PROCESS_DATE, REQ_DATE,REQ_BY, RANK() OVER (PARTITION BY NO_DOKUMEN ORDER BY REQ_DATE DESC,SEQN_ID DESC) RNK FROM BSRE_KELAHIRAN ) WHERE RNK = 1) A INNER JOIN CAPIL_LAHIR D ON A.NO_DOKUMEN = D.ADM_AKTA_NO LEFT JOIN (SELECT  NO_DOKUMEN, PRINTED_DATE FROM (SELECT  NO_DOKUMEN, PRINTED_DATE, RANK() OVER (PARTITION BY NO_DOKUMEN ORDER BY PRINTED_DATE DESC, SEQN_ID DESC) RNK FROM T5_SEQN_PRINT_KELAHIRAN ) WHERE RNK = 1) B ON A.NO_DOKUMEN = B.NO_DOKUMEN WHERE A.PEJABAT_PROCESS_DATE IS NULL";
			if($is_user != '0'){
				$sql .= " AND LOWER(A.REQ_BY) = '$is_user'";
			}
			if($is_lahir != '0'){
				$sql .= " AND A.NO_DOKUMEN LIKE '%$is_lahir%'";
			}
			$sql .= " AND A.REQ_DATE >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.REQ_DATE < TO_DATE('$tgl_end','DD-MM-YYYY') +1
				ORDER BY TRUNC(A.REQ_DATE) DESC, A.NO_DOKUMEN 
			";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}

		public function scr_cek_req_data_dttelhr($is_user,$is_lahir,$tgl_start,$tgl_end){
			$sql = "";
			$sql .= "SELECT 
					  D.ADM_AKTA_NO
					  , D.BAYI_NAMA_LGKP
            		  , A.NO_KEC
					  , F5_GET_NAMA_KECAMATAN(A.NO_PROV,A.NO_KAB,A.NO_KEC) NAMA_KEC
					  , A.NO_KEL
					  , F5_GET_NAMA_KELURAHAN(A.NO_PROV,A.NO_KAB,A.NO_KEC,A.NO_KEL) NAMA_KEL
					  , CASE WHEN TRUNC(A.REQ_DATE) > TRUNC(B.PRINTED_DATE) OR B.PRINTED_DATE  IS NULL THEN 'BELUM CETAK' ELSE 'SUDAH CETAK' END STATUS_CETAK
					  , A.REQ_BY
					  , TO_CHAR(A.REQ_DATE,'DD-MM-YYYY') REQ_DATE
					  , CASE WHEN B.PRINTED_DATE IS NULL THEN '-' ELSE TO_CHAR(B.PRINTED_DATE,'DD-MM-YYYY') END TANGGAL_CETAK
					  , CASE WHEN A.CERT_STATUS = 1 THEN 'TELAH DIAJUKAN' WHEN A.CERT_STATUS = 2 THEN 'PROSES PENERBITAN TTE' WHEN A.CERT_STATUS = 3 THEN 'SUDAH TERSERTIFIKASI'  WHEN A.CERT_STATUS = 9 THEN 'BELUM DI VERIVIKASI' ELSE 'DOKUMEN TELAH DIPUBLISH' END STATUS_TTE
					  FROM (SELECT NO_PROV, NO_KAB, NO_KEC, NO_KEL,NO_DOKUMEN,PEJABAT_PROCESS_DATE, CERT_STATUS, REQ_DATE,REQ_BY FROM (SELECT  NO_PROV, NO_KAB, NO_KEC, NO_KEL,NO_DOKUMEN, CERT_STATUS,PEJABAT_PROCESS_DATE, REQ_DATE,REQ_BY, RANK() OVER (PARTITION BY NO_DOKUMEN ORDER BY REQ_DATE DESC,SEQN_ID DESC) RNK FROM BSRE_KELAHIRAN ) WHERE RNK = 1) A INNER JOIN CAPIL_LAHIR D ON A.NO_DOKUMEN = D.ADM_AKTA_NO LEFT JOIN (SELECT  NO_DOKUMEN, PRINTED_DATE FROM (SELECT  NO_DOKUMEN, PRINTED_DATE, RANK() OVER (PARTITION BY NO_DOKUMEN ORDER BY PRINTED_DATE DESC, SEQN_ID DESC) RNK FROM T5_SEQN_PRINT_KELAHIRAN ) WHERE RNK = 1) B ON A.NO_DOKUMEN = B.NO_DOKUMEN WHERE A.PEJABAT_PROCESS_DATE IS NOT NULL";
			if($is_user != '0'){
				$sql .= " AND LOWER(A.REQ_BY) = '$is_user'";
			}
			if($is_lahir != '0'){
				$sql .= " AND A.NO_DOKUMEN LIKE '%$is_lahir%'";
			}
			$sql .= " AND A.REQ_DATE >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.REQ_DATE < TO_DATE('$tgl_end','DD-MM-YYYY') +1
				ORDER BY TRUNC(A.REQ_DATE) DESC, A.NO_DOKUMEN 
			";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}
		public function scr_cek_req_data_uttekk($tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			$sql .= "SELECT 
					  D.NO_KK
					  , D.NAMA_KEP
					  , D.ALAMAT
					  , D.NO_KEC
					  , F5_GET_NAMA_KECAMATAN(D.NO_PROP,D.NO_KAB,D.NO_KEC) NAMA_KEC
					  , D.NO_KEL
					  , F5_GET_NAMA_KELURAHAN(D.NO_PROP,D.NO_KAB,D.NO_KEC,D.NO_KEL) NAMA_KEL
					  , CASE WHEN TRUNC(A.REQ_DATE) > TRUNC(B.PRINTED_DATE) OR B.PRINTED_DATE  IS NULL THEN 'BELUM CETAK' ELSE 'SUDAH CETAK' END STATUS_CETAK
					  , A.REQ_BY
					  , TO_CHAR(A.REQ_DATE,'DD-MM-YYYY') REQ_DATE
					  , CASE WHEN B.PRINTED_DATE IS NULL THEN '-' ELSE TO_CHAR(B.PRINTED_DATE,'DD-MM-YYYY') END TANGGAL_CETAK
					  , CASE WHEN A.CERT_STATUS = 1 THEN 'TELAH DIAJUKAN' WHEN A.CERT_STATUS = 2 THEN 'PROSES PENERBITAN TTE' WHEN A.CERT_STATUS = 3 THEN 'SUDAH TERSERTIFIKASI'  WHEN A.CERT_STATUS = 9 THEN 'BELUM DI VERIVIKASI' ELSE 'DOKUMEN TELAH DIPUBLISH' END STATUS_TTE
					  FROM (SELECT  NO_DOC,PEJABAT_PROCESS_DATE, CERT_STATUS, REQ_DATE,REQ_BY FROM (SELECT  NO_DOC, CERT_STATUS,PEJABAT_PROCESS_DATE, REQ_DATE,REQ_BY, RANK() OVER (PARTITION BY NO_DOC ORDER BY REQ_DATE DESC,SEQN_ID DESC) RNK FROM BSRE_KARTU_KELUARGA ) WHERE RNK = 1) A INNER JOIN DATA_KELUARGA D ON A.NO_DOC = D.NO_KK LEFT JOIN (SELECT  NO_KK, PRINTED_DATE FROM (SELECT  NO_KK, PRINTED_DATE, RANK() OVER (PARTITION BY NO_KK ORDER BY PRINTED_DATE DESC, SEQN_ID DESC) RNK FROM T5_SEQN_PRINT_KK ) WHERE RNK = 1) B ON A.NO_DOC = B.NO_KK WHERE A.PEJABAT_PROCESS_DATE IS NULL ";
			if($no_kec != 0){
				$sql .= " AND D.NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND D.NO_KEL = $no_kel";
			} 
			$sql .= " AND A.REQ_DATE >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.REQ_DATE < TO_DATE('$tgl_end','DD-MM-YYYY') +1
				ORDER BY D.NO_KEC, D.NO_KEL, A.REQ_DATE
			";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}

		public function scr_cek_req_data_dttekk($tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			$sql .= "SELECT 
					  D.NO_KK
					  , D.NAMA_KEP
					  , D.ALAMAT
					  , D.NO_KEC
					  , F5_GET_NAMA_KECAMATAN(D.NO_PROP,D.NO_KAB,D.NO_KEC) NAMA_KEC
					  , D.NO_KEL
					  , F5_GET_NAMA_KELURAHAN(D.NO_PROP,D.NO_KAB,D.NO_KEC,D.NO_KEL) NAMA_KEL
					  , CASE WHEN TRUNC(A.REQ_DATE) > TRUNC(B.PRINTED_DATE) OR B.PRINTED_DATE  IS NULL THEN 'BELUM CETAK' ELSE 'SUDAH CETAK' END STATUS_CETAK
					  , A.REQ_BY
					  , TO_CHAR(A.REQ_DATE,'DD-MM-YYYY') REQ_DATE
					  , CASE WHEN B.PRINTED_DATE IS NULL THEN '-' ELSE TO_CHAR(B.PRINTED_DATE,'DD-MM-YYYY') END TANGGAL_CETAK
					  , CASE WHEN A.CERT_STATUS = 1 THEN 'TELAH DIAJUKAN' WHEN A.CERT_STATUS = 2 THEN 'PROSES PENERBITAN TTE' WHEN A.CERT_STATUS = 3 THEN 'SUDAH TERSERTIFIKASI'  WHEN A.CERT_STATUS = 9 THEN 'BELUM DI VERIVIKASI' ELSE 'DOKUMEN TELAH DIPUBLISH' END STATUS_TTE
					  FROM (SELECT  NO_DOC,PEJABAT_PROCESS_DATE, CERT_STATUS, REQ_DATE,REQ_BY FROM (SELECT  NO_DOC, CERT_STATUS,PEJABAT_PROCESS_DATE, REQ_DATE,REQ_BY, RANK() OVER (PARTITION BY NO_DOC ORDER BY REQ_DATE DESC,SEQN_ID DESC) RNK FROM BSRE_KARTU_KELUARGA ) WHERE RNK = 1) A INNER JOIN DATA_KELUARGA D ON A.NO_DOC = D.NO_KK LEFT JOIN (SELECT  NO_KK, PRINTED_DATE FROM (SELECT  NO_KK, PRINTED_DATE, RANK() OVER (PARTITION BY NO_KK ORDER BY PRINTED_DATE DESC, SEQN_ID DESC) RNK FROM T5_SEQN_PRINT_KK ) WHERE RNK = 1) B ON A.NO_DOC = B.NO_KK WHERE A.PEJABAT_PROCESS_DATE IS NOT NULL ";
			if($no_kec != 0){
				$sql .= " AND D.NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND D.NO_KEL = $no_kel";
			} 
			$sql .= " AND A.REQ_DATE >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.REQ_DATE < TO_DATE('$tgl_end','DD-MM-YYYY') +1
				ORDER BY D.NO_KEC, D.NO_KEL, A.REQ_DATE
			";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}

		
		public function scr_pengajuan_kk_wait($tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			$sql .= "SELECT D.NO_KK
					  , D.NAMA_KEP
					  , D.ALAMAT
					  , D.NO_KEC
					  , F5_GET_NAMA_KECAMATAN(D.NO_PROP,D.NO_KAB,D.NO_KEC) NAMA_KEC
					  , D.NO_KEL
					  , F5_GET_NAMA_KELURAHAN(D.NO_PROP,D.NO_KAB,D.NO_KEC,D.NO_KEL) NAMA_KEL
					  , CASE WHEN TRUNC(X.REQ_DATE) > TRUNC(B.PRINTED_DATE) OR B.PRINTED_DATE  IS NULL THEN 'BELUM CETAK' ELSE 'SUDAH CETAK' END STATUS_CETAK
					  , X.REQ_BY
					  , TO_CHAR(X.REQ_DATE,'DD-MM-YYYY') REQ_DATE
           			  , CASE WHEN A.REQ_BY_TTE IS NULL THEN '-' ELSE A.REQ_BY_TTE END REQ_BY_TTE
					  , CASE WHEN TO_CHAR(A.REQ_DATE_TTE,'DD-MM-YYYY') IS NULL THEN '-' ELSE TO_CHAR(A.REQ_DATE_TTE,'DD-MM-YYYY') END REQ_DATE_TTE
					  , CASE WHEN B.PRINTED_DATE IS NULL THEN '-' ELSE TO_CHAR(B.PRINTED_DATE,'DD-MM-YYYY') END TANGGAL_CETAK
					  , CASE  WHEN A.CERT_STATUS IS NULL THEN 'BELUM DIAJUKAN' WHEN A.CERT_STATUS = 1 THEN 'TELAH DIAJUKAN' WHEN A.CERT_STATUS = 2 THEN 'PROSES PENERBITAN TTE' WHEN A.CERT_STATUS = 3 THEN 'SUDAH TERSERTIFIKASI'  WHEN A.CERT_STATUS = 9 THEN 'BELUM DI VERIVIKASI' ELSE 'DOKUMEN TELAH DIPUBLISH' END STATUS_TTE 
            FROM (SELECT  NO_KK,NO_KEC, NO_KEL, REQ_DATE,REQ_BY FROM (SELECT  NO_KK, NO_KEC,NO_KEL, REQ_DATE,REQ_BY, RANK() OVER (PARTITION BY NO_KK ORDER BY REQ_DATE DESC,ID_CETAK DESC) RNK FROM T5_REQ_CETAK_KK ) WHERE RNK = 1) X 
            INNER JOIN DATA_KELUARGA D ON X.NO_KK = D.NO_KK 
            LEFT JOIN (SELECT  NO_KK, PRINTED_DATE FROM (SELECT  NO_KK, PRINTED_DATE, RANK() OVER (PARTITION BY NO_KK ORDER BY PRINTED_DATE DESC, SEQN_ID DESC) RNK FROM T5_SEQN_PRINT_KK ) WHERE RNK = 1) B ON B.NO_KK = D.NO_KK
            LEFT JOIN (SELECT  NO_DOC,PEJABAT_PROCESS_DATE, CERT_STATUS, REQ_DATE REQ_DATE_TTE,REQ_BY REQ_BY_TTE FROM (SELECT  NO_DOC, CERT_STATUS,PEJABAT_PROCESS_DATE, REQ_DATE,REQ_BY, RANK() OVER (PARTITION BY NO_DOC ORDER BY REQ_DATE DESC,SEQN_ID DESC) RNK FROM BSRE_KARTU_KELUARGA ) WHERE RNK = 1) A ON X.NO_KK = A.NO_DOC 
            WHERE 
            (A.REQ_DATE_TTE IS NULL OR TRUNC(A.REQ_DATE_TTE) < TRUNC(X.REQ_DATE))
            ";
			if($no_kec != 0){
				$sql .= " AND D.NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND D.NO_KEL = $no_kel";
			} 
			$sql .= " AND X.REQ_DATE >= TO_DATE('$tgl_start','DD-MM-YYYY') AND X.REQ_DATE < TO_DATE('$tgl_end','DD-MM-YYYY') +1
				ORDER BY D.NO_KEC, D.NO_KEL, X.REQ_DATE DESC
			";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}

		public function scr_pengajuan_kk_done($tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			$sql .= "SELECT D.NO_KK
					  , D.NAMA_KEP
					  , D.ALAMAT
					  , D.NO_KEC
					  , F5_GET_NAMA_KECAMATAN(D.NO_PROP,D.NO_KAB,D.NO_KEC) NAMA_KEC
					  , D.NO_KEL
					  , F5_GET_NAMA_KELURAHAN(D.NO_PROP,D.NO_KAB,D.NO_KEC,D.NO_KEL) NAMA_KEL
					  , CASE WHEN TRUNC(X.REQ_DATE) > TRUNC(B.PRINTED_DATE) OR B.PRINTED_DATE  IS NULL THEN 'BELUM CETAK' ELSE 'SUDAH CETAK' END STATUS_CETAK
					  , X.REQ_BY
					  , TO_CHAR(X.REQ_DATE,'DD-MM-YYYY') REQ_DATE
           			  , CASE WHEN A.REQ_BY_TTE IS NULL THEN '-' ELSE A.REQ_BY_TTE END REQ_BY_TTE
					  , CASE WHEN TO_CHAR(A.REQ_DATE_TTE,'DD-MM-YYYY') IS NULL THEN '-' ELSE TO_CHAR(A.REQ_DATE_TTE,'DD-MM-YYYY') END REQ_DATE_TTE
					  , CASE WHEN B.PRINTED_DATE IS NULL THEN '-' ELSE TO_CHAR(B.PRINTED_DATE,'DD-MM-YYYY') END TANGGAL_CETAK
					  , CASE  WHEN A.CERT_STATUS IS NULL THEN 'BELUM DIAJUKAN' WHEN A.CERT_STATUS = 1 THEN 'TELAH DIAJUKAN' WHEN A.CERT_STATUS = 2 THEN 'PROSES PENERBITAN TTE' WHEN A.CERT_STATUS = 3 THEN 'SUDAH TERSERTIFIKASI'  WHEN A.CERT_STATUS = 9 THEN 'BELUM DI VERIVIKASI' ELSE 'DOKUMEN TELAH DIPUBLISH' END STATUS_TTE 
            FROM (SELECT  NO_KK,NO_KEC, NO_KEL, REQ_DATE,REQ_BY FROM (SELECT  NO_KK, NO_KEC,NO_KEL, REQ_DATE,REQ_BY, RANK() OVER (PARTITION BY NO_KK ORDER BY REQ_DATE DESC,ID_CETAK DESC) RNK FROM T5_REQ_CETAK_KK ) WHERE RNK = 1) X 
            INNER JOIN DATA_KELUARGA D ON X.NO_KK = D.NO_KK LEFT JOIN (SELECT  NO_KK, PRINTED_DATE FROM (SELECT  NO_KK, PRINTED_DATE, RANK() OVER (PARTITION BY NO_KK ORDER BY PRINTED_DATE DESC, SEQN_ID DESC) RNK FROM T5_SEQN_PRINT_KK ) WHERE RNK = 1) B ON B.NO_KK = D.NO_KK
            LEFT JOIN (SELECT  NO_DOC,PEJABAT_PROCESS_DATE, CERT_STATUS, REQ_DATE REQ_DATE_TTE,REQ_BY REQ_BY_TTE FROM (SELECT  NO_DOC, CERT_STATUS,PEJABAT_PROCESS_DATE, REQ_DATE,REQ_BY, RANK() OVER (PARTITION BY NO_DOC ORDER BY REQ_DATE DESC,SEQN_ID DESC) RNK FROM BSRE_KARTU_KELUARGA ) WHERE RNK = 1) A ON X.NO_KK = A.NO_DOC 
            WHERE 
            TRUNC(A.REQ_DATE_TTE) >= TRUNC(X.REQ_DATE)
            ";
			if($no_kec != 0){
				$sql .= " AND D.NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND D.NO_KEL = $no_kel";
			} 
			$sql .= " AND X.REQ_DATE >= TO_DATE('$tgl_start','DD-MM-YYYY') AND X.REQ_DATE < TO_DATE('$tgl_end','DD-MM-YYYY') +1
				ORDER BY D.NO_KEC, D.NO_KEL, X.REQ_DATE DESC
			";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}

		
		public function scr_cek_req_data_bio($nik,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			$sql .= "SELECT X.NO_KEC,X.NIK,X.NAMA_LGKP AS NAMA_LENGKAP, Y.ALAMAT, CASE WHEN TO_CHAR(Y.KODE_POS) IS NULL THEN '-' ELSE TO_CHAR(Y.KODE_POS) END KODE_POS, LPAD(TO_CHAR(Y.NO_RT), 3, '0') AS RT, LPAD(TO_CHAR(Y.NO_RW), 3, '0') AS RW,  CASE WHEN B.CURRENT_STATUS_CODE IS NULL THEN 'REQ BIO' ELSE B.CURRENT_STATUS_CODE END AS CURRENT_STATUS_CODE,TO_CHAR(A.REQ_DATE,'DD-MM-YYYY') AS REQ_DATE,A.REQ_BY, TO_CHAR(C.CREATED,'DD-MM-YYYY') AS PRINTED_DATE,C.CREATED_USERNAME AS PRINTED_BY FROM BIODATA_WNI X LEFT JOIN DATA_KELUARGA Y ON X.NO_KK = Y.NO_KK INNER JOIN (SELECT ID_CETAK,NO_KEC, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY FROM (SELECT  ID_CETAK, NO_KEC, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY, RANK() OVER (PARTITION BY NIK ORDER BY REQ_DATE DESC, ID_CETAK DESC) RNK FROM SIAK_REQ_CETAK_KTP@YZDB WHERE STEP_PROC = 'A') WHERE RNK = 1) A ON A.NIK = X.NIK LEFT JOIN DEMOGRAPHICS_ALL@DB2 B ON A.NIK = B.NIK LEFT JOIN DEMOGRAPHICS@DB2 D ON A.NIK = D.NIK LEFT JOIN (SELECT NIK,CREATED,CREATED_USERNAME FROM (SELECT NIK,CREATED,CREATED_USERNAME, RANK() OVER (PARTITION BY NIK ORDER BY CREATED DESC) RNK FROM CARD_MANAGEMENT@DB2) WHERE RNK = 1) C ON A.NIK = C.NIK  WHERE 1=1 AND ((B.CURRENT_STATUS_CODE IS NULL) AND (D.CURRENT_STATUS_CODE IS NULL))";
			if($no_kec != 0){
				$sql .= " AND X.NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND X.NO_KEL = $no_kel";
			} 
			if($nik != ""){
				$sql .= " AND X.NIK IN ($nik)";
			}
			$sql .= " AND A.REQ_DATE >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.REQ_DATE < TO_DATE('$tgl_end','DD-MM-YYYY') +1";			
			$r = DB::connection('db222')->select($sql);
			return $r;
		}
		public function prr_get_list_kkroket($no_kec,$no_kel){
			$sql = "SELECT 
						  A.NO_KK
						  , A.NAMA_KEP
						  , A.ALAMAT
						  , A.NO_PROP
						  , A.NO_KAB
						  , A.NO_KEC
						  , A.NO_KEL
						  , F5_GET_BSRE_KK_JML(A.NO_KK) JML_PENGAJUAN
						  , F5_GET_NAMA_KECAMATAN(A.NO_PROP,A.NO_KAB,A.NO_KEC) NAMA_KEC
            			  , F5_GET_NAMA_KELURAHAN(A.NO_PROP,A.NO_KAB,A.NO_KEC,A.NO_KEL) NAMA_KEL
						  , LPAD(TO_CHAR(A.NO_RT), 3, '0') AS NO_RT
            			  , LPAD(TO_CHAR(A.NO_RW), 3, '0') AS NO_RW
						  , CASE WHEN A.KODE_POS IS NULL THEN '-' ELSE TO_CHAR(A.KODE_POS) END KODE_POS
						  , A.COUNT_KK
						  , CASE WHEN B.CERT_STATUS IS NULL THEN '-'  ELSE TO_CHAR(B.CERT_STATUS) END CERT_STATUS
						  , CASE WHEN B.REQ_DATE IS NULL THEN '-'  ELSE TO_CHAR(B.REQ_DATE,'DD-MM-YYYY') END REQ_DATE
						  , CASE WHEN B.REQ_BY IS NULL THEN '-'  ELSE TO_CHAR(B.REQ_BY) END REQ_BY
						  , CASE WHEN B.URL_DOKUMEN IS NULL THEN '-'  ELSE TO_CHAR(B.URL_DOKUMEN) END URL_DOKUMEN
						  , CASE WHEN B.URL_DOKUMEN IS NULL THEN '-'  ELSE SUBSTR(B.URL_DOKUMEN,20) END URL_KK
						  , CASE WHEN B.URL_DOKUMEN IS NULL THEN '-'  ELSE SUBSTR(B.URL_DOKUMEN,11,8) END URL_DATE
						  , CASE WHEN B.URL_DOKUMEN IS NULL THEN '-'  ELSE CONCAT(CONCAT(CONCAT(CONCAT('https://10.32.73.222/Siak/cetak/main/view_pdf/',SUBSTR(B.URL_DOKUMEN,20)),'/'),SUBSTR(B.URL_DOKUMEN,11,8)),'/cetak_kartu_keluarga.pdf') END URL_DOWNLOAD 
						  , CASE WHEN B.PEJABAT_PROCESS_DATE IS NULL THEN '-'  ELSE TO_CHAR(B.PEJABAT_PROCESS_DATE,'DD-MM-YYYY') END APROVE_DATE
						FROM DATA_KELUARGA A 
						INNER JOIN 
						  (
						    SELECT 
						    NO_DOC
						    , CERT_STATUS
						    , REQ_DATE
						    , REQ_BY
						    , SEQN_ID
						    , URL_DOKUMEN
						    , PEJABAT_PROCCESS_BY
						    , PEJABAT_PROCESS_DATE  
						  FROM 
						  (
						    SELECT 
						    NO_DOC
						    , CERT_STATUS
						    , REQ_DATE
						    , REQ_BY
						    , SEQN_ID
						    , URL_DOKUMEN
						    , PEJABAT_PROCCESS_BY
						    , PEJABAT_PROCESS_DATE 
						    , RANK() OVER (PARTITION BY NO_DOC ORDER BY REQ_DATE DESC,SEQN_ID DESC) RNK FROM BSRE_KARTU_KELUARGA WHERE CERT_STATUS IN (2,3)) 
						    WHERE RNK = 1) B ON A.NO_KK = B.NO_DOC
						    WHERE 1=1 ";
			if($no_kec != 0){
				$sql .= " AND A.NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND A.NO_KEL = $no_kel";
			} 
			$sql .=" ORDER BY A.NO_KEC, A.NO_KEL, A.NO_RW, A.NO_RT";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}
		public function prr_get_list_prr($no_kec,$no_kel){
			$sql = "SELECT 
					TO_CHAR(B.NIK) AS NIK
					, B.NAMA_LGKP
					, B.TMPT_LHR, TO_CHAR(B.TGL_LHR,'DD-MM-YYYY') AS TGL_LHR
					, TRUNC(MONTHS_BETWEEN(SYSDATE,B.TGL_LHR)/12) UMUR
					, CASE WHEN C.ALAMAT IS NULL THEN '-' ELSE C.ALAMAT END AS ALAMAT
					, B.NO_KEC, F5_GET_NAMA_KECAMATAN(B.NO_PROP,B.NO_KAB,B.NO_KEC) NAMA_KEC
					, B.NO_KEL, F5_GET_NAMA_KELURAHAN(B.NO_PROP,B.NO_KAB,B.NO_KEC,B.NO_KEL) NAMA_KEL
					, CASE WHEN C.NO_RW IS NULL THEN 0 ELSE C.NO_RW END AS NO_RW 
					, CASE WHEN C.NO_RT IS NULL THEN 0 ELSE C.NO_RT END AS NO_RT 
					, CASE WHEN TO_CHAR(C.KODE_POS) IS NULL THEN '-' ELSE TO_CHAR(C.KODE_POS) END KODE_POS
					, UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(B.AGAMA,7), 501)) AGAMA
					, UPPER(F5_GET_REF_WNI(B.STAT_KWN, 601)) STATUS_KAWIN
					, UPPER(F5_GET_REF_WNI(B.JENIS_PKRJN, 201)) PEKERJAAN
					, UPPER(F5_GET_REF_WNI(B.GOL_DRH, 401)) GOL_DRH
					, TO_CHAR(A.CREATED,'YYYY-MM-DD') AS TGL_REKAM
					, A.CURRENT_STATUS_CODE AS STATUS_KTP
					, A.CREATED_USERNAME AS PETUGAS_REKAM
					FROM DEMOGRAPHICS@DB221 A 
					INNER JOIN BIODATA_WNI B ON A.NIK = B.NIK
					LEFT JOIN DATA_KELUARGA C ON B.NO_KK = C.NO_KK
					LEFT JOIN CARD_MANAGEMENT@DB2 D ON A.NIK = D.NIK
					WHERE A.CURRENT_STATUS_CODE = 'PRINT_READY_RECORD' AND D.NIK IS NULL
					AND (B.TGL_LHR<=ADD_MONTHS(SYSDATE,-12*17) OR B.STAT_KWN<>1) 
					AND (B.FLAG_STATUS = '0' OR (B.FLAG_STATUS = '2' AND B.FLAG_PINDAH IN(1,2,3))) 
					AND B.NO_PROP = 32 AND B.NO_KAB =73";
			if($no_kec != 0){
				$sql .= " AND B.NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND B.NO_KEL = $no_kel";
			} 
			$sql .=" ORDER BY A.CREATED, B.NO_KEL, C.NO_RW, C.NO_RT";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}
		public function prr_get_list_perekaman($tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "SELECT 
					TO_CHAR(B.NIK) AS NIK
					, B.NAMA_LGKP
					, B.TMPT_LHR, TO_CHAR(B.TGL_LHR,'DD-MM-YYYY') AS TGL_LHR
					, CASE WHEN C.ALAMAT IS NULL THEN '-' ELSE C.ALAMAT END AS ALAMAT
					, B.NO_KEC, F5_GET_NAMA_KECAMATAN(B.NO_PROP,B.NO_KAB,B.NO_KEC) NAMA_KEC
					, B.NO_KEL, F5_GET_NAMA_KELURAHAN(B.NO_PROP,B.NO_KAB,B.NO_KEC,B.NO_KEL) NAMA_KEL
					, CASE WHEN C.NO_RW IS NULL THEN 0 ELSE C.NO_RW END AS NO_RW 
					, CASE WHEN C.NO_RT IS NULL THEN 0 ELSE C.NO_RT END AS NO_RT 
					, CASE WHEN TO_CHAR(C.KODE_POS) IS NULL THEN '-' ELSE TO_CHAR(C.KODE_POS) END KODE_POS
					, UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(B.AGAMA,7), 501)) AGAMA
					, UPPER(F5_GET_REF_WNI(B.STAT_KWN, 601)) STATUS_KAWIN
					, UPPER(F5_GET_REF_WNI(B.JENIS_PKRJN, 201)) PEKERJAAN
					, TO_CHAR(A.CREATED,'YYYY-MM-DD') AS TGL_REKAM
         			, TO_CHAR(A.CREATED,'HH24:MM') AS JAM_REKAM
					, A.CURRENT_STATUS_CODE AS STATUS_KTP
					, A.CREATED_USERNAME AS PETUGAS_REKAM
					FROM DEMOGRAPHICS@DB221 A 
					INNER JOIN BIODATA_WNI B ON A.NIK = B.NIK
					INNER JOIN FACES@DB221 X ON A.NIK = X.NIK
					LEFT JOIN DATA_KELUARGA C ON B.NO_KK = C.NO_KK
					WHERE 1=1
					AND B.NO_PROP = 32 AND B.NO_KAB =73
          			AND A.CREATED >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.CREATED < TO_DATE('$tgl_end','DD-MM-YYYY') +1 
                    ";
			if($no_kec != 0){
				$sql .= " AND B.NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND B.NO_KEL = $no_kel";
			} 
			$sql .=" ORDER BY A.CREATED";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}
		public function prr_get_list_cetak_kk($tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "SELECT
					 NO_KK
					 , NIK_KK
					 , NAMA_KEP
					 , ALAMAT
					 , PRINTED_BY
					 , TO_CHAR(PRINTED_DATE,'DD-MM-YYYY') PRINTED_DATE
					 , NO_PROP, NO_KAB, NO_KEC, NO_KEL
					 , F5_GET_NAMA_KECAMATAN(NO_PROP, NO_KAB, NO_KEC) NAMA_KEC
					 , F5_GET_NAMA_KELURAHAN(NO_PROP, NO_KAB, NO_KEC, NO_KEL) NAMA_KEL
					FROM T5_SEQN_PRINT_KK WHERE 1=1
					AND NO_PROP = 32 AND NO_KAB =73
          			AND PRINTED_DATE >= TO_DATE('$tgl_start','DD-MM-YYYY') AND PRINTED_DATE < TO_DATE('$tgl_end','DD-MM-YYYY') +1 
                    ";
			if($no_kec != 0){
				$sql .= " AND NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND NO_KEL = $no_kel";
			} 
			$sql .=" ORDER BY PRINTED_DATE,NO_KEC, NO_KEL, NAMA_KEP";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}
		public function prr_get_list_nik_baru($tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "SELECT 
						NO_KK
						, NIK
						, NAMA_LGKP
						, TMPT_LHR
						, TO_CHAR(TGL_LHR,'DD-MM-YYYY') TGL_LHR
						, TRUNC(MONTHS_BETWEEN(TGL_ENTRI,TGL_LHR)/12) UMUR
						, NAMA_PET_ENTRI
						, TO_CHAR(TGL_ENTRI,'DD-MM-YYYY') TGL_ENTRI
						, NO_PROP
						, NO_KAB
						, NO_KEC
						, NO_KEL
						, F5_GET_NAMA_KECAMATAN(NO_PROP, NO_KAB, NO_KEC) NAMA_KEC
						, F5_GET_NAMA_KELURAHAN(NO_PROP, NO_KAB, NO_KEC, NO_KEL) NAMA_KEL 
						FROM BIODATA_WNI WHERE 1=1
					AND NO_PROP = 32 AND NO_KAB =73
          			AND TGL_ENTRI >= TO_DATE('$tgl_start','DD-MM-YYYY') AND TGL_ENTRI < TO_DATE('$tgl_end','DD-MM-YYYY') +1 
                    ";
			if($no_kec != 0){
				$sql .= " AND NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND NO_KEL = $no_kel";
			} 
			$sql .=" ORDER BY TGL_ENTRI";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}
		public function prr_get_group_nik_baru($tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "SELECT 
						TO_NUMBER(TRUNC(MONTHS_BETWEEN(TGL_ENTRI,TGL_LHR)/12)) UMUR
						, COUNT(1) JML
						FROM BIODATA_WNI WHERE 1=1
					AND NO_PROP = 32 AND NO_KAB =73
          			AND TGL_ENTRI >= TO_DATE('$tgl_start','DD-MM-YYYY') AND TGL_ENTRI < TO_DATE('$tgl_end','DD-MM-YYYY') +1 
                    ";
			if($no_kec != 0){
				$sql .= " AND NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND NO_KEL = $no_kel";
			} 
			$sql .=" GROUP BY TO_NUMBER(TRUNC(MONTHS_BETWEEN(TGL_ENTRI,TGL_LHR)/12)) ORDER BY TO_NUMBER(TRUNC(MONTHS_BETWEEN(TGL_ENTRI,TGL_LHR)/12))";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}
		public function prr_get_list_sudah_cetak($tgl_start,$tgl_end,$no_kec,$no_kel,$user){
			$sql = "SELECT 
					TO_CHAR(B.NIK) AS NIK
					, B.NAMA_LGKP
					, B.TMPT_LHR, TO_CHAR(B.TGL_LHR,'DD-MM-YYYY') AS TGL_LHR
					, CASE WHEN C.ALAMAT IS NULL THEN '-' ELSE C.ALAMAT END AS ALAMAT
					, B.NO_KEC, F5_GET_NAMA_KECAMATAN(B.NO_PROP,B.NO_KAB,B.NO_KEC) NAMA_KEC
					, B.NO_KEL, F5_GET_NAMA_KELURAHAN(B.NO_PROP,B.NO_KAB,B.NO_KEC,B.NO_KEL) NAMA_KEL
					, CASE WHEN C.NO_RW IS NULL THEN 0 ELSE C.NO_RW END AS NO_RW 
					, CASE WHEN C.NO_RT IS NULL THEN 0 ELSE C.NO_RT END AS NO_RT 
					, CASE WHEN TO_CHAR(C.KODE_POS) IS NULL THEN '-' ELSE TO_CHAR(C.KODE_POS) END KODE_POS
					, UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(B.AGAMA,7), 501)) AGAMA
					, UPPER(F5_GET_REF_WNI(B.STAT_KWN, 601)) STATUS_KAWIN
					, UPPER(F5_GET_REF_WNI(B.JENIS_PKRJN, 201)) PEKERJAAN
					, TO_CHAR(A.PERSONALIZED_DATE,'DD-MM-YYYY') AS TGL_CETAK
					, CASE WHEN TO_CHAR(A.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(A.CREATED_USERNAME) IS NULL  THEN '-' WHEN TO_CHAR(A.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(A.CREATED_USERNAME) IS NOT NULL THEN TO_CHAR(A.CREATED_USERNAME) ELSE TO_CHAR(A.LAST_UPDATED_USERNAME) END AS PETUGAS_CETAK
					FROM CARD_MANAGEMENT@DB2 A 
					INNER JOIN BIODATA_WNI B ON A.NIK = B.NIK
					LEFT JOIN DATA_KELUARGA C ON B.NO_KK = C.NO_KK
					LEFT JOIN SIAK_CETAK_GOIB@YZDB D ON  CASE WHEN UPPER(A.LAST_UPDATED_USERNAME) IS NULL THEN UPPER(A.CREATED_USERNAME) ELSE UPPER(A.LAST_UPDATED_USERNAME) END =  UPPER(D.USER_BCARD) AND D.IS_ACTIVE = 1
					WHERE 1=1 AND D.USER_BCARD IS NULL
					AND (B.TGL_LHR<=ADD_MONTHS(SYSDATE,-12*17) OR B.STAT_KWN<>1) 
					AND (B.FLAG_STATUS = '0' OR (B.FLAG_STATUS = '2' AND B.FLAG_PINDAH IN(1,2,3))) 
					AND B.NO_PROP = 32 AND B.NO_KAB =73 AND CASE WHEN A.LAST_UPDATE IS NULL THEN A.PERSONALIZED_DATE ELSE A.LAST_UPDATE END >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND CASE WHEN A.LAST_UPDATE IS NULL THEN A.PERSONALIZED_DATE ELSE A.LAST_UPDATE END < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1
					    
                    ";
			if($no_kec != 0){
				$sql .= " AND B.NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND B.NO_KEL = $no_kel";
			} 
			if($user != ''){
				$sql .= " AND CASE WHEN TO_CHAR(A.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(A.CREATED_USERNAME) IS NULL  THEN '-' WHEN TO_CHAR(A.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(A.CREATED_USERNAME) IS NOT NULL THEN TO_CHAR(A.CREATED_USERNAME) ELSE TO_CHAR(A.LAST_UPDATED_USERNAME) END = '$user'";
			} 
			$sql .=" ORDER BY A.PERSONALIZED_DATE DESC, B.NO_KEL, C.NO_RW, C.NO_RT";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}
		public function prr_get_list_sfe($no_kec,$no_kel){
			$sql = "SELECT 
					TO_CHAR(B.NIK) AS NIK
					, B.NAMA_LGKP
					, B.TMPT_LHR, TO_CHAR(B.TGL_LHR,'DD-MM-YYYY') AS TGL_LHR
					, CASE WHEN C.ALAMAT IS NULL THEN '-' ELSE C.ALAMAT END AS ALAMAT
					, B.NO_KEC, F5_GET_NAMA_KECAMATAN(B.NO_PROP,B.NO_KAB,B.NO_KEC) NAMA_KEC
					, B.NO_KEL, F5_GET_NAMA_KELURAHAN(B.NO_PROP,B.NO_KAB,B.NO_KEC,B.NO_KEL) NAMA_KEL
					, CASE WHEN C.NO_RW IS NULL THEN 0 ELSE C.NO_RW END AS NO_RW 
					, CASE WHEN C.NO_RT IS NULL THEN 0 ELSE C.NO_RT END AS NO_RT 
					, CASE WHEN TO_CHAR(C.KODE_POS) IS NULL THEN '-' ELSE TO_CHAR(C.KODE_POS) END KODE_POS
					, UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(B.AGAMA,7), 501)) AGAMA
					, UPPER(F5_GET_REF_WNI(B.STAT_KWN, 601)) STATUS_KAWIN
					, UPPER(F5_GET_REF_WNI(B.JENIS_PKRJN, 201)) PEKERJAAN
					, TO_CHAR(A.CREATED,'YYYY-MM-DD') AS TGL_REKAM
					, A.CURRENT_STATUS_CODE AS STATUS_KTP
					FROM DEMOGRAPHICS@DB221 A 
					INNER JOIN BIODATA_WNI@DB222 B ON A.NIK = B.NIK
					LEFT JOIN DATA_KELUARGA@DB222 C ON B.NO_KK = C.NO_KK
					WHERE A.CURRENT_STATUS_CODE = 'SENT_FOR_ENROLLMENT'
					AND (B.TGL_LHR<=ADD_MONTHS(SYSDATE,-12*17) OR B.STAT_KWN<>1) 
					AND (B.FLAG_STATUS = '0' OR (B.FLAG_STATUS = '2' AND B.FLAG_PINDAH IN(1,2,3))) 
					AND B.NO_PROP = 32 AND B.NO_KAB =73";
			if($no_kec != 0){
				$sql .= " AND B.NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND B.NO_KEL = $no_kel";
			} 
			$sql .=" ORDER BY A.CREATED, B.NO_KEL, C.NO_RW, C.NO_RT";
			$r = DB::select($sql);
			return $r;
		}

		public function prr_get_list_duplicate($no_kec,$no_kel){
			$sql = "SELECT 
					TO_CHAR(B.NIK) AS NIK
                    , CASE WHEN D.NIK_SINGLE IS NULL THEN '-' ELSE D.NIK_SINGLE END NIK_SINGLE
					, B.NAMA_LGKP
					, B.TMPT_LHR, TO_CHAR(B.TGL_LHR,'DD-MM-YYYY') AS TGL_LHR
					, CASE WHEN C.ALAMAT IS NULL THEN '-' ELSE C.ALAMAT END AS ALAMAT
					, B.NO_KEC, F5_GET_NAMA_KECAMATAN(B.NO_PROP,B.NO_KAB,B.NO_KEC) NAMA_KEC
					, B.NO_KEL, F5_GET_NAMA_KELURAHAN(B.NO_PROP,B.NO_KAB,B.NO_KEC,B.NO_KEL) NAMA_KEL
					, CASE WHEN C.NO_RW IS NULL THEN 0 ELSE C.NO_RW END AS NO_RW 
					, CASE WHEN C.NO_RT IS NULL THEN 0 ELSE C.NO_RT END AS NO_RT 
					, CASE WHEN TO_CHAR(C.KODE_POS) IS NULL THEN '-' ELSE TO_CHAR(C.KODE_POS) END KODE_POS
					, UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(B.AGAMA,7), 501)) AGAMA
					, UPPER(F5_GET_REF_WNI(B.STAT_KWN, 601)) STATUS_KAWIN
					, UPPER(F5_GET_REF_WNI(B.JENIS_PKRJN, 201)) PEKERJAAN
					, TO_CHAR(A.CREATED,'YYYY-MM-DD') AS TGL_REKAM
					, A.CURRENT_STATUS_CODE AS STATUS_KTP
					FROM DEMOGRAPHICS@DB221 A 
					INNER JOIN BIODATA_WNI@DB222 B ON A.NIK = B.NIK
					LEFT JOIN DATA_KELUARGA@DB222 C ON B.NO_KK = C.NO_KK
                    LEFT JOIN (SELECT  A.NIK
					, A.NIK_SINGLE
                    FROM 
                    (SELECT  A.NIK
					, TO_CHAR(A.NIK_SINGLE) NIK_SINGLE
          			FROM SIAK_DUPLICATE_FULL A
                        UNION ALL
                        SELECT  B.NIK
                            , B.NIK_SINGLE
                            FROM (
                            SELECT  C.NIK
                            , TO_CHAR(C.DUPLICATE_NIK) NIK_SINGLE
                   FROM DUPLICATE_RESULTS@DB221 C WHERE 
                   NOT EXISTS (SELECT 1 FROM SIAK_DUPLICATE_FULL A WHERE A.NIK = C.NIK)) B
                        ) A) D ON A.NIK = D.NIK
					WHERE A.CURRENT_STATUS_CODE = 'DUPLICATE_RECORD' 
					AND (B.TGL_LHR<=ADD_MONTHS(SYSDATE,-12*17) OR B.STAT_KWN<>1) 
					AND (B.FLAG_STATUS = '0' OR (B.FLAG_STATUS = '2' AND B.FLAG_PINDAH IN(1,2,3))) 
					AND B.NO_PROP = 32 AND B.NO_KAB =73";
			if($no_kec != 0){
				$sql .= " AND B.NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND B.NO_KEL = $no_kel";
			} 
			$sql .=" ORDER BY A.CREATED, B.NO_KEL, C.NO_RW, C.NO_RT";
			$r = DB::select($sql);
			return $r;
		}
		public function prr_get_list_failure($no_kec,$no_kel){
			$sql = "SELECT 
					TO_CHAR(B.NIK) AS NIK
					, B.NAMA_LGKP
					, B.TMPT_LHR, TO_CHAR(B.TGL_LHR,'DD-MM-YYYY') AS TGL_LHR
					, CASE WHEN C.ALAMAT IS NULL THEN '-' ELSE C.ALAMAT END AS ALAMAT
					, B.NO_KEC, F5_GET_NAMA_KECAMATAN(B.NO_PROP,B.NO_KAB,B.NO_KEC) NAMA_KEC
					, B.NO_KEL, F5_GET_NAMA_KELURAHAN(B.NO_PROP,B.NO_KAB,B.NO_KEC,B.NO_KEL) NAMA_KEL
					, CASE WHEN C.NO_RW IS NULL THEN 0 ELSE C.NO_RW END AS NO_RW 
					, CASE WHEN C.NO_RT IS NULL THEN 0 ELSE C.NO_RT END AS NO_RT 
					, CASE WHEN TO_CHAR(C.KODE_POS) IS NULL THEN '-' ELSE TO_CHAR(C.KODE_POS) END KODE_POS
					, UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(B.AGAMA,7), 501)) AGAMA
					, UPPER(F5_GET_REF_WNI(B.STAT_KWN, 601)) STATUS_KAWIN
					, UPPER(F5_GET_REF_WNI(B.JENIS_PKRJN, 201)) PEKERJAAN
					, TO_CHAR(A.CREATED,'YYYY-MM-DD') AS TGL_REKAM
					, A.CURRENT_STATUS_CODE AS STATUS_KTP
					FROM DEMOGRAPHICS@DB221 A 
					INNER JOIN BIODATA_WNI@DB222 B ON A.NIK = B.NIK
					LEFT JOIN DATA_KELUARGA@DB222 C ON B.NO_KK = C.NO_KK
					WHERE A.CURRENT_STATUS_CODE NOT LIKE '%CARD%' AND A.CURRENT_STATUS_CODE NOT LIKE '%PRINT%' AND  A.CURRENT_STATUS_CODE NOT LIKE '%DUPLICATE%' AND A.CURRENT_STATUS_CODE NOT LIKE 'SENT_FOR_ENROLLMENT'
					AND (B.TGL_LHR<=ADD_MONTHS(SYSDATE,-12*17) OR B.STAT_KWN<>1) 
					AND (B.FLAG_STATUS = '0' OR (B.FLAG_STATUS = '2' AND B.FLAG_PINDAH IN(1,2,3))) 
					AND B.NO_PROP = 32 AND B.NO_KAB =73";
			if($no_kec != 0){
				$sql .= " AND B.NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND B.NO_KEL = $no_kel";
			} 
			$sql .=" ORDER BY A.CREATED, B.NO_KEL, C.NO_RW, C.NO_RT";
			$r = DB::select($sql);
			return $r;
		}
		public function prr_get_list_belum_rekam($no_kec,$no_kel){
			$sql = "SELECT 
					  B.NIK
					  ,B.NO_KK
					  , B.NAMA_LGKP
					  , UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(B.JENIS_KLMIN,7), 801)) JENIS_KLMIN
					  , TO_CHAR(B.TGL_LHR,'DD-MM-YYYY') TGL_LHR
					  , TRUNC(MONTHS_BETWEEN(SYSDATE,B.TGL_LHR)/12) UMUR
					  , UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(B.AGAMA,7), 501)) AGAMA
						, UPPER(F5_GET_REF_WNI(B.STAT_KWN, 601)) STATUS_KAWIN
					  , UPPER(F5_GET_REF_WNI(B.JENIS_PKRJN, 201)) PEKERJAAN
					  , UPPER(F5_GET_REF_WNI(B.STAT_HBKEL, 301)) STAT_HBKEL
					  , A.WKTP
					  , B.NO_PROP
					  , B.NO_KAB
					  , B.NO_KEC
					  , B.NO_KEL
					  , F5_GET_NAMA_KECAMATAN(B.NO_PROP,B.NO_KAB,B.NO_KEC) NAMA_KEC
					  , F5_GET_NAMA_KELURAHAN(B.NO_PROP,B.NO_KAB,B.NO_KEC,B.NO_KEL) NAMA_KEL
					  , CASE WHEN D.NO_RT IS NULL THEN '-' ELSE LPAD(TO_CHAR(D.NO_RT), 3, '0') END AS RT
						, CASE WHEN D.NO_RW IS NULL THEN '-' ELSE LPAD(TO_CHAR(D.NO_RW), 3, '0') END AS RW
					  , CASE WHEN D.ALAMAT IS NULL THEN '-' ELSE D.ALAMAT END ALAMAT
					FROM WKTP2_NOREC_FULL A 
					INNER JOIN BIODATA_WNI@DB222 B ON A.NIK = B.NIK
					INNER JOIN DATA_KELUARGA@DB222 D ON B.NO_KK = D.NO_KK
					LEFT JOIN DEMOGRAPHICS_ALL@DB2 C ON A.NIK = C.NIK WHERE C.NIK IS NULL 
					AND (B.TGL_LHR<=ADD_MONTHS(TRUNC(ADD_MONTHS(SYSDATE,12)-1,'YYYY'),-12*17) OR B.STAT_KWN<>1) 
					AND (B.FLAG_STATUS = '0' OR (B.FLAG_STATUS = '2' AND B.FLAG_PINDAH IN(1,2,3))) 
					AND B.NO_PROP = 32 AND B.NO_KAB =73 ";
			if($no_kec != 0){
				$sql .= " AND B.NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND B.NO_KEL = $no_kel";
			} 
			$sql .="  ORDER BY B.NO_PROP, B.NO_KAB, B.NO_KEC, B.NO_KEL, D.NO_RW, D.NO_RT, D.NO_KK, B.STAT_HBKEL, B.NAMA_LGKP";
			$r = DB::select($sql);
			return $r;
		}
		public function prr_nik_pemula($tgl_start,$tgl_end){
			$sql = "";
			$sql .= "SELECT 
					TO_CHAR(A.NIK) NIK
					, A.NAMA_LGKP
					, TO_CHAR(A.TGL_LHR,'DD-MM-YYYY') TGL_LHR
					, A.NO_KEC
					, F5_GET_NAMA_KECAMATAN(A.NO_PROP,A.NO_KAB,A.NO_KEC) NAMA_KEC
					, A.NO_KEL
					, F5_GET_NAMA_KELURAHAN(A.NO_PROP,A.NO_KAB,A.NO_KEC,A.NO_KEL) NAMA_KEL
					, B.ALAMAT
					, B.KODE_POS
					, B.NO_RW
					, B.NO_RT
					, CASE WHEN C.CREATED IS NULL THEN '-' ELSE TO_CHAR(C.CREATED,'DD-MM-YYYY') END TGL_REKAM
					, CASE WHEN TO_CHAR(C.CREATED_USERNAME) IS NULL THEN '-' ELSE  TO_CHAR(C.CREATED_USERNAME) END  PETUGAS_REKAM
					, CASE WHEN C.CURRENT_STATUS_CODE IS NULL THEN 'BELUM REKAM' ELSE C.CURRENT_STATUS_CODE END CURRENT_STATUS_CODE
					, CASE WHEN G.NIK IS NULL THEN 'BELUM CETAK' ELSE 'SUDAH CETAK KTP' END STATUS_PENCETAKAN
          			, CASE WHEN  TO_CHAR(G.LAST_UPDATE,'DD-MM-YYYY') IS NULL AND TO_CHAR(G.PERSONALIZED_DATE,'DD-MM-YYYY') IS NULL THEN '-' WHEN TO_CHAR(G.LAST_UPDATE,'DD-MM-YYYY') IS NULL AND TO_CHAR(G.PERSONALIZED_DATE,'DD-MM-YYYY') IS NOT NULL THEN TO_CHAR(G.PERSONALIZED_DATE,'DD-MM-YYYY') ELSE  TO_CHAR(G.LAST_UPDATE,'DD-MM-YYYY') END AS KTP_DT
          			, CASE WHEN TO_CHAR(G.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(G.CREATED_USERNAME) IS NULL  THEN '-' WHEN TO_CHAR(G.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(G.CREATED_USERNAME) IS NOT NULL THEN TO_CHAR(G.CREATED_USERNAME) ELSE TO_CHAR(G.LAST_UPDATED_USERNAME) END AS KTP_BY
					FROM BIODATA_WNI A INNER JOIN DATA_KELUARGA B ON A.NO_KK = B.NO_KK
					LEFT JOIN DEMOGRAPHICS@DB221 C ON A.NIK = C.NIK
          			LEFT JOIN (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME,LAST_UPDATE,CREATED FROM (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME,LAST_UPDATE,CREATED, RANK() OVER (PARTITION BY NIK ORDER BY  PERSONALIZED_DATE DESC, LAST_UPDATE DESC, CHIP_ID DESC) RNK FROM CARD_MANAGEMENT@DB2) WHERE RNK = 1) G ON G.NIK =A.NIK
					WHERE A.TGL_LHR >= ADD_MONTHS(TO_DATE('$tgl_start','DD-MM-YYYY'),-(12*17))
          			AND A.TGL_LHR < ADD_MONTHS(TO_DATE('$tgl_end','DD-MM-YYYY'),-(12*17))+1
					ORDER BY CURRENT_STATUS_CODE,TO_CHAR(A.TGL_LHR,'DD/MM/YYYY'), A.NO_KEC,A.NAMA_LGKP";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}

		public function prr_get_list_pr_suket($no_kec,$no_kel){
			$sql = "SELECT 
					  A.NO_KEC
					  , A.NAMA_KEC
					  , A.NO_KEL
					  , A.NAMA_KEL
					  , TO_CHAR(A.NIK) NIK
					  , TO_CHAR(A.NO_KK) NO_KK
					  , A.NAMA_LGKP
					  , A.CURRENT_STATUS_CODE
					  , UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(A.AGAMA,7), 501)) AGAMA
					  , TO_CHAR(A.PRINTED_DATE,'YYYY-MM-DD') TGL_SUKET   
					  , TO_CHAR(A.PRINTED_DATE,'YYYY') THN_SUKET   
					  , CASE WHEN TO_CHAR(A.KTP_DT,'YYYY-MM-DD') IS NULL THEN '-' ELSE TO_CHAR(A.KTP_DT,'YYYY-MM-DD') END TGL_KTP
					  , RW
					  , RT
					  , A.NO_DOKUMEN
					  FROM (SELECT 
					  A.NO_KEC
					  , F5_GET_NAMA_KECAMATAN(A.NO_PROP,A.NO_KAB,A.NO_KEC) NAMA_KEC
					  , A.NO_KEL
					  , F5_GET_NAMA_KELURAHAN(A.NO_PROP,A.NO_KAB,A.NO_KEC,A.NO_KEL) NAMA_KEL
					  , LPAD(TO_CHAR(F.NO_RT), 3, '0') AS RT
					  , LPAD(TO_CHAR(F.NO_RW), 3, '0') AS RW
					  , A.NIK
					  , A.AGAMA
					  , A.NO_KK
					  , A.NAMA_LGKP
					  , CASE WHEN D.CURRENT_STATUS_CODE IS NULL THEN '-' ELSE D.CURRENT_STATUS_CODE END AS CURRENT_STATUS_CODE
					  , B.PRINTED_DATE
					  , B.NO_DOKUMEN
					  , CASE WHEN  C.LAST_UPDATE IS NULL AND C.PERSONALIZED_DATE IS NULL THEN NULL WHEN C.LAST_UPDATE IS NULL AND C.PERSONALIZED_DATE IS NOT NULL THEN C.PERSONALIZED_DATE ELSE  C.LAST_UPDATE END AS KTP_DT
					  FROM BIODATA_WNI A INNER JOIN DATA_KELUARGA F ON A.NO_KK = F.NO_KK
					INNER JOIN (SELECT NIK, PRINTED_DATE,PRINTED_BY,NO_DOKUMEN  FROM (SELECT NIK,PRINTED_DATE,PRINTED_BY,NO_DOKUMEN, RANK() OVER (PARTITION BY NIK ORDER BY PRINTED_DATE,NO_DOKUMEN DESC) RNK FROM T7_HIST_SUKET) 
					WHERE RNK = 1) B ON A.NIK = B.NIK
					LEFT JOIN (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME,LAST_UPDATE,CREATED FROM (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME,LAST_UPDATE,CREATED, RANK() OVER (PARTITION BY NIK ORDER BY  PERSONALIZED_DATE DESC, LAST_UPDATE DESC, CHIP_ID DESC) RNK FROM CARD_MANAGEMENT@DB2) 
					WHERE RNK = 1) C ON A.NIK =C.NIK
					LEFT JOIN DEMOGRAPHICS_ALL@DB2 D ON A.NIK = D.NIK
					WHERE  (A.TGL_LHR<=ADD_MONTHS(SYSDATE,-12*17) OR A.STAT_KWN<>1) 
                    AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3))) 
                    AND A.NO_PROP = 32 AND A.NO_KAB =73 
					) A WHERE 
					-- TRUNC(PRINTED_DATE) > TO_DATE('19-02-2020','DD-MM-YYYY') AND  
					(KTP_DT IS NULL OR (TRUNC(PRINTED_DATE) > TRUNC(KTP_DT))) AND 
					(CURRENT_STATUS_CODE LIKE '%CARD%' OR CURRENT_STATUS_CODE = 'PRINT_READY_RECORD') 
					 ";
			if($no_kec != 0){
				$sql .= " AND A.NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND A.NO_KEL = $no_kel";
			} 
			$sql .=" ORDER BY A.NO_KEC ASC,A.NO_KEL ASC,A.PRINTED_DATE DESC";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}
		public function del_suket($no_dokumen){
			$sql = "DELETE FROM T7_HIST_SUKET WHERE NO_DOKUMEN = '$no_dokumen'";
			$r = DB::connection('db222')->delete($sql);
			return $r;
		}

		public function prr_get_list_cetak_suket($tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			$sql .= "SELECT 
					X.NO_KEC
					,X.NIK
					,X.NAMA_LGKP AS NAMA_LENGKAP, Y.ALAMAT, CASE WHEN TO_CHAR(Y.KODE_POS) IS NULL THEN '-' ELSE TO_CHAR(Y.KODE_POS) END KODE_POS, LPAD(TO_CHAR(Y.NO_RT), 3, '0') AS RT, LPAD(TO_CHAR(Y.NO_RW), 3, '0') AS RW,CASE WHEN B.CURRENT_STATUS_CODE IS NULL THEN '-' ELSE B.CURRENT_STATUS_CODE END AS CURRENT_STATUS_CODE
					,TO_CHAR(S.PRINTED_DATE,'YYYY-MM-DD') AS SUKET_DATE
                    ,S.PRINTED_BY SUKET_BY
                    ,TO_CHAR(A.REQ_DATE,'YYYY-MM-DD') AS REQ_DATE
					,A.REQ_BY
					, CASE WHEN A.STEP_PROC = 'T' THEN 'DITOLAK' WHEN A.STEP_PROC = 'C' THEN 'DICETAK' ELSE 'PROSSESS' END AS STATUS_PENGAJUAN
					, CASE WHEN A.STEP_PROC = 'T' THEN UPPER(A.PROC_BY) WHEN A.STEP_PROC = 'C' THEN 'SUDAH DICATAK' ELSE '-' END AS KETERANGAN_STATUS
					, CASE WHEN TO_CHAR(C.LAST_UPDATE,'DD-MM-YYYY') IS NULL AND TO_CHAR(C.PERSONALIZED_DATE,'DD-MM-YYYY') IS NULL THEN '-' WHEN TO_CHAR(C.LAST_UPDATE,'DD-MM-YYYY') IS NULL THEN TO_CHAR(C.PERSONALIZED_DATE,'DD-MM-YYYY') ELSE TO_CHAR(C.LAST_UPDATE,'DD-MM-YYYY') END AS PRINTED_DATE
					, CASE WHEN TO_CHAR(C.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(C.CREATED_USERNAME) IS NULL  THEN '-' WHEN TO_CHAR(C.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(C.CREATED_USERNAME) IS NOT NULL THEN TO_CHAR(C.CREATED_USERNAME) ELSE TO_CHAR(C.LAST_UPDATED_USERNAME) END AS PRINTED_BY
					, F5_GET_NAMA_KECAMATAN(X.NO_PROP,X.NO_KAB,X.NO_KEC) NAMA_KEC
					, F5_GET_NAMA_KELURAHAN(X.NO_PROP,X.NO_KAB,X.NO_KEC,X.NO_KEL) NAMA_KEL
					, UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(X.AGAMA,7), 501)) AGAMA
					, UPPER(F5_GET_REF_WNI(X.JENIS_PKRJN, 201)) JENIS_PKRJN
					, UPPER(F5_GET_REF_WNI(X.STAT_KWN, 601)) STAT_KWN
					FROM BIODATA_WNI X LEFT JOIN DATA_KELUARGA Y ON X.NO_KK = Y.NO_KK 
                    INNER JOIN (SELECT NIK, PRINTED_DATE,PRINTED_BY,NO_DOKUMEN  FROM (SELECT NIK,PRINTED_DATE,PRINTED_BY,NO_DOKUMEN, RANK() OVER (PARTITION BY NIK ORDER BY PRINTED_DATE,NO_DOKUMEN DESC) RNK FROM T7_HIST_SUKET) 
					WHERE RNK = 1) S ON X.NIK = S.NIK
                    LEFT JOIN (SELECT ID_CETAK,NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY,STEP_PROC,PROC_BY FROM 
					(SELECT  ID_CETAK, NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY,STEP_PROC,PROC_BY, RANK() OVER (PARTITION BY NIK ORDER BY REQ_DATE DESC, ID_CETAK DESC) RNK FROM SIAK_REQ_CETAK_KTP@YZDB WHERE STEP_PROC <> 'C') WHERE RNK = 1) A ON A.NIK =X.NIK
					LEFT JOIN DEMOGRAPHICS_ALL@DB2 B ON A.NIK = B.NIK LEFT JOIN (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME,LAST_UPDATE FROM (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME,LAST_UPDATE, RANK() OVER (PARTITION BY NIK ORDER BY  PERSONALIZED_DATE DESC, LAST_UPDATE DESC, CHIP_ID DESC) RNK FROM CARD_MANAGEMENT@DB2) WHERE RNK = 1) C ON A.NIK = C.NIK  WHERE 1=1  ";
			if($no_kec != 0){
				$sql .= " AND X.NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND X.NO_KEL = $no_kel";
			} 
			$sql .= " AND S.PRINTED_DATE >= TO_DATE('$tgl_start','DD-MM-YYYY') AND S.PRINTED_DATE < TO_DATE('$tgl_end','DD-MM-YYYY') +1 ORDER BY X.NO_KEC, X.NO_KEL, X.NAMA_LGKP ";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}
		public function prr_get_list_prr_fcl($no_kec,$no_kel){
			$sql = "SELECT 
					TO_CHAR(B.NIK) AS NIK
					, B.NAMA_LGKP
					, B.TMPT_LHR, TO_CHAR(B.TGL_LHR,'DD-MM-YYYY') AS TGL_LHR
					, CASE WHEN C.ALAMAT IS NULL THEN '-' ELSE C.ALAMAT END AS ALAMAT
					, B.NO_KEC, F5_GET_NAMA_KECAMATAN(B.NO_PROP,B.NO_KAB,B.NO_KEC) NAMA_KEC
					, B.NO_KEL, F5_GET_NAMA_KELURAHAN(B.NO_PROP,B.NO_KAB,B.NO_KEC,B.NO_KEL) NAMA_KEL
					, CASE WHEN C.NO_RW IS NULL THEN 0 ELSE C.NO_RW END AS NO_RW 
					, CASE WHEN C.NO_RT IS NULL THEN 0 ELSE C.NO_RT END AS NO_RT 
					, CASE WHEN TO_CHAR(C.KODE_POS) IS NULL THEN '-' ELSE TO_CHAR(C.KODE_POS) END KODE_POS
					, UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(B.AGAMA,7), 501)) AGAMA
					, UPPER(F5_GET_REF_WNI(B.STAT_KWN, 601)) STATUS_KAWIN
					, UPPER(F5_GET_REF_WNI(B.JENIS_PKRJN, 201)) PEKERJAAN
					, TO_CHAR(A.CREATED,'YYYY-MM-DD') AS TGL_REKAM
					, A.CURRENT_STATUS_CODE AS STATUS_KTP
					, A.CREATED_USERNAME AS PETUGAS_REKAM
					FROM DEMOGRAPHICS@DB221 A 
					INNER JOIN BIODATA_WNI B ON A.NIK = B.NIK
					LEFT JOIN DATA_KELUARGA C ON B.NO_KK = C.NO_KK
					WHERE A.CURRENT_STATUS_CODE = 'PRINT_READY_RECORD' 
					AND A.CREATED_USERNAME IN ('327300drajat','327300irwan','327300irwan ','327300ogi','327300bila')
					AND (B.TGL_LHR<=ADD_MONTHS(SYSDATE,-12*17) OR B.STAT_KWN<>1) 
					AND (B.FLAG_STATUS = '0' OR (B.FLAG_STATUS = '2' AND B.FLAG_PINDAH IN(1,2,3))) 
					AND B.NO_PROP = 32 AND B.NO_KAB =73";
			if($no_kec != 0){
				$sql .= " AND B.NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND B.NO_KEL = $no_kel";
			} 
			$sql .=" AND NOT EXISTS(SELECT 1 FROM CARD_MANAGEMENT@DB2 D WHERE A.NIK = D.NIK)
					AND NOT EXISTS(SELECT 1 FROM DEMOGRAPHICS@DB2 D WHERE A.NIK = D.NIK AND D.CURRENT_STATUS_CODE LIKE '%CARD%')
          			ORDER BY A.CREATED, B.NO_KEL, C.NO_RW, C.NO_RT";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}
		public function prr_get_list_sudah_cetak_fcl($tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "SELECT 
					TO_CHAR(B.NIK) AS NIK
					, B.NAMA_LGKP
					, B.TMPT_LHR, TO_CHAR(B.TGL_LHR,'DD-MM-YYYY') AS TGL_LHR
					, CASE WHEN C.ALAMAT IS NULL THEN '-' ELSE C.ALAMAT END AS ALAMAT
					, B.NO_KEC, F5_GET_NAMA_KECAMATAN(B.NO_PROP,B.NO_KAB,B.NO_KEC) NAMA_KEC
					, B.NO_KEL, F5_GET_NAMA_KELURAHAN(B.NO_PROP,B.NO_KAB,B.NO_KEC,B.NO_KEL) NAMA_KEL
					, CASE WHEN C.NO_RW IS NULL THEN 0 ELSE C.NO_RW END AS NO_RW 
					, CASE WHEN C.NO_RT IS NULL THEN 0 ELSE C.NO_RT END AS NO_RT 
					, CASE WHEN TO_CHAR(C.KODE_POS) IS NULL THEN '-' ELSE TO_CHAR(C.KODE_POS) END KODE_POS
					, UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(B.AGAMA,7), 501)) AGAMA
					, UPPER(F5_GET_REF_WNI(B.STAT_KWN, 601)) STATUS_KAWIN
					, UPPER(F5_GET_REF_WNI(B.JENIS_PKRJN, 201)) PEKERJAAN
					,CASE WHEN TO_CHAR(A.LAST_UPDATE,'DD-MM-YYYY') IS NULL THEN TO_CHAR(A.PERSONALIZED_DATE,'DD-MM-YYYY') ELSE TO_CHAR(A.LAST_UPDATE,'DD-MM-YYYY') END AS TGL_CETAK
					, CASE WHEN TO_CHAR(A.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(A.CREATED_USERNAME) IS NULL  THEN '-' WHEN TO_CHAR(A.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(A.CREATED_USERNAME) IS NOT NULL THEN TO_CHAR(A.CREATED_USERNAME) ELSE TO_CHAR(A.LAST_UPDATED_USERNAME) END AS PETUGAS_CETAK
					FROM CARD_MANAGEMENT@DB2 A 
					INNER JOIN BIODATA_WNI@DB222 B ON A.NIK = B.NIK
					LEFT JOIN DATA_KELUARGA@DB222 C ON B.NO_KK = C.NO_KK
					WHERE 1=1 
					AND CASE WHEN TO_CHAR(A.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(A.CREATED_USERNAME) IS NULL  THEN '-' WHEN TO_CHAR(A.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(A.CREATED_USERNAME) IS NOT NULL THEN TO_CHAR(A.CREATED_USERNAME) ELSE TO_CHAR(A.LAST_UPDATED_USERNAME) END IN ('32730000irwan','32730000yogi','32730000cl-abah')
					AND (B.TGL_LHR<=ADD_MONTHS(SYSDATE,-12*17) OR B.STAT_KWN<>1) 
					AND (B.FLAG_STATUS = '0' OR (B.FLAG_STATUS = '2' AND B.FLAG_PINDAH IN(1,2,3))) 
					AND B.NO_PROP = 32 AND B.NO_KAB =73 AND A.PERSONALIZED_DATE >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.PERSONALIZED_DATE < TO_DATE('$tgl_end','DD-MM-YYYY') +1 AND (UPPER(A.CREATED_USERNAME) NOT IN (SELECT UPPER(USER_BCARD) FROM SIAK_CETAK_GOIB WHERE IS_ACTIVE = 1) OR UPPER(A.LAST_UPDATED_USERNAME) NOT IN (SELECT UPPER(USER_BCARD) FROM SIAK_CETAK_GOIB WHERE IS_ACTIVE = 1))
                    ";
			if($no_kec != 0){
				$sql .= " AND B.NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND B.NO_KEL = $no_kel";
			} 
			$sql .=" ORDER BY A.PERSONALIZED_DATE DESC, B.NO_KEL, C.NO_RW, C.NO_RT";
			$r = DB::select($sql);
			return $r;
		}
		public function prr_get_list_perekaman_fcl($tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "SELECT 
					TO_CHAR(B.NIK) AS NIK
					, B.NAMA_LGKP
					, B.TMPT_LHR, TO_CHAR(B.TGL_LHR,'DD-MM-YYYY') AS TGL_LHR
					, CASE WHEN C.ALAMAT IS NULL THEN '-' ELSE C.ALAMAT END AS ALAMAT
					, B.NO_KEC, F5_GET_NAMA_KECAMATAN(B.NO_PROP,B.NO_KAB,B.NO_KEC) NAMA_KEC
					, B.NO_KEL, F5_GET_NAMA_KELURAHAN(B.NO_PROP,B.NO_KAB,B.NO_KEC,B.NO_KEL) NAMA_KEL
					, CASE WHEN C.NO_RW IS NULL THEN 0 ELSE C.NO_RW END AS NO_RW 
					, CASE WHEN C.NO_RT IS NULL THEN 0 ELSE C.NO_RT END AS NO_RT 
					, CASE WHEN TO_CHAR(C.KODE_POS) IS NULL THEN '-' ELSE TO_CHAR(C.KODE_POS) END KODE_POS
					, UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(B.AGAMA,7), 501)) AGAMA
					, UPPER(F5_GET_REF_WNI(B.STAT_KWN, 601)) STATUS_KAWIN
					, UPPER(F5_GET_REF_WNI(B.JENIS_PKRJN, 201)) PEKERJAAN
					, TO_CHAR(A.CREATED,'YYYY-MM-DD') AS TGL_REKAM
         			, TO_CHAR(A.CREATED,'HH24:MM') AS JAM_REKAM
					, A.CURRENT_STATUS_CODE AS STATUS_KTP
					, A.CREATED_USERNAME AS PETUGAS_REKAM
					FROM DEMOGRAPHICS@DB221 A 
					INNER JOIN BIODATA_WNI@DB222 B ON A.NIK = B.NIK
					LEFT JOIN DATA_KELUARGA@DB222 C ON B.NO_KK = C.NO_KK
					WHERE 1=1
					AND A.CREATED_USERNAME IN ('327300drajat','327300irwan','327300irwan ','327300ogi','327300bila')
					AND B.NO_PROP = 32 AND B.NO_KAB =73
          			AND A.CREATED >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.CREATED < TO_DATE('$tgl_end','DD-MM-YYYY') +1 
                    ";
			if($no_kec != 0){
				$sql .= " AND B.NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND B.NO_KEL = $no_kel";
			} 
			$sql .=" ORDER BY A.CREATED";
			$r = DB::select($sql);
			return $r;
		}
		public function prr_get_list_kia($tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "SELECT 
						A.NIK
						, A.NO_KK
						, A.NO_PROP
						, A.NO_KAB
						, A.NO_KEC
						, A.NO_KEL
						, A.NAMA_LGKP
						, A.TMPT_LHR
						, TO_CHAR(A.TGL_LHR,'DD-MM-YYYY') TGL_LHR
						, TRUNC(MONTHS_BETWEEN(C.PRINTED_DATE,A.TGL_LHR)/12) UMUR
						, CASE WHEN A.JENIS_KLMIN = 1 THEN 'LAKI-LAKI' ELSE 'PEREMPUAN' END AS JENIS_KLMIN
						, A.NO_AKTA_LHR
						, C.PRINTED_BY
						, TO_CHAR(C.PRINTED_DATE,'DD-MM-YYYY') PRINTED_DATE
						, F5_GET_NAMA_KECAMATAN(A.NO_PROP,A.NO_KAB,A.NO_KEC) NAMA_KEC
					    , F5_GET_NAMA_KELURAHAN(A.NO_PROP,A.NO_KAB,A.NO_KEC,A.NO_KEL) NAMA_KEL
					    , LPAD(TO_CHAR(B.NO_RT), 3, '0') AS RT
					    , LPAD(TO_CHAR(B.NO_RW), 3, '0') AS RW
					    , B.ALAMAT 
					FROM  BIODATA_WNI A INNER JOIN DATA_KELUARGA B ON A.NO_KK = B.NO_KK 
					INNER JOIN 
						(
							SELECT 
								NIK
								, PRINTED_DATE
								, PRINTED_BY
								, SEQN_ID  FROM 
								(
									SELECT NIK
									, PRINTED_DATE
									, PRINTED_BY
									, SEQN_ID
									, RANK() OVER 
										(
											PARTITION BY NIK 
											ORDER BY 
											PRINTED_DATE DESC
											, SEQN_ID DESC
										) 
									RNK FROM T5_SEQN_KIA_PRINT
								)  WHERE RNK = 1
						) C ON A.NIK = C.NIK 
					WHERE 1=1
					AND A.NO_PROP = 32 AND A.NO_KAB =73
          			AND C.PRINTED_DATE >= TO_DATE('$tgl_start','DD-MM-YYYY') AND C.PRINTED_DATE < TO_DATE('$tgl_end','DD-MM-YYYY') +1 
                    ";
			if($no_kec != 0){
				$sql .= " AND A.NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND A.NO_KEL = $no_kel";
			} 
			$sql .=" ORDER BY C.PRINTED_DATE DESC, A.NO_KEC, A.NO_KEL, B.NO_RW, B.NO_RT, A.NAMA_LGKP";
			$r = DB::select($sql);
			return $r;
		}
		public function prr_get_list_skts($tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "SELECT 
				    B.NO_SKTS
				    , A.NIK
				    , B.NAMA_LGKP
				    , B.TMPT_LHR
				    , TO_CHAR(B.TGL_LHR,'DD-MM-YYYY') TGL_LHR
				    , TRUNC(MONTHS_BETWEEN(SYSDATE,B.TGL_LHR)/12) UMUR
				    , UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(B.JENIS_KLMIN,7), 801)) JENIS_KLMIN
				    , UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(B.AGAMA,7), 501)) AGAMA
				    , UPPER(F5_GET_REF_WNI(B.PEKERJAAN, 201)) PEKERJAAN
				    , UPPER(F5_GET_REF_WNI(B.PENDIDIKAN, 101)) PENDIDIKAN
				    , UPPER(F5_GET_REF_WNI(B.STAT_KWN, 601)) STAT_KWN
				    , UPPER(F5_GET_REF_WNI(B.GOL_DRH, 401)) GOL_DRH
				    , 32 NO_PROP, F5_GET_NAMA_PROVINSI(32) NAMA_PROP
				    , 73 NO_KAB, F5_GET_NAMA_KABUPATEN(32,73) NAMA_KAB
				    , B.KEC, F5_GET_NAMA_KECAMATAN(32,73,B.KEC) NAMA_KEC
				    , B.KEL, F5_GET_NAMA_KELURAHAN(32,73,B.KEC,B.KEL) NAMA_KEL
				    , B.ALAMAT
				    , LPAD(TO_CHAR(B.NO_RT), 3, '0') AS RT
				    , LPAD(TO_CHAR(B.NO_RW), 3, '0') AS RW
				    , B.SRC_PROV, F5_GET_NAMA_PROVINSI(B.SRC_PROV) SRC_NAMA_PROP
				    , B.SRC_KAB, F5_GET_NAMA_KABUPATEN(B.SRC_PROV,B.SRC_KAB) SRC_NAMA_KAB
				    , B.SRC_KEC, F5_GET_NAMA_KECAMATAN(B.SRC_PROV,B.SRC_KAB,B.SRC_KEC) SRC_NAMA_KEC
				    , B.SRC_KEL, F5_GET_NAMA_KELURAHAN(B.SRC_PROV,B.SRC_KAB,B.SRC_KEC,B.SRC_KEL) SRC_NAMA_KEL
				    , B.SRC_ALAMAT
				    , LPAD(TO_CHAR(B.SRC_RT), 3, '0') AS SRC_RT
				    , LPAD(TO_CHAR(B.SRC_RW), 3, '0') AS SRC_RW
				    , CASE WHEN B.TELEPON IS NULL THEN '-' ELSE B.TELEPON END TELEPON
				    , CASE WHEN B.EMAIL IS NULL THEN '-' ELSE B.EMAIL END EMAIL
				    , TO_CHAR(A.UPDATED_AT,'DD-MM-YYYY') TGL_VERIFIKASI
				    , TO_CHAR(B.TGL_BERLAKU,'DD-MM-YYYY') TGL_BERLAKU
				    FROM PENGAJUAN A LEFT JOIN USERS B ON A.NIK = B.NIK  WHERE TRUNC(A.UPDATED_AT) >= TO_DATE('$tgl_start','DD-MM-YYYY') AND TRUNC(A.UPDATED_AT) < TO_DATE('$tgl_end','DD-MM-YYYY')+1 AND B.STATUS = 3
                    ";
			if($no_kec != 0){
				$sql .= " AND B.KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND B.KEL = $no_kel";
			} 
			$sql .=" ORDER BY A.UPDATED_AT DESC, B.KEC, B.KEL, B.NO_RW, B.NO_RT, B.NAMA_LGKP";
			$r = DB::connection('epunten')->select($sql);
			return $r;
		}
		public function do_plb_get_nik($nik){
			$sql = "SELECT COUNT(1) AS JML FROM BIODATA_WNI@DB222 WHERE NIK = $nik";
			$r = DB::select($sql);
			return $r[0]->jml;
		}
		public function do_plb_get_data($nik){
			$sql = "SELECT COUNT(1) AS JML FROM SIAK_PLB WHERE NIK = $nik";
			$r = DB::select($sql);
			return $r[0]->jml;
		}
		public function do_plb_get_data_nik($nik){
			$sql = "SELECT 
					TO_CHAR(B.NIK) AS NIK
					, B.NAMA_LGKP
					, B.TMPT_LHR, TO_CHAR(B.TGL_LHR,'DD-MM-YYYY') AS TGL_LHR
					, TRUNC(MONTHS_BETWEEN(SYSDATE,B.TGL_LHR)/12) UMUR
					, UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(B.JENIS_KLMIN,7), 801)) JENIS_KLMIN
					, CASE WHEN C.ALAMAT IS NULL THEN '-' ELSE C.ALAMAT END AS ALAMAT
					, B.NO_PROP, F5_GET_NAMA_PROVINSI(B.NO_PROP) NAMA_PROP
					, B.NO_KAB, F5_GET_NAMA_KABUPATEN(B.NO_PROP,B.NO_KAB) NAMA_KAB
					, B.NO_KEC, F5_GET_NAMA_KECAMATAN(B.NO_PROP,B.NO_KAB,B.NO_KEC) NAMA_KEC
					, B.NO_KEL, F5_GET_NAMA_KELURAHAN(B.NO_PROP,B.NO_KAB,B.NO_KEC,B.NO_KEL) NAMA_KEL
					, CASE WHEN C.NO_RW IS NULL THEN 0 ELSE C.NO_RW END AS RW 
					, CASE WHEN C.NO_RT IS NULL THEN 0 ELSE C.NO_RT END AS RT 
					, UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(B.AGAMA,7), 501)) AGAMA
					, UPPER(F5_GET_REF_WNI(B.STAT_KWN, 601)) STATUS_KAWIN
					, UPPER(F5_GET_REF_WNI(B.JENIS_PKRJN, 201)) PEKERJAAN
					, UPPER(F5_GET_REF_WNI(B.GOL_DRH, 401)) GOL_DRH
					FROM BIODATA_WNI B 
					LEFT JOIN DATA_KELUARGA C ON B.NO_KK = C.NO_KK
					WHERE 1=1
					AND (B.TGL_LHR<=ADD_MONTHS(SYSDATE,-12*17) OR B.STAT_KWN<>1) 
					AND (B.FLAG_STATUS = '0' OR (B.FLAG_STATUS = '2' AND B.FLAG_PINDAH IN(1,2,3))) 
					AND B.NIK = $nik";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}
		public function plb_save($plb,$nik,$nama,$umur,$tgl_lhr,$tmpt_lhr,$jenis_klmin,$jenis_pkrjn,$no_prop,$no_kab,$no_kec,$no_kel,$alamat,$rt,$rw,$user_name){
			$sql = "INSERT INTO SIAK_PLB (NO_PLB ,NIK ,NAMA_LGKP ,TMPT_LHR ,TGL_LHR ,UMUR ,JENIS_KLMIN ,JENIS_PKRJN ,NO_PROP ,NO_KAB ,NO_KEC ,NO_KEL ,ALAMAT ,RW ,RT ,CREATED_BY ,CREATED_DT) VALUES ('$plb',$nik,'$nama','$tmpt_lhr',TO_DATE('$tgl_lhr','DD-MM-YYYY'),$umur,'$jenis_klmin','$jenis_pkrjn',$no_prop,$no_kab,$no_kec,$no_kel,'$alamat',$rt,$rw,'$user_name',SYSDATE)";
			$r = DB::insert($sql);
			return $r;
		}
		public function plb_edit($plb,$nik,$user_name){
			$sql = "UPDATE SIAK_PLB SET 
					NO_PLB = '$plb'
					, UPDATED_BY = '$user_name'
					, UPDATED_DT = SYSDATE
					WHERE NIK = $nik";
			$r = DB::update($sql);
			return $r;
		}
		public function plb_delete($nik){
			$sql = "DELETE FROM SIAK_PLB 
					WHERE NIK = $nik";
			$r = DB::update($sql);
			return $r;
		}
		public function plb_cek_rekap_lintas_batas($nik,$nama){
			$sql ="";
			$sql .= " SELECT NO_PLB, CASE WHEN NAMA_LGKP IS NULL THEN '-' ELSE NAMA_LGKP END AS NAMA_LGKP, NIK, TMPT_LHR, TO_CHAR(TGL_LHR,'DD-MM-YYYY') TGL_LHR,UMUR,JENIS_KLMIN,JENIS_PKRJN, CONCAT(CONCAT(TMPT_LHR,' / '),TO_CHAR(TGL_LHR,'DD-MM-YYYY')) TTL,ALAMAT, RT, RW, NO_PROP, F5_GET_NAMA_PROVINSI(NO_PROP) NAMA_PROP, NO_KAB, F5_GET_NAMA_KABUPATEN(NO_PROP,NO_KAB) NAMA_KAB, NO_KEC, F5_GET_NAMA_KECAMATAN(NO_PROP,NO_KAB,NO_KEC) NAMA_KEC, NO_KEL, F5_GET_NAMA_KELURAHAN(NO_PROP,NO_KAB,NO_KEC,NO_KEL) NAMA_KEL, TO_CHAR(CREATED_DT,'DD-MM-YYYY') AS CREATED_DT, CREATED_BY FROM SIAK_PLB WHERE 1=1";
			if ($nik != ''){
				$sql .= " AND NIK IN ($nik)";	
			}
			if ($nama != ''){
				$sql .= " AND UPPER(NAMA_LGKP) LIKE '".$nama."'";
			}
			$r = DB::select($sql);
			return $r;
		}
		public function do_ph_get_nik($nik){
			$sql = "SELECT COUNT(1) AS JML FROM BIODATA_WNI@DB222 WHERE NIK = $nik";
			$r = DB::select($sql);
			return $r[0]->jml;
		}
		public function do_ph_get_data($nik){
			$sql = "SELECT COUNT(1) AS JML FROM SIAK_PETUGAS_KHUSUS WHERE NIK = $nik";
			$r = DB::select($sql);
			return $r[0]->jml;
		}
		public function do_ph_get_data_nik($nik){
			$sql = "SELECT 
					TO_CHAR(B.NIK) AS NIK
					, B.NAMA_LGKP
					, B.TMPT_LHR, TO_CHAR(B.TGL_LHR,'DD-MM-YYYY') AS TGL_LHR
					, TRUNC(MONTHS_BETWEEN(SYSDATE,B.TGL_LHR)/12) UMUR
					, UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(B.JENIS_KLMIN,7), 801)) JENIS_KLMIN
					, CASE WHEN C.ALAMAT IS NULL THEN '-' ELSE C.ALAMAT END AS ALAMAT
					, B.NO_PROP, F5_GET_NAMA_PROVINSI(B.NO_PROP) NAMA_PROP
					, B.NO_KAB, F5_GET_NAMA_KABUPATEN(B.NO_PROP,B.NO_KAB) NAMA_KAB
					, B.NO_KEC, F5_GET_NAMA_KECAMATAN(B.NO_PROP,B.NO_KAB,B.NO_KEC) NAMA_KEC
					, B.NO_KEL, F5_GET_NAMA_KELURAHAN(B.NO_PROP,B.NO_KAB,B.NO_KEC,B.NO_KEL) NAMA_KEL
					, CASE WHEN C.NO_RW IS NULL THEN 0 ELSE C.NO_RW END AS RW 
					, CASE WHEN C.NO_RT IS NULL THEN 0 ELSE C.NO_RT END AS RT 
					, UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(B.AGAMA,7), 501)) AGAMA
					, UPPER(F5_GET_REF_WNI(B.STAT_KWN, 601)) STATUS_KAWIN
					, UPPER(F5_GET_REF_WNI(B.JENIS_PKRJN, 201)) PEKERJAAN
					, UPPER(F5_GET_REF_WNI(B.GOL_DRH, 401)) GOL_DRH
					FROM BIODATA_WNI B 
					LEFT JOIN DATA_KELUARGA C ON B.NO_KK = C.NO_KK
					WHERE 1=1
					AND (B.TGL_LHR<=ADD_MONTHS(SYSDATE,-12*17) OR B.STAT_KWN<>1) 
					AND (B.FLAG_STATUS = '0' OR (B.FLAG_STATUS = '2' AND B.FLAG_PINDAH IN(1,2,3))) 
					AND B.NIK = $nik";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}
		public function ph_save($ph,$nik,$nama,$umur,$tgl_lhr,$tmpt_lhr,$jenis_klmin,$jenis_pkrjn,$no_prop,$no_kab,$no_kec,$no_kel,$alamat,$rt,$rw,$user_name){
			$sql = "INSERT INTO SIAK_PETUGAS_KHUSUS (NO_PETUGAS_KHUSUS ,NIK ,NAMA_LGKP ,TMPT_LHR ,TGL_LHR ,UMUR ,JENIS_KLMIN ,JENIS_PKRJN ,NO_PROP ,NO_KAB ,NO_KEC ,NO_KEL ,ALAMAT ,RW ,RT ,CREATED_BY ,CREATED_DT) VALUES ('$ph',$nik,'$nama','$tmpt_lhr',TO_DATE('$tgl_lhr','DD-MM-YYYY'),$umur,'$jenis_klmin','$jenis_pkrjn',$no_prop,$no_kab,$no_kec,$no_kel,'$alamat',$rt,$rw,'$user_name',SYSDATE)";
			$r = DB::insert($sql);
			return $r;
		}
		public function ph_edit($ph,$nik,$user_name){
			$sql = "UPDATE SIAK_PETUGAS_KHUSUS SET 
					NO_PETUGAS_KHUSUS = '$ph'
					, UPDATED_BY = '$user_name'
					, UPDATED_DT = SYSDATE
					WHERE NIK = $nik";
			$r = DB::update($sql);
			return $r;
		}
		public function ph_delete($nik){
			$sql = "DELETE FROM SIAK_PETUGAS_KHUSUS 
					WHERE NIK = $nik";
			$r = DB::update($sql);
			return $r;
		}
		public function ph_cek_rekap_petugas_khusus($nik,$nama){
			$sql ="";
			$sql .= " SELECT NO_PETUGAS_KHUSUS NO_PH, CASE WHEN NAMA_LGKP IS NULL THEN '-' ELSE NAMA_LGKP END AS NAMA_LGKP, NIK, TMPT_LHR, TO_CHAR(TGL_LHR,'DD-MM-YYYY') TGL_LHR,UMUR,JENIS_KLMIN,JENIS_PKRJN, CONCAT(CONCAT(TMPT_LHR,' / '),TO_CHAR(TGL_LHR,'DD-MM-YYYY')) TTL,ALAMAT, RT, RW, NO_PROP, F5_GET_NAMA_PROVINSI(NO_PROP) NAMA_PROP, NO_KAB, F5_GET_NAMA_KABUPATEN(NO_PROP,NO_KAB) NAMA_KAB, NO_KEC, F5_GET_NAMA_KECAMATAN(NO_PROP,NO_KAB,NO_KEC) NAMA_KEC, NO_KEL, F5_GET_NAMA_KELURAHAN(NO_PROP,NO_KAB,NO_KEC,NO_KEL) NAMA_KEL, TO_CHAR(CREATED_DT,'DD-MM-YYYY') AS CREATED_DT, CREATED_BY FROM SIAK_PETUGAS_KHUSUS WHERE 1=1";
			if ($nik != ''){
				$sql .= " AND NIK IN ($nik)";	
			}
			if ($nama != ''){
				$sql .= " AND UPPER(NAMA_LGKP) LIKE '".$nama."'";
			}
			$r = DB::select($sql);
			return $r;
		}
		
}
