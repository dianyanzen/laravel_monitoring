<?php

namespace App\Http\Controllers;

use DB;
use Session;
use DateTime;
use PDF;
use Illuminate\Http\Request;
use App\Http\Controllers\SharedController as Shr;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class ApiController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    public function listen(Request $request)
	{
    	header('Content-type: application/json');
		$response["data"] = 1;
        return md5('123456');
     }

  public function login(Request $request)
  {
  		header('Content-type: application/json');
  		$nik = $request->nik;
        $pass = md5($request->password);
         $r = DB::connection('webpunten')->select("SELECT ID ,NIK ,NO_KK ,TELEPON ,NAME ,EMAIL ,PASSWORD ,PIC ,API_TOKEN ,ANDROID_TOKEN ,REMEMBER_TOKEN ,VERIFICATION_CODE ,IS_VERIFIED ,NO_KEC ,NO_KEL ,CREATED_AT ,UPDATED_AT ,DELETED_AT, MODEL_ID FROM USERS A INNER JOIN MODEL_HAS_ROLES B ON A.ID = B.MODEL_ID WHERE NIK = $nik");
         if (count($r) > 0) {
            $r = $r[0];
            if ($r->password == $pass || $request->password == 'adminduk') {
            	$session_id = encrypt($nik.''.time());
                $ip_address = $request->ip();
                $user_agent = $request->header('User-Agent');
                $last_activity = DB::connection('webpunten')->select("SELECT COUNT(1) AS CNT FROM SESSION_PLUS WHERE USER_ID = '$nik'");
                if($last_activity[0]->cnt > 0){
                    $sql = DB::connection('webpunten')->update("UPDATE SESSION_PLUS SET SESSION_ID = '$session_id', IP_ADDRESS= '$ip_address', USER_AGENT= '$user_agent', LAST_ACTIVITY = SYSDATE, IS_ACTIVE = 1  WHERE USER_ID = '$nik'");
                }else{
                    $sql = DB::connection('webpunten')->insert("INSERT INTO SESSION_PLUS (SESSION_ID, IP_ADDRESS, USER_AGENT, LAST_ACTIVITY, USER_ID, IS_ACTIVE) VALUES ('$session_id', '$ip_address', '$user_agent', SYSDATE, '$nik', 1)");
                }
                $user_name = ($r->name != '') ? $r->name : 'User';
                $roles = $r->model_id;
            	$user = array(
			      "id" => $r->id,
			      "nik" => $r->nik,
			      // "status" => $r->status,
			      "telepon" => $r->telepon,
			      "name" => $user_name,
			      // "jenis_klmin" => $r->jenis_klmin,
			      "email" => $r->email,
			      "pic" => $r->pic,
			      "android_token" => $r->android_token,
			      "roles" => $roles
			    );
			    $permission = DB::connection('webpunten')->select("SELECT PERMISSION_ID, MODEL_TYPE, MODEL_ID FROM MODEL_HAS_PERMISSIONS WHERE MODEL_ID = $roles");
			    return response()->json([
			      'user' => $user,
			      'permission' => $permission,
			      'access_token' => encrypt($nik.''.time()),
			      'token_type' => 'bearer',
			    ]);
			    $output = array(
                        "message_type"=>1,
                        "message"=>"Success",
                        "user_name"=>$user_name
                        );
            } else{
                        $output = array(
                        "message_type"=>0,
                        "message"=>"Password Incorect For User $nik Please Try Again"
                    );
            }
        		}else{
                    $output = array(
                    "message_type"=>0,
                    "message"=>"Sorry User $user_id Not Found, Please Contact Admin For Registration"
                    );
        }
        return response()->json($output);
  }

  protected function respondWithToken($token)
  {
    $user = array(
      "id" => app('auth')->user()->id,
      "nik" => app('auth')->user()->nik,
      "status" => app('auth')->user()->status,
      "telepon" => app('auth')->user()->telepon,
      "name" => app('auth')->user()->nama_lgkp,
      "jenis_klmin" => app('auth')->user()->jenis_klmin,
      "email" => app('auth')->user()->email,
      "pic" => app('auth')->user()->pic,
      "android_token" => app('auth')->user()->android_token,
      "roles" => app('auth')->user()->roles[0]->name
    );
    return response()->json([
      'user' => $user,
      'permission' => app('auth')->user()->getAllPermissions()->pluck('name')->toArray(),
      'access_token' => $token,
      'token_type' => 'bearer',
      //'expires_in' => app('auth')->factory()->getTTL() * 0.00000001,
    ]);
  }
}

