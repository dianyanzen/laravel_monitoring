<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use Session;
use DateTime;
use PDF;
use Validator;
use App\User; 
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\cache;
use App\Http\Controllers\SharedController as Shr;

// use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class AndroidApiController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
   public $successStatus = 200;

   public function get_data_suket(Request $request)
	{
		header('Content-type: application/json');
		$nik = $request->nik;
		$count = $this->get_count_suket($nik);	
		if ($count >0){
			$output = $this->get_data_suket_andro($nik);
			$data["NIK"] = $output[0]->nik;
			$data["NO_KK"] = $output[0]->no_kk;
			$data["NAMA_LGKP"] = $output[0]->nama_lgkp;
			$data["TMPT_LHR"] = $output[0]->tmpt_lhr;
			$data["TGL_LHR"] = $output[0]->tgl_lhr;
			$data["JENIS_KLMIN"] = $output[0]->jenis_klmin;
			$data["STAT_HBKEL"] = $output[0]->stat_hbkel;
			$data["AKTA_LHR"] = $output[0]->akta_lhr;
			$data["NO_AKTA_LHR"] = $output[0]->no_akta_lhr;
			$data["ALAMAT"] = $output[0]->alamat;
			$data["RT"] = $output[0]->rt;
			$data["RW"] = $output[0]->rw;
			$data["NAMA_KEC"] = $output[0]->nama_kec;
			$data["NAMA_KEL"] = $output[0]->nama_kel;
			$data["AGAMA"] = $output[0]->agama;
			$data["JENIS_PKRJN"] = $output[0]->jenis_pkrjn;
			$data["STAT_KWN"] = $output[0]->stat_kwn;
			$data["NO_AKTA_KWN"] = $output[0]->no_akta_kwn;
			$data["CURRENT_STATUS_CODE"] = $output[0]->current_status_code;
			$data["SUKET_BY"] = $output[0]->suket_by;
			$data["SUKET_DT"] = $output[0]->suket_dt;
			$data["KTP_BY"] = ($output[0]->ktp_by == '32730000yanzen') ? '32730000dns' : $output[0]->ktp_by;
			$data["KTP_DT"] = $output[0]->ktp_dt;
			$response["data"] = $data;
			$response["jumlah"] = $count;
		}else{
			$response["data"] = "";
			$response["jumlah"] = 0;
		}
        return $response;
	}
	public function listen()
	{
		header('Content-type: application/json');
			$response["data"] = 1;
        return $response;
	}

    //Region Api Dashboard
    public function get_dashboard_time()
	{
		header('Content-type: application/json');
		$get_dashboard = DB::select("SELECT TO_CHAR(MAX(CREATED_DT),'DD-MM-YYYY') LAST_UPDATE, TO_CHAR(MAX(CREATED_DT),'HH24:MI:SS') LAST_UPDATE_TIME,TO_CHAR(MAX(CREATED_DT),'YYYY-MM-DD HH24:MI:SS') LAST_UPDATE_FULLTIME  FROM SIAK_DASHBOARD");
		$response["dashboard_date"] = $get_dashboard[0]->last_update;
		$response["dashboard_time"] = $get_dashboard[0]->last_update_time;
		$time = new DateTime($get_dashboard[0]->last_update_fulltime);
		$diff = $time->diff(new DateTime()); 
		$minutes = ($diff->days * 24 * 60) + ($diff->h * 60) + $diff->i;
		if ($minutes > 10){
			$response["dashboard_between"] = 0;
			$response["dashboard_minutes"] = $minutes;
		}else{
			$response["dashboard_between"] = 1;
			$response["dashboard_minutes"] = $minutes;
		}

        return $response;
	}
    public function get_dashboard_m1()
	{
		header('Content-type: application/json');
		$get_dashboard = DB::select("SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_PEREKAMAN'");
		$response["perekaman_today"] = $get_dashboard[0]->jml;
        return $response;
	}
    public function get_dashboard_m2()
	{
		header('Content-type: application/json');
		$get_dashboard = DB::select("SELECT 
				CASE WHEN (CASE WHEN (SELECT (D.AMBIL-D.RUSAK-D.PENGEMBALIAN) CETAK_LUDO FROM SIAK_MONEV_BLANGKO_LUDO D WHERE TRUNC(D.TANGGAL) = TRUNC(SYSDATE)) IS NOT NULL THEN 
				(CASE WHEN B.VAL IS NULL THEN 0 ELSE B.VAL END) + 
				(SELECT (D.AMBIL-D.RUSAK-D.PENGEMBALIAN) CETAK_LUDO FROM SIAK_MONEV_BLANGKO_LUDO D WHERE TRUNC(D.TANGGAL) = TRUNC(SYSDATE)) ELSE CASE WHEN B.VAL IS NULL THEN 0 ELSE B.VAL END END) IS NULL THEN 0 ELSE (CASE WHEN  (SELECT (D.AMBIL-D.RUSAK-D.PENGEMBALIAN) CETAK_LUDO FROM SIAK_MONEV_BLANGKO_LUDO D WHERE TRUNC(D.TANGGAL) = TRUNC(SYSDATE)) IS NOT NULL THEN 
				(CASE WHEN B.VAL IS NULL THEN 0 ELSE B.VAL END) + 
				(SELECT (D.AMBIL-D.RUSAK-D.PENGEMBALIAN) CETAK_LUDO FROM SIAK_MONEV_BLANGKO_LUDO D WHERE TRUNC(D.TANGGAL) = TRUNC(SYSDATE)) ELSE CASE WHEN B.VAL IS NULL THEN 0 ELSE B.VAL END END) END AS JML
				FROM VW_SIAK_DASHBOARD B WHERE ID = 'GET_PENCETAKAN_KTP'");
		$response["pencetakan_today"] = $get_dashboard[0]->jml;
        return $response;
	}
	public function update_prr(Request $request) 
	{
			header('Content-type: application/json');
			DB::delete("DELETE FROM SIAK_DASHBOARD WHERE ID = 'GET_PRR' AND TRUNC(CREATED_DT) = TRUNC(SYSDATE)");
			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Hapus";
       		return $data;
	}
	public function update_sfe(Request $request) 
	{
			header('Content-type: application/json');
			DB::delete("DELETE FROM SIAK_DASHBOARD WHERE ID = 'GET_SFE' AND TRUNC(CREATED_DT) = TRUNC(SYSDATE)");
			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Hapus";
       		return $data;
	}
	public function get_time_m3()
	{
		header('Content-type: application/json');
		$get_dashboard = DB::select("SELECT NVL(MAX(TO_CHAR(CREATED_DT,'HH24:MI')),'00:00') JML FROM SIAK_DASHBOARD WHERE ID = 'GET_PRR' AND TRUNC(CREATED_DT) = TRUNC(SYSDATE)");
		$response["jam_prr"] = $get_dashboard[0]->jml;
        return $response;
	}
	public function get_time_m4()
	{
		header('Content-type: application/json');
		$get_dashboard = DB::select("SELECT NVL(MAX(TO_CHAR(CREATED_DT,'HH24:MI')),'00:00') JML FROM SIAK_DASHBOARD WHERE ID = 'GET_SFE' AND TRUNC(CREATED_DT) = TRUNC(SYSDATE)");
		$response["jam_sfe"] = $get_dashboard[0]->jml;
        return $response;
	}
    public function get_dashboard_m3()
	{
		header('Content-type: application/json');
		$get_dashboard = DB::select("SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_PRR'");
		$response["sisa_prr"] = $get_dashboard[0]->jml;
        return $response;
	}
    public function get_dashboard_m4()
	{
		header('Content-type: application/json');
		$get_dashboard = DB::select("SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_SFE'");
		$response["sisa_sfe"] = $get_dashboard[0]->jml;
        return $response;
	}
    public function get_dashboard_m5()
	{
		header('Content-type: application/json');
		$get_dashboard = DB::select("SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_SISA_SUKET'");
		$response["sisa_suket"] = $get_dashboard[0]->jml;
        return $response;
	}
    public function get_dashboard_m6()
	{
		header('Content-type: application/json');
		$get_dashboard = DB::select("SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_BLANGKO_OUT'");
		$response["blangko_out"] = $get_dashboard[0]->jml;
        return $response;
	}
    public function get_dashboard_m7()
	{
		header('Content-type: application/json');
		$get_dashboard = DB::select("SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_DUPLICATE'");
		$response["duplicate"] = $get_dashboard[0]->jml;
        return $response;
	}
    public function get_dashboard_m8()
	{
		header('Content-type: application/json');
		$get_dashboard = DB::select("SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_BLANGKO_SISA'");
		$response["sisa_blangko"] = $get_dashboard[0]->jml;
        return $response;
	}
    public function get_dashboard_kk()
	{
		header('Content-type: application/json');
		$get_dashboard = DB::select("SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_CETAK_KK'");
		$response["pen_kk"] = $get_dashboard[0]->jml;
        return $response;
	}
    public function get_dashboard_kia()
	{
		header('Content-type: application/json');
		$get_dashboard = DB::select("SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_CETAK_KIA'");
		$response["pen_kia"] = $get_dashboard[0]->jml;
        return $response;
	}
    public function get_dashboard_nik()
	{
		header('Content-type: application/json');
		$get_dashboard = DB::select("SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_NIK_BARU'");
		$response["pen_nik_baru"] = $get_dashboard[0]->jml;
        return $response;
	}
    public function get_dashboard_akta_lu()
	{
		header('Content-type: application/json');
		$get_dashboard = DB::select("SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_AKTA_LHR_UM'");
		$response["pen_lahir_lu"] = $get_dashboard[0]->jml;
        return $response;
	}
    public function get_dashboard_akta_lt()
	{
		header('Content-type: application/json');
		$get_dashboard = DB::select("SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_AKTA_LHR_LT'");
		$response["pen_lahir_lt"] = $get_dashboard[0]->jml;
        return $response;
	}
    public function get_dashboard_akta_mt()
	{
		header('Content-type: application/json');
		$get_dashboard = DB::select("SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_AKTA_MT'");
		$response["pen_mati"] = $get_dashboard[0]->jml;
        return $response;
	}
    public function get_dashboard_akta_kwn()
	{
		header('Content-type: application/json');
		$get_dashboard = DB::select("SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_AKTA_KWN'");
		$response["pen_kawin"] = $get_dashboard[0]->jml;
        return $response;
	}
    public function get_dashboard_akta_cry()
	{
		header('Content-type: application/json');
		$get_dashboard = DB::select("SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_AKTA_CRY'");
		$response["pen_cerai"] = $get_dashboard[0]->jml;
        return $response;
	}
    public function get_dashboard_pdh_akab()
	{
		header('Content-type: application/json');
		$get_dashboard = DB::select("SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_PINDAH_ANTAR_KAB'");
		$response["pen_pindah_akab"] = $get_dashboard[0]->jml;
        return $response;
	}
    public function get_dashboard_pdh_akec()
	{
		header('Content-type: application/json');
		$get_dashboard = DB::select("SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_PINDAH_ANTAR_KEC'");
		$response["pen_pindah_akec"] = $get_dashboard[0]->jml;
        return $response;
	}
    public function get_dashboard_pdh_dkec()
	{
		header('Content-type: application/json');
		$get_dashboard = DB::select("SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_PINDAH_DALAM_KEC'");
		$response["pen_pindah_dkec"] = $get_dashboard[0]->jml;
        return $response;
	}
    public function get_dashboard_dtg_akab()
	{
		header('Content-type: application/json');
		$get_dashboard = DB::select("SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_DATANG_ANTAR_KAB'");
		$response["pen_datang_akab"] = $get_dashboard[0]->jml;
        return $response;
	}
    public function get_dashboard_dtg_akec()
	{
		header('Content-type: application/json');
		$get_dashboard = DB::select("SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_DATANG_ANTAR_KEC'");
		$response["pen_datang_akec"] = $get_dashboard[0]->jml;
        return $response;
	}
    public function get_dashboard_dtg_dkec()
	{
		header('Content-type: application/json');
		$get_dashboard = DB::select("SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_DATANG_DALAM_KEC'");
		$response["pen_datang_dkec"] = $get_dashboard[0]->jml;
        return $response;
	}
	//End Region

	//Region Api Dashboard Ktpel
	public function get_data_rkm()
	{
		header('Content-type: application/json');
		 $sql = "SELECT 
                        SUM( CASE WHEN TO_CHAR(CREATED,'DD/MM/YYYY') = 
                        TO_CHAR(SYSDATE,'DD/MM/YYYY') THEN 1 ELSE 0 END) AS TODAY 
                        , SUM( CASE WHEN CURRENT_STATUS_CODE = 'BIO_CAPTURED' AND TO_CHAR(CREATED,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') THEN 1 ELSE 0 END) AS N_BIO_CAPTURED
                        , SUM( CASE WHEN CURRENT_STATUS_CODE = 'SENT_FOR_ENROLLMENT' AND TO_CHAR(CREATED,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') THEN 1 ELSE 0 END) AS N_SENT_FOR_ENROLLMENT
                        , SUM( CASE WHEN CURRENT_STATUS_CODE = 'PRINT_READY_RECORD' AND TO_CHAR(CREATED,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') THEN 1 ELSE 0 END) AS N_PRINT_READY_RECORD
                        , SUM( CASE WHEN CURRENT_STATUS_CODE = 'DUPLICATE_RECORD' AND TO_CHAR(CREATED,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') THEN 1 ELSE 0 END) AS N_DUPLICATE_RECORD
                        , SUM( CASE WHEN CURRENT_STATUS_CODE = 'PROCESSING' AND TO_CHAR(CREATED,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') THEN 1 ELSE 0 END) AS N_PROCESSING
                        , SUM( CASE WHEN CURRENT_STATUS_CODE = 'ADJUDICATE_RECORD' AND TO_CHAR(CREATED,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') THEN 1 ELSE 0 END) AS N_ADJUDICATE_RECORD
                        , SUM( CASE WHEN CURRENT_STATUS_CODE = 'ENROLL_FAILURE_AT_CENTRAL' AND TO_CHAR(CREATED,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') THEN 1 ELSE 0 END) AS N_ENROLL_FAILURE_AT_CENTRAL
                        , SUM(CASE WHEN CURRENT_STATUS_CODE <> 'SENT_FOR_ENROLLMENT' AND CURRENT_STATUS_CODE <> 'BIO_CAPTURED' AND CURRENT_STATUS_CODE <> 'DUPLICATE_RECORD' AND CURRENT_STATUS_CODE <> 'PRINT_READY_RECORD' AND CURRENT_STATUS_CODE <> 'PROCESSING' AND CURRENT_STATUS_CODE <> 'ADJUDICATE_RECORD' AND CURRENT_STATUS_CODE <> 'ENROLL_FAILURE_AT_CENTRAL' AND TO_CHAR(CREATED,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') THEN 1 ELSE 0 END) AS N_OTHER 
                        , SUM( CASE WHEN TO_CHAR(CREATED,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') THEN 1 ELSE 0 END) AS YESTERDAY
                        , SUM( CASE WHEN CURRENT_STATUS_CODE = 'BIO_CAPTURED' AND TO_CHAR(CREATED,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') THEN 1 ELSE 0 END) AS Y_BIO_CAPTURED
                        , SUM( CASE WHEN CURRENT_STATUS_CODE = 'SENT_FOR_ENROLLMENT' AND TO_CHAR(CREATED,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') THEN 1 ELSE 0 END) AS Y_SENT_FOR_ENROLLMENT
                        , SUM( CASE WHEN CURRENT_STATUS_CODE = 'PRINT_READY_RECORD' AND TO_CHAR(CREATED,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') THEN 1 ELSE 0 END) AS Y_PRINT_READY_RECORD
                        , SUM( CASE WHEN CURRENT_STATUS_CODE = 'DUPLICATE_RECORD' AND TO_CHAR(CREATED,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') THEN 1 ELSE 0 END) AS Y_DUPLICATE_RECORD
                        , SUM( CASE WHEN CURRENT_STATUS_CODE = 'PROCESSING' AND TO_CHAR(CREATED,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') THEN 1 ELSE 0 END) AS Y_PROCESSING
                        , SUM( CASE WHEN CURRENT_STATUS_CODE = 'ADJUDICATE_RECORD' AND TO_CHAR(CREATED,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') THEN 1 ELSE 0 END) AS Y_ADJUDICATE_RECORD
                        , SUM( CASE WHEN CURRENT_STATUS_CODE = 'ENROLL_FAILURE_AT_CENTRAL' AND TO_CHAR(CREATED,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') THEN 1 ELSE 0 END) AS Y_ENROLL_FAILURE_AT_CENTRAL
                        , SUM(CASE WHEN CURRENT_STATUS_CODE <> 'SENT_FOR_ENROLLMENT' AND CURRENT_STATUS_CODE <> 'BIO_CAPTURED' AND CURRENT_STATUS_CODE <> 'DUPLICATE_RECORD' AND CURRENT_STATUS_CODE <> 'PRINT_READY_RECORD' AND CURRENT_STATUS_CODE <> 'PROCESSING' AND CURRENT_STATUS_CODE <> 'ADJUDICATE_RECORD' AND CURRENT_STATUS_CODE <> 'ENROLL_FAILURE_AT_CENTRAL' AND TO_CHAR(CREATED,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') THEN 1 ELSE 0 END) AS Y_OTHER 
                        FROM DEMOGRAPHICS WHERE EXISTS(SELECT 1 FROM FACES WHERE DEMOGRAPHICS.NIK = FACES.NIK)";
		 $r = DB::connection('db221')->select($sql);
            $output = array(
               "rekam_today"=>$r[0]->today,
                "rekam_yesterday"=>$r[0]->yesterday,
                "bio_today"=>$r[0]->n_bio_captured,
                "bio_yesterday"=>$r[0]->y_bio_captured,
                "sfe_today"=>$r[0]->n_sent_for_enrollment,
                "sfe_yesterday"=>$r[0]->y_sent_for_enrollment,
                "prr_today"=>$r[0]->n_print_ready_record,
                "prr_yesterday"=>$r[0]->y_print_ready_record,
                "durec_today"=>$r[0]->n_duplicate_record,
                "durec_yesterday"=>$r[0]->y_duplicate_record,
                "process_today"=>$r[0]->n_processing,
                "process_yesterday"=>$r[0]->y_processing,
                "adjudicate_today"=>$r[0]->n_adjudicate_record,
                "adjudicate_yesterday"=>$r[0]->y_adjudicate_record,
                "enroll_failure_today"=>$r[0]->n_enroll_failure_at_central,
                "enroll_failure_yesterday"=>$r[0]->y_enroll_failure_at_central,
                "other_today"=>$r[0]->n_other,
                "other_yesterday"=>$r[0]->y_other
            );
        return $output;
	}
	public function get_data_ctk()
	{
		header('Content-type: application/json');
		 $sql = "SELECT 
                        SUM( CASE WHEN (CASE WHEN TO_CHAR(A.LAST_UPDATE,'DD/MM/YYYY') IS NULL THEN TO_CHAR(A.PERSONALIZED_DATE,'DD/MM/YYYY') ELSE TO_CHAR(A.LAST_UPDATE,'DD/MM/YYYY') END =  TO_CHAR(SYSDATE,'DD/MM/YYYY')) THEN 1 ELSE 0 END) AS HARI_INI, 
                        SUM( CASE WHEN (CASE WHEN TO_CHAR(A.LAST_UPDATE,'DD/MM/YYYY') IS NULL THEN TO_CHAR(A.PERSONALIZED_DATE,'DD/MM/YYYY') ELSE TO_CHAR(A.LAST_UPDATE,'DD/MM/YYYY') END =  TO_CHAR(SYSDATE-1,'DD/MM/YYYY')) THEN 1 ELSE 0 END) AS KEMARIN
                        FROM CARD_MANAGEMENT@DB2 A INNER JOIN BIODATA_WNI B ON A.NIK = B.NIK LEFT JOIN SIAK_CETAK_GOIB@YZDB C ON  CASE WHEN UPPER(A.LAST_UPDATED_USERNAME) IS NULL THEN UPPER(A.CREATED_USERNAME) ELSE UPPER(A.LAST_UPDATED_USERNAME) END =  UPPER(C.USER_BCARD) AND C.IS_ACTIVE = 1  WHERE 1=1 AND C.USER_BCARD IS NULL
                        ";
		 $r = DB::connection('db222')->select($sql);
		 $output = array(
            "cetak_today"=>$r[0]->hari_ini,
            "cetak_yesterday"=>$r[0]->kemarin
         );
        return $output;
	}
	public function get_data_suket_dashboard()
	{
		header('Content-type: application/json');
		$sql = "SELECT 
                            (SELECT COUNT(DISTINCT(A.NIK)) AS JUMLAH FROM T7_HIST_SUKET A WHERE TO_CHAR(A.PRINTED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY')) N_SUKET,
                            (SELECT COUNT(DISTINCT(A.NIK)) AS JUMLAH FROM T7_HIST_SUKET A WHERE TO_CHAR(A.PRINTED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND NOT EXISTS(SELECT 1 FROM SIAK_REQ_CETAK_KTP@YZDB B WHERE A.NIK = B.NIK AND TO_CHAR(B.REQ_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY')) AND NOT EXISTS(SELECT 1 FROM SIAK_REQ_CETAK_KTP@YZDB B WHERE A.NIK = B.NIK AND TO_CHAR(B.REQ_DATE,'DD/MM/YYYY') <> TO_CHAR(SYSDATE,'DD/MM/YYYY'))) N_NREQ_JUMLAH,
                            (SELECT COUNT(DISTINCT(A.NIK)) AS JUMLAH FROM T7_HIST_SUKET A WHERE TO_CHAR(A.PRINTED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND NOT EXISTS(SELECT 1 FROM SIAK_REQ_CETAK_KTP@YZDB B WHERE A.NIK = B.NIK AND TO_CHAR(B.REQ_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY')) AND EXISTS(SELECT 1 FROM SIAK_REQ_CETAK_KTP@YZDB B WHERE A.NIK = B.NIK AND TO_CHAR(B.REQ_DATE,'DD/MM/YYYY') <> TO_CHAR(SYSDATE,'DD/MM/YYYY'))) N_DREQ_JUMLAH,
                            (SELECT COUNT(DISTINCT(A.NIK)) AS JUMLAH  FROM T7_HIST_SUKET A WHERE TO_CHAR(A.PRINTED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND EXISTS(SELECT 1 FROM SIAK_REQ_CETAK_KTP@YZDB B WHERE A.NIK = B.NIK AND TO_CHAR(B.REQ_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY'))) AS N_REQ_CETAK,
                            (SELECT COUNT(DISTINCT(A.NIK)) AS JUMLAH  FROM SIAK_REQ_CETAK_KTP@YZDB A WHERE TO_CHAR(A.REQ_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY')) AS N_REQ_JUMLAH,
                            (SELECT COUNT(DISTINCT(A.NIK)) AS JUMLAH  FROM SIAK_REQ_CETAK_KTP@YZDB A WHERE TO_CHAR(A.REQ_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND NOT EXISTS(SELECT 1 FROM T7_HIST_SUKET B WHERE A.NIK = B.NIK AND TO_CHAR(B.PRINTED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY'))) AS N_REQ_BELUM,
                            (SELECT COUNT(DISTINCT(A.NIK)) AS JUMLAH FROM T7_HIST_SUKET A WHERE TO_CHAR(A.PRINTED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY')) Y_SUKET,
                            (SELECT COUNT(DISTINCT(A.NIK)) AS JUMLAH FROM T7_HIST_SUKET A WHERE TO_CHAR(A.PRINTED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND NOT EXISTS(SELECT 1 FROM SIAK_REQ_CETAK_KTP@YZDB B WHERE A.NIK = B.NIK AND TO_CHAR(B.REQ_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY')) AND NOT EXISTS(SELECT 1 FROM SIAK_REQ_CETAK_KTP@YZDB B WHERE A.NIK = B.NIK AND TO_CHAR(B.REQ_DATE,'DD/MM/YYYY') <> TO_CHAR(SYSDATE-1,'DD/MM/YYYY'))) Y_NREQ_JUMLAH,
                            (SELECT COUNT(DISTINCT(A.NIK)) AS JUMLAH FROM T7_HIST_SUKET A WHERE TO_CHAR(A.PRINTED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND NOT EXISTS(SELECT 1 FROM SIAK_REQ_CETAK_KTP@YZDB B WHERE A.NIK = B.NIK AND TO_CHAR(B.REQ_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY')) AND EXISTS(SELECT 1 FROM SIAK_REQ_CETAK_KTP@YZDB B WHERE A.NIK = B.NIK AND TO_CHAR(B.REQ_DATE,'DD/MM/YYYY') <> TO_CHAR(SYSDATE-1,'DD/MM/YYYY'))) Y_DREQ_JUMLAH,
                            (SELECT COUNT(DISTINCT(A.NIK)) AS JUMLAH  FROM T7_HIST_SUKET A WHERE TO_CHAR(A.PRINTED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND EXISTS(SELECT 1 FROM SIAK_REQ_CETAK_KTP@YZDB B WHERE A.NIK = B.NIK AND TO_CHAR(B.REQ_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY'))) AS Y_REQ_CETAK,
                            (SELECT COUNT(DISTINCT(A.NIK)) AS JUMLAH  FROM SIAK_REQ_CETAK_KTP@YZDB A WHERE TO_CHAR(A.REQ_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY')) AS Y_REQ_JUMLAH,
                            (SELECT COUNT(DISTINCT(A.NIK)) AS JUMLAH  FROM SIAK_REQ_CETAK_KTP@YZDB A WHERE TO_CHAR(A.REQ_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND NOT EXISTS(SELECT 1 FROM T7_HIST_SUKET B WHERE A.NIK = B.NIK AND TO_CHAR(B.PRINTED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY'))) AS Y_REQ_BELUM
                            FROM DUAL";
		 $r = DB::connection('db222')->select($sql);
		  $output = array(
                   "suket_today"=>$r[0]->n_suket,
                    "suket_yesterday"=>$r[0]->y_suket,
                    "dreq_jumlah_yesterday"=>$r[0]->y_dreq_jumlah,
                    "dreq_jumlah_today"=>$r[0]->n_dreq_jumlah,
                    "nreq_jumlah_today"=>$r[0]->n_nreq_jumlah,
                    "nreq_jumlah_yesterday"=>$r[0]->y_nreq_jumlah,
                    "req_jumlah_today"=>$r[0]->n_req_jumlah,
                    "req_jumlah_yesterday"=>$r[0]->y_req_jumlah,
                    "req_cetak_today"=>$r[0]->n_req_cetak,
                    "req_cetak_yesterday"=>$r[0]->y_req_cetak,
                    "req_belum_today"=>$r[0]->n_req_belum,
                    "req_belum_yesterday"=>$r[0]->y_req_belum
                );
        return $output;
	}
	public function get_data_kia()
	{
		header('Content-type: application/json');
		$sql = "SELECT 
                        SUM( CASE WHEN TO_CHAR(A.PRINTED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') THEN 1 ELSE 0 END) AS HARI_INI, 
                        SUM( CASE WHEN TO_CHAR(A.PRINTED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') THEN 1 ELSE 0 END) AS KEMARIN
                        FROM T5_SEQN_KIA_PRINT A WHERE EXISTS(SELECT 1 FROM BIODATA_WNI B WHERE A.NIK =B.NIK) 
                        ";
		 $r = DB::connection('db222')->select($sql);
		 $output = array(
                   "kia_today"=>$r[0]->hari_ini,
                    "kia_yesterday"=>$r[0]->kemarin
                );
        return $output;
	}

	public function dashboard_biodata()
	{
		header('Content-type: application/json');
		 $sql = "SELECT
                        (SELECT COUNT(DISTINCT(B.NO_KK)) AS JUMLAH from DATA_KELUARGA B INNER JOIN BIODATA_WNI A ON A.NO_KK = B.NO_KK WHERE 
                        A.FLAG_STATUS = 0 AND TO_CHAR(B.TGL_INSERTION,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY')) N_KK,
                        (select COUNT(1) AS COUNT from BIODATA_WNI WHERE TO_CHAR(TGL_ENTRI,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY')) N_BIO,
                        (select COUNT(DISTINCT(B.NO_KK)) AS JUMLAH from DATA_KELUARGA B INNER JOIN BIODATA_WNI A ON A.NO_KK = B.NO_KK WHERE 
                        A.FLAG_STATUS = 0 AND TO_CHAR(B.TGL_INSERTION,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY')) Y_KK,
                        (select COUNT(1) AS COUNT from BIODATA_WNI WHERE TO_CHAR(TGL_ENTRI,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY')) Y_BIO
                        FROM DUAL
                        ";
		 $r = DB::connection('db222')->select($sql);
		$output = array(
                   "kk_n"=>$r[0]->n_kk,
                   "bio_n"=>$r[0]->n_bio,
                   "kk_y"=>$r[0]->y_kk,
                   "bio_y"=>$r[0]->y_bio
                  
                );
        return $output;
	}

	public function dashboard_mobilitas()
	{
		header('Content-type: application/json');
		 $sql = "SELECT
                        (SELECT COUNT(DISTINCT(A.NO_PINDAH)) AS COUNT FROM PINDAH_HEADER A INNER JOIN PINDAH_DETAIL B ON A.NO_PINDAH = B.NO_PINDAH
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.FROM_NO_PROP = 32 AND A.FROM_NO_KAB = 73) N_PINDAH_H,
                        (SELECT COUNT(A.NO_PINDAH) AS COUNT FROM PINDAH_HEADER A INNER JOIN PINDAH_DETAIL B ON A.NO_PINDAH = B.NO_PINDAH
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.FROM_NO_PROP = 32 AND A.FROM_NO_KAB = 73) N_PINDAH_D,
                        (SELECT COUNT(DISTINCT(A.NO_DATANG)) AS COUNT FROM DATANG_HEADER A INNER JOIN DATANG_DETAIL B ON A.NO_DATANG = B.NO_DATANG
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.NO_PROP = 32 AND A.NO_KAB = 73) N_DATANG_H,
                        (SELECT COUNT(A.NO_DATANG) AS COUNT FROM DATANG_HEADER A INNER JOIN DATANG_DETAIL B ON A.NO_DATANG = B.NO_DATANG
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.NO_PROP = 32 AND A.NO_KAB = 73) N_DATANG_D,
                        (SELECT COUNT(DISTINCT(A.NO_PINDAH)) AS COUNT FROM PINDAH_HEADER A INNER JOIN PINDAH_DETAIL B ON A.NO_PINDAH = B.NO_PINDAH
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND A.FROM_NO_PROP = 32 AND A.FROM_NO_KAB = 73) Y_PINDAH_H,
                        (SELECT COUNT(A.NO_PINDAH) AS COUNT FROM PINDAH_HEADER A INNER JOIN PINDAH_DETAIL B ON A.NO_PINDAH = B.NO_PINDAH
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND A.FROM_NO_PROP = 32 AND A.FROM_NO_KAB = 73) Y_PINDAH_D,
                        (SELECT COUNT(DISTINCT(A.NO_DATANG)) AS COUNT FROM DATANG_HEADER A INNER JOIN DATANG_DETAIL B ON A.NO_DATANG = B.NO_DATANG
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND A.NO_PROP = 32 AND A.NO_KAB = 73) Y_DATANG_H,
                        (SELECT COUNT(A.NO_DATANG) AS COUNT FROM DATANG_HEADER A INNER JOIN DATANG_DETAIL B ON A.NO_DATANG = B.NO_DATANG
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND A.NO_PROP = 32 AND A.NO_KAB = 73) Y_DATANG_D
                        FROM DUAL
                        ";
		 $r = DB::connection('db222')->select($sql);
		$output = array(
                   "pindah_hn"=>$r[0]->n_pindah_h,
                   "pindah_dn"=>$r[0]->n_pindah_d,
                   "datang_hn"=>$r[0]->n_datang_h,
                   "datang_dn"=>$r[0]->n_datang_d,
                   "pindah_hy"=>$r[0]->y_pindah_h,
                   "pindah_dy"=>$r[0]->y_pindah_d,
                   "datang_hy"=>$r[0]->y_datang_h,
                   "datang_dy"=>$r[0]->y_datang_d
                );
        return $output;
	}

	public function dashboard_mobilitas_detail()
	{
		header('Content-type: application/json');
		 $sql = "SELECT
                        (SELECT COUNT(DISTINCT(A.NO_PINDAH)) AS COUNT FROM PINDAH_HEADER A INNER JOIN PINDAH_DETAIL B ON A.NO_PINDAH = B.NO_PINDAH
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH IN (4,5) AND A.FROM_NO_PROP = 32  AND A.FROM_NO_KAB = 73) N_PINDAH_H_ANTARKAB,
                        (SELECT COUNT(A.NO_PINDAH) AS COUNT FROM PINDAH_HEADER A INNER JOIN PINDAH_DETAIL B ON A.NO_PINDAH = B.NO_PINDAH
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH IN (4,5) AND A.FROM_NO_PROP = 32  AND A.FROM_NO_KAB = 73) N_PINDAH_D_ANTARKAB,
                            (SELECT COUNT(DISTINCT(A.NO_PINDAH)) AS COUNT FROM PINDAH_HEADER A INNER JOIN PINDAH_DETAIL B ON A.NO_PINDAH = B.NO_PINDAH
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH IN (3) AND A.FROM_NO_PROP = 32  AND A.FROM_NO_KAB = 73) N_PINDAH_H_ANTARKEC,
                        (SELECT COUNT(A.NO_PINDAH) AS COUNT FROM PINDAH_HEADER A INNER JOIN PINDAH_DETAIL B ON A.NO_PINDAH = B.NO_PINDAH
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH IN (3) AND A.FROM_NO_PROP = 32  AND A.FROM_NO_KAB = 73) N_PINDAH_D_ANTARKEC,
                            (SELECT COUNT(DISTINCT(A.NO_PINDAH)) AS COUNT FROM PINDAH_HEADER A INNER JOIN PINDAH_DETAIL B ON A.NO_PINDAH = B.NO_PINDAH
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH NOT IN (3,4,5) AND A.FROM_NO_PROP = 32  AND A.FROM_NO_KAB = 73) N_PINDAH_H_DALAMKEC,
                        (SELECT COUNT(A.NO_PINDAH) AS COUNT FROM PINDAH_HEADER A INNER JOIN PINDAH_DETAIL B ON A.NO_PINDAH = B.NO_PINDAH
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH NOT IN (3,4,5) AND A.FROM_NO_PROP = 32  AND A.FROM_NO_KAB = 73) N_PINDAH_D_DALAMKEC,
                        (SELECT COUNT(DISTINCT(A.NO_DATANG)) AS COUNT FROM DATANG_HEADER A INNER JOIN DATANG_DETAIL B ON A.NO_DATANG = B.NO_DATANG
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH IN (4,5) AND A.NO_PROP = 32  AND A.NO_KAB = 73) N_DATANG_H_ANTARKAB,
                        (SELECT COUNT(A.NO_DATANG) AS COUNT FROM DATANG_HEADER A INNER JOIN DATANG_DETAIL B ON A.NO_DATANG = B.NO_DATANG
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH IN (4,5) AND A.NO_PROP = 32  AND A.NO_KAB = 73) N_DATANG_D_ANTARKAB,
                            (SELECT COUNT(DISTINCT(A.NO_DATANG)) AS COUNT FROM DATANG_HEADER A INNER JOIN DATANG_DETAIL B ON A.NO_DATANG = B.NO_DATANG
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH IN (3) AND A.NO_PROP = 32  AND A.NO_KAB = 73) N_DATANG_H_ANTARKEC,
                        (SELECT COUNT(A.NO_DATANG) AS COUNT FROM DATANG_HEADER A INNER JOIN DATANG_DETAIL B ON A.NO_DATANG = B.NO_DATANG
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH IN (3) AND A.NO_PROP = 32  AND A.NO_KAB = 73) N_DATANG_D_ANTARKEC,
                            (SELECT COUNT(DISTINCT(A.NO_DATANG)) AS COUNT FROM DATANG_HEADER A INNER JOIN DATANG_DETAIL B ON A.NO_DATANG = B.NO_DATANG
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH NOT IN (3,4,5) AND A.NO_PROP = 32  AND A.NO_KAB = 73) N_DATANG_H_DALAMKEC,
                        (SELECT COUNT(A.NO_DATANG) AS COUNT FROM DATANG_HEADER A INNER JOIN DATANG_DETAIL B ON A.NO_DATANG = B.NO_DATANG
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH NOT IN (3,4,5) AND A.NO_PROP = 32  AND A.NO_KAB = 73) N_DATANG_D_DALAMKEC,
                        (SELECT COUNT(DISTINCT(A.NO_PINDAH)) AS COUNT FROM PINDAH_HEADER A INNER JOIN PINDAH_DETAIL B ON A.NO_PINDAH = B.NO_PINDAH
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH IN (4,5) AND A.FROM_NO_PROP = 32  AND A.FROM_NO_KAB = 73) Y_PINDAH_H_ANTARKAB,
                        (SELECT COUNT(A.NO_PINDAH) AS COUNT FROM PINDAH_HEADER A INNER JOIN PINDAH_DETAIL B ON A.NO_PINDAH = B.NO_PINDAH
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH IN (4,5) AND A.FROM_NO_PROP = 32  AND A.FROM_NO_KAB = 73) Y_PINDAH_D_ANTARKAB,
                            (SELECT COUNT(DISTINCT(A.NO_PINDAH)) AS COUNT FROM PINDAH_HEADER A INNER JOIN PINDAH_DETAIL B ON A.NO_PINDAH = B.NO_PINDAH
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH IN (3) AND A.FROM_NO_PROP = 32  AND A.FROM_NO_KAB = 73) Y_PINDAH_H_ANTARKEC,
                        (SELECT COUNT(A.NO_PINDAH) AS COUNT FROM PINDAH_HEADER A INNER JOIN PINDAH_DETAIL B ON A.NO_PINDAH = B.NO_PINDAH
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH IN (3) AND A.FROM_NO_PROP = 32  AND A.FROM_NO_KAB = 73) Y_PINDAH_D_ANTARKEC,
                            (SELECT COUNT(DISTINCT(A.NO_PINDAH)) AS COUNT FROM PINDAH_HEADER A INNER JOIN PINDAH_DETAIL B ON A.NO_PINDAH = B.NO_PINDAH
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH NOT IN (3,4,5) AND A.FROM_NO_PROP = 32  AND A.FROM_NO_KAB = 73) Y_PINDAH_H_DALAMKEC,
                        (SELECT COUNT(A.NO_PINDAH) AS COUNT FROM PINDAH_HEADER A INNER JOIN PINDAH_DETAIL B ON A.NO_PINDAH = B.NO_PINDAH
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH NOT IN (3,4,5) AND A.FROM_NO_PROP = 32  AND A.FROM_NO_KAB = 73) Y_PINDAH_D_DALAMKEC,
                        (SELECT COUNT(DISTINCT(A.NO_DATANG)) AS COUNT FROM DATANG_HEADER A INNER JOIN DATANG_DETAIL B ON A.NO_DATANG = B.NO_DATANG
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH IN (4,5) AND A.NO_PROP = 32  AND A.NO_KAB = 73) Y_DATANG_H_ANTARKAB,
                        (SELECT COUNT(A.NO_DATANG) AS COUNT FROM DATANG_HEADER A INNER JOIN DATANG_DETAIL B ON A.NO_DATANG = B.NO_DATANG
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH IN (4,5) AND A.NO_PROP = 32  AND A.NO_KAB = 73) Y_DATANG_D_ANTARKAB,
                            (SELECT COUNT(DISTINCT(A.NO_DATANG)) AS COUNT FROM DATANG_HEADER A INNER JOIN DATANG_DETAIL B ON A.NO_DATANG = B.NO_DATANG
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH IN (3) AND A.NO_PROP = 32  AND A.NO_KAB = 73) Y_DATANG_H_ANTARKEC,
                        (SELECT COUNT(A.NO_DATANG) AS COUNT FROM DATANG_HEADER A INNER JOIN DATANG_DETAIL B ON A.NO_DATANG = B.NO_DATANG
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH IN (3) AND A.NO_PROP = 32  AND A.NO_KAB = 73) Y_DATANG_D_ANTARKEC,
                            (SELECT COUNT(DISTINCT(A.NO_DATANG)) AS COUNT FROM DATANG_HEADER A INNER JOIN DATANG_DETAIL B ON A.NO_DATANG = B.NO_DATANG
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH NOT IN (3,4,5) AND A.NO_PROP = 32  AND A.NO_KAB = 73) Y_DATANG_H_DALAMKEC,
                        (SELECT COUNT(A.NO_DATANG) AS COUNT FROM DATANG_HEADER A INNER JOIN DATANG_DETAIL B ON A.NO_DATANG = B.NO_DATANG
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH NOT IN (3,4,5) AND A.NO_PROP = 32  AND A.NO_KAB = 73) Y_DATANG_D_DALAMKEC
                        FROM DUAL
                        ";
		 $r = DB::connection('db222')->select($sql);
		$output = array(
                   "pindah_hn_akab"=>$r[0]->n_pindah_h_antarkab,
                   "pindah_dn_akab"=>$r[0]->n_pindah_d_antarkab,
                   "pindah_hn_akec"=>$r[0]->n_pindah_h_antarkec,
                   "pindah_dn_akec"=>$r[0]->n_pindah_d_antarkec,
                   "pindah_hn_dkec"=>$r[0]->n_pindah_h_dalamkec,
                   "pindah_dn_dkec"=>$r[0]->n_pindah_d_dalamkec,
                   "datang_hn_akab"=>$r[0]->n_datang_h_antarkab,
                   "datang_dn_akab"=>$r[0]->n_datang_d_antarkab,
                   "datang_hn_akec"=>$r[0]->n_datang_h_antarkec,
                   "datang_dn_akec"=>$r[0]->n_datang_d_antarkec,
                   "datang_hn_dkec"=>$r[0]->n_datang_h_dalamkec,
                   "datang_dn_dkec"=>$r[0]->n_datang_d_dalamkec,
                   "pindah_hy_akab"=>$r[0]->y_pindah_h_antarkab,
                   "pindah_dy_akab"=>$r[0]->y_pindah_d_antarkab,
                   "pindah_hy_akec"=>$r[0]->y_pindah_h_antarkec,
                   "pindah_dy_akec"=>$r[0]->y_pindah_d_antarkec,
                   "pindah_hy_dkec"=>$r[0]->y_pindah_h_dalamkec,
                   "pindah_dy_dkec"=>$r[0]->y_pindah_d_dalamkec,
                   "datang_hy_akab"=>$r[0]->y_datang_h_antarkab,
                   "datang_dy_akab"=>$r[0]->y_datang_d_antarkab,
                   "datang_hy_akec"=>$r[0]->y_datang_h_antarkec,
                   "datang_dy_akec"=>$r[0]->y_datang_d_antarkec,
                   "datang_hy_dkec"=>$r[0]->y_datang_h_dalamkec,
                   "datang_dy_dkec"=>$r[0]->y_datang_d_dalamkec
                );
        return $output;
	}

	public function dashboard_capil()
	{
		header('Content-type: application/json');
		 $sql = "SELECT
                        (SELECT COUNT(1) AS COUNT FROM CAPIL_LAHIR A WHERE TO_CHAR(A.PLPR_TGL_LAPOR,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.ADM_AKTA_NO LIKE '%LU%' AND EXISTS(SELECT 1 FROM BIODATA_WNI B WHERE A.BAYI_NIK =B.NIK) AND A.ADM_NO_PROV = 32 AND A.ADM_NO_KAB= 73 AND A.BAYI_ORG_ASING = 'T' AND A.BAYI_DOM_LAHIR = 'D') N_LAHIR_U,
                        (SELECT COUNT(1) AS COUNT FROM CAPIL_LAHIR A WHERE TO_CHAR(A.PLPR_TGL_LAPOR,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.ADM_AKTA_NO LIKE '%LT%' AND EXISTS(SELECT 1 FROM BIODATA_WNI B WHERE A.BAYI_NIK =B.NIK) AND A.ADM_NO_PROV = 32 AND A.ADM_NO_KAB= 73 AND A.BAYI_ORG_ASING = 'T' AND A.BAYI_DOM_LAHIR = 'D') N_LAHIR_T,
                        (SELECT COUNT(1) AS COUNT FROM CAPIL_MATI  WHERE  TO_CHAR(PLPR_TGL_LAPOR,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND ADM_NO_PROV = 32 AND ADM_NO_KAB= 73  AND MATI_LUAR_NEGERI = 'T' AND MATI_DOM_MATI = 'D') N_MATI,
                        (SELECT COUNT(1) AS COUNT FROM CAPIL_KAWIN  WHERE  TO_CHAR(KAWIN_TGL_LAPOR,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND ADM_NO_PROV =32AND ADM_NO_KAB = 73) N_KAWIN,
                        (SELECT COUNT(1) AS COUNT FROM CAPIL_CERAI  WHERE  TO_CHAR(CERAI_TGL_LAPOR,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND ADM_NO_PROV =32AND ADM_NO_KAB = 73) N_CERAI,
                        (SELECT COUNT(1) AS COUNT FROM CAPIL_LAHIR A WHERE TO_CHAR(A.PLPR_TGL_LAPOR,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND A.ADM_AKTA_NO LIKE '%LU%' AND EXISTS(SELECT 1 FROM BIODATA_WNI B WHERE A.BAYI_NIK =B.NIK) AND A.ADM_NO_PROV = 32 AND A.ADM_NO_KAB= 73 AND A.BAYI_ORG_ASING = 'T' AND A.BAYI_DOM_LAHIR = 'D') Y_LAHIR_U,
                        (SELECT COUNT(1) AS COUNT FROM CAPIL_LAHIR A WHERE TO_CHAR(A.PLPR_TGL_LAPOR,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND A.ADM_AKTA_NO LIKE '%LT%' AND EXISTS(SELECT 1 FROM BIODATA_WNI B WHERE A.BAYI_NIK =B.NIK) AND A.ADM_NO_PROV = 32 AND A.ADM_NO_KAB= 73 AND A.BAYI_ORG_ASING = 'T' AND A.BAYI_DOM_LAHIR = 'D') Y_LAHIR_T,
                        (SELECT COUNT(1) AS COUNT FROM CAPIL_MATI  WHERE  TO_CHAR(PLPR_TGL_LAPOR,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND ADM_NO_PROV = 32 AND ADM_NO_KAB= 73  AND MATI_LUAR_NEGERI = 'T' AND MATI_DOM_MATI = 'D') Y_MATI,
                        (SELECT COUNT(1) AS COUNT FROM CAPIL_KAWIN  WHERE  TO_CHAR(KAWIN_TGL_LAPOR,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND ADM_NO_PROV =32AND ADM_NO_KAB = 73) Y_KAWIN,
                        (SELECT COUNT(1) AS COUNT FROM CAPIL_CERAI  WHERE  TO_CHAR(CERAI_TGL_LAPOR,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND ADM_NO_PROV =32AND ADM_NO_KAB = 73) Y_CERAI
                        FROM DUAL
                        ";
		 $r = DB::connection('db222')->select($sql);
		$output = array(
                   "capil_nulahir"=>$r[0]->n_lahir_u,
                   "capil_ntlahir"=>$r[0]->n_lahir_t,
                   "capil_nmati"=>$r[0]->n_mati,
                   "capil_nkawin"=>$r[0]->n_kawin,
                   "capil_ncerai"=>$r[0]->n_cerai,
                   "capil_yulahir"=>$r[0]->y_lahir_u,
                   "capil_ytlahir"=>$r[0]->y_lahir_t,
                   "capil_ymati"=>$r[0]->y_mati,
                   "capil_ykawin"=>$r[0]->y_kawin,
                   "capil_ycerai"=>$r[0]->y_cerai
                );
        return $output;
	}
	public function get_count_suket($nik){
			if (!empty($nik)){
				if (is_numeric($nik)){
					$sql = "SELECT COUNT(1) AS JML FROM BIODATA_WNI WHERE NIK = $nik";
					$r = DB::connection('db222')->select($sql);
					return $r[0]->jml;
				}else{
					return 0;
				}
			}else{
				return 0;
			}
	}
	public function get_data_suket_andro($nik){
			$sql = " SELECT 
					  B.NIK
					  , B.NO_KK
					  , B.NAMA_LGKP
					  , B.TMPT_LHR
					  , TO_CHAR(B.TGL_LHR,'DD-MM-YYYY') AS TGL_LHR
					  , TRUNC(MONTHS_BETWEEN(SYSDATE,B.TGL_LHR)/12) UMUR
					  , CASE WHEN B.JENIS_KLMIN = 1 THEN 'LAKI-LAKI' ELSE 'PEREMPUAN' END AS JENIS_KLMIN
					  , D.ALAMAT
					  , LPAD(TO_CHAR(D.NO_RT), 3, '0') AS RT
					  , LPAD(TO_CHAR(D.NO_RW), 3, '0') AS RW
					  , F5_GET_NAMA_KECAMATAN(B.NO_PROP,B.NO_KAB,B.NO_KEC) NAMA_KEC
					  , F5_GET_NAMA_KELURAHAN(B.NO_PROP,B.NO_KAB,B.NO_KEC,B.NO_KEL) NAMA_KEL
					  , (SELECT UPPER(C.DESCRIP) FROM REF_SIAK_WNI C WHERE C.NO = B.AGAMA AND SECT =501) AS AGAMA
					  , (SELECT UPPER(C.DESCRIP) FROM REF_SIAK_WNI C WHERE C.NO = B.JENIS_PKRJN AND SECT =201) AS JENIS_PKRJN
					  , (SELECT UPPER(C.DESCRIP) FROM REF_SIAK_WNI C WHERE C.NO = B.STAT_HBKEL AND SECT =301) AS STAT_HBKEL
					  , CASE WHEN B.AKTA_LHR = 1 THEN 'TIDAK ADA' ELSE 'ADA' END AS AKTA_LHR
					  , CASE WHEN B.NO_AKTA_LHR IS NULL THEN '-' ELSE B.NO_AKTA_LHR END AS NO_AKTA_LHR
					  , CASE 
					    WHEN B.STAT_KWN = 2 AND B.AKTA_KWN = 1 THEN 'KAWIN TIDAK TERCATAT' 
					    WHEN B.STAT_KWN = 2 AND B.AKTA_KWN = 2 THEN 'KAWIN TERCATAT'
					    ELSE(SELECT UPPER(C.DESCRIP) FROM REF_SIAK_WNI C WHERE C.NO = B.STAT_KWN AND SECT =601) END AS STAT_KWN
					  , CASE WHEN B.NO_AKTA_KWN IS NULL THEN '-' ELSE B.NO_AKTA_KWN END AS NO_AKTA_KWN
					  , CASE WHEN A.CURRENT_STATUS_CODE IS NULL THEN '-' ELSE A.CURRENT_STATUS_CODE END AS CURRENT_STATUS_CODE
					  , CASE WHEN F.PRINTED_BY IS NULL THEN '-' ELSE F.PRINTED_BY END AS SUKET_BY
					  , CASE WHEN TO_CHAR(F.PRINTED_DATE,'DD-MM-YYYY') IS NULL THEN '-' ELSE TO_CHAR(F.PRINTED_DATE,'DD-MM-YYYY') END AS SUKET_DT
					  , CASE WHEN  TO_CHAR(G.LAST_UPDATE,'DD-MM-YYYY') IS NULL AND TO_CHAR(G.PERSONALIZED_DATE,'DD-MM-YYYY') IS NULL THEN '-' WHEN TO_CHAR(G.LAST_UPDATE,'DD-MM-YYYY') IS NULL AND TO_CHAR(G.PERSONALIZED_DATE,'DD-MM-YYYY') IS NOT NULL THEN TO_CHAR(G.PERSONALIZED_DATE,'DD-MM-YYYY') ELSE  TO_CHAR(G.LAST_UPDATE,'DD-MM-YYYY') END AS KTP_DT
					  , CASE WHEN TO_CHAR(G.CREATED_USERNAME) IS NULL THEN '-' ELSE TO_CHAR(G.CREATED_USERNAME) END AS KTP_BY 
					  FROM BIODATA_WNI B LEFT JOIN DATA_KELUARGA D ON B.NO_KK = D.NO_KK LEFT JOIN DEMOGRAPHICS_ALL A  ON A.NIK = B.NIK LEFT JOIN (SELECT NIK, PRINTED_DATE,PRINTED_BY FROM 
					  (SELECT NIK,PRINTED_DATE,PRINTED_BY, RANK() OVER (PARTITION BY NIK ORDER BY PRINTED_DATE DESC) RNK FROM T7_HIST_SUKET WHERE TIPE = 1) WHERE RNK = 1) F ON F.NIK = B.NIK LEFT JOIN 
					  (SELECT NIK,CREATED,CREATED_USERNAME,LAST_UPDATE,PERSONALIZED_DATE FROM (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME,LAST_UPDATE,CREATED, RANK() OVER (PARTITION BY NIK ORDER BY  PERSONALIZED_DATE DESC, LAST_UPDATE DESC, CHIP_ID DESC) RNK FROM CARD_MANAGEMENT) WHERE RNK = 1) G ON G.NIK =B.NIK WHERE B.NIK = $nik";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}

	// Region Setting API
	public function get_helpdesk_option()
	{
		header("Content-Type: application/json", true);
		 $sql = "SELECT A.HELPDESK_ID, A.HELPDESK_DESCRIPTION FROM SIAK_MASTER_HELPDESK A";
         $r = DB::select($sql);
		return $r;
	}
	public function fotocetak(Request $request)
	{
			$nik = $request->nik;
			 if ($nik != null){
			 	$sql = "SELECT FACE FROM FACES WHERE NIK=  $nik";
			 $r = DB::connection('db2')->select($sql);
			 $img = $r[0]->face;
	         echo "<img src='data:image/png;base64,".base64_encode($img)."'>";
	     }else{
	     	echo "Data TidaK Ditemukan";
	     }
		 
	}
	public function fotorekam(Request $request)
	{
			$nik = $request->nik;
			 if ($nik != null){
			 	$sql = "SELECT FACE FROM FACES WHERE NIK=  $nik";
			 $r = DB::connection('db221')->select($sql);
			 $img = $r[0]->face;
	         echo "<img src='data:image/png;base64,".base64_encode($img)."'>";
	     }else{
	     	echo "Data TidaK Ditemukan";
	     }
		 
	}

	public function cek_nik(Request $request) 
	{
	    $nik = $request->nik;
	    $query=DB::select("SELECT COUNT(1) JUMLAH FROM USERS WHERE NIK = ".$nik."");
	    if($query[0]->jumlah > 0){
	      return response()->json([
	        'status' => 'error',
	        'data' => 'NIK sudah pernah terdaftar'
	      ]);
	    }
	}
	public function get_data_kk(Request $request)
	{
		header('Content-type: application/json');
		$no_kk = $request->no_kk;
		$count = $this->get_count_data_kk($no_kk);	
		if ($count >0){
			$output = $this->src_get_data_kk($no_kk);
			$response["data"] = $output[0];
			$response["jumlah"] = $count;
		}else{
			$response["data"] = "";
			$response["jumlah"] = 0;
		}
        return $response;
	}
	public function get_data_nik(Request $request)
	{
		header('Content-type: application/json');
		$nik = $request->nik;
		$count = $this->get_count_data_nik($nik);	
		if ($count >0){
			$output = $this->src_get_data_nik($nik);
			$response["data"] = $output[0];
			$response["jumlah"] = $count;
		}else{
			$response["data"] = "";
			$response["jumlah"] = 0;
		}
        return $response;
	}

	public function get_count_data_kk($no_kk){
			if (!empty($no_kk)){
				if (is_numeric($no_kk)){
					$sql = "SELECT COUNT(1) AS JML FROM DATA_KELUARGA WHERE NO_KK = $no_kk";
					$r = DB::connection('db222')->select($sql);
					return $r[0]->jml;
				}else{
					return 0;
				}
			}else{
				return 0;
			}
		}

	public function src_get_data_kk($no_kk){
			$sql = " SELECT 
					  D.NO_KK
					  , D.NIK_KK
                      , D.NAMA_KEP
                      , D.NO_PROP
                      , D.NO_KAB
                      , D.NO_KEC
                      , D.NO_KEL
                      , F5_GET_NAMA_PROVINSI(D.NO_PROP) NAMA_PROP
                      , F5_GET_NAMA_KABUPATEN(D.NO_PROP,D.NO_KAB) NAMA_KAB
                      , F5_GET_NAMA_KECAMATAN(D.NO_PROP,D.NO_KAB,D.NO_KEC) NAMA_KEC
					  , F5_GET_NAMA_KELURAHAN(D.NO_PROP,D.NO_KAB,D.NO_KEC,D.NO_KEL) NAMA_KEL
                      
					  FROM  DATA_KELUARGA D WHERE D.NO_KK = $no_kk";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}

	public function get_count_data_nik($nik){
			if (!empty($nik)){
				if (is_numeric($nik)){
					$sql = "SELECT COUNT(1) AS JML FROM BIODATA_WNI WHERE NIK = $nik";
					$r = DB::connection('db222')->select($sql);
					return $r[0]->jml;
				}else{
					return 0;
				}
			}else{
				return 0;
			}
		}

	public function src_get_data_nik($nik){
			$sql = " SELECT 
					  A.NIK
                      , A.NAMA_LGKP
                      , A.JENIS_KLMIN
                      , UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(A.JENIS_KLMIN,7), 801)) JENIS_KLMIN_DESC
                      -- , TO_CHAR(TRUNC(MONTHS_BETWEEN(TO_DATE(SYSDATE,'DD/MM/YYYY'),A.TGL_LHR)/12)) UMUR
                      , TO_CHAR(A.TGL_LHR,'DD-MM-YYYY') TGL_LHR
                      , TO_CHAR(A.TGL_LHR,'HH24:MI') JAM_LHR
                      , A.TMPT_LHR
                      , A.JENIS_PKRJN
                      , UPPER(F5_GET_REF_WNI(A.JENIS_PKRJN, 201)) JENIS_PKRJN_DESC
                      , B.NO_KK
					  , B.NIK_KK
                      , B.NAMA_KEP
                      , B.NO_PROP
                      , B.NO_KAB
                      , B.NO_KEC
                      , B.NO_KEL
                      , F5_GET_NAMA_PROVINSI(B.NO_PROP) NAMA_PROP
                      , F5_GET_NAMA_KABUPATEN(B.NO_PROP,B.NO_KAB) NAMA_KAB
                      , F5_GET_NAMA_KECAMATAN(B.NO_PROP,B.NO_KAB,B.NO_KEC) NAMA_KEC
					  , F5_GET_NAMA_KELURAHAN(B.NO_PROP,B.NO_KAB,B.NO_KEC,B.NO_KEL) NAMA_KEL
                      , B.NO_RT
                      , B.NO_RW
                      , B.ALAMAT
                      , C.NIK NIK_IBU
                      , C.NAMA_LGKP NAMA_LGKP_IBU
                      -- , TRUNC(MONTHS_BETWEEN(TO_DATE(SYSDATE,'DD/MM/YYYY'),C.TGL_LHR)/12) UMUR_IBU
                      , TO_CHAR(C.TGL_LHR,'DD-MM-YYYY') TGL_LHR_IBU
                      , C.TMPT_LHR TMPT_LHR_IBU
                      , C.JENIS_PKRJN JENIS_PKRJN_IBU
                      , UPPER(F5_GET_REF_WNI(C.JENIS_PKRJN, 201)) JENIS_PKRJN_DESC_IBU
                      , TO_CHAR(C.TGL_KWN,'DD-MM-YYYY') TGL_KAWIN_IBU
                      , CASE WHEN C.NIK IS NOT NULL THEN '1 - WARGA NEGARA INDONESIA' END KEBANGSAAN_IBU
                      , D.NO_KK NO_KK_IBU
                      , D.NIK_KK NIK_KK_IBU
                      , D.NAMA_KEP NAMA_KEP_IBU
                      , D.NO_PROP NO_PROP_IBU
                      , D.NO_KAB NO_KAB_IBU
                      , D.NO_KEC NO_KEC_IBU
                      , D.NO_KEL NO_KEL_IBU
                      , F5_GET_NAMA_PROVINSI(D.NO_PROP)  NAMA_PROP_IBU
                      , F5_GET_NAMA_KABUPATEN(D.NO_PROP,D.NO_KAB)  NAMA_KAB_IBU
                      , F5_GET_NAMA_KECAMATAN(D.NO_PROP,D.NO_KAB,D.NO_KEC)  NAMA_KEC_IBU
                      , F5_GET_NAMA_KELURAHAN(D.NO_PROP,D.NO_KAB,D.NO_KEC,D.NO_KEL)  NAMA_KEL_IBU
                      , D.NO_RT NO_RT_IBU
                      , D.NO_RW NO_RW_IBU
                      , D.ALAMAT ALAMAT_IBU
                      , E.NIK NIK_AYAH
                      , E.NAMA_LGKP NAMA_LGKP_AYAH
                      -- , TRUNC(MONTHS_BETWEEN(TO_DATE(SYSDATE,'DD/MM/YYYY'),E.TGL_LHR)/12) UMUR_AYAH
                      , TO_CHAR(E.TGL_LHR,'DD-MM-YYYY') TGL_LHR_AYAH
                      , E.TMPT_LHR TMPT_LHR_AYAH
                      , E.JENIS_PKRJN JENIS_PKRJN_AYAH
                      , UPPER(F5_GET_REF_WNI(E.JENIS_PKRJN, 201)) JENIS_PKRJN_DESC_AYAH
                      , TO_CHAR(E.TGL_KWN,'DD-MM-YYYY') TGL_KAWIN_AYAH
                      , CASE WHEN E.NIK IS NOT NULL THEN '1 - WARGA NEGARA INDONESIA' END KEBANGSAAN_AYAH
                      , F.NO_KK NO_KK_AYAH
                      , F.NIK_KK NIK_KK_AYAH
                      , F.NAMA_KEP NAMA_KEP_AYAH
                      , F.NO_PROP NO_PROP_AYAH
                      , F.NO_KAB NO_KAB_AYAH
                      , F.NO_KEC NO_KEC_AYAH
                      , F.NO_KEL NO_KEL_AYAH
                      , F5_GET_NAMA_PROVINSI(F.NO_PROP)  NAMA_PROP_AYAH
                      , F5_GET_NAMA_KABUPATEN(F.NO_PROP,F.NO_KAB)  NAMA_KAB_AYAH
                      , F5_GET_NAMA_KECAMATAN(F.NO_PROP,F.NO_KAB,F.NO_KEC)  NAMA_KEC_AYAH
                      , F5_GET_NAMA_KELURAHAN(F.NO_PROP,F.NO_KAB,F.NO_KEC,F.NO_KEL)  NAMA_KEL_AYAH
                      , F.NO_RT NO_RT_AYAH
                      , F.NO_RW NO_RW_AYAH
                      , F.ALAMAT ALAMAT_AYAH
					  FROM  BIODATA_WNI A 
                      INNER JOIN DATA_KELUARGA B ON A.NO_KK = B.NO_KK 
                      LEFT JOIN BIODATA_WNI C ON A.NIK_IBU = C.NIK 
                      LEFT JOIN DATA_KELUARGA D ON C.NO_KK = D.NO_KK
                      LEFT JOIN BIODATA_WNI E ON A.NIK_AYAH = E.NIK 
                      LEFT JOIN DATA_KELUARGA F ON E.NO_KK = F.NO_KK
                      WHERE A.NIK = $nik";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}

    
}

