<?php

namespace App\Http\Controllers;

use DB;
use Session;
use DateTime;
use PDF;
use Illuminate\Http\Request;
use App\Http\Controllers\SharedController as Shr;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class SiakController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index(Request $request){
    	return redirect()->route('logout');
    }
    public function repair_local_biometric(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {

			$menu_id = 120;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->nik != null){
			$nik = $request->nik;
			$bo = $request->biometric_option;
			if ($bo == 1 || $bo == 0 || $bo == 3){
				$r = $this->get_data_rekam($nik,$request->session()->get('S_NO_KEC'));
				if(count($r) == 0){
					$r = $this->get_data_rekam_delete($nik,$request->session()->get('S_NO_KEC'));
				}
				$j = $this->get_count_face_rekam($nik);
				if ($j > 0) {
				$f = $this->get_face_rekam($nik);
				}else{
				$f = [];	
				}	
			}else{
				$r = $this->get_data_cetak($nik,$request->session()->get('S_NO_KEC'));
				if(count($r) == 0){
					$r = $this->get_data_cetak_delete($nik,$request->session()->get('S_NO_KEC'));
				}
				$j = $this->get_count_face_cetak($nik);
				if ($j > 0) {
				$f = $this->get_face_cetak($nik);
				}else{
				$f = [];	
				}	
			}
			
			$data = array(
		 		"stitle"=>'Perbaikan Biometric Local',
		 		"mtitle"=>'Perbaikan Biometric Local',
		 		"my_url"=>'repair_local_biometric',
		 		"type_tgl"=>'Tanggal',
		 		"data"=>$r,
		 		"face"=>$f,
		 		"option"=>$bo,
		 		"is_ada"=>"ada",
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'Perbaikan Biometric Local',
		 		"mtitle"=>'Perbaikan Biometric Local',
		 		"my_url"=>'repair_local_biometric',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('Siak_hapus_rekam_local/main',$data);
		}else{
			return redirect()->route('logout');
		}
		
	}
    public function Dewa_kematian(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {

			$menu_id = 120;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->akta_kmt != null){
			$akta_kmt = $request->akta_kmt;
			$r = $this->get_data_kematian($akta_kmt);
			if (count($r) > 0){
				$nik = $r[0]->mati_nik;
				if ($nik != null && is_numeric($nik)){
					$j = $this->get_count_face_rekam($nik);
					if($j>0){
						$f = $this->get_face_rekam($nik);
					}else{
						$j = $this->get_count_face_cetak($nik);
						if ($j > 0) {
							$f = $this->get_face_cetak($nik);
						}else{
							$f = [];	
						}	
					}
				}else{
					$f = [];
				}
			}else{
					$f = [];
			}
			
			
			$data = array(
		 		"stitle"=>'Perbaikan Akta Kematian',
		 		"mtitle"=>'Perbaikan Akta Kematian',
		 		"my_url"=>'Dewa_kematian',
		 		"type_tgl"=>'Tanggal',
		 		"data"=>$r,
		 		"face"=>$f,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'Perbaikan Akta Kematian',
		 		"mtitle"=>'Perbaikan Akta Kematian',
		 		"my_url"=>'Dewa_kematian',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('Siak_dewa_kematian/main',$data);
		}else{
			return redirect()->route('logout');
		}
		
	}
	public function delete_biometric_cetak(Request $request) 
	{
		if($request->nik != null){
			$nik = $request->nik;
			$j = $this->get_data_cetak($nik,$request->session()->get('S_NO_KEC'));
			if($j > 0){
				$this->do_hist_delete($nik,$request->session()->get('S_USER_ID'),$request->ip(),'BIOMETRIC CETAK');
				$this->do_delete_cetak_full($nik);
				// $this->do_delete_demographics_all($nik);
				$data["success"] = TRUE;
				$data["is_done"] = 0;
        		$data["message"] = "Data Berhasil Di Bersihkan";
        		return $data;
			}else{
				$data["success"] = FALSE;
				$data["is_done"] = 1;
        		$data["message"] = "Data Gagal Di Bersihkan";
        		return $data;
			}
		}else{
			return redirect()->route('logout');
		}
	}
	public function delete_biometric_rekam(Request $request) 
	{
		if($request->nik != null){
			$nik = $request->nik;
			$j = $this->get_data_rekam($nik,$request->session()->get('S_NO_KEC'));
			if($j > 0){
				$this->do_hist_delete($nik,$request->session()->get('S_USER_ID'),$request->ip(),'BIOMETRIC REKAM');
				$this->do_delete_demographics_all($nik);
				$this->do_delete_rekam_full($nik);
				$this->do_delete_cetak_full($nik);
				$data["success"] = TRUE;
				$data["is_done"] = 0;
        		$data["message"] = "Data Berhasil Di Bersihkan";
        		return $data;
			}else{
				$data["success"] = FALSE;
				$data["is_done"] = 1;
        		$data["message"] = "Data Gagal Di Bersihkan";
        		return $data;
			}
		}else{
			return redirect()->route('logout');
		}
	}
	public function restore_bio_wni(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 113;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->nik != null){
			$nik = $request->nik;
			$no_kec = $request->no_kec;
				$r = [];
				$r_pindah = [];
				$r_mati = [];
				$r_kk = [];
				$j = $this->count_restore_cek_siak($nik,$no_kec);
				if ($j > 0) {
					$r = $this->get_restore_cek_siak($nik);
					$r_kk = $this->get_restore_cek_wni_kk($nik);
				}else{
					$j = $this->count_restore_cek_delete($nik,$no_kec);
					if ($j > 0) {
					$r = $this->get_restore_cek_delete($nik);
					$r_kk = $this->get_restore_cek_delete_kk($nik);
					$r_pindah = $this->get_history_pindah($nik);
					$r_mati = $this->get_history_kematian($nik);
					}
				}
				
			
			$data = array(
		 		"stitle"=>'Restore Biodata Wni',
		 		"mtitle"=>'Restore Biodata Wni',
		 		"my_url"=>'Restore',
		 		"type_tgl"=>'Tanggal',
		 		"pindah"=>$r_pindah,
		 		"mati"=>$r_mati,
		 		"data_kk"=>$r_kk,
		 		"data"=>$r,
		 		"is_ada"=>"ada",
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else if($request->nik != null){
			$nik = $request->nik;
			$no_kec = $request->no_kec;
				$r = [];
				$r_pindah = [];
				$r_mati = [];
				$r_kk = [];
				$j = $this->count_restore_cek_siak($nik,$no_kec);
				if ($j > 0) {
					$r = $this->get_restore_cek_siak($nik);
					$r_kk = $this->get_restore_cek_wni_kk($nik);
				}else{
					$j = $this->count_restore_cek_delete($nik,$no_kec);
					if ($j > 0) {
					$r = $this->get_restore_cek_delete($nik);
					$r_kk = $this->get_restore_cek_delete_kk($nik);
					$r_pindah = $this->get_history_pindah($nik);
					$r_mati = $this->get_history_kematian($nik);
					}
				}
				
			
			$data = array(
		 		"stitle"=>'Restore Biodata Wni',
		 		"mtitle"=>'Restore Biodata Wni',
		 		"my_url"=>'Restore',
		 		"type_tgl"=>'Tanggal',
		 		"pindah"=>$r_pindah,
		 		"mati"=>$r_mati,
		 		"data_kk"=>$r_kk,
		 		"data"=>$r,
		 		"is_ada"=>"ada",
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else if($request->no_kk != null){
			$no_kk = $request->no_kk;
			$no_kec = $request->no_kec;
				$r = [];
				$r_pindah = [];
				$r_mati = [];
				$r_kk = [];
				$j = $this->count_restore_cek_siak_no_kk($no_kk,$no_kec);
				if ($j > 0) {
					$r_kk = $this->get_restore_cek_wni_nokk($no_kk);
				}else{
					$j = $this->count_restore_cek_delete_no_kk($no_kk,$no_kec);
					if ($j > 0) {
					$r_kk = $this->get_restore_cek_delete_nokk($no_kk);
					}
				}
				
			
			$data = array(
		 		"stitle"=>'Restore Biodata Wni',
		 		"mtitle"=>'Restore Biodata Wni',
		 		"my_url"=>'Restore',
		 		"type_tgl"=>'Tanggal',
		 		"pindah"=>$r_pindah,
		 		"mati"=>$r_mati,
		 		"data_kk"=>$r_kk,
		 		"data"=>$r,
		 		"is_ada"=>"ada",
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'Restore Biodata Wni',
		 		"mtitle"=>'Restore Biodata Wni',
		 		"my_url"=>'Restore',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('Siak_restore_bio_wni/main',$data);
		}else{
			return redirect()->route('logout');
		}
		
	}
	public function restore_biodata_wni(Request $request) 
	{
		if($request->nik != null){
			$nik = $request->nik;
			$no_kk = $request->no_kk;
			$no_kec = 0;
			$stat_hbkel = ($request->stat_hbkel == 'KEPALA KELUARGA') ? 1 : 0;
			$j = $this->count_restore_cek_delete($nik,$no_kec);
			if($j > 0){
				if($stat_hbkel > 0){
				$this->do_hist_restore($nik,$no_kk,$request->session()->get('S_USER_ID'),$request->ip(),($stat_hbkel == 1) ? "RESTORE WNI KEPALA KELUARGA" : "RESTORE WNI ANGGOTA KELUARGA");
				$this->do_restore($nik,$no_kk,$stat_hbkel,$request->session()->get('S_USER_ID'));
				$data["success"] = TRUE;
				$data["is_done"] = 0;
        		$data["message"] = "Data Berhasil Di Restore";
        		return $data;
        		}else{
        			$j = $this->count_restore_cek_kk($nik,$no_kec);
        			if($j > 0){
						$this->do_hist_restore($nik,$no_kk,$request->session()->get('S_USER_ID'),$request->ip(),($stat_hbkel == 1) ? "RESTORE WNI KEPALA KELUARGA" : "RESTORE WNI ANGGOTA KELUARGA");
						$this->do_restore($nik,$no_kk,$stat_hbkel,$request->session()->get('S_USER_ID'));
						$data["success"] = TRUE;
						$data["is_done"] = 0;
		        		$data["message"] = "Data Berhasil Di Restore";
		        		return $data;
        			}else{
        				$data["success"] = FALSE;
						$data["is_done"] = 1;
        				$data["message"] = "Data Gagal Di Restore, Karena Tidak Memiliki Kepala Keluarga atau Kepala Keluarga Tidak Aktif Di Nomor KK Tujuan !";
        				return $data;
        			}
        		}
			}else{
				$data["success"] = FALSE;
				$data["is_done"] = 1;
        		$data["message"] = "Data Gagal Di Restore";
        		return $data;
			}
		}else{
			return redirect()->route('logout');
		}
	}
	public function repair_biodata_wni(Request $request) 
	{
		if($request->nik_duplicate != null){
			$nik_duplicate = $request->nik_duplicate;
			$nik_single = $request->nik_single;
			$status_ektp_duplicate = $request->status_ektp_duplicate;
			$status_ektp_single = $request->status_ektp_single;
			$nama_duplicate = str_replace('\'', '',$request->nama_duplicate);
			$nama_single = str_replace('\'', '',$request->nama_single);
			
			$j = $this->count_repair_cek($nik_duplicate);
			if($j > 0){
				$j = $this->count_repair_cek_delete($nik_single);
				if($j > 0){
				$this->do_hist_repair($nik_duplicate,$nik_single,$status_ektp_duplicate,$status_ektp_single,$nama_duplicate,$nama_single,$request->session()->get('S_USER_ID'),$request->ip());
					$this->do_repair($nik_duplicate,$nik_single);
					$data["success"] = TRUE;
					$data["is_done"] = 0;
	        		$data["message"] = "Data Berhasil Di Sesuaikan";
	        		return $data;
        		}else{
					$data["success"] = FALSE;
					$data["is_done"] = 1;
	        		$data["message"] = "Data Gagal Di Sesuaikan";
	        		return $data;
				}
			}else{
				$data["success"] = FALSE;
				$data["is_done"] = 1;
        		$data["message"] = "Data Gagal Di Sesuaikan";
        		return $data;
			}
		}else{
			return redirect()->route('logout');
		}
	}
	public function restore_biodata_pindah_wni(Request $request) 
	{
		if($request->nik != null){
			$nik = $request->nik;
			$no_kk = $request->no_kk;
			$no_kec = 0;
			$stat_hbkel = ($request->stat_hbkel == 'KEPALA KELUARGA') ? 1 : 0;
			$j = $this->count_restore_cek_delete($nik,$no_kec);
			if($j > 0){
				if($stat_hbkel > 0){
				$this->do_hist_restore($nik,$no_kk,$request->session()->get('S_USER_ID'),$request->ip(),($stat_hbkel == 1) ? "RESTORE WNI PINDAH KEPALA KELUARGA" : "RESTORE WNI PINDAH ANGGOTA KELUARGA");
				$this->do_restore($nik,$no_kk,$stat_hbkel,$request->session()->get('S_USER_ID'));
				$data["success"] = TRUE;
				$data["is_done"] = 0;
        		$data["message"] = "Data Berhasil Di Restore";
        		return $data;
        		}else{
        			$j = $this->count_restore_cek_kk($nik,$no_kec);
        			if($j > 0){
						$this->do_hist_restore($nik,$no_kk,$request->session()->get('S_USER_ID'),$request->ip(),($stat_hbkel == 1) ? "RESTORE WNI PINDAH KEPALA KELUARGA" : "RESTORE WNI PINDAH ANGGOTA KELUARGA");
						$this->do_restore($nik,$no_kk,$stat_hbkel,$request->session()->get('S_USER_ID'));
						$data["success"] = TRUE;
						$data["is_done"] = 0;
		        		$data["message"] = "Data Berhasil Di Restore";
		        		return $data;
        			}else{
        				$data["success"] = FALSE;
						$data["is_done"] = 1;
        				$data["message"] = "Data Gagal Di Restore, Karena Tidak Memiliki Kepala Keluarga atau Kepala Keluarga Tidak Aktif Di Nomor KK Tujuan !";
        				return $data;
        			}
        		}
			}else{
				$data["success"] = FALSE;
				$data["is_done"] = 1;
        		$data["message"] = "Data Gagal Di Restore";
        		return $data;
			}
		}else{
			return redirect()->route('logout');
		}
	}

	public function delete_biodata_wni(Request $request) 
	{
		if($request->nik != null){
			$nik = $request->nik;
			$no_kk = $request->no_kk;
			$no_kec = 0;
			$j = $this->count_restore_cek_siak($nik,$no_kec);
			$option = "DELETE WNI ANGGOTA KELUARGA";
			if($j > 0){
				$this->do_hist_delete_wni($nik,$no_kk,$request->session()->get('S_USER_ID'),$request->ip(),$option);
				$this->do_delete_wni($nik);
				$data["success"] = TRUE;
				$data["is_done"] = 0;
        		$data["message"] = "Data Berhasil Di Delete";
        		return $data;
			}else{
				$data["success"] = FALSE;
				$data["is_done"] = 1;
        		$data["message"] = "Data Gagal Di Delete";
        		return $data;
			}
		}else{
			return redirect()->route('logout');
		}
	}
	public function cek_no_kk_baru(Request $request) 
	{
		if($request->no_kk_baru != null){
			$no_kk_baru = $request->no_kk_baru;
			$j = $this->count_no_kk_baru($no_kk_baru);
			if($j > 0){
				$r = $this->get_no_kk_baru($no_kk_baru);
				$element = [];
				$element["no_kk"] =  $r[0]->no_kk;
				$element["nama_kep"] =  $r[0]->nama_kep;
				$element["no_prop"] =  $r[0]->no_prop;
				$element["no_kab"] =  $r[0]->no_kab;
				$element["no_kec"] =  $r[0]->no_kec;
				$element["no_kel"] =  $r[0]->no_kel;
				$element["rw"] =  $r[0]->rw;
				$element["rt"] =  $r[0]->rt;
				$element["alamat"] =  $r[0]->alamat;
				$data["data"] = $element;
				$data["success"] = TRUE;
				$data["is_done"] = 0;
        		$data["message"] = "Nomor KK Baru Ditemukan";
        		return $data;
			}else{
				$element = [];
				$data["data"] = $element;
				$data["success"] = FALSE;
				$data["is_done"] = 1;
        		$data["message"] = "Nomor KK Baru Tidak Ditemukan";
        		return $data;
			}
		}else{
			return redirect()->route('logout');
		}
	}
	public function change_no_kk(Request $request) 
	{
		if($request->nik != null){
			$nik = $request->nik;
			$no_kk_lama = $request->no_kk_lama;
			$no_kec_lama = $request->no_kec_lama;
			$no_kel_lama = $request->no_kel_lama;
			$no_rw_lama = $request->no_rw_lama;
			$no_rt_lama = $request->no_rt_lama;
			$alamat_lama = $request->alamat_lama;
			$no_kk_baru = $request->no_kk_baru;
			$no_kec_baru = $request->no_kec_baru;
			$no_kel_baru = $request->no_kel_baru;
			$no_rw_baru = $request->no_rw_baru;
			$no_rt_baru = $request->no_rt_baru;
			$alamat_baru = $request->alamat_baru;
			$no_kec = 0;
			$j = $this->count_restore_cek_delete($nik,$no_kec);
			if ($j > 0) {
				$this->do_hist_change_kk($nik,$no_kk_lama,$no_kec_lama,$no_kel_lama,$no_rw_lama,$no_rt_lama,$alamat_lama,$no_kk_baru,$no_kec_baru,$no_kel_baru,$no_rw_baru,$no_rt_baru,$alamat_baru,$request->session()->get('S_USER_ID'),$request->ip());
				$this->do_change_kk($nik,$no_kk_baru,$no_kec_baru,$no_kel_baru);
				$data["success"] = TRUE;
				$data["is_done"] = 0;
        		$data["message"] = "Nomor KK Berhasil Di Rubah";
        		return $data;
			}else{
				$data["success"] = FALSE;
				$data["is_done"] = 1;
        		$data["message"] = "Nomor KK Gagal Di Rubah";
        		return $data;
			}
		}else{
			return redirect()->route('logout');
		}
	}
	public function get_js_kk(Request $request){
		if($request->nik != null){
			$nik = $request->nik;
			$no_kec = 0;
				$j = $this->count_restore_cek_siak($nik,$no_kec);
				if ($j > 0) {
					$data = $this->get_restore_cek_wni_kk($nik);
				}else{
					$j = $this->count_restore_cek_delete($nik,$no_kec);
					if ($j > 0) {
						$data = $this->get_restore_cek_delete_kk($nik);
					}
				}
			return $data;
		}
	}
	public function repair_bio_wni(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 116;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->nik != null && $request->nik_single != null){
			$nik = $request->nik;
			$nik_single = $request->nik_single;
			$no_kec = 0;
				$r = [];
				$r_pindah = [];
				$r_mati = [];
				$j = $this->count_restore_cek_siak($nik,$no_kec);
				if ($j > 0) {
					$r = $this->get_restore_cek_siak($nik);
				}else{
					$j = $this->count_restore_cek_delete($nik,$no_kec);
					if ($j > 0) {
					$r = $this->get_restore_cek_delete($nik);
					$r_pindah = $this->get_history_pindah($nik);
					$r_mati = $this->get_history_kematian($nik);
					}
				}
				$sr = [];
				$sr_pindah = [];
				$sr_mati = [];
				$sj = $this->count_restore_cek_siak($nik_single,$no_kec);
				if ($sj > 0) {
					$sr = $this->get_restore_cek_siak($nik_single);
				}else{
					$sj = $this->count_restore_cek_delete($nik_single,$no_kec);
					if ($sj > 0) {
					$sr = $this->get_restore_cek_delete($nik_single);
					$sr_pindah = $this->get_history_pindah($nik_single);
					$sr_mati = $this->get_history_kematian($nik_single);
					}
				}
			
			$data = array(
		 		"stitle"=>'Penyesuaian Biodata Wni',
		 		"mtitle"=>'Penyesuaian Biodata Wni',
		 		"my_url"=>'Penyesuaian',
		 		"type_tgl"=>'Tanggal',
		 		"pindah"=>$r_pindah,
		 		"mati"=>$r_mati,
		 		"data"=>$r,
		 		"spindah"=>$sr_pindah,
		 		"smati"=>$sr_mati,
		 		"sdata"=>$sr,
		 		"is_ada"=>"ada",
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'Penyesuaian Biodata Wni',
		 		"mtitle"=>'Penyesuaian Biodata Wni',
		 		"my_url"=>'Penyesuaian',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('Siak_repair_bio_wni/main',$data);
		}else{
			return redirect()->route('logout');
		}
		
	}
	public function do_perbaikan_data(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 136;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Perbaikan Data',
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_kantor"=>$request->session()->get('S_NAMA_KANTOR'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('Perbaikan_data/main',$data);
		}else{
			return redirect()->route('logout');
		}
		
	}
	public function update_dewa_kematian(Request $request)
	{
		if(Shr::validatedatetime($request->mati_tgl_lahir,'d-m-Y') != 1){
			$data["success"] = FALSE;
        	$data["message"] = "Format Tanggal Lahir Salah !";
        	return $data;
		}
		if(Shr::validatedatetime($request->mati_tgl_mati,'d-m-Y') != 1){
			$data["success"] = FALSE;
        	$data["message"] = "Format Tanggal Mati Salah !";
        	return $data;
		}
		if($request->adm_akta_no != null){
			$adm_akta_no = $request->adm_akta_no;
			$mati_nik = $request->mati_nik;
			$mati_no_kk = $request->mati_no_kk;
			$mati_jns_kelamin = strtoupper($request->mati_jns_kelamin);
			$mati_nama_lgkp = strtoupper(str_replace('\'', '',$request->mati_nama_lgkp));
			$mati_tmpt_lahir = strtoupper($request->mati_tmpt_lahir);
			$mati_tgl_lahir = $request->mati_tgl_lahir;
			$mati_tmpt_mati = strtoupper($request->mati_tmpt_mati);
			$mati_tgl_mati = $request->mati_tgl_mati;
			$ibu_nama_lgkp = strtoupper(str_replace('\'', '',$request->ibu_nama_lgkp));
			$ayah_nama_lgkp = strtoupper($request->ayah_nama_lgkp);
			$this->do_hist_dewa_kematian($adm_akta_no,$mati_nik,$mati_no_kk,$mati_jns_kelamin,$mati_nama_lgkp,$mati_tmpt_lahir,$mati_tgl_lahir,$mati_tmpt_mati,$mati_tgl_mati,$ibu_nama_lgkp,$ayah_nama_lgkp,$request->session()->get('S_USER_ID'),$request->ip());
			$this->do_update_dewa_kematian($adm_akta_no,$mati_nik,$mati_no_kk,$mati_jns_kelamin,$mati_nama_lgkp,$mati_tmpt_lahir,$mati_tgl_lahir,$mati_tmpt_mati,$mati_tgl_mati,$ibu_nama_lgkp,$ayah_nama_lgkp);
				$data["success"] = TRUE;
        		$data["message"] = "Nomor Akta Kematian Berhasil Di Rubah";
        		return $data;
			
		}else{
			return redirect()->route('logout');
		}
		
	}
	public function list_perbaikan_data(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 136;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'List Perbaikan Data',
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_kantor"=>$request->session()->get('S_NAMA_KANTOR'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('Perbaikan_list_data/main',$data);
		}else{
			return redirect()->route('logout');
		}
		
	}
	public function send_request(Request $request) 
	{
		header("Content-Type: application/json", true);
		if($request->request_id != null){
			$request_id =  $request->request_id;
	    	$keterangan =  strtoupper($this->clean($request->keterangan));
			$user_id =  $request->user_id;
				$this->insert_helpdesk_request($request_id,$keterangan,$user_id);
				$output = array(
	    			"message_type"=>1,
	    			"request_id"=>$request_id,
	    			"message"=> "Terima Kasih Sudah Mengajukan Request Anda <span class='fa fa-smile-o'></span>"
	    		);
			return $output;
		}

	}
	public function send_request_aprove(Request $request) 
	{
		header("Content-Type: application/json", true);
		if($request->aprove_seq_id != null){
			$aprove_seq_id =  $request->aprove_seq_id;
	    	$balasan_aprove =  strtoupper($this->clean($request->balasan_aprove));
			$user_id =  $request->user_id;
				$this->aprove_helpdesk_request($aprove_seq_id,$balasan_aprove,$user_id);
				$output = array(
	    			"message_type"=>1,
	    			"aprove_seq_id"=>$aprove_seq_id,
	    			"message"=> "Request Telah Di Aprove <span class='fa fa-smile-o'></span>"
	    		);
			return $output;
		}

	}
	public function send_request_reject(Request $request) 
	{
		header("Content-Type: application/json", true);
		if($request->reject_seq_id != null){
			$reject_seq_id =  $request->reject_seq_id;
	    	$balasan_reject =  strtoupper($this->clean($request->balasan_reject));
			$user_id =  $request->user_id;
				$this->reject_helpdesk_request($reject_seq_id,$balasan_reject,$user_id);
				$output = array(
	    			"message_type"=>1,
	    			"reject_seq_id"=>$reject_seq_id,
	    			"message"=> "Request Telah Di Reject <span class='fa fa-smile-o'></span>"
	    		);
			return $output;
		}

	}
	public function do_request_pending(Request $request){
		header('Content-type: application/json');
		$user_id = $request->user_id;
		$output = $this->request_pending($user_id);
        return $output;	
	}
	public function do_request_success(Request $request){
		header('Content-type: application/json');
		$user_id = $request->user_id;
		$output = $this->request_success($user_id);
        return $output;	
	}
	public function do_request_reject(Request $request){
		header('Content-type: application/json');
		$user_id = $request->user_id;
		$output = $this->request_reject($user_id);
        return $output;	
	}
	public function do_list_request_pending(Request $request){
		header('Content-type: application/json');
		$user_id = $request->user_id;
		$output = $this->list_request_pending($user_id);
        return $output;	
	}
	public function do_list_request_success(Request $request){
		header('Content-type: application/json');
		$user_id = $request->user_id;
		$output = $this->list_request_success($user_id);
        return $output;	
	}
	public function do_list_request_reject(Request $request){
		header('Content-type: application/json');
		$user_id = $request->user_id;
		$output = $this->list_request_reject($user_id);
        return $output;	
	}
	public function do_get_request(Request $request){
		header('Content-type: application/json');
		$seq_id = $request->seq_id;
		$output = $this->get_request($seq_id);
        return $output;	
	}
	public function clean($string) {
	   	$string = str_replace(' ', ' ', $string); // Replaces all spaces with hyphens.
	   	return preg_replace('/[^A-Za-z0-9\- ,.]/', '', $string); // Removes special chars.
	}

	public function get_data_rekam($nik,$no_kec){
			$sql = "SELECT 
					  A.NIK
					  , A.NO_KK
					  , A.NAMA_LGKP
					  , A.TMPT_LHR
					  , TO_CHAR(A.TGL_LHR,'DD-MM-YYYY') TGL_LHR
					  , CASE WHEN A.JENIS_KLMIN = 1 THEN 'LAKI-LAKI' ELSE 'PEREMPUAN' END JENIS_KLMIN
					  , CASE WHEN  B.CURRENT_STATUS_CODE IS NULL AND  C.CURRENT_STATUS_CODE IS NULL THEN 'BELUM REKAM' WHEN B.CURRENT_STATUS_CODE IS NULL THEN C.CURRENT_STATUS_CODE ELSE B.CURRENT_STATUS_CODE END CURRENT_STATUS_CODE
					  FROM BIODATA_WNI A 
					  LEFT JOIN DEMOGRAPHICS@DB221 B  
					  ON A.NIK = B.NIK 
					  LEFT JOIN DEMOGRAPHICS_ALL@DB2 C 
					  ON A.NIK = C.NIK 
					  WHERE A.NIK = $nik";
			if($no_kec != 0){
				$sql .= " AND A.NO_KEC = $no_kec";
			}
			$r = DB::connection('db222')->select($sql);
			return $r;
		}
		public function get_count_rekam($nik,$no_kec){
			$sql = "SELECT 
					  COUNT(1) JML
					  FROM BIODATA_WNI A 
					  INNER JOIN DEMOGRAPHICS@DB221 B 
					  ON A.NIK = B.NIK 
					  WHERE A.NIK = $nik";
			if($no_kec != 0){
				$sql .= " AND A.NO_KEC = $no_kec";
			}
			$r = DB::connection('db222')->select($sql);
			return $r[0]->jml;
		}
		public function get_data_rekam_delete($nik,$no_kec){
			$sql = "SELECT 
					  A.NIK
					  , A.NO_KK
					  , A.NAMA_LGKP
					  , A.TMPT_LHR
					  , TO_CHAR(A.TGL_LHR,'DD-MM-YYYY') TGL_LHR
					  , CASE WHEN A.JENIS_KLMIN = 1 THEN 'LAKI-LAKI' ELSE 'PEREMPUAN' END JENIS_KLMIN
					  , CASE WHEN  B.CURRENT_STATUS_CODE IS NULL AND  C.CURRENT_STATUS_CODE IS NULL THEN 'BELUM REKAM' WHEN B.CURRENT_STATUS_CODE IS NULL THEN C.CURRENT_STATUS_CODE ELSE B.CURRENT_STATUS_CODE END CURRENT_STATUS_CODE
					  FROM BIODATA_WNI_DELETE A 
					  LEFT JOIN DEMOGRAPHICS@DB221 B  
					  ON A.NIK = B.NIK 
					  LEFT JOIN DEMOGRAPHICS_ALL@DB2 C 
					  ON A.NIK = C.NIK 
					  WHERE A.NIK = $nik";
			if($no_kec != 0){
				$sql .= " AND A.NO_KEC = $no_kec";
			}
			$r = DB::connection('db222')->select($sql);
			return $r;
		}
		public function get_count_rekam_delete($nik,$no_kec){
			$sql = "SELECT 
					  COUNT(1) JML
					  FROM BIODATA_WNI_DELETE A 
					  INNER JOIN DEMOGRAPHICS@DB221 B 
					  ON A.NIK = B.NIK 
					  WHERE A.NIK = $nik";
			if($no_kec != 0){
				$sql .= " AND A.NO_KEC = $no_kec";
			}
			$r = DB::connection('db222')->select($sql);
			return $r[0]->jml;
		}
		public function get_count_face_rekam($nik){
			$sql = "SELECT 
					  COUNT(1) JML
					  FROM FACES A 
					  WHERE A.NIK = $nik";
			$r = DB::connection('db221')->select($sql);
			return $r[0]->jml;
		}
		public function get_face_rekam($nik){
			$sql = "SELECT 
					  A.FACE
					  FROM FACES A 
					  WHERE A.NIK = $nik";
			$r = DB::connection('db221')->select($sql);
			return $r;
		}
		public function get_data_cetak($nik,$no_kec){
			$sql = "SELECT 
					  A.NIK
					  , A.NO_KK
					  , A.NAMA_LGKP
					  , A.TMPT_LHR
					  , TO_CHAR(A.TGL_LHR,'DD-MM-YYYY') TGL_LHR
					  , CASE WHEN A.JENIS_KLMIN = 1 THEN 'LAKI-LAKI' ELSE 'PEREMPUAN' END JENIS_KLMIN
					  , B.CURRENT_STATUS_CODE
					  FROM BIODATA_WNI A 
					  INNER JOIN DEMOGRAPHICS@DB2 B 
					  ON A.NIK = B.NIK 
					  WHERE A.NIK = $nik";
			if($no_kec != 0){
				$sql .= " AND A.NO_KEC = $no_kec";
			}
			$r = DB::connection('db222')->select($sql);
			return $r;
		}
		public function get_count_cetak($nik,$no_kec){
			$sql = "SELECT 
					  COUNT(1) JML
					  FROM BIODATA_WNI A 
					  INNER JOIN DEMOGRAPHICS@DB2 B 
					  ON A.NIK = B.NIK 
					  WHERE A.NIK = $nik";
			if($no_kec != 0){
				$sql .= " AND A.NO_KEC = $no_kec";
			}
			$r = DB::connection('db222')->select($sql);
			return $r[0]->jml;
		}

		public function get_data_cetak_delete($nik,$no_kec){
			$sql = "SELECT 
					  A.NIK
					  , A.NO_KK
					  , A.NAMA_LGKP
					  , A.TMPT_LHR
					  , TO_CHAR(A.TGL_LHR,'DD-MM-YYYY') TGL_LHR
					  , CASE WHEN A.JENIS_KLMIN = 1 THEN 'LAKI-LAKI' ELSE 'PEREMPUAN' END JENIS_KLMIN
					  , B.CURRENT_STATUS_CODE
					  FROM BIODATA_WNI A 
					  INNER JOIN DEMOGRAPHICS@DB2 B 
					  ON A.NIK = B.NIK 
					  WHERE A.NIK = $nik";
			if($no_kec != 0){
				$sql .= " AND A.NO_KEC = $no_kec";
			}
			$r = DB::connection('db222')->select($sql);
			return $r;
		}
		public function get_count_cetak_delete($nik,$no_kec){
			$sql = "SELECT 
					  COUNT(1) JML
					  FROM BIODATA_WNI A 
					  INNER JOIN DEMOGRAPHICS@DB2 B 
					  ON A.NIK = B.NIK 
					  WHERE A.NIK = $nik";
			if($no_kec != 0){
				$sql .= " AND A.NO_KEC = $no_kec";
			}
			$r = DB::connection('db222')->select($sql);
			return $r[0]->jml;
		}
		public function get_count_face_cetak($nik){
			$sql = "SELECT 
					  COUNT(1) JML
					  FROM FACES A 
					  WHERE A.NIK = $nik";
			$r = DB::connection('db2')->select($sql);
			return $r[0]->jml;
		}
		public function get_face_cetak($nik){
			$sql = "SELECT 
					  A.FACE
					  FROM FACES A 
					  WHERE A.NIK = $nik";
			$r = DB::connection('db2')->select($sql);
			return $r;
		}

	public function do_hist_delete($nik,$user_id,$ip_address,$del_option){
            $sql = "INSERT INTO SIAK_HIST_DEL_BIOMETRIC (ID,NIK,DELETE_DT,DELETE_BY,IP_ADDRESS,OPTION_DELETE) VALUES ('$nik-DELBIO-".time()."',$nik,SYSDATE,'$user_id','$ip_address','$del_option')";
            $r = DB::insert($sql);
        }

	public function do_hist_dewa_kematian($adm_akta_no,$mati_nik,$mati_no_kk,$mati_jns_kelamin,$mati_nama_lgkp,$mati_tmpt_lahir,$mati_tgl_lahir,$mati_tmpt_mati,$mati_tgl_mati,$ibu_nama_lgkp,$ayah_nama_lgkp,$user_id,$ip){
			$sql = "";
            $sql .= "
            	INSERT INTO 
            	SIAK_HIST_AKTA_KMT 
            	(
            		ID
            		, NIK
            		, NO_KK
            		, ADM_AKTA_NO
            		, MATI_JENIS_LMIN
            		, MATI_NAMA_LGKP
            		, MATI_TMPT_LHR
            		, MATI_TGL_LHR
            		, MATI_TMPT_MATI
            		, MATI_TGL_MATI
            		, MATI_NAMA_IBU
            		, MATI_NAMA_AYAH
            		, UPDATE_BY
            		, IP_ADDRESS
            	) 
            	VALUES 
            	(
	            	'$adm_akta_no-EDITKMT-".time()."' ";
	     	$sql .=  ($mati_nik != '-') ?  " , '$mati_nik' " : " , NULL ";
	        $sql .=  ($mati_no_kk != '-') ?  " , '$mati_no_kk' " : " , NULL ";
	        $sql .= ", '$adm_akta_no'
	            	, '$mati_jns_kelamin'
	            	, '$mati_nama_lgkp'
	            	, '$mati_tmpt_lahir'
	            	, TO_DATE('$mati_tgl_lahir','DD-MM-YYYY')
	            	, '$mati_tmpt_mati'
	            	, TO_DATE('$mati_tgl_mati','DD-MM-YYYY') ";
	        $sql .=  ($ibu_nama_lgkp != null) ?  " , '$ibu_nama_lgkp' " : " , NULL ";
	        $sql .=  ($ayah_nama_lgkp != null) ?  " , '$ayah_nama_lgkp' " : " , NULL ";
	        $sql .= ", '$user_id'
	            	, '$ip'
	            )";
            $r = DB::insert($sql);
        }
        public function do_update_dewa_kematian($adm_akta_no,$mati_nik,$mati_no_kk,$mati_jns_kelamin,$mati_nama_lgkp,$mati_tmpt_lahir,$mati_tgl_lahir,$mati_tmpt_mati,$mati_tgl_mati,$ibu_nama_lgkp,$ayah_nama_lgkp){
			$sql = "";
			$sql .= " UPDATE CAPIL_MATI SET ";
			$sql .=  ($mati_nik != '-') ?  " MATI_NIK = '$mati_nik' " : " MATI_NIK = NULL ";
			$sql .=  ($mati_no_kk != '-') ?  ", MATI_NO_KK = '$mati_no_kk' " : ", MATI_NO_KK = NULL ";
			$sql .= " , MATI_NAMA_LGKP = '$mati_nama_lgkp' ";
			$sql .= " , MATI_TMPT_LAHIR = '$mati_tmpt_lahir' ";
			$sql .= " , MATI_TGL_LAHIR = TO_DATE('$mati_tgl_lahir','DD-MM-YYYY') ";
			$sql .= " , MATI_TMPT_MATI = '$mati_tmpt_mati' ";
			$sql .= " , MATI_TGL_MATI = TO_DATE('$mati_tgl_mati','DD-MM-YYYY') ";
			$sql .=  ($ibu_nama_lgkp != null) ?  " , IBU_NAMA_LGKP = '$ibu_nama_lgkp' " : " , IBU_NAMA_LGKP = NULL ";
	        $sql .=  ($ayah_nama_lgkp != null) ?  " , AYAH_NAMA_LGKP = '$ayah_nama_lgkp' " : " , AYAH_NAMA_LGKP = NULL ";
			$sql .= " WHERE ADM_AKTA_NO = '$adm_akta_no' ";
            $r = DB::connection('db222')->update($sql);
        }
	
	public function do_delete_cetak_full($nik){ 
		$sql = "DELETE FROM DEMOGRAPHICS WHERE NIK = $nik"; 
		$q = DB::connection('db2')->delete($sql);
		$sql = "DELETE FROM FACES WHERE NIK = $nik"; 
		$q = DB::connection('db2')->delete($sql);
		$sql = "DELETE FROM FINGERS WHERE NIK = $nik"; 
		$q = DB::connection('db2')->delete($sql);
		$sql = "DELETE FROM IRIS WHERE NIK = $nik"; 
		$q = DB::connection('db2')->delete($sql);
		$sql = "DELETE FROM SIGNATURES WHERE NIK = $nik"; 
		$q = DB::connection('db2')->delete($sql);
		$sql = "DELETE FROM FACE_TEMPLATES WHERE NIK = $nik"; 
		$q = DB::connection('db2')->delete($sql);
		$sql = "DELETE FROM FINGER_TEMPLATES WHERE NIK = $nik"; 
		$q = DB::connection('db2')->delete($sql);
		$sql = "DELETE FROM IRIS_TEMPLATES WHERE NIK = $nik"; 
		$q = DB::connection('db2')->delete($sql);
		$sql = "DELETE FROM SIGNATURE_TEMPLATES WHERE NIK = $nik"; 
		$q = DB::connection('db2')->delete($sql);
		$sql = "DELETE FROM BIOMETRICS_LOSSLESS WHERE NIK = $nik"; 
		$q = DB::connection('db2')->delete($sql);
		$sql = "DELETE FROM BIOMETRIC_EXCEPTIONS WHERE NIK = $nik"; 
		$q = DB::connection('db2')->delete($sql);
		$sql = "DELETE FROM BIOMETRIC_DIAGNOSTICS WHERE NIK = $nik"; 
		$q = DB::connection('db2')->delete($sql);
		$sql = "DELETE FROM AUDITS WHERE NIK = $nik"; 
		$q = DB::connection('db2')->delete($sql);
		$sql = "DELETE FROM DUPLICATE_RESULTS WHERE NIK = $nik"; 
		$q = DB::connection('db2')->delete($sql);
		$sql = "DELETE FROM CARD_ISSUANCE_EVIDENCE WHERE NIK = $nik"; 
		$q = DB::connection('db2')->delete($sql);
		$sql = "DELETE FROM MIDDLEWARE_DIAGNOSTICS WHERE NIK = $nik"; 
		$q = DB::connection('db2')->delete($sql);
		$sql = "DELETE FROM REPROCESS_FAILED_NIKS WHERE NIK = $nik"; 
		$q = DB::connection('db2')->delete($sql);
		$sql = "DELETE FROM MANUAL_DEDUP_DIAGNOSTICS WHERE NIK = $nik"; 
		$q = DB::connection('db2')->delete($sql);
 	}

	public function do_delete_demographics_all($nik){ 
		$sql = "DELETE FROM DEMOGRAPHICS_ALL WHERE NIK = $nik"; 
		$q = DB::connection('db2')->delete($sql);
 	}
 	
	public function do_delete_rekam_full($nik){ 
		$sql = "INSERT INTO YZ_BIOMETRICS_LOSSLESS SELECT * FROM BIOMETRICS_LOSSLESS@DB221 A WHERE A.NIK = $nik AND NOT EXISTS (SELECT 1 FROM YZ_BIOMETRICS_LOSSLESS B WHERE A.NIK = B.NIK)"; 
		$q = DB::insert($sql);
		$sql = "DELETE FROM BIOMETRICS_LOSSLESS WHERE NIK = $nik"; 
		$q = DB::connection('db221')->delete($sql);
		$sql = "INSERT INTO YZ_BIOMETRIC_EXCEPTIONS SELECT * FROM BIOMETRIC_EXCEPTIONS@DB221 A WHERE A.NIK = $nik AND NOT EXISTS (SELECT 1 FROM YZ_BIOMETRIC_EXCEPTIONS B WHERE A.NIK = B.NIK)"; 
		$q = DB::insert($sql); 
		$sql = "DELETE FROM BIOMETRIC_EXCEPTIONS WHERE NIK = $nik"; 
		$q = DB::connection('db221')->delete($sql);
		$sql = "INSERT INTO YZ_BIOMETRIC_DIAGNOSTICS SELECT * FROM BIOMETRIC_DIAGNOSTICS@DB221 A WHERE A.NIK = $nik AND NOT EXISTS (SELECT 1 FROM YZ_BIOMETRIC_DIAGNOSTICS B WHERE A.NIK = B.NIK)"; 
		$q = DB::insert($sql); 
		$sql = "DELETE FROM BIOMETRIC_DIAGNOSTICS WHERE NIK = $nik"; 
		$q = DB::connection('db221')->delete($sql);
		$sql = "INSERT INTO YZ_AUDITS SELECT * FROM AUDITS@DB221 A WHERE A.NIK = $nik AND NOT EXISTS (SELECT 1 FROM YZ_AUDITS B WHERE A.NIK = B.NIK)"; 
		$q = DB::insert($sql); 
		$sql = "DELETE FROM AUDITS WHERE NIK = $nik"; 
		$q = DB::connection('db221')->delete($sql);
		$sql = "INSERT INTO YZ_DUPLICATE_RESULTS SELECT * FROM DUPLICATE_RESULTS@DB221 A WHERE A.NIK = $nik AND NOT EXISTS (SELECT 1 FROM YZ_DUPLICATE_RESULTS B WHERE A.NIK = B.NIK)"; 
		$q = DB::insert($sql); 
		$sql = "DELETE FROM DUPLICATE_RESULTS WHERE NIK = $nik"; 
		$q = DB::connection('db221')->delete($sql);
		$sql = "INSERT INTO YZ_CARD_ISSUANCE_EVIDENCE SELECT * FROM CARD_ISSUANCE_EVIDENCE@DB221 A WHERE A.NIK = $nik AND NOT EXISTS (SELECT 1 FROM YZ_CARD_ISSUANCE_EVIDENCE B WHERE A.NIK = B.NIK)"; 
		$q = DB::insert($sql); 
		$sql = "DELETE FROM CARD_ISSUANCE_EVIDENCE WHERE NIK = $nik"; 
		$q = DB::connection('db221')->delete($sql);
		$sql = "INSERT INTO YZ_MIDDLEWARE_DIAGNOSTICS SELECT * FROM MIDDLEWARE_DIAGNOSTICS@DB221 A WHERE A.NIK = $nik AND NOT EXISTS (SELECT 1 FROM YZ_MIDDLEWARE_DIAGNOSTICS B WHERE A.NIK = B.NIK)"; 
		$q = DB::insert($sql); 
		$sql = "DELETE FROM MIDDLEWARE_DIAGNOSTICS WHERE NIK = $nik"; 
		$q = DB::connection('db221')->delete($sql);
		$sql = "INSERT INTO YZ_REPROCESS_FAILED_NIKS SELECT * FROM REPROCESS_FAILED_NIKS@DB221 A WHERE A.NIK = $nik AND NOT EXISTS (SELECT 1 FROM YZ_REPROCESS_FAILED_NIKS B WHERE A.NIK = B.NIK)"; 
		$q = DB::insert($sql); 
		$sql = "DELETE FROM REPROCESS_FAILED_NIKS WHERE NIK = $nik"; 
		$q = DB::connection('db221')->delete($sql);
		$sql = "INSERT INTO YZ_MANUAL_DEDUP_DIAGNOSTICS SELECT * FROM MANUAL_DEDUP_DIAGNOSTICS@DB221 A WHERE A.NIK = $nik AND NOT EXISTS (SELECT 1 FROM YZ_MANUAL_DEDUP_DIAGNOSTICS B WHERE A.NIK = B.NIK)"; 
		$q = DB::insert($sql); 
		$sql = "DELETE FROM MANUAL_DEDUP_DIAGNOSTICS WHERE NIK = $nik"; 
		$q = DB::connection('db221')->delete($sql);
		$sql = "INSERT INTO YZ_FACES SELECT * FROM FACES@DB221 A WHERE A.NIK = $nik AND NOT EXISTS (SELECT 1 FROM YZ_FACES B WHERE A.NIK = B.NIK)"; 
		$q = DB::insert($sql);
		$sql = "DELETE FROM FACES WHERE NIK = $nik"; 
		$q = DB::connection('db221')->delete($sql);
		$sql = "INSERT INTO YZ_FINGERS SELECT * FROM FINGERS@DB221 A WHERE A.NIK = $nik AND NOT EXISTS (SELECT 1 FROM YZ_FINGERS B WHERE A.NIK = B.NIK)"; 
		$q = DB::insert($sql);
		$sql = "DELETE FROM FINGERS WHERE NIK = $nik"; 
		$q = DB::connection('db221')->delete($sql);
		$sql = "INSERT INTO YZ_IRIS SELECT * FROM IRIS@DB221 A WHERE A.NIK = $nik AND NOT EXISTS (SELECT 1 FROM YZ_IRIS B WHERE A.NIK = B.NIK)"; 
		$q = DB::insert($sql);
		$sql = "DELETE FROM IRIS WHERE NIK = $nik"; 
		$q = DB::connection('db221')->delete($sql);
		$sql = "INSERT INTO YZ_SIGNATURES SELECT * FROM SIGNATURES@DB221 A WHERE A.NIK = $nik AND NOT EXISTS (SELECT 1 FROM YZ_SIGNATURES B WHERE A.NIK = B.NIK)"; 
		$q = DB::insert($sql);
		$sql = "DELETE FROM SIGNATURES WHERE NIK = $nik"; 
		$q = DB::connection('db221')->delete($sql);
		$sql = "INSERT INTO YZ_FACE_TEMPLATES SELECT * FROM FACE_TEMPLATES@DB221 A WHERE A.NIK = $nik AND NOT EXISTS (SELECT 1 FROM YZ_FACE_TEMPLATES B WHERE A.NIK = B.NIK)"; 
		$q = DB::insert($sql);
		$sql = "DELETE FROM FACE_TEMPLATES WHERE NIK = $nik"; 
		$q = DB::connection('db221')->delete($sql);
		$sql = "INSERT INTO YZ_FINGER_TEMPLATES SELECT * FROM FINGER_TEMPLATES@DB221 A WHERE A.NIK = $nik AND NOT EXISTS (SELECT 1 FROM YZ_FINGER_TEMPLATES B WHERE A.NIK = B.NIK)"; 
		$q = DB::insert($sql);
		$sql = "DELETE FROM FINGER_TEMPLATES WHERE NIK = $nik"; 
		$q = DB::connection('db221')->delete($sql);
		$sql = "INSERT INTO YZ_IRIS_TEMPLATES SELECT * FROM IRIS_TEMPLATES@DB221 A WHERE A.NIK = $nik AND NOT EXISTS (SELECT 1 FROM YZ_IRIS_TEMPLATES B WHERE A.NIK = B.NIK)"; 
		$q = DB::insert($sql);
		$sql = "DELETE FROM IRIS_TEMPLATES WHERE NIK = $nik"; 
		$q = DB::connection('db221')->delete($sql);
		$sql = "INSERT INTO YZ_SIGNATURE_TEMPLATES SELECT * FROM SIGNATURE_TEMPLATES@DB221 A WHERE A.NIK = $nik AND NOT EXISTS (SELECT 1 FROM YZ_SIGNATURE_TEMPLATES B WHERE A.NIK = B.NIK)"; 
		$q = DB::insert($sql); 
		$sql = "DELETE FROM SIGNATURE_TEMPLATES WHERE NIK = $nik"; 
		$q = DB::connection('db221')->delete($sql);
		$sql = "INSERT INTO YZ_DEMOGRAPHICS SELECT * FROM DEMOGRAPHICS@DB221 A WHERE A.NIK = $nik AND NOT EXISTS (SELECT 1 FROM YZ_DEMOGRAPHICS B WHERE A.NIK = B.NIK)"; 
		$q = DB::insert($sql);
		$sql = "DELETE FROM DEMOGRAPHICS WHERE NIK = $nik"; 
		$q = DB::connection('db221')->delete($sql);

		$sql = "DELETE FROM BIOMETRICS_LOSSLESS WHERE NIK = $nik"; 
		$q = DB::connection('db5')->delete($sql);
		$sql = "DELETE FROM BIOMETRIC_EXCEPTIONS WHERE NIK = $nik"; 
		$q = DB::connection('db5')->delete($sql);
		$sql = "DELETE FROM BIOMETRIC_DIAGNOSTICS WHERE NIK = $nik"; 
		$q = DB::connection('db5')->delete($sql);
		$sql = "DELETE FROM AUDITS WHERE NIK = $nik"; 
		$q = DB::connection('db5')->delete($sql);
		$sql = "DELETE FROM DUPLICATE_RESULTS WHERE NIK = $nik"; 
		$q = DB::connection('db5')->delete($sql);
		$sql = "DELETE FROM CARD_ISSUANCE_EVIDENCE WHERE NIK = $nik"; 
		$q = DB::connection('db5')->delete($sql);
		$sql = "DELETE FROM MIDDLEWARE_DIAGNOSTICS WHERE NIK = $nik"; 
		$q = DB::connection('db5')->delete($sql);
		$sql = "DELETE FROM REPROCESS_FAILED_NIKS WHERE NIK = $nik"; 
		$q = DB::connection('db5')->delete($sql);
		$sql = "DELETE FROM MANUAL_DEDUP_DIAGNOSTICS WHERE NIK = $nik"; 
		$q = DB::connection('db5')->delete($sql);
		$sql = "DELETE FROM FACES WHERE NIK = $nik"; 
		$q = DB::connection('db5')->delete($sql);
		$sql = "DELETE FROM FINGERS WHERE NIK = $nik"; 
		$q = DB::connection('db5')->delete($sql);
		$sql = "DELETE FROM IRIS WHERE NIK = $nik"; 
		$q = DB::connection('db5')->delete($sql);
		$sql = "DELETE FROM SIGNATURES WHERE NIK = $nik"; 
		$q = DB::connection('db5')->delete($sql);
		$sql = "DELETE FROM FACE_TEMPLATES WHERE NIK = $nik"; 
		$q = DB::connection('db5')->delete($sql);
		$sql = "DELETE FROM FINGER_TEMPLATES WHERE NIK = $nik"; 
		$q = DB::connection('db5')->delete($sql);
		$sql = "DELETE FROM IRIS_TEMPLATES WHERE NIK = $nik"; 
		$q = DB::connection('db5')->delete($sql);
		$sql = "DELETE FROM SIGNATURE_TEMPLATES WHERE NIK = $nik"; 
		$q = DB::connection('db5')->delete($sql);
		$sql = "DELETE FROM DEMOGRAPHICS WHERE NIK = $nik"; 
		$q = DB::connection('db5')->delete($sql);

		$sql = "DELETE FROM BIOMETRICS_LOSSLESS WHERE NIK = $nik"; 
		$q = DB::connection('db5cetak')->delete($sql);
		$sql = "DELETE FROM BIOMETRIC_EXCEPTIONS WHERE NIK = $nik"; 
		$q = DB::connection('db5cetak')->delete($sql);
		$sql = "DELETE FROM BIOMETRIC_DIAGNOSTICS WHERE NIK = $nik"; 
		$q = DB::connection('db5cetak')->delete($sql);
		$sql = "DELETE FROM AUDITS WHERE NIK = $nik"; 
		$q = DB::connection('db5cetak')->delete($sql);
		$sql = "DELETE FROM DUPLICATE_RESULTS WHERE NIK = $nik"; 
		$q = DB::connection('db5cetak')->delete($sql);
		$sql = "DELETE FROM CARD_ISSUANCE_EVIDENCE WHERE NIK = $nik"; 
		$q = DB::connection('db5cetak')->delete($sql);
		$sql = "DELETE FROM MIDDLEWARE_DIAGNOSTICS WHERE NIK = $nik"; 
		$q = DB::connection('db5cetak')->delete($sql);
		$sql = "DELETE FROM REPROCESS_FAILED_NIKS WHERE NIK = $nik"; 
		$q = DB::connection('db5cetak')->delete($sql);
		$sql = "DELETE FROM MANUAL_DEDUP_DIAGNOSTICS WHERE NIK = $nik"; 
		$q = DB::connection('db5cetak')->delete($sql);
		$sql = "DELETE FROM FACES WHERE NIK = $nik"; 
		$q = DB::connection('db5cetak')->delete($sql);
		$sql = "DELETE FROM FINGERS WHERE NIK = $nik"; 
		$q = DB::connection('db5cetak')->delete($sql);
		$sql = "DELETE FROM IRIS WHERE NIK = $nik"; 
		$q = DB::connection('db5cetak')->delete($sql);
		$sql = "DELETE FROM SIGNATURES WHERE NIK = $nik"; 
		$q = DB::connection('db5cetak')->delete($sql);
		$sql = "DELETE FROM FACE_TEMPLATES WHERE NIK = $nik"; 
		$q = DB::connection('db5cetak')->delete($sql);
		$sql = "DELETE FROM FINGER_TEMPLATES WHERE NIK = $nik"; 
		$q = DB::connection('db5cetak')->delete($sql);
		$sql = "DELETE FROM IRIS_TEMPLATES WHERE NIK = $nik"; 
		$q = DB::connection('db5cetak')->delete($sql);
		$sql = "DELETE FROM SIGNATURE_TEMPLATES WHERE NIK = $nik"; 
		$q = DB::connection('db5cetak')->delete($sql);
		$sql = "DELETE FROM DEMOGRAPHICS WHERE NIK = $nik"; 
		$q = DB::connection('db5cetak')->delete($sql);
	
	
		$sql = "DELETE FROM SIAK_DUPLICATE_FULL WHERE NIK = $nik"; 
		$q = DB::delete($sql);
 	}


		public function count_restore_cek_siak($nik,$no_kec){
			$sql = "";
			$sql .= "SELECT 
					  COUNT(1) JML
					  FROM BIODATA_WNI A 
					  WHERE A.NIK = $nik";
			  if ($no_kec != 0){
			  		$sql .= " AND A.NO_KEC = $no_kec";
			  }
			$r = DB::connection('db222')->select($sql);
			return $r[0]->jml;
		}
		public function count_restore_cek_siak_no_kk($no_kk,$no_kec){
			$sql = "";
			$sql .= "SELECT 
					  COUNT(1) JML
					  FROM BIODATA_WNI A 
					  WHERE A.NO_KK = $no_kk";
				if ($no_kec != 0){
				  	$sql .= " AND A.NO_KEC = $no_kec";
				}
			$r = DB::connection('db222')->select($sql);
			return $r[0]->jml;
		}
		public function get_restore_cek_siak($nik){
			$sql = "SELECT 
					  A.NIK
					  , A.NO_KK
					  , A.NAMA_LGKP
					  , A.TMPT_LHR
					  , TO_CHAR(A.TGL_LHR,'DD-MM-YYYY') TGL_LHR
					  , A.NO_KEC
				      , A.NO_KEL
				      , A.AKTA_LHR
				      , A.NO_AKTA_LHR
            		  , F5_GET_NAMA_KECAMATAN(A.NO_PROP,A.NO_KAB,A.NO_KEC) NAMA_KEC
            		  , F5_GET_NAMA_KELURAHAN(A.NO_PROP,A.NO_KAB,A.NO_KEC,A.NO_KEL) NAMA_KEL
            		  , CASE WHEN D.NO_RT IS NULL THEN '-' ELSE LPAD(TO_CHAR(D.NO_RT), 3, '0') END AS RT
					  , CASE WHEN D.NO_RW IS NULL THEN '-' ELSE LPAD(TO_CHAR(D.NO_RW), 3, '0') END AS RW
					  , CASE WHEN D.ALAMAT IS NULL THEN '-' ELSE D.ALAMAT END ALAMAT
					  , CASE WHEN D.NAMA_KEP IS NULL THEN '-' ELSE D.NAMA_KEP END NAMA_KEP
            		  , UPPER(F5_GET_REF_WNI(A.STAT_HBKEL, 301)) STAT_HBKEL
					  , CASE WHEN A.JENIS_KLMIN = 1 THEN 'LAKI-LAKI' ELSE 'PEREMPUAN' END JENIS_KLMIN
					  , CASE WHEN B.CURRENT_STATUS_CODE IS NULL THEN 'BELUM REKAM' ELSE B.CURRENT_STATUS_CODE END CURRENT_STATUS_CODE
					  , 'AKTIF DI SIAK' KET
					  , CASE WHEN E.PATH IS NULL THEN '-' ELSE E.PATH END PATH
					  FROM BIODATA_WNI A LEFT JOIN DATA_KELUARGA D ON A.NO_KK = D.NO_KK
					  LEFT JOIN DEMOGRAPHICS_ALL@DB2 B 
					  ON A.NIK = B.NIK 
					  LEFT JOIN T5_FOTO E ON A.NIK = E.NIK
					  WHERE A.NIK = $nik";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}
		public function count_restore_cek_delete($nik,$no_kec){
			$sql = "";
			$sql .= "SELECT 
					  COUNT(1) JML
					  FROM BIODATA_WNI_DELETE A 
					  LEFT JOIN BIODATA_WNI X ON A.NIK = X.NIK
					  WHERE A.NIK = $nik
					  AND X.NIK IS NULL
					  ";
				if ($no_kec != 0){
				  	$sql .= " AND A.NO_KEC = $no_kec";
				}
			$r = DB::connection('db222')->select($sql);
			return $r[0]->jml;
		}
		public function count_restore_cek_delete_no_kk($no_kk,$no_kec){
			$sql = "SELECT 
					  COUNT(1) JML
					  FROM BIODATA_WNI_DELETE A 
					  LEFT JOIN BIODATA_WNI X ON A.NO_KK = X.NO_KK
					  WHERE A.NO_KK = $no_kk
					  AND X.NO_KK IS NULL
					  ";
			$r = DB::connection('db222')->select($sql);
			return $r[0]->jml;
		}
		public function count_restore_cek_kk($nik,$no_kec){
			$sql = "";
			$sql .= "SELECT 
					  COUNT(1) JML
					  FROM BIODATA_WNI_DELETE A 
					  LEFT JOIN BIODATA_WNI X ON A.NIK = X.NIK
					  WHERE A.NIK = $nik
					  AND X.NIK IS NULL
					  AND EXISTS (SELECT 1 FROM BIODATA_WNI G WHERE A.NO_KK = G.NO_KK AND G.STAT_HBKEL =1)
					  ";
				if ($no_kec != 0){
				  	$sql .= " AND A.NO_KEC = $no_kec";
				}
			$r = DB::connection('db222')->select($sql);
			return $r[0]->jml;
		}
		public function get_restore_cek_delete($nik){
			$sql = "SELECT 
					  A.NIK
					  , A.NO_KK
					  , A.NAMA_LGKP
					  , A.TMPT_LHR
					  , TO_CHAR(A.TGL_LHR,'DD-MM-YYYY') TGL_LHR
					  , A.NO_KEC
				      , A.NO_KEL
				      , A.AKTA_LHR
				      , A.NO_AKTA_LHR
            		  , F5_GET_NAMA_KECAMATAN(A.NO_PROP,A.NO_KAB,A.NO_KEC) NAMA_KEC
            		  , F5_GET_NAMA_KELURAHAN(A.NO_PROP,A.NO_KAB,A.NO_KEC,A.NO_KEL) NAMA_KEL
            		  , CASE WHEN D.NO_RT IS NULL THEN (CASE WHEN C.NO_RT IS NULL THEN '-' ELSE LPAD(TO_CHAR(C.NO_RT), 3, '0') END) ELSE LPAD(TO_CHAR(D.NO_RT), 3, '0') END AS RT
					  , CASE WHEN D.NO_RW IS NULL THEN (CASE WHEN C.NO_RW IS NULL THEN '-' ELSE LPAD(TO_CHAR(C.NO_RW), 3, '0') END) ELSE LPAD(TO_CHAR(D.NO_RW), 3, '0') END AS RW
					  , CASE WHEN D.ALAMAT IS NULL THEN (CASE WHEN C.ALAMAT IS NULL THEN '-' ELSE C.ALAMAT END) ELSE D.ALAMAT END ALAMAT
					  , CASE WHEN D.NAMA_KEP IS NULL THEN (CASE WHEN C.NAMA_KEP IS NULL THEN '-' ELSE C.NAMA_KEP END) ELSE D.NAMA_KEP END NAMA_KEP
            		  , UPPER(F5_GET_REF_WNI(A.STAT_HBKEL, 301)) STAT_HBKEL
					  , CASE WHEN A.JENIS_KLMIN = 1 THEN 'LAKI-LAKI' ELSE 'PEREMPUAN' END JENIS_KLMIN
					  , CASE WHEN B.CURRENT_STATUS_CODE IS NULL THEN 'BELUM REKAM' ELSE B.CURRENT_STATUS_CODE END CURRENT_STATUS_CODE
					  , 'TIDAK AKTIF DI SIAK' KET
					  , CASE WHEN E.PATH IS NULL THEN '-' ELSE E.PATH END PATH
                      , CASE WHEN Y.ALASAN IS NULL THEN '-' ELSE Y.ALASAN END ALASAN
                      , CASE WHEN Y.DESC_ALASAN IS NULL THEN '-' ELSE Y.DESC_ALASAN END DESC_ALASAN
                      , CASE WHEN Y.CREATED_BY IS NULL THEN '-' ELSE UPPER(Y.CREATED_BY) END CREATED_BY
                      , CASE WHEN Y.TGL_DELETE IS NULL THEN '-' ELSE Y.TGL_DELETE END DELETE_DT
					  FROM BIODATA_WNI_DELETE A 
					  LEFT JOIN DATA_KELUARGA D ON A.NO_KK = D.NO_KK
					  LEFT JOIN DATA_KELUARGA_DELETE C ON A.NO_KK = C.NO_KK
					  LEFT JOIN DEMOGRAPHICS_ALL@DB2 B 
            		  ON A.NIK = B.NIK 
					  LEFT JOIN T5_FOTO E ON A.NIK = E.NIK
					  LEFT JOIN BIODATA_WNI X ON A.NIK = X.NIK
					  LEFT JOIN (SELECT ID_TABLE, TBL_REF, ALASAN,CASE WHEN DESC_ALASAN IS NULL THEN '-' ELSE UPPER(DESC_ALASAN) END DESC_ALASAN,REF_DELETE_KEY, CREATED_BY, TO_CHAR(CREATED_DATE,'DD-MM-YYYY') TGL_DELETE,REF_DELETE  FROM (SELECT ID_TABLE, TBL_REF, ALASAN,DESC_ALASAN,REF_DELETE_KEY, CREATED_BY, CREATED_DATE, REF_DELETE, RANK() OVER (PARTITION BY REF_DELETE_KEY ORDER BY CREATED_DATE DESC,ID_TABLE DESC) RNK FROM T7_NOTE_DELETE)  WHERE RNK = 1 AND REF_DELETE = 'BIODATA_WNI') Y ON A.NIK = Y.REF_DELETE_KEY
					  WHERE A.NIK = $nik
					  AND X.NIK IS NULL";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}
		public function get_restore_cek_delete_kk($nik){
			$sql = "SELECT 
				    A.NIK
				    , A.NO_KK
				    , UPPER(F5_GET_REF_WNI(A.STAT_HBKEL, 301)) STAT_HUBKEL
				    , A.STAT_HBKEL
				    , A.NAMA_LGKP
				    , TO_CHAR(A.TGL_LHR,'DD-MM-YYYY') TGL_LHR
				    , A.NO_KEC
				    , A.NO_KEL
				    , A.AKTA_LHR
				    , A.NO_AKTA_LHR
				    , F5_GET_NAMA_KECAMATAN (A.NO_PROP,A.NO_KAB,A.NO_KEC) NAMA_KECAMATAN
				    , F5_GET_NAMA_KELURAHAN (A.NO_PROP,A.NO_KAB,A.NO_KEC,A.NO_KEL) NAMA_KELURAHAN
				    , CASE WHEN B.CURRENT_STATUS_CODE IS NULL THEN 'BELUM REKAM' ELSE B.CURRENT_STATUS_CODE END CURRENT_STATUS_CODE
				    , CASE WHEN A.FLAG_STATUS = 0 THEN 'AKTIF' ELSE 'TIDAK AKTIF' END FLAG_STATUS
				    , 'TIDAK AKTIF DI SIAK'  DELETED_BY
				    FROM BIODATA_WNI_DELETE A
				    LEFT JOIN DEMOGRAPHICS_ALL@DB2 B ON A.NIK = B.NIK
				    WHERE A.NO_KK IN 
				    (SELECT C.NO_KK FROM BIODATA_WNI_DELETE C WHERE C.NIK =$nik  )
				    UNION ALL
				    SELECT 
				    A.NIK
				    , A.NO_KK
				    , UPPER(F5_GET_REF_WNI(A.STAT_HBKEL, 301)) STAT_HUBKEL
				    , A.STAT_HBKEL
				    , A.NAMA_LGKP
				    , TO_CHAR(A.TGL_LHR,'DD-MM-YYYY') TGL_LHR
				    , A.NO_KEC
				    , A.NO_KEL
				    , A.AKTA_LHR
				    , A.NO_AKTA_LHR
				    , F5_GET_NAMA_KECAMATAN (A.NO_PROP,A.NO_KAB,A.NO_KEC) NAMA_KECAMATAN
				    , F5_GET_NAMA_KELURAHAN (A.NO_PROP,A.NO_KAB,A.NO_KEC,A.NO_KEL) NAMA_KELURAHAN
				    , CASE WHEN B.CURRENT_STATUS_CODE IS NULL THEN 'BELUM REKAM' ELSE B.CURRENT_STATUS_CODE END CURRENT_STATUS_CODE
				    , CASE WHEN A.FLAG_STATUS = 0 THEN 'AKTIF' ELSE 'TIDAK AKTIF' END FLAG_STATUS
				    , 'AKTIF DI SIAK'
				    FROM BIODATA_WNI A
				    LEFT JOIN DEMOGRAPHICS_ALL@DB2 B ON A.NIK = B.NIK
				    WHERE A.NO_KK IN 
				    (SELECT C.NO_KK FROM BIODATA_WNI_DELETE C WHERE C.NIK =$nik  ) ORDER BY STAT_HBKEL";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}
		public function get_restore_cek_delete_nokk($no_kk){
			$sql = "SELECT 
				    A.NIK
				    , A.NO_KK
				    , UPPER(F5_GET_REF_WNI(A.STAT_HBKEL, 301)) STAT_HUBKEL
				    , A.STAT_HBKEL
				    , A.NAMA_LGKP
				    , TO_CHAR(A.TGL_LHR,'DD-MM-YYYY') TGL_LHR
				    , A.NO_KEC
				    , A.NO_KEL
				    , A.AKTA_LHR
				    , A.NO_AKTA_LHR
				    , F5_GET_NAMA_KECAMATAN (A.NO_PROP,A.NO_KAB,A.NO_KEC) NAMA_KECAMATAN
				    , F5_GET_NAMA_KELURAHAN (A.NO_PROP,A.NO_KAB,A.NO_KEC,A.NO_KEL) NAMA_KELURAHAN
				    , CASE WHEN B.CURRENT_STATUS_CODE IS NULL THEN 'BELUM REKAM' ELSE B.CURRENT_STATUS_CODE END CURRENT_STATUS_CODE
				    , CASE WHEN A.FLAG_STATUS = 0 THEN 'AKTIF' ELSE 'TIDAK AKTIF' END FLAG_STATUS
				    , 'TIDAK AKTIF DI SIAK'  DELETED_BY
				    FROM BIODATA_WNI_DELETE A
				    LEFT JOIN DEMOGRAPHICS_ALL@DB2 B ON A.NIK = B.NIK
				    WHERE A.NO_KK = $no_kk
				    UNION ALL
				    SELECT 
				    A.NIK
				    , A.NO_KK
				    , UPPER(F5_GET_REF_WNI(A.STAT_HBKEL, 301)) STAT_HUBKEL
				    , A.STAT_HBKEL
				    , A.NAMA_LGKP
				    , TO_CHAR(A.TGL_LHR,'DD-MM-YYYY') TGL_LHR
				    , A.NO_KEC
				    , A.NO_KEL
				    , A.AKTA_LHR
				    , A.NO_AKTA_LHR
				    , F5_GET_NAMA_KECAMATAN (A.NO_PROP,A.NO_KAB,A.NO_KEC) NAMA_KECAMATAN
				    , F5_GET_NAMA_KELURAHAN (A.NO_PROP,A.NO_KAB,A.NO_KEC,A.NO_KEL) NAMA_KELURAHAN
				    , CASE WHEN B.CURRENT_STATUS_CODE IS NULL THEN 'BELUM REKAM' ELSE B.CURRENT_STATUS_CODE END CURRENT_STATUS_CODE
				    , CASE WHEN A.FLAG_STATUS = 0 THEN 'AKTIF' ELSE 'TIDAK AKTIF' END FLAG_STATUS
				    , 'AKTIF DI SIAK'
				    FROM BIODATA_WNI A
				    LEFT JOIN DEMOGRAPHICS_ALL@DB2 B ON A.NIK = B.NIK
				    WHERE A.NO_KK = $no_kk ORDER BY STAT_HBKEL";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}
		public function get_restore_cek_wni_kk($nik){
			$sql = "SELECT 
				    A.NIK
				    , A.NO_KK
				    , UPPER(F5_GET_REF_WNI(A.STAT_HBKEL, 301)) STAT_HUBKEL
				    , A.STAT_HBKEL
				    , A.NAMA_LGKP
				    , TO_CHAR(A.TGL_LHR,'DD-MM-YYYY') TGL_LHR
				    , A.NO_KEC
				    , A.NO_KEL
				    , A.AKTA_LHR
				    , A.NO_AKTA_LHR
				    , F5_GET_NAMA_KECAMATAN (A.NO_PROP,A.NO_KAB,A.NO_KEC) NAMA_KECAMATAN
				    , F5_GET_NAMA_KELURAHAN (A.NO_PROP,A.NO_KAB,A.NO_KEC,A.NO_KEL) NAMA_KELURAHAN
				    , CASE WHEN B.CURRENT_STATUS_CODE IS NULL THEN 'BELUM REKAM' ELSE B.CURRENT_STATUS_CODE END CURRENT_STATUS_CODE
				    , CASE WHEN A.FLAG_STATUS = 0 THEN 'AKTIF' ELSE 'TIDAK AKTIF' END FLAG_STATUS
				    , 'TIDAK AKTIF DI SIAK'  DELETED_BY
				    FROM BIODATA_WNI_DELETE A
				    LEFT JOIN DEMOGRAPHICS_ALL@DB2 B ON A.NIK = B.NIK
				    WHERE A.NO_KK IN 
				    (SELECT C.NO_KK FROM BIODATA_WNI C WHERE C.NIK =$nik  )
				    UNION ALL
				    SELECT 
				    A.NIK
				    , A.NO_KK
				    , UPPER(F5_GET_REF_WNI(A.STAT_HBKEL, 301)) STAT_HUBKEL
				    , A.STAT_HBKEL
				    , A.NAMA_LGKP
				    , TO_CHAR(A.TGL_LHR,'DD-MM-YYYY') TGL_LHR
				    , A.NO_KEC
				    , A.NO_KEL
				    , A.AKTA_LHR
				    , A.NO_AKTA_LHR
				    , F5_GET_NAMA_KECAMATAN (A.NO_PROP,A.NO_KAB,A.NO_KEC) NAMA_KECAMATAN
				    , F5_GET_NAMA_KELURAHAN (A.NO_PROP,A.NO_KAB,A.NO_KEC,A.NO_KEL) NAMA_KELURAHAN
				    , CASE WHEN B.CURRENT_STATUS_CODE IS NULL THEN 'BELUM REKAM' ELSE B.CURRENT_STATUS_CODE END CURRENT_STATUS_CODE
				    , CASE WHEN A.FLAG_STATUS = 0 THEN 'AKTIF' ELSE 'TIDAK AKTIF' END FLAG_STATUS
				    , 'AKTIF DI SIAK'
				    FROM BIODATA_WNI A
				    LEFT JOIN DEMOGRAPHICS_ALL@DB2 B ON A.NIK = B.NIK
				    WHERE A.NO_KK IN 
				    (SELECT C.NO_KK FROM BIODATA_WNI C WHERE C.NIK =$nik  ) ORDER BY STAT_HBKEL";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}
		public function get_restore_cek_wni_nokk($no_kk){
			$sql = "SELECT 
				    A.NIK
				    , A.NO_KK
				    , UPPER(F5_GET_REF_WNI(A.STAT_HBKEL, 301)) STAT_HUBKEL
				    , A.STAT_HBKEL
				    , A.NAMA_LGKP
				    , TO_CHAR(A.TGL_LHR,'DD-MM-YYYY') TGL_LHR
				    , A.NO_KEC
				    , A.NO_KEL
				    , A.AKTA_LHR
				    , A.NO_AKTA_LHR
				    , F5_GET_NAMA_KECAMATAN (A.NO_PROP,A.NO_KAB,A.NO_KEC) NAMA_KECAMATAN
				    , F5_GET_NAMA_KELURAHAN (A.NO_PROP,A.NO_KAB,A.NO_KEC,A.NO_KEL) NAMA_KELURAHAN
				    , CASE WHEN B.CURRENT_STATUS_CODE IS NULL THEN 'BELUM REKAM' ELSE B.CURRENT_STATUS_CODE END CURRENT_STATUS_CODE
				    , CASE WHEN A.FLAG_STATUS = 0 THEN 'AKTIF' ELSE 'TIDAK AKTIF' END FLAG_STATUS
				    , 'TIDAK AKTIF DI SIAK'  DELETED_BY
				    FROM BIODATA_WNI_DELETE A
				    LEFT JOIN DEMOGRAPHICS_ALL@DB2 B ON A.NIK = B.NIK
				    WHERE A.NO_KK = $no_kk
				    UNION ALL
				    SELECT 
				    A.NIK
				    , A.NO_KK
				    , UPPER(F5_GET_REF_WNI(A.STAT_HBKEL, 301)) STAT_HUBKEL
				    , A.STAT_HBKEL
				    , A.NAMA_LGKP
				    , TO_CHAR(A.TGL_LHR,'DD-MM-YYYY') TGL_LHR
				    , A.NO_KEC
				    , A.NO_KEL
				    , A.AKTA_LHR
				    , A.NO_AKTA_LHR
				    , F5_GET_NAMA_KECAMATAN (A.NO_PROP,A.NO_KAB,A.NO_KEC) NAMA_KECAMATAN
				    , F5_GET_NAMA_KELURAHAN (A.NO_PROP,A.NO_KAB,A.NO_KEC,A.NO_KEL) NAMA_KELURAHAN
				    , CASE WHEN B.CURRENT_STATUS_CODE IS NULL THEN 'BELUM REKAM' ELSE B.CURRENT_STATUS_CODE END CURRENT_STATUS_CODE
				    , CASE WHEN A.FLAG_STATUS = 0 THEN 'AKTIF' ELSE 'TIDAK AKTIF' END FLAG_STATUS
				    , 'AKTIF DI SIAK'
				    FROM BIODATA_WNI A
				    LEFT JOIN DEMOGRAPHICS_ALL@DB2 B ON A.NIK = B.NIK
				    WHERE A.NO_KK = $no_kk ORDER BY STAT_HBKEL";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}
		public function get_history_pindah($nik){
			$sql = "SELECT 
				  NO_PINDAH
				  	,KLASIFIKASI_PINDAH
				  	,FROM_NO_PROP
				    ,FROM_NO_KAB
				    ,FROM_NO_KEC
				    ,FROM_NO_KEL
				    ,DEST_NO_PROP
				    ,DEST_NO_KAB
				    ,DEST_NO_KEC
				    ,DEST_NO_KEL
				    ,DARI_NAMA_PROVINSI
				    ,DARI_NAMA_KABUPATEN
				    ,DARI_NAMA_KECAMATAN
				    ,DARI_NAMA_KELURAHAN
				    ,TUJUAN_NAMA_PROVINSI
				    ,TUJUAN_NAMA_KABUPATEN
				    ,TUJUAN_NAMA_KECAMATAN
				    ,TUJUAN_NAMA_KELURAHAN
				  	,CREATED_DATE
				  	,NIK  
				  FROM 
				  (SELECT 
				    A.NO_PINDAH
				    ,A.FROM_NO_PROP
				    ,A.FROM_NO_KAB
				    ,A.FROM_NO_KEC
				    ,A.FROM_NO_KEL
				    ,A.DEST_NO_PROP
				    ,A.DEST_NO_KAB
				    ,A.DEST_NO_KEC
				    ,A.DEST_NO_KEL
				    ,F5_GET_NAMA_PROVINSI (A.FROM_NO_PROP) DARI_NAMA_PROVINSI
				    ,F5_GET_NAMA_KABUPATEN (A.FROM_NO_PROP,A.FROM_NO_KAB) DARI_NAMA_KABUPATEN
				    ,F5_GET_NAMA_KECAMATAN (A.FROM_NO_PROP,A.FROM_NO_KAB,A.FROM_NO_KEC) DARI_NAMA_KECAMATAN
				    ,F5_GET_NAMA_KELURAHAN (A.FROM_NO_PROP,A.FROM_NO_KAB,A.FROM_NO_KEC,A.FROM_NO_KEL) DARI_NAMA_KELURAHAN
				    ,F5_GET_NAMA_PROVINSI (A.DEST_NO_PROP) TUJUAN_NAMA_PROVINSI
				    ,F5_GET_NAMA_KABUPATEN (A.DEST_NO_PROP,A.DEST_NO_KAB) TUJUAN_NAMA_KABUPATEN
				    ,F5_GET_NAMA_KECAMATAN (A.DEST_NO_PROP,A.DEST_NO_KAB,A.DEST_NO_KEC) TUJUAN_NAMA_KECAMATAN
				    ,F5_GET_NAMA_KELURAHAN (A.DEST_NO_PROP,A.DEST_NO_KAB,A.DEST_NO_KEC,A.DEST_NO_KEL) TUJUAN_NAMA_KELURAHAN
				    ,UPPER(F5_GET_REF_WNI(A.KLASIFIKASI_PINDAH,106)) KLASIFIKASI_PINDAH
				    ,B.NIK
				    , TO_CHAR(A.CREATED_DATE,'DD-MM-YYYY') CREATED_DATE
				    , RANK() OVER 
				  (PARTITION BY NIK ORDER BY A.CREATED_DATE DESC,A.NO_PINDAH DESC) RNK 
				  FROM PINDAH_HEADER A INNER JOIN PINDAH_DETAIL B ON A.NO_PINDAH = B.NO_PINDAH AND B.NIK =  $nik WHERE A.FROM_NO_PROP =32 AND A.FROM_NO_KAB=73)  WHERE RNK = 1";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}
		public function get_history_kematian($nik){
			$sql = "SELECT CONCAT(CONCAT('NIK : ',MATI_NIK),CONCAT('<br> TELAH DICATATAKAN AKTA KEMATIANNYA <br>DENGAN NO AKTA : ',ADM_AKTA_NO)) AS KET FROM  CAPIL_MATI 
				WHERE MATI_NIK = $nik ";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}
		public function do_hist_restore($nik,$no_kk,$user_id,$ip_address,$res_option){
            $sql = "INSERT INTO SIAK_HIST_RESTORE (ID,NIK,NO_KK,RESTORE_DT,RESTORE_BY,IP_ADDRESS,OPTION_RESTORE) VALUES ('$nik-RESWNI-".time()."',$nik,$no_kk,SYSDATE,'$user_id','$ip_address','$res_option')";
            $q = DB::insert($sql);
            
        }
		public function do_hist_delete_wni($nik,$no_kk,$user_id,$ip_address,$res_option){
            $sql = "INSERT INTO SIAK_HIST_RESTORE (ID,NIK,NO_KK,RESTORE_DT,RESTORE_BY,IP_ADDRESS,OPTION_RESTORE) VALUES ('$nik-DELWNI-".time()."',$nik,$no_kk,SYSDATE,'$user_id','$ip_address','$res_option')";
            $q = DB::insert($sql);
            
        }
		public function do_hist_change_kk($nik,$no_kk_lama,$no_kec_lama,$no_kel_lama,$no_rw_lama,$no_rt_lama,$alamat_lama,$no_kk_baru,$no_kec_baru,$no_kel_baru,$no_rw_baru,$no_rt_baru,$alamat_baru,$user_id,$ip_address){
            $sql = "INSERT INTO SIAK_HIST_CHANGEKK (ID, NIK, NO_KK_LAMA, NO_KEC_LAMA, NO_KEL_LAMA, NO_RW_LAMA, NO_RT_LAMA, ALAMAT_LAMA, NO_KK_BARU, NO_KEC_BARU, NO_KEL_BARU, NO_RW_BARU, NO_RT_BARU, ALAMAT_BARU, UPDATE_DT, UPDATE_BY, IP_ADDRESS) VALUES ('$nik-CHGKK-".time()."',$nik,$no_kk_lama,$no_kec_lama,$no_kel_lama,'$no_rw_lama','$no_rt_lama','$alamat_lama',$no_kk_baru,$no_kec_baru,$no_kel_baru,'$no_rw_baru','$no_rt_baru','$alamat_baru',SYSDATE,'$user_id','$ip_address')";
            $q = DB::insert($sql);
            
        }
		public function do_change_kk($nik,$no_kk_baru,$no_kec_baru,$no_kel_baru){
            $sql = "UPDATE BIODATA_WNI_DELETE SET NO_KK = $no_kk_baru, NO_KEC = $no_kec_baru, NO_KEL = $no_kel_baru WHERE NIK = $nik";
            $q = DB::connection('db222')->update($sql);;
            
        }
		public function do_delete_wni($nik){
            	$sql = "DELETE FROM BIODATA_WNI A WHERE A.NIK = $nik";
            	$q = DB::connection('db222')->delete($sql);;
        }
        public function do_restore($nik,$no_kk,$res_option,$user_id){
        	if($res_option == 1){
        		$sql = "INSERT INTO BIODATA_WNI A (A.NIK, A.NO_KTP, A.TMPT_SBL, A.NO_PASPOR, A.TGL_AKH_PASPOR, A.NAMA_LGKP, A.JENIS_KLMIN, A.TMPT_LHR, A.TGL_LHR, A.AKTA_LHR, A.NO_AKTA_LHR, A.GOL_DRH, A.AGAMA, A.STAT_KWN, A.AKTA_KWN, A.NO_AKTA_KWN, A.TGL_KWN, A.AKTA_CRAI, A.NO_AKTA_CRAI, A.TGL_CRAI, A.STAT_HBKEL, A.KLAIN_FSK, A.PNYDNG_CCT, A.PDDK_AKH, A.JENIS_PKRJN, A.NIK_IBU, A.NAMA_LGKP_IBU, A.NIK_AYAH, A.NAMA_LGKP_AYAH, A.NAMA_KET_RT, A.NAMA_KET_RW, A.NAMA_PET_REG, A.NIP_PET_REG, A.NAMA_PET_ENTRI, A.NIP_PET_ENTRI, A.TGL_ENTRI, A.NO_KK, A.JENIS_BNTU, A.NO_PROP, A.NO_KAB, A.NO_KEC, A.NO_KEL, A.STAT_HIDUP, A.TGL_UBAH, A.TGL_CETAK_KTP, A.TGL_GANTI_KTP, A.TGL_PJG_KTP, A.STAT_KTP, A.ALS_NUMPANG, A.PFLAG, A.CFLAG, A.SYNC_FLAG, A.GLR_AKADEMIS, A.GLR_AGAMA, A.GLR_BANGSAWAN, A.DESC_KEPERCAYAAN, A.DESC_PEKERJAAN, A.IS_PROS_DATANG, A.FLAG_STATUS, A.FLAGSINK, A.CREATED_BY, A.MODIFIED_BY) SELECT B.NIK, B.NO_KTP, B.TMPT_SBL, B.NO_PASPOR, B.TGL_AKH_PASPOR, B.NAMA_LGKP, B.JENIS_KLMIN, B.TMPT_LHR, B.TGL_LHR, B.AKTA_LHR, B.NO_AKTA_LHR, B.GOL_DRH, B.AGAMA, B.STAT_KWN, B.AKTA_KWN, B.NO_AKTA_KWN, B.TGL_KWN, B.AKTA_CRAI, B.NO_AKTA_CRAI, B.TGL_CRAI, B.STAT_HBKEL, B.KLAIN_FSK, B.PNYDNG_CCT, B.PDDK_AKH, B.JENIS_PKRJN, B.NIK_IBU, B.NAMA_LGKP_IBU, B.NIK_AYAH, B.NAMA_LGKP_AYAH, B.NAMA_KET_RT, B.NAMA_KET_RW, B.NAMA_PET_REG, B.NIP_PET_REG, B.NAMA_PET_ENTRI, B.NIP_PET_ENTRI, SYSDATE, B.NO_KK, B.JENIS_BNTU, B.NO_PROP, B.NO_KAB, B.NO_KEC, B.NO_KEL, B.STAT_HIDUP, SYSDATE, B.TGL_CETAK_KTP, B.TGL_GANTI_KTP, B.TGL_PJG_KTP, B.STAT_KTP, B.ALS_NUMPANG, B.PFLAG, B.CFLAG, B.SYNC_FLAG, B.GLR_AKADEMIS, B.GLR_AGAMA, B.GLR_BANGSAWAN, B.DESC_KEPERCAYAAN, B.DESC_PEKERJAAN, B.IS_PROS_DATANG, 0, B.FLAGSINK, 'Restore Monitoring' AS CREATED_BY, '$user_id' FROM BIODATA_WNI_DELETE B WHERE B.NIK = $nik AND NOT EXISTS (SELECT 1 FROM BIODATA_WNI C WHERE B.NIK = C.NIK) AND NOT EXISTS (SELECT 1 FROM PINDAH_HEADER D INNER JOIN PINDAH_DETAIL E ON D.NO_PINDAH = E.NO_PINDAH WHERE D.KLASIFIKASI_PINDAH > 3 AND B.NIK= E.NIK AND NOT EXISTS (SELECT 1 FROM DATANG_HEADER G INNER JOIN DATANG_DETAIL H ON G.NO_DATANG = H.NO_DATANG WHERE H.NIK =  B.NIK)) AND NOT EXISTS (SELECT 1 FROM CAPIL_MATI F WHERE B.NIK = F.MATI_NIK)";
            	$q = DB::connection('db222')->insert($sql);
            	$sql = "INSERT INTO DATA_KELUARGA A (A.NO_KK, A.NAMA_KEP, A.ALAMAT, A.NO_RT, A.NO_RW, A.DUSUN, A.KODE_POS, A.TELP, A.ALS_PRMOHON, A.ALS_NUMPANG, A.NO_PROP, A.NO_KAB, A.NO_KEC, A.NO_KEL, A.USERID, A.TGL_INSERTION, A.TGL_UPDATION, A.PFLAG, A.CFLAG, A.SYNC_FLAG, A.NIK_KK, A.TIPE_KK, A.OA_NAMA_KELUARGA, A.OA_NAMA_PERTAMA, A.FLAGSINK, A.CREATED_BY) SELECT B.NO_KK,B.NAMA_KEP, B.ALAMAT, B.NO_RT, B.NO_RW, B.DUSUN, B.KODE_POS, B.TELP, B.ALS_PRMOHON, B.ALS_NUMPANG, B.NO_PROP, B.NO_KAB, B.NO_KEC, B.NO_KEL, B.USERID, SYSDATE, SYSDATE, B.PFLAG, B.CFLAG, B.SYNC_FLAG, B.NIK_KK, B.TIPE_KK, B.OA_NAMA_KELUARGA, B.OA_NAMA_PERTAMA, B.FLAGSINK, 'Restore Monitoring' AS CREATED_BY FROM DATA_KELUARGA_DELETE B WHERE B.NO_KK = $no_kk AND NOT EXISTS (SELECT 1 FROM DATA_KELUARGA C WHERE B.NO_KK = C.NO_KK) AND EXISTS (SELECT 1 FROM BIODATA_WNI D WHERE B.NO_KK = D.NO_KK AND D.STAT_HBKEL = 1)";
            	$q = DB::connection('db222')->insert($sql);
            	$sql = "DELETE FROM BIODATA_WNI_DELETE A WHERE A.NIK = $nik AND EXISTS (SELECT 1 FROM BIODATA_WNI B WHERE A.NIK = B.NIK)";
            	$q = DB::connection('db222')->delete($sql);
            	$sql = "DELETE FROM DATA_KELUARGA_DELETE A WHERE A.NO_KK = $no_kk AND EXISTS (SELECT 1 FROM DATA_KELUARGA B WHERE A.NO_KK = B.NO_KK)";
            	$q = DB::connection('db222')->delete($sql);
        	}else if ($res_option == 0){
        		$sql = "INSERT INTO BIODATA_WNI A (A.NIK, A.NO_KTP, A.TMPT_SBL, A.NO_PASPOR, A.TGL_AKH_PASPOR, A.NAMA_LGKP, A.JENIS_KLMIN, A.TMPT_LHR, A.TGL_LHR, A.AKTA_LHR, A.NO_AKTA_LHR, A.GOL_DRH, A.AGAMA, A.STAT_KWN, A.AKTA_KWN, A.NO_AKTA_KWN, A.TGL_KWN, A.AKTA_CRAI, A.NO_AKTA_CRAI, A.TGL_CRAI, A.STAT_HBKEL, A.KLAIN_FSK, A.PNYDNG_CCT, A.PDDK_AKH, A.JENIS_PKRJN, A.NIK_IBU, A.NAMA_LGKP_IBU, A.NIK_AYAH, A.NAMA_LGKP_AYAH, A.NAMA_KET_RT, A.NAMA_KET_RW, A.NAMA_PET_REG, A.NIP_PET_REG, A.NAMA_PET_ENTRI, A.NIP_PET_ENTRI, A.TGL_ENTRI, A.NO_KK, A.JENIS_BNTU, A.NO_PROP, A.NO_KAB, A.NO_KEC, A.NO_KEL, A.STAT_HIDUP, A.TGL_UBAH, A.TGL_CETAK_KTP, A.TGL_GANTI_KTP, A.TGL_PJG_KTP, A.STAT_KTP, A.ALS_NUMPANG, A.PFLAG, A.CFLAG, A.SYNC_FLAG, A.GLR_AKADEMIS, A.GLR_AGAMA, A.GLR_BANGSAWAN, A.DESC_KEPERCAYAAN, A.DESC_PEKERJAAN, A.IS_PROS_DATANG, A.FLAG_STATUS, A.FLAGSINK, A.CREATED_BY, A.MODIFIED_BY) SELECT B.NIK, B.NO_KTP, B.TMPT_SBL, B.NO_PASPOR, B.TGL_AKH_PASPOR, B.NAMA_LGKP, B.JENIS_KLMIN, B.TMPT_LHR, B.TGL_LHR, B.AKTA_LHR, B.NO_AKTA_LHR, B.GOL_DRH, B.AGAMA, B.STAT_KWN, B.AKTA_KWN, B.NO_AKTA_KWN, B.TGL_KWN, B.AKTA_CRAI, B.NO_AKTA_CRAI, B.TGL_CRAI, B.STAT_HBKEL, B.KLAIN_FSK, B.PNYDNG_CCT, B.PDDK_AKH, B.JENIS_PKRJN, B.NIK_IBU, B.NAMA_LGKP_IBU, B.NIK_AYAH, B.NAMA_LGKP_AYAH, B.NAMA_KET_RT, B.NAMA_KET_RW, B.NAMA_PET_REG, B.NIP_PET_REG, B.NAMA_PET_ENTRI, B.NIP_PET_ENTRI, SYSDATE, B.NO_KK, B.JENIS_BNTU, B.NO_PROP, B.NO_KAB, B.NO_KEC, B.NO_KEL, B.STAT_HIDUP, SYSDATE, B.TGL_CETAK_KTP, B.TGL_GANTI_KTP, B.TGL_PJG_KTP, B.STAT_KTP, B.ALS_NUMPANG, B.PFLAG, B.CFLAG, B.SYNC_FLAG, B.GLR_AKADEMIS, B.GLR_AGAMA, B.GLR_BANGSAWAN, B.DESC_KEPERCAYAAN, B.DESC_PEKERJAAN, B.IS_PROS_DATANG, 0, B.FLAGSINK, 'Restore Monitoring' AS CREATED_BY, '$user_id' FROM BIODATA_WNI_DELETE B WHERE B.NIK = $nik AND NOT EXISTS (SELECT 1 FROM BIODATA_WNI C WHERE B.NIK = C.NIK) AND NOT EXISTS (SELECT 1 FROM PINDAH_HEADER D INNER JOIN PINDAH_DETAIL E ON D.NO_PINDAH = E.NO_PINDAH WHERE D.KLASIFIKASI_PINDAH > 3 AND B.NIK= E.NIK AND NOT EXISTS (SELECT 1 FROM DATANG_HEADER G INNER JOIN DATANG_DETAIL H ON G.NO_DATANG = H.NO_DATANG WHERE H.NIK =  B.NIK)) AND NOT EXISTS (SELECT 1 FROM CAPIL_MATI F WHERE B.NIK = F.MATI_NIK) AND EXISTS (SELECT 1 FROM BIODATA_WNI G WHERE B.NO_KK = G.NO_KK AND G.STAT_HBKEL =1)";
            	$q = DB::connection('db222')->insert($sql);
            	$sql = "DELETE FROM BIODATA_WNI_DELETE A WHERE A.NIK = $nik AND EXISTS (SELECT 1 FROM BIODATA_WNI B WHERE A.NIK = B.NIK)";
            	$q = DB::connection('db222')->delete($sql);
        	}
        }
        public function count_no_kk_baru($no_kk){
			$sql = "SELECT COUNT(1) JML FROM DATA_KELUARGA WHERE NO_KK = $no_kk";
			$r = DB::connection('db222')->select($sql);
			return $r[0]->jml;
		}
        public function get_no_kk_baru($no_kk){
			$sql = "SELECT 
				  NO_KK
				  , NAMA_KEP
				  , ALAMAT
				  , LPAD(TO_CHAR(NO_RT), 3, '0') AS RT
				  , LPAD(TO_CHAR(NO_RW), 3, '0') AS RW
				  , ALAMAT 
				  , NO_PROP
				  , NO_KAB
				  , NO_KEC
				  , NO_KEL
				  FROM DATA_KELUARGA WHERE NO_KK =$no_kk";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}
		 public function count_repair_cek($nik){
			$sql = "SELECT COUNT(1) JML FROM BIODATA_WNI WHERE NIK = $nik";
			$r = DB::connection('db222')->select($sql);
			return $r[0]->jml;
		}
		 public function count_repair_cek_delete($nik){
			$sql = "SELECT COUNT(1) JML FROM BIODATA_WNI_DELETE WHERE NIK = $nik";
			$r = DB::connection('db222')->select($sql);
			return $r[0]->jml;
		}
		public function do_hist_repair($nik_duplicate,$nik_single,$status_ektp_duplicate,$status_ektp_single,$nama_duplicate,$nama_single,$user_id,$ip_address){
            $sql = "INSERT INTO SIAK_HIST_SWITCH_NIK (ID,NIK_SINGLE,NIK_DUPLICATE,EKTP_SINGLE,EKTP_DUPLICATE,NAMA_SINGLE,NAMA_DUPLICATE,SWITCH_DT,SWITCH_BY,IP_ADDRESS) VALUES ('$nik_duplicate-SWC-".time()."',$nik_single,$nik_duplicate,'$status_ektp_single','$status_ektp_duplicate','$nama_single','$nama_duplicate',SYSDATE,'$user_id','$ip_address')";
            $q = DB::insert($sql);
        }
		public function do_repair($nik_duplicate,$nik_single){
            $sql = "UPDATE BIODATA_WNI SET NIK = $nik_single, TMPT_SBL = 'Penyesuaian Nik Monitoring Dari Nik $nik_duplicate' WHERE NIK = $nik_duplicate";
            $r = DB::connection('db222')->update($sql);
            $sql = "UPDATE BIODATA_WNI_DELETE SET NIK = $nik_duplicate, TMPT_SBL = 'Penyesuaian Nik Monitoring Dari Nik $nik_single' WHERE NIK = $nik_single";
            $r = DB::connection('db222')->update($sql);
            $sql = "DELETE FROM BIODATA_WNI_DELETE WHERE NIK = $nik_duplicate AND TMPT_SBL NOT LIKE 'Penyesuaian Nik Monitoring%'";
            $r = DB::connection('db222')->delete($sql);
        }
        public function insert_helpdesk_request($request_id,$keterangan,$user_id){
            $sql = "INSERT INTO SIAK_HELPDESK (SEQ_ID,HELPDESK_ID, USER_ID, DESCRIPTION, CREATED_BY, CREATED_DT, STATUS) VALUES (CONCAT(TO_CHAR(SYSDATE,'DDMMYYYY'),'-$user_id-hlp-".time()."'),$request_id,'$user_id','$keterangan','$user_id',SYSDATE,0)";
            $q = DB::insert($sql);
        }

        public function aprove_helpdesk_request($aprove_seq_id,$balasan_aprove,$user_id){
            $sql = "UPDATE SIAK_HELPDESK SET APROVE_DESC = '$balasan_aprove', APROVE_BY = '$user_id', APROVE_DT = SYSDATE, STATUS = 1 WHERE SEQ_ID = '$aprove_seq_id'";
            $q = DB::update($sql);
        }
        public function reject_helpdesk_request($reject_seq_id,$balasan_reject,$user_id){
            $sql = "UPDATE SIAK_HELPDESK SET REJECTED_DESC = '$balasan_reject', REJECTED_BY = '$user_id', REJECT_DT = SYSDATE, STATUS = 2 WHERE SEQ_ID = '$reject_seq_id'";
            $q = DB::update($sql);
        }
        public function request_pending($user_id){
			$sql = "SELECT B.SEQ_ID, A.USER_ID, A.NIK, A.NAMA_LGKP, TO_CHAR(B.CREATED_DT,'DD-MM-YYYY HH24:MI:SS') ACTIVITY_DATE,CASE WHEN C.HELPDESK_DESCRIPTION IS NULL THEN 'LAINNYA' ELSE C.HELPDESK_DESCRIPTION END P1, B.DESCRIPTION P2, ' ' P3 FROM SIAK_USER_PLUS A INNER JOIN SIAK_HELPDESK B ON A.USER_ID = B.USER_ID LEFT JOIN SIAK_MASTER_HELPDESK C ON B.HELPDESK_ID = C.HELPDESK_ID WHERE A.USER_ID = '$user_id' AND B.STATUS =0 ORDER BY B.CREATED_DT";
			$r = DB::select($sql);
			return $r;
		}
        public function request_success($user_id){
			$sql = "SELECT USER_ID, NIK, NAMA_LGKP, ACTIVITY_DATE, P1, P2, P3, P4 FROM (SELECT A.USER_ID, A.NIK, A.NAMA_LGKP, TO_CHAR(B.CREATED_DT,'DD-MM-YYYY') ACTIVITY_DATE,CASE WHEN C.HELPDESK_DESCRIPTION IS NULL THEN 'LAINNYA' ELSE C.HELPDESK_DESCRIPTION END P1, B.DESCRIPTION P2, CASE WHEN B.APROVE_DESC IS NULL THEN ' ' ELSE CONCAT('KETERANGAN : ', B.APROVE_DESC) END P3, CASE WHEN B.APROVE_BY IS NULL THEN ' ' ELSE UPPER(F5_GET_NAMA_USER(B.APROVE_BY)) END P4 FROM SIAK_USER_PLUS A INNER JOIN SIAK_HELPDESK B ON A.USER_ID = B.USER_ID LEFT JOIN SIAK_MASTER_HELPDESK C ON B.HELPDESK_ID = C.HELPDESK_ID WHERE A.USER_ID = '$user_id' AND B.STATUS =1 ORDER BY B.CREATED_DT DESC) WHERE ROWNUM <= 100";
			$r = DB::select($sql);
			return $r;
		}
        public function request_reject($user_id){
			$sql = "SELECT USER_ID, NIK, NAMA_LGKP, ACTIVITY_DATE, P1, P2, P3, P4 FROM (SELECT A.USER_ID, A.NIK, A.NAMA_LGKP, TO_CHAR(B.CREATED_DT,'DD-MM-YYYY') ACTIVITY_DATE,CASE WHEN C.HELPDESK_DESCRIPTION IS NULL THEN 'LAINNYA' ELSE C.HELPDESK_DESCRIPTION END P1, B.DESCRIPTION P2, CASE WHEN B.REJECTED_DESC IS NULL THEN ' ' ELSE CONCAT('KETERANGAN : ', B.REJECTED_DESC) END P3, CASE WHEN B.REJECTED_BY IS NULL THEN ' ' ELSE UPPER(F5_GET_NAMA_USER(B.REJECTED_BY)) END P4 FROM SIAK_USER_PLUS A INNER JOIN SIAK_HELPDESK B ON A.USER_ID = B.USER_ID LEFT JOIN SIAK_MASTER_HELPDESK C ON B.HELPDESK_ID = C.HELPDESK_ID WHERE A.USER_ID = '$user_id' AND B.STATUS =2 ORDER BY B.CREATED_DT DESC) WHERE ROWNUM <= 100";
			$r = DB::select($sql);
			return $r;
		}
        public function list_request_pending($user_id){
			$sql = "SELECT B.SEQ_ID, A.USER_ID, A.NIK, A.NAMA_LGKP, TO_CHAR(B.CREATED_DT,'DD-MM-YYYY HH24:MI:SS') ACTIVITY_DATE,CASE WHEN C.HELPDESK_DESCRIPTION IS NULL THEN 'LAINNYA' ELSE C.HELPDESK_DESCRIPTION END P1, B.DESCRIPTION P2, ' ' P3 FROM SIAK_USER_PLUS A INNER JOIN SIAK_HELPDESK B ON A.USER_ID = B.USER_ID LEFT JOIN SIAK_MASTER_HELPDESK C ON B.HELPDESK_ID = C.HELPDESK_ID WHERE  B.STATUS =0 ORDER BY B.CREATED_DT";
			$r = DB::select($sql);
			return $r;
		}
        public function list_request_success($user_id){
			$sql = "SELECT USER_ID, NIK, NAMA_LGKP, ACTIVITY_DATE, P1, P2, P3, P4 FROM (SELECT A.USER_ID, A.NIK, A.NAMA_LGKP, TO_CHAR(B.CREATED_DT,'DD-MM-YYYY') ACTIVITY_DATE,CASE WHEN C.HELPDESK_DESCRIPTION IS NULL THEN 'LAINNYA' ELSE C.HELPDESK_DESCRIPTION END P1, B.DESCRIPTION P2, CASE WHEN B.APROVE_DESC IS NULL THEN ' ' ELSE CONCAT('KETERANGAN : ', B.APROVE_DESC) END P3, CASE WHEN B.APROVE_BY IS NULL THEN ' ' ELSE UPPER(F5_GET_NAMA_USER(B.APROVE_BY)) END P4 FROM SIAK_USER_PLUS A INNER JOIN SIAK_HELPDESK B ON A.USER_ID = B.USER_ID LEFT JOIN SIAK_MASTER_HELPDESK C ON B.HELPDESK_ID = C.HELPDESK_ID WHERE  B.STATUS =1 ORDER BY B.CREATED_DT DESC) WHERE ROWNUM <= 100";
			$r = DB::select($sql);
			return $r;
		}
        public function list_request_reject($user_id){
			$sql = "SELECT USER_ID, NIK, NAMA_LGKP, ACTIVITY_DATE, P1, P2, P3, P4 FROM (SELECT A.USER_ID, A.NIK, A.NAMA_LGKP, TO_CHAR(B.CREATED_DT,'DD-MM-YYYY') ACTIVITY_DATE,CASE WHEN C.HELPDESK_DESCRIPTION IS NULL THEN 'LAINNYA' ELSE C.HELPDESK_DESCRIPTION END P1, B.DESCRIPTION P2, CASE WHEN B.REJECTED_DESC IS NULL THEN ' ' ELSE CONCAT('KETERANGAN : ', B.REJECTED_DESC) END P3, CASE WHEN B.REJECTED_BY IS NULL THEN ' ' ELSE UPPER(F5_GET_NAMA_USER(B.REJECTED_BY)) END P4 FROM SIAK_USER_PLUS A INNER JOIN SIAK_HELPDESK B ON A.USER_ID = B.USER_ID LEFT JOIN SIAK_MASTER_HELPDESK C ON B.HELPDESK_ID = C.HELPDESK_ID WHERE  B.STATUS =2 ORDER BY B.CREATED_DT DESC) WHERE ROWNUM <= 100";
			$r = DB::select($sql);
			return $r;
		}

        public function get_request($seq_id){
			$sql = "SELECT B.SEQ_ID, A.USER_ID, A.NIK, A.NAMA_LGKP, TO_CHAR(B.CREATED_DT,'DD-MM-YYYY HH24:MI:SS') ACTIVITY_DATE,CASE WHEN C.HELPDESK_DESCRIPTION IS NULL THEN 'LAINNYA' ELSE C.HELPDESK_DESCRIPTION END P1, B.DESCRIPTION P2, ' ' P3 FROM SIAK_USER_PLUS A INNER JOIN SIAK_HELPDESK B ON A.USER_ID = B.USER_ID LEFT JOIN SIAK_MASTER_HELPDESK C ON B.HELPDESK_ID = C.HELPDESK_ID WHERE B.SEQ_ID = '$seq_id' AND B.STATUS =0";
			$r = DB::select($sql);
			return $r;
		}

        public function get_data_kematian($akta_kmt){
			$sql = "SELECT ADM_AKTA_NO,CASE WHEN MATI_NIK IS NULL THEN '-' ELSE TO_CHAR(MATI_NIK) END MATI_NIK,CASE WHEN MATI_NO_KK IS NULL THEN '-' ELSE TO_CHAR(MATI_NO_KK) END MATI_NO_KK, MATI_NAMA_LGKP,  TO_CHAR(MATI_TGL_LAHIR,'DD-MM-YYYY') MATI_TGL_LAHIR, MATI_TMPT_LAHIR, MATI_TMPT_MATI, TO_CHAR(MATI_TGL_MATI,'DD-MM-YYYY') MATI_TGL_MATI, CASE WHEN MATI_JNS_KELAMIN = 1 THEN 'LAKI-LAKI' ELSE 'PEREMPUAN' END MATI_JNS_KELAMIN, IBU_NAMA_LGKP, AYAH_NAMA_LGKP  FROM CAPIL_MATI WHERE ADM_AKTA_NO LIKE '%$akta_kmt%' ORDER BY ADM_TGL_ENTRY DESC";
			$r = DB::select($sql);
			return $r;
		}
}
