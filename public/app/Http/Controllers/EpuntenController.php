<?php

namespace App\Http\Controllers;

use App\Pengajuanskts;
use App\Pengajuanwna;
use App\Pengajuandatang;

use Auth;
use DB;
use Session;
use DateTime;
use PDF;
use Validator;
use App\User; 
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\cache;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Str;
use App\Http\Controllers\SharedController as Shr;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class EpuntenController extends Controller
{

  public $successStatus = 200;
  
  public function weblogin(){ 


       $sql= "SELECT COUNT(1) JUMLAH FROM USERS WHERE NIK = '".request('nik')."' ";
        $query=DB::select($sql);
        if ($query[0]->jumlah == 0) {
          return response()->json([
            'status' => 'error',
            'data' => 'NIK Belum Terdaftar'
          ]); 
        }
        if(Auth::attempt(['nik' => request('nik'), 'password' => request('password')])){ 
            $user = Auth::user(); 
            if(request('android_token')) {
              $id = app('auth')->user()->id;
              $sql = User::findOrFail($id);
              $sql->android_token = request('android_token');
              $sql->update();
            }
            $access_token =  $user->createToken('MyApp')->accessToken; 
            return response()->json(['user' => $user, 'access_token' => $access_token,'token_type' => 'bearer'], $this->successStatus); 
        }else{ 
             return response()->json([
            'status' => 'error',
            'data' => 'NIK atau Password yang anda masukan tidak sesuai'
          ]); 
        } 
    }

    public function weblogout()
    { 
        if (Auth::check()) {
            Auth::user()->AauthAcessToken()->delete();
        }
    }

    public function webregister(Request $request) 
    { 
      $email1 = "083817743009.auh@gmail.com";
      $email2 = "ripalmuhamad023@gmail.com";
      $telepon1 = "81324253921";
      $telepon2 = "83169933068";
      $telepon3 = "83174606809";

      // Hardcore Email Calo
      if($request->email) {
        if ( (strcmp($email1, $request->email) == 0) || (strcmp($email2, $request->email) == 0) ) {
          return response()->json([
            'status' => 'error',
            'data' => 'Pembuatan akun ditolak'
          ]);
        }

        // Cek Email Sudah Terdaftar Atau Tidak
        $sql = "SELECT COUNT(1) JUMLAH FROM USERS WHERE EMAIL = '$request->email' AND EMAIL <> 'yanz.bbh@gmail.com'";
        $query=DB::select($sql);
        if($query[0]->jumlah > 0){
          return response()->json([
            'status' => 'error',
            'data' => 'Email sudah digunakan'
          ]);
        }

        // Cek Nik Sudah Terdaftar Atau Tidak
        $sql = "SELECT COUNT(1) JUMLAH FROM USERS WHERE NIK = '$request->nik'";
        $query=DB::select($sql);
        if($query[0]->jumlah > 0){
          return response()->json([
            'status' => 'error',
            'data' => 'Nik sudah digunakan'
          ]);
        }
      }

      if($request->telepon) {
        if ( (strcmp($telepon1, $request->telepon) == 0) || (strcmp($telepon2, $request->telepon) == 0) || (strcmp($telepon3, $request->telepon) == 0) ) {
          return response()->json([
            'status' => 'error',
            'data' => 'Pembuatan akun ditolak'
          ]);
        }
        $sql= "SELECT COUNT(1) JUMLAH FROM USERS WHERE TELEPON = '$request->telepon' AND TELEPON <> '081320405615' ";
        $query=DB::select($sql);
        if($query[0]->jumlah > 0){
          return response()->json([
            'status' => 'error',
            'data' => 'Telepon sudah digunakan'
          ]);
        }
      }


        $validator = Validator::make($request->all(), [ 
            'nik' => 'required|string|max:16|min:16', 
            'name' => 'required', 
            'email' => 'required|email', 
            'telepon' => 'required|string|max:14|min:9', 
            'password' => 'required', 
            'c_password' => 'required|same:password',
            
        ]);
        if ($validator->fails()) { 
          return response()->json([
            'status' => 'error',
            'data' => $validator->errors()
          ]);
        }
        
        $request->merge([
            'roles' => 2,
            'remember_token' => Str::random(10),
            'verification_code' => Str::random(5),
            'api_token' => Str::random(100),
        ]);
        

        $input = $request->all(); 
        $input['password'] = bcrypt($input['password']); 
        $user = User::create($input); 
        $success['token'] =  $user->createToken('MyApp')-> accessToken; 
        $success['name'] =  $user->name;
        $success['nik'] =  $user->nik;
        return response()->json([
            'status' => 'success',
            'data' => $success
          ]);
    }

    public function webdetails() 
    { 
        $user = Auth::user();
        return response()->json(['success' => $user], $this->successStatus);
    }

    public function total_user() 
    { 
        return response()->json([
            'total' => '1000',
            'lk' => '600',
            'pr' => '400',
            'aktv' => '800'
          ]);
    }

   public function cek_notif(Request $request)
    {
          $nik = Auth::user()->nik;
          $sql = "SELECT COUNT(1) JUMLAH FROM NOTIFICATION WHERE NIK = $nik";
          $query=DB::connection('webpunten')
          ->select($sql);
          if($query[0]->jumlah > 0){
            $sql = "SELECT  
              ID
              ,TYPE
              ,PENGAJUAN_ID
              ,JENIS_PENGAJUAN
              ,NIK
              ,FROM_USER_ID
              ,TO_USER_ID
              ,MESSAGE
              ,SEEN_AT
              ,CREATED_AT
              ,UPDATED_AT
              ,DELETED_AT
              ,FLAG_READ
               FROM NOTIFICATION WHERE NIK = $nik";
            $query=DB::connection('webpunten')->select($sql);
              return response()->json([
               $query[0]
            ]);
          }else{
            return response()->json([
              'status' => 'error',
              'data' => 'NIK Tidak Ada'
            ]);
          }
    }
    public function read_notif(Request $request)
    {
        $id=$request->id;
        $query=DB::connection('webpunten')
          ->select("select count(1) jumlah from NOTIFICATION where ID = ".$id."");
     
      if($query[0]->jumlah > 0){
        $query=DB::connection('webpunten')
          ->update("UPDATE NOTIFICATION SET FLAG_READ = 1 WHERE ID = ".$id."");
           return response()->json([
            'status' => 'success',
            'data' => 'NOTIFIKASI SUDAH DI BACA'
        ]);
      }else{
        return response()->json([
          'status' => 'error',
          'data' => 'NOTIFIKASI TIDAK ADA'
        ]);
      }
    }
    public function update_pengajuan(Request $request)
    {
        $stat_pngjuan=$request->stat_pngjuan;
        $id_pengajuan=$request->id_pengajuan;
        $jenis_layanan=$request->jenis_layanan;
        if ($jenis_layanan == 1){
          DB::connection('webpunten')->table('pengajuan')
              ->where('id', $id_pengajuan)
              ->update(['proc_stat' => $stat_pngjuan]);
            return response()->json([
              'status' => 'success',
              'data' => 'Pengajuan Berhasil Diupdate'
            ]);
        }else if ($jenis_layanan == 3){
              DB::connection('webpunten')->table('pengajuanwna')
              ->where('id', $id_pengajuan)
              ->update(['proc_stat' => $stat_pngjuan]);
            return response()->json([
              'status' => 'success',
              'data' => 'Pengajuan Berhasil Diupdate'
            ]);
        }
        
    }
    public function cek_negara(Request $request)
    {
        $query=DB::select("SELECT NO ,DESCRIP ,KODE_NUM ,ALPHA2 FROM REF_NEGARA ORDER BY NO");
          return $query;
    }
     public function mst_wna(Request $request)
    {
        $sect=$request->sect;
        $query=DB::select("SELECT NO ,DESCRIP ,SECT FROM REF_SIAK_WNA WHERE SECT = ".$sect."");
          return $query;
    }
    public function cek_user_petugas(Request $request)
    {
        $query=DB::select("SELECT ID ,NIK ,NAME FROM USERS");
        return $query;
    }

    public function get_master_combobox()
    {
      $expiresAt = Carbon::createFromFormat('Y-m-d', Carbon::now()->format('Y-m-d')) ->addDays(30);
      if (Cache::has('e-punten-get-data-master-register-prov')){
        $prov = Cache::get('e-punten-get-data-master-register-prov');
      } else {
        $prov=DB::select("SELECT NO_PROP,NO_PROP || ' - ' || NAMA_PROP NAMA_PROP FROM SETUP_PROP ORDER BY NO_PROP ASC");
        Cache::put('e-punten-get-data-master-register-prov', $prov, $expiresAt);
      }

      if (Cache::has('e-punten-get-data-master-register-kec')){
        $kecamatan = Cache::get('e-punten-get-data-master-register-kec');
      } else {
        $kecamatan=DB::select("SELECT NO_KEC,NO_KEC || ' - ' || NAMA_KEC NAMA_KEC FROM SETUP_KEC WHERE NO_PROP=32 AND NO_KAB=73 ORDER BY NO_KEC ASC");
        Cache::put('e-punten-get-data-master-register-kec', $kecamatan, $expiresAt);
      }

      if (Cache::has('e-punten-get-data-master-register-jenis_klmin')){
        $jenis_klmin = Cache::get('e-punten-get-data-master-register-jenis_klmin');
      } else {
        $jenis_klmin=DB::select("SELECT NO,DESCRIP JENIS_KLMIN FROM REF_SIAK_WNI WHERE SECT = 801 ORDER BY NO ASC");
        Cache::put('e-punten-get-data-master-register-jenis_klmin', $jenis_klmin, $expiresAt);
      }

      if (Cache::has('e-punten-get-data-master-register-agama')){
        $agama = Cache::get('e-punten-get-data-master-register-agama');
      } else {
        $agama=DB::select("SELECT NO,NO || ' - ' || DESCRIP AGAMA FROM REF_SIAK_WNI WHERE SECT = 501 ORDER BY NO ASC");
        Cache::put('e-punten-get-data-master-register-agama', $agama, $expiresAt);
      }

      if (Cache::has('e-punten-get-data-master-register-stat_kwn')){
        $stat_kwn = Cache::get('e-punten-get-data-master-register-stat_kwn');
      } else {
        $stat_kwn=DB::select("SELECT NO,NO || ' - ' || DESCRIP STAT_KWN FROM REF_SIAK_WNI WHERE SECT = 601 ORDER BY NO ASC");
        Cache::put('e-punten-get-data-master-register-stat_kwn', $stat_kwn, $expiresAt);
      }

      if (Cache::has('e-punten-get-data-master-register-gol_drh')){
        $gol_drh = Cache::get('e-punten-get-data-master-register-gol_drh');
      } else {
        $gol_drh=DB::select("SELECT NO,NO || ' - ' || DESCRIP GOL_DRH FROM REF_SIAK_WNI WHERE SECT = 401 ORDER BY NO ASC");
        Cache::put('e-punten-get-data-master-register-gol_drh', $gol_drh, $expiresAt);
      }

      if (Cache::has('e-punten-get-data-master-register-pendidikan')){
        $pendidikan = Cache::get('e-punten-get-data-master-register-pendidikan');
      } else {
        $pendidikan=DB::select("SELECT NO,NO || ' - ' || DESCRIP PENDIDIKAN FROM REF_SIAK_WNI WHERE SECT = 101 ORDER BY NO ASC");
        Cache::put('e-punten-get-data-master-register-pendidikan', $pendidikan, $expiresAt);
      }
      if (Cache::has('e-punten-get-data-master-register-pekerjaan')){
        $pekerjaan = Cache::get('e-punten-get-data-master-register-pekerjaan');
      } else {
        $pekerjaan=DB::select("SELECT NO,NO || ' - ' || DESCRIP PEKERJAAN FROM REF_SIAK_WNI WHERE SECT = 201 ORDER BY NO ASC");
        Cache::put('e-punten-get-data-master-register-pekerjaan', $pekerjaan, $expiresAt);
      }

      return compact('prov', 'kecamatan', 'jenis_klmin', 'agama', 'stat_kwn', 'gol_drh', 'pendidikan', 'pekerjaan');
    }
    public function get_master_combobox_agama()
    {
      $agama=DB::select("SELECT NO,DESCRIP AGAMA FROM REF_SIAK_WNI WHERE SECT = 501 ORDER BY NO ASC");
      return $agama;
    }

    public function get_master_combobox_stat_kwn()
    {
      $stat_kwn=DB::select("SELECT NO,DESCRIP STAT_KWN FROM REF_SIAK_WNI WHERE SECT = 601 ORDER BY NO ASC");
      return $stat_kwn;
    }

    public function get_master_combobox_gol_drh()
    {
      $gol_drh=DB::select("SELECT NO,DESCRIP GOL_DRH FROM REF_SIAK_WNI WHERE SECT = 401 ORDER BY NO ASC");
      return $gol_drh;
    }

    public function get_master_combobox_pendidikan()
    {
      $pendidikan=DB::select("SELECT NO,DESCRIP PENDIDIKAN FROM REF_SIAK_WNI WHERE SECT = 101 ORDER BY NO ASC");
      return $pendidikan;
    }

    public function get_master_combobox_pekerjaan()
    {
      $pekerjaan=DB::select("SELECT NO,DESCRIP||' ('||NO||')' PEKERJAAN FROM REF_SIAK_WNI WHERE SECT = 201 ORDER BY NO ASC");
      return $pekerjaan;
    }

    public function get_master_combobox_prov()
    {
      $prov=DB::select("SELECT NO_PROP,NO_PROP || ' - ' || NAMA_PROP NAMA_PROP FROM SETUP_PROP ORDER BY NO_PROP ASC");
      return $prov;
    }

    public function get_master_combobox_kab(Request $request)
    {
      $expiresAt = Carbon::createFromFormat('Y-m-d', Carbon::now()->format('Y-m-d')) ->addDays(30);
      if (Cache::has('e-punten-data-req-kab2-'.$request->no_prop)){
        $kabupaten = Cache::get('e-punten-data-req-kab2-'.$request->no_prop);
      } else {
        $kabupaten=DB::select("SELECT NO_KAB,NO_KAB || ' - ' || NAMA_KAB NAMA_KAB FROM SETUP_KAB WHERE NO_PROP= ".$request->no_prop."  ORDER BY NO_KAB ASC");
        Cache::put('e-punten-data-req-kab2-'.$request->no_prop, $kabupaten, $expiresAt);
      }

      return compact('kabupaten');
    }

    public function get_master_combobox_kec(Request $request)
    {
      $expiresAt = Carbon::createFromFormat('Y-m-d', Carbon::now()->format('Y-m-d')) ->addDays(30);
      if (Cache::has('e-punten-data-req-kec-'.$request->no_prop.$request->no_kab)){
        $kecamatan = Cache::get('e-punten-data-req-kec-'.$request->no_prop.$request->no_kab);
      } else {
        $kecamatan=DB::select("SELECT NO_KEC,NO_KEC || ' - ' || NAMA_KEC NAMA_KEC FROM SETUP_KEC WHERE NO_PROP= ".$request->no_prop."
        AND NO_KAB= ".$request->no_kab." ORDER BY NO_KEC ASC");
        Cache::put('e-punten-data-req-kec-'.$request->no_prop.$request->no_kab, $kecamatan, $expiresAt);
      }

      return compact('kecamatan');
    }

    public function get_master_combobox_kel(Request $request)
    {
      $expiresAt = Carbon::createFromFormat('Y-m-d', Carbon::now()->format('Y-m-d')) ->addDays(30);
      if (Cache::has('e-punten-data-req-kel-'.$request->no_prop.$request->no_kab.$request->no_kec)){
        $kelurahan = Cache::get('e-punten-data-req-kel-'.$request->no_prop.$request->no_kab.$request->no_kec);
      } else {
        $kelurahan=DB::select("SELECT NO_KEL,NO_KEL || ' - ' || NAMA_KEL NAMA_KEL FROM SETUP_KEL WHERE NO_PROP= ".$request->no_prop."
        AND NO_KAB= ".$request->no_kab." AND NO_KEC= ".$request->no_kec." ORDER BY NO_KEL ASC");
        Cache::put('e-punten-data-req-kel-'.$request->no_prop.$request->no_kab.$request->no_kec, $kelurahan, $expiresAt);
      }

      return compact('kelurahan');
    }

    public function get_master_combobox_dest_kel(Request $request)
    {
      $expiresAt = Carbon::createFromFormat('Y-m-d', Carbon::now()->format('Y-m-d')) ->addDays(30);
      if (Cache::has('e-punten-data-req-dest_kel-'.$request->prov.$request->kab.$request->no_kec)){
        $kelurahan = Cache::get('e-punten-data-req-dest_kel-'.$request->prov.$request->kab.$request->no_kec);
      } else {
        $kelurahan=DB::select("SELECT NO_KEL,NO_KEL || ' - ' || NAMA_KEL NAMA_KEL FROM SETUP_KEL WHERE NO_PROP= ".$request->prov."
        AND NO_KAB= ".$request->kab." AND NO_KEC= ".$request->no_kec." ORDER BY NO_KEL ASC");
        Cache::put('e-punten-data-req-dest_kel-'.$request->prov.$request->kab.$request->no_kec, $kelurahan, $expiresAt);
      }

      return compact('kelurahan');
    }

    public function store(Request $request)
    {
      $nik= Auth::user()->nik;
      $jenis_layanan=$request->jenis_layanan;
      if ($jenis_layanan == 1){
        $query=DB::connection('webpunten')
        ->select("SELECT COUNT(1) JUMLAH FROM PENGAJUANSKTS WHERE NIK = ".$nik." AND JENIS_LAYANAN = 1 AND PROC_STAT = 1");
      }else if ($jenis_layanan == 2){
         $query=DB::connection('webpunten')
         ->select("SELECT COUNT(1) JUMLAH FROM PENGAJUANDATANG WHERE NIK = ".$nik." AND JENIS_LAYANAN = 2 AND PROC_STAT = 1");
      }else{
        $query=DB::connection('webpunten')
        ->select("SELECT COUNT(1) JUMLAH FROM PENGAJUANWNA WHERE NIK = ".$nik." AND JENIS_LAYANAN = 3 AND PROC_STAT = 1");
      }
      
    if($query[0]->jumlah > 0){
      return response()->json([
        'status' => 'error',
        'data' => 'NIK sudah pernah mengajukan sebelumnya'
      ]);
    }else{
      if ($jenis_layanan == 1){
        $destinationPath=$this->pengajuan_path().'/pengajuan/skts/'.$nik.'/ori/';
        $destinationPathThumb=$this->pengajuan_path().'/pengajuan/skts/'.$nik.'/thumb/';
        $imgName = 'pengajuan/skts/'.$nik.'/ori/';
        $imgThumbName = 'pengajuan/skts/'.$nik.'/thumb/';
      }else if($jenis_layanan == 2){
        $destinationPath=$this->pengajuan_path().'/pengajuan/datang/'.$nik.'/ori/';
        $destinationPathThumb=$this->pengajuan_path().'/pengajuan/datang/'.$nik.'/thumb/';
        $imgName = 'pengajuan/datang/'.$nik.'/ori/';
        $imgThumbName = 'pengajuan/datang/'.$nik.'/thumb/';
      }else if($jenis_layanan == 3){
        $destinationPath=$this->pengajuan_path().'/pengajuan/wna/'.$nik.'/ori/';
        $destinationPathThumb=$this->pengajuan_path().'/pengajuan/wna/'.$nik.'/thumb/';
        $imgName = 'pengajuan/wna/'.$nik.'/ori/';
        $imgThumbName = 'pengajuan/wna/'.$nik.'/thumb/';
      }else{
         return response()->json([
          'status' => 'error',
          'data' => 'Jenis Pengajuan Salah Mohon Cek Kemabali'
        ]);
      }


      $picname = "imgupload_thumb_1.jpg";
      if (!file_exists($destinationPath)) {
        mkdir($destinationPath, 0777, true);
      }
      if (!file_exists($destinationPathThumb)) {
        mkdir($destinationPathThumb, 0777, true);
      }
      
      if ($jenis_layanan == 1){
        $pengajuan = new Pengajuanskts;
      }else if($jenis_layanan == 2){
         $pengajuan = new Pengajuandatang;
      }else if($jenis_layanan == 3){
        $pengajuan = new Pengajuanwna;
      }else{
          return response()->json([
          'status' => 'error',
          'data' => 'Jenis Pengajuan Salah Mohon Cek Kemabali'
        ]);
      }
        
        $pengajuan->proc_stat=1;
        $pengajuan->nik=$nik;
        $pengajuan->nama_lgkp=$request->nama_lgkp;
        $pengajuan->jenis_klmin=$request->jenis_klmin;
        $pengajuan->tmpt_lhr=$request->tmpt_lhr;
        $pengajuan->tgl_lhr=$request->tgl_lhr;
        $pengajuan->agama=$request->agama;
        $pengajuan->stat_kwn=$request->stat_kwn;
        $pengajuan->gol_drh=$request->gol_drh;
        $pengajuan->pendidikan=$request->pendidikan;
        $pengajuan->pekerjaan=$request->pekerjaan;
        $pengajuan->src_prov=$request->src_prov;
        $pengajuan->src_kab=$request->src_kab;
        $pengajuan->src_kec=$request->src_kec;
        $pengajuan->src_kel=$request->src_kel;
        $pengajuan->src_alamat=$request->src_alamat;
        $pengajuan->src_rt=$request->src_rt;
        $pengajuan->src_rw=$request->src_rw;
        $pengajuan->kec=$request->kec;
        $pengajuan->kel=$request->kel;
        $pengajuan->alamat=$request->alamat;
        $pengajuan->no_rw=$request->no_rw;
        $pengajuan->no_rt=$request->no_rt;
        $pengajuan->status=$request->status;
        $pengajuan->jenis_layanan=$request->jenis_layanan;
        if ($request->jenis_layanan == 3){
        $pengajuan->kwrngrn=$request->kwrngrn;
        $pengajuan->no_paspor=$request->no_paspor;
        $pengajuan->tgl_paspor=$request->tgl_paspor;
        $pengajuan->tgl_akh_paspor=$request->tgl_akh_paspor;
        $pengajuan->no_dok=$request->no_doc;
        $pengajuan->dok_imgr=$request->doc_imigrasi;
        $pengajuan->tgl_doc_imigrasi=$request->tgl_doc_imigrasi;
        $pengajuan->tgl_stay=$request->tgl_stay;
       }
        
        $pengajuan->tgl_berlaku= Carbon::now()->addMonths(6);

        /////////////////
        if($request->hasFile('imgupload1')){
        $photo=$request->file('imgupload1')->getClientOriginalName();
        $extension = $request->file('imgupload1')->getClientOriginalExtension();
        $filename = "imgupload1.".$extension;
        $request->file('imgupload1')->move($destinationPath,$filename);
        $pengajuan->img_1=$imgName.$filename;

        $img1 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_1.jpg";
        $img1->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_1=$imgThumbName.$filename;
        }

        /////////////////
        if($request->hasFile('imgupload2')){
        $photo=$request->file('imgupload2')->getClientOriginalName();
        $extension = $request->file('imgupload2')->getClientOriginalExtension();
        $filename = "imgupload2.".$extension;
        $request->file('imgupload2')->move($destinationPath,$filename);
        $pengajuan->img_2=$imgName.$filename;

        $img2 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_2.jpg";
        $img2->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_2=$imgThumbName.$filename;
        }
        /////////////////
        if($request->hasFile('imgupload3')){
        $photo=$request->file('imgupload3')->getClientOriginalName();
        $extension = $request->file('imgupload3')->getClientOriginalExtension();
        $filename = "imgupload3.".$extension;
        $request->file('imgupload3')->move($destinationPath,$filename);
        $pengajuan->img_3=$imgName.$filename;

        $img3 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_3.jpg";
        $img3->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_3=$imgThumbName.$filename;
        }
        /////////////////
        if($request->hasFile('imgupload4')){
        $photo=$request->file('imgupload4')->getClientOriginalName();
        $extension = $request->file('imgupload4')->getClientOriginalExtension();
        $filename = "imgupload4.".$extension;
        $request->file('imgupload4')->move($destinationPath,$filename);
        $pengajuan->img_4=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_4.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_4=$imgThumbName.$filename;
        }
        /////////////////
        if($request->hasFile('imgupload5')){
        $photo=$request->file('imgupload5')->getClientOriginalName();
        $extension = $request->file('imgupload5')->getClientOriginalExtension();
        $filename = "imgupload5.".$extension;
        $request->file('imgupload5')->move($destinationPath,$filename);
        $pengajuan->img_5=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_5.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_5=$imgThumbName.$filename;
        }
        /////////////////
        if($request->hasFile('imgupload6')){
        $photo=$request->file('imgupload6')->getClientOriginalName();
        $extension = $request->file('imgupload6')->getClientOriginalExtension();
        $filename = "imgupload6.".$extension;
        $request->file('imgupload6')->move($destinationPath,$filename);
        $pengajuan->img_6=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_6.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_6=$imgThumbName.$filename;
        }
        /////////////////
        if($request->hasFile('imgupload7')){
        $photo=$request->file('imgupload7')->getClientOriginalName();
        $extension = $request->file('imgupload7')->getClientOriginalExtension();
        $filename = "imgupload7.".$extension;
        $request->file('imgupload7')->move($destinationPath,$filename);
        $pengajuan->img_7=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_7.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_7=$imgThumbName.$filename;
        }
        /////////////////
        if($request->hasFile('imgupload8')){
        $photo=$request->file('imgupload8')->getClientOriginalName();
        $extension = $request->file('imgupload8')->getClientOriginalExtension();
        $filename = "imgupload8.".$extension;
        $request->file('imgupload8')->move($destinationPath,$filename);
        $pengajuan->img_8=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_8.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_8=$imgThumbName.$filename;
        }
        /////////////////
        if($request->hasFile('imgupload9')){
        $photo=$request->file('imgupload9')->getClientOriginalName();
        $extension = $request->file('imgupload9')->getClientOriginalExtension();
        $filename = "imgupload9.".$extension;
        $request->file('imgupload9')->move($destinationPath,$filename);
        $pengajuan->img_9=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_9.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_9=$imgThumbName.$filename;
        }
        /////////////////
        if($request->hasFile('imgupload10')){
        $photo=$request->file('imgupload10')->getClientOriginalName();
        $extension = $request->file('imgupload10')->getClientOriginalExtension();
        $filename = "imgupload10.".$extension;
        $request->file('imgupload10')->move($destinationPath,$filename);
        $pengajuan->img_10=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_10.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_10=$imgThumbName.$filename;
        }
        /////////////////
        if($request->hasFile('imgupload11')){
        $photo=$request->file('imgupload11')->getClientOriginalName();
        $extension = $request->file('imgupload11')->getClientOriginalExtension();
        $filename = "imgupload11.".$extension;
        $request->file('imgupload11')->move($destinationPath,$filename);
        $pengajuan->img_11=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_11.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_11=$imgThumbName.$filename;
        }
        /////////////////
        if($request->hasFile('imgupload12')){
        $photo=$request->file('imgupload12')->getClientOriginalName();
        $extension = $request->file('imgupload12')->getClientOriginalExtension();
        $filename = "imgupload12.".$extension;
        $request->file('imgupload12')->move($destinationPath,$filename);
        $pengajuan->img_12=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_12.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_12=$imgThumbName.$filename;
        }
        /////////////////
        if($request->hasFile('imgupload13')){
        $photo=$request->file('imgupload13')->getClientOriginalName();
        $extension = $request->file('imgupload13')->getClientOriginalExtension();
        $filename = "imgupload13.".$extension;
        $request->file('imgupload13')->move($destinationPath,$filename);
        $pengajuan->img_13=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_13.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_13=$imgThumbName.$filename;
        }
        /////////////////
        if($request->hasFile('imgupload14')){
        $photo=$request->file('imgupload14')->getClientOriginalName();
        $extension = $request->file('imgupload14')->getClientOriginalExtension();
        $filename = "imgupload14.".$extension;
        $request->file('imgupload14')->move($destinationPath,$filename);
        $pengajuan->img_14=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_14.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_14=$imgThumbName.$filename;
        }
        /////////////////
        if($request->hasFile('imgupload15')){
        $photo=$request->file('imgupload15')->getClientOriginalName();
        $extension = $request->file('imgupload15')->getClientOriginalExtension();
        $filename = "imgupload15.".$extension;
        $request->file('imgupload15')->move($destinationPath,$filename);
        $pengajuan->img_15=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_15.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_15=$imgThumbName.$filename;
        }

        $pengajuan->save();
        return response()->json([
          'status' => 'success',
          'data' => 'Pendaftaran berhasil'
        ]);
      }
    }
    public function store_capil(Request $request)
    {
      $nik= Auth::user()->nik;
      $jenis_layanan=$request->jenis_layanan;
      if ($jenis_layanan == 1){
        $query=DB::connection('webpunten')
        ->select("SELECT COUNT(1) JUMLAH FROM PENGAJUANLAHIR WHERE NIK = ".$nik." AND JENIS_LAYANAN = 1 AND PROC_STAT = 1");
      }else if ($jenis_layanan == 2){
         $query=DB::connection('webpunten')
         ->select("SELECT COUNT(1) JUMLAH FROM PENGAJUANMATI WHERE NIK = ".$nik." AND JENIS_LAYANAN = 2 AND PROC_STAT = 1");
      }
      
    if($query[0]->jumlah > 0){
      return response()->json([
        'status' => 'error',
        'data' => 'NIK sudah pernah mengajukan sebelumnya'
      ]);
    }else{
      if ($jenis_layanan == 1){
        $destinationPath=$this->pengajuan_path().'/pengajuan/lahir/'.$nik.'/ori/';
        $destinationPathThumb=$this->pengajuan_path().'/pengajuan/lahir/'.$nik.'/thumb/';
        $imgName = 'pengajuan/lahir/'.$nik.'/ori/';
        $imgThumbName = 'pengajuan/lahir/'.$nik.'/thumb/';
      }else if($jenis_layanan == 2){
        $destinationPath=$this->pengajuan_path().'/pengajuan/mati/'.$nik.'/ori/';
        $destinationPathThumb=$this->pengajuan_path().'/pengajuan/mati/'.$nik.'/thumb/';
        $imgName = 'pengajuan/mati/'.$nik.'/ori/';
        $imgThumbName = 'pengajuan/mati/'.$nik.'/thumb/';
      }else{
         return response()->json([
          'status' => 'error',
          'data' => 'Jenis Pengajuan Salah Mohon Cek Kemabali'
        ]);
      }


      $picname = "imgupload_thumb_1.jpg";
      if (!file_exists($destinationPath)) {
        mkdir($destinationPath, 0777, true);
      }
      if (!file_exists($destinationPathThumb)) {
        mkdir($destinationPathThumb, 0777, true);
      }
      
      if ($jenis_layanan == 1){
        $pengajuan = new Pengajuanlahir;
      }else if($jenis_layanan == 2){
         $pengajuan = new Pengajuanmati;
      }else{
          return response()->json([
          'status' => 'error',
          'data' => 'Jenis Pengajuan Salah Mohon Cek Kemabali'
        ]);
      }
        
        $pengajuan->proc_stat=1;
        $pengajuan->nik=$nik;
        $pengajuan->nama_lgkp=$request->nama_lgkp;
        $pengajuan->jenis_klmin=$request->jenis_klmin;
        $pengajuan->tmpt_lhr=$request->tmpt_lhr;
        $pengajuan->tgl_lhr=$request->tgl_lhr;
        $pengajuan->agama=$request->agama;
        $pengajuan->stat_kwn=$request->stat_kwn;
        $pengajuan->gol_drh=$request->gol_drh;
        $pengajuan->pendidikan=$request->pendidikan;
        $pengajuan->pekerjaan=$request->pekerjaan;
        $pengajuan->src_prov=$request->src_prov;
        $pengajuan->src_kab=$request->src_kab;
        $pengajuan->src_kec=$request->src_kec;
        $pengajuan->src_kel=$request->src_kel;
        $pengajuan->src_alamat=$request->src_alamat;
        $pengajuan->src_rt=$request->src_rt;
        $pengajuan->src_rw=$request->src_rw;
        $pengajuan->kec=$request->kec;
        $pengajuan->kel=$request->kel;
        $pengajuan->alamat=$request->alamat;
        $pengajuan->no_rw=$request->no_rw;
        $pengajuan->no_rt=$request->no_rt;
        $pengajuan->status=$request->status;
        $pengajuan->jenis_layanan=$request->jenis_layanan;
        if ($request->jenis_layanan == 3){
        $pengajuan->kwrngrn=$request->kwrngrn;
        $pengajuan->no_paspor=$request->no_paspor;
        $pengajuan->tgl_paspor=$request->tgl_paspor;
        $pengajuan->tgl_akh_paspor=$request->tgl_akh_paspor;
        $pengajuan->no_dok=$request->no_doc;
        $pengajuan->dok_imgr=$request->doc_imigrasi;
        $pengajuan->tgl_doc_imigrasi=$request->tgl_doc_imigrasi;
        $pengajuan->tgl_stay=$request->tgl_stay;
       }
        
        $pengajuan->tgl_berlaku= Carbon::now()->addMonths(6);

        /////////////////
        if($request->hasFile('imgupload1')){
        $photo=$request->file('imgupload1')->getClientOriginalName();
        $extension = $request->file('imgupload1')->getClientOriginalExtension();
        $filename = "imgupload1.".$extension;
        $request->file('imgupload1')->move($destinationPath,$filename);
        $pengajuan->img_1=$imgName.$filename;

        $img1 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_1.jpg";
        $img1->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_1=$imgThumbName.$filename;
        }

        /////////////////
        if($request->hasFile('imgupload2')){
        $photo=$request->file('imgupload2')->getClientOriginalName();
        $extension = $request->file('imgupload2')->getClientOriginalExtension();
        $filename = "imgupload2.".$extension;
        $request->file('imgupload2')->move($destinationPath,$filename);
        $pengajuan->img_2=$imgName.$filename;

        $img2 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_2.jpg";
        $img2->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_2=$imgThumbName.$filename;
        }
        /////////////////
        if($request->hasFile('imgupload3')){
        $photo=$request->file('imgupload3')->getClientOriginalName();
        $extension = $request->file('imgupload3')->getClientOriginalExtension();
        $filename = "imgupload3.".$extension;
        $request->file('imgupload3')->move($destinationPath,$filename);
        $pengajuan->img_3=$imgName.$filename;

        $img3 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_3.jpg";
        $img3->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_3=$imgThumbName.$filename;
        }
        /////////////////
        if($request->hasFile('imgupload4')){
        $photo=$request->file('imgupload4')->getClientOriginalName();
        $extension = $request->file('imgupload4')->getClientOriginalExtension();
        $filename = "imgupload4.".$extension;
        $request->file('imgupload4')->move($destinationPath,$filename);
        $pengajuan->img_4=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_4.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_4=$imgThumbName.$filename;
        }
        /////////////////
        if($request->hasFile('imgupload5')){
        $photo=$request->file('imgupload5')->getClientOriginalName();
        $extension = $request->file('imgupload5')->getClientOriginalExtension();
        $filename = "imgupload5.".$extension;
        $request->file('imgupload5')->move($destinationPath,$filename);
        $pengajuan->img_5=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_5.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_5=$imgThumbName.$filename;
        }
        /////////////////
        if($request->hasFile('imgupload6')){
        $photo=$request->file('imgupload6')->getClientOriginalName();
        $extension = $request->file('imgupload6')->getClientOriginalExtension();
        $filename = "imgupload6.".$extension;
        $request->file('imgupload6')->move($destinationPath,$filename);
        $pengajuan->img_6=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_6.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_6=$imgThumbName.$filename;
        }
        /////////////////
        if($request->hasFile('imgupload7')){
        $photo=$request->file('imgupload7')->getClientOriginalName();
        $extension = $request->file('imgupload7')->getClientOriginalExtension();
        $filename = "imgupload7.".$extension;
        $request->file('imgupload7')->move($destinationPath,$filename);
        $pengajuan->img_7=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_7.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_7=$imgThumbName.$filename;
        }
        /////////////////
        if($request->hasFile('imgupload8')){
        $photo=$request->file('imgupload8')->getClientOriginalName();
        $extension = $request->file('imgupload8')->getClientOriginalExtension();
        $filename = "imgupload8.".$extension;
        $request->file('imgupload8')->move($destinationPath,$filename);
        $pengajuan->img_8=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_8.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_8=$imgThumbName.$filename;
        }
        /////////////////
        if($request->hasFile('imgupload9')){
        $photo=$request->file('imgupload9')->getClientOriginalName();
        $extension = $request->file('imgupload9')->getClientOriginalExtension();
        $filename = "imgupload9.".$extension;
        $request->file('imgupload9')->move($destinationPath,$filename);
        $pengajuan->img_9=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_9.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_9=$imgThumbName.$filename;
        }
        /////////////////
        if($request->hasFile('imgupload10')){
        $photo=$request->file('imgupload10')->getClientOriginalName();
        $extension = $request->file('imgupload10')->getClientOriginalExtension();
        $filename = "imgupload10.".$extension;
        $request->file('imgupload10')->move($destinationPath,$filename);
        $pengajuan->img_10=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_10.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_10=$imgThumbName.$filename;
        }
        /////////////////
        if($request->hasFile('imgupload11')){
        $photo=$request->file('imgupload11')->getClientOriginalName();
        $extension = $request->file('imgupload11')->getClientOriginalExtension();
        $filename = "imgupload11.".$extension;
        $request->file('imgupload11')->move($destinationPath,$filename);
        $pengajuan->img_11=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_11.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_11=$imgThumbName.$filename;
        }
        /////////////////
        if($request->hasFile('imgupload12')){
        $photo=$request->file('imgupload12')->getClientOriginalName();
        $extension = $request->file('imgupload12')->getClientOriginalExtension();
        $filename = "imgupload12.".$extension;
        $request->file('imgupload12')->move($destinationPath,$filename);
        $pengajuan->img_12=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_12.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_12=$imgThumbName.$filename;
        }
        /////////////////
        if($request->hasFile('imgupload13')){
        $photo=$request->file('imgupload13')->getClientOriginalName();
        $extension = $request->file('imgupload13')->getClientOriginalExtension();
        $filename = "imgupload13.".$extension;
        $request->file('imgupload13')->move($destinationPath,$filename);
        $pengajuan->img_13=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_13.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_13=$imgThumbName.$filename;
        }
        /////////////////
        if($request->hasFile('imgupload14')){
        $photo=$request->file('imgupload14')->getClientOriginalName();
        $extension = $request->file('imgupload14')->getClientOriginalExtension();
        $filename = "imgupload14.".$extension;
        $request->file('imgupload14')->move($destinationPath,$filename);
        $pengajuan->img_14=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_14.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_14=$imgThumbName.$filename;
        }
        /////////////////
        if($request->hasFile('imgupload15')){
        $photo=$request->file('imgupload15')->getClientOriginalName();
        $extension = $request->file('imgupload15')->getClientOriginalExtension();
        $filename = "imgupload15.".$extension;
        $request->file('imgupload15')->move($destinationPath,$filename);
        $pengajuan->img_15=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_15.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_15=$imgThumbName.$filename;
        }

        $pengajuan->save();
        return response()->json([
          'status' => 'success',
          'data' => 'Pendaftaran berhasil'
        ]);
      }
    }

     public function restore(Request $request)
    {
      $nik= Auth::user()->nik;
      $jenis_layanan=$request->jenis_layanan;
      if ($jenis_layanan == 1){
        $query=DB::connection('webpunten')
        ->select("SELECT COUNT(1) JUMLAH FROM PENGAJUANSKTS WHERE NIK = ".$nik." AND JENIS_LAYANAN = 1 AND PROC_STAT = 1");
      }else if ($jenis_layanan == 2){
         $query=DB::connection('webpunten')
         ->select("SELECT COUNT(1) JUMLAH FROM PENGAJUANDATANG WHERE NIK = ".$nik." AND JENIS_LAYANAN = 2 AND PROC_STAT = 1");
      }else{
        $query=DB::connection('webpunten')
        ->select("SELECT COUNT(1) JUMLAH FROM PENGAJUANWNA WHERE NIK = ".$nik." AND JENIS_LAYANAN = 3 AND PROC_STAT = 1");
      }
      
    if($query[0]->jumlah == 0){
      return response()->json([
        'status' => 'error',
        'data' => 'NIK belum pernah mengajukan sebelumnya'
      ]);
    }else{
      if ($jenis_layanan == 1){
        $destinationPath=$this->pengajuan_path().'/pengajuan/skts/'.$nik.'/ori/';
        $destinationPathThumb=$this->pengajuan_path().'/pengajuan/skts/'.$nik.'/thumb/';
        $imgName = 'pengajuan/skts/'.$nik.'/ori/';
        $imgThumbName = 'pengajuan/skts/'.$nik.'/thumb/';
      }else if($jenis_layanan == 2){
        $destinationPath=$this->pengajuan_path().'/pengajuan/datang/'.$nik.'/ori/';
        $destinationPathThumb=$this->pengajuan_path().'/pengajuan/datang/'.$nik.'/thumb/';
        $imgName = 'pengajuan/datang/'.$nik.'/ori/';
        $imgThumbName = 'pengajuan/datang/'.$nik.'/thumb/';
      }else if($jenis_layanan == 3){
        $destinationPath=$this->pengajuan_path().'/pengajuan/wna/'.$nik.'/ori/';
        $destinationPathThumb=$this->pengajuan_path().'/pengajuan/wna/'.$nik.'/thumb/';
        $imgName = 'pengajuan/wna/'.$nik.'/ori/';
        $imgThumbName = 'pengajuan/wna/'.$nik.'/thumb/';
      }else{
         return response()->json([
          'status' => 'error',
          'data' => 'Jenis Pengajuan Salah Mohon Cek Kemabali'
        ]);
      }


      $picname = "re_imgupload_thumb_1.jpg";
      if (!file_exists($destinationPath)) {
        mkdir($destinationPath, 0777, true);
      }
      if (!file_exists($destinationPathThumb)) {
        mkdir($destinationPathThumb, 0777, true);
      }
      
      if ($jenis_layanan == 1){
        $pengajuan = new Pengajuanskts;
      }else if($jenis_layanan == 2){
         $pengajuan = new Pengajuandatang;
      }else if($jenis_layanan == 3){
        $pengajuan = new Pengajuanwna;
      }else{
          return response()->json([
          'status' => 'error',
          'data' => 'Jenis Pengajuan Salah Mohon Cek Kemabali'
        ]);
      }
        
        $pengajuan->proc_stat=2;
        $pengajuan->stat_berkas=1;
        $pengajuan->nik=$nik;
        $pengajuan->nama_lgkp=$request->nama_lgkp;
        $pengajuan->jenis_klmin=$request->jenis_klmin;
        $pengajuan->tmpt_lhr=$request->tmpt_lhr;
        $pengajuan->tgl_lhr=$request->tgl_lhr;
        $pengajuan->agama=$request->agama;
        $pengajuan->stat_kwn=$request->stat_kwn;
        $pengajuan->gol_drh=$request->gol_drh;
        $pengajuan->pendidikan=$request->pendidikan;
        $pengajuan->pekerjaan=$request->pekerjaan;
        $pengajuan->src_prov=$request->src_prov;
        $pengajuan->src_kab=$request->src_kab;
        $pengajuan->src_kec=$request->src_kec;
        $pengajuan->src_kel=$request->src_kel;
        $pengajuan->src_alamat=$request->src_alamat;
        $pengajuan->src_rt=$request->src_rt;
        $pengajuan->src_rw=$request->src_rw;
        $pengajuan->kec=$request->kec;
        $pengajuan->kel=$request->kel;
        $pengajuan->alamat=$request->alamat;
        $pengajuan->no_rw=$request->no_rw;
        $pengajuan->no_rt=$request->no_rt;
        $pengajuan->status=$request->status;
        $pengajuan->jenis_layanan=$request->jenis_layanan;
        if ($request->jenis_layanan == 3){
        $pengajuan->kwrngrn=$request->kwrngrn;
        $pengajuan->no_paspor=$request->no_paspor;
        $pengajuan->tgl_paspor=$request->tgl_paspor;
        $pengajuan->tgl_akh_paspor=$request->tgl_akh_paspor;
        $pengajuan->no_dok=$request->no_doc;
        $pengajuan->dok_imgr=$request->doc_imigrasi;
        $pengajuan->tgl_doc_imigrasi=$request->tgl_doc_imigrasi;
        $pengajuan->tgl_stay=$request->tgl_stay;
       }
        
        $pengajuan->tgl_berlaku= Carbon::now()->addMonths(6);

        /////////////////
        if($request->hasFile('re_imgupload1')){
        $photo=$request->file('re_imgupload1')->getClientOriginalName();
        $extension = $request->file('re_imgupload1')->getClientOriginalExtension();
        $filename = "re_imgupload1.".$extension;
        $request->file('re_imgupload1')->move($destinationPath,$filename);
        $pengajuan->img_1=$imgName.$filename;

        $img1 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "re_imgupload_thumb_1.jpg";
        $img1->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_1=$imgThumbName.$filename;
        }

        /////////////////
        if($request->hasFile('re_imgupload2')){
        $photo=$request->file('re_imgupload2')->getClientOriginalName();
        $extension = $request->file('re_imgupload2')->getClientOriginalExtension();
        $filename = "re_imgupload2.".$extension;
        $request->file('re_imgupload2')->move($destinationPath,$filename);
        $pengajuan->img_2=$imgName.$filename;

        $img2 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "re_imgupload_thumb_2.jpg";
        $img2->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_2=$imgThumbName.$filename;
        }
        /////////////////
        if($request->hasFile('re_imgupload3')){
        $photo=$request->file('re_imgupload3')->getClientOriginalName();
        $extension = $request->file('re_imgupload3')->getClientOriginalExtension();
        $filename = "re_imgupload3.".$extension;
        $request->file('re_imgupload3')->move($destinationPath,$filename);
        $pengajuan->img_3=$imgName.$filename;

        $img3 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "re_imgupload_thumb_3.jpg";
        $img3->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_3=$imgThumbName.$filename;
        }
        /////////////////
        if($request->hasFile('re_imgupload4')){
        $photo=$request->file('re_imgupload4')->getClientOriginalName();
        $extension = $request->file('re_imgupload4')->getClientOriginalExtension();
        $filename = "re_imgupload4.".$extension;
        $request->file('re_imgupload4')->move($destinationPath,$filename);
        $pengajuan->img_4=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "re_imgupload_thumb_4.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_4=$imgThumbName.$filename;
        }
        /////////////////
        if($request->hasFile('re_imgupload5')){
        $photo=$request->file('re_imgupload5')->getClientOriginalName();
        $extension = $request->file('re_imgupload5')->getClientOriginalExtension();
        $filename = "re_imgupload5.".$extension;
        $request->file('re_imgupload5')->move($destinationPath,$filename);
        $pengajuan->img_5=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "re_imgupload_thumb_5.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_5=$imgThumbName.$filename;
        }
        /////////////////
        if($request->hasFile('re_imgupload6')){
        $photo=$request->file('re_imgupload6')->getClientOriginalName();
        $extension = $request->file('re_imgupload6')->getClientOriginalExtension();
        $filename = "re_imgupload6.".$extension;
        $request->file('re_imgupload6')->move($destinationPath,$filename);
        $pengajuan->img_6=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "re_imgupload_thumb_6.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_6=$imgThumbName.$filename;
        }
        /////////////////
        if($request->hasFile('re_imgupload7')){
        $photo=$request->file('re_imgupload7')->getClientOriginalName();
        $extension = $request->file('re_imgupload7')->getClientOriginalExtension();
        $filename = "re_imgupload7.".$extension;
        $request->file('re_imgupload7')->move($destinationPath,$filename);
        $pengajuan->img_7=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "re_imgupload_thumb_7.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_7=$imgThumbName.$filename;
        }
        /////////////////
        if($request->hasFile('re_imgupload8')){
        $photo=$request->file('re_imgupload8')->getClientOriginalName();
        $extension = $request->file('re_imgupload8')->getClientOriginalExtension();
        $filename = "re_imgupload8.".$extension;
        $request->file('re_imgupload8')->move($destinationPath,$filename);
        $pengajuan->img_8=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "re_imgupload_thumb_8.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_8=$imgThumbName.$filename;
        }
        /////////////////
        if($request->hasFile('re_imgupload9')){
        $photo=$request->file('re_imgupload9')->getClientOriginalName();
        $extension = $request->file('re_imgupload9')->getClientOriginalExtension();
        $filename = "re_imgupload9.".$extension;
        $request->file('re_imgupload9')->move($destinationPath,$filename);
        $pengajuan->img_9=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "re_imgupload_thumb_9.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_9=$imgThumbName.$filename;
        }
        /////////////////
        if($request->hasFile('re_imgupload10')){
        $photo=$request->file('re_imgupload10')->getClientOriginalName();
        $extension = $request->file('re_imgupload10')->getClientOriginalExtension();
        $filename = "re_imgupload10.".$extension;
        $request->file('re_imgupload10')->move($destinationPath,$filename);
        $pengajuan->img_10=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "re_imgupload_thumb_10.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_10=$imgThumbName.$filename;
        }
        /////////////////
        if($request->hasFile('re_imgupload11')){
        $photo=$request->file('re_imgupload11')->getClientOriginalName();
        $extension = $request->file('re_imgupload11')->getClientOriginalExtension();
        $filename = "re_imgupload11.".$extension;
        $request->file('re_imgupload11')->move($destinationPath,$filename);
        $pengajuan->img_11=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "re_imgupload_thumb_11.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_11=$imgThumbName.$filename;
        }
        /////////////////
        if($request->hasFile('re_imgupload12')){
        $photo=$request->file('re_imgupload12')->getClientOriginalName();
        $extension = $request->file('re_imgupload12')->getClientOriginalExtension();
        $filename = "re_imgupload12.".$extension;
        $request->file('re_imgupload12')->move($destinationPath,$filename);
        $pengajuan->img_12=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "re_imgupload_thumb_12.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_12=$imgThumbName.$filename;
        }
        /////////////////
        if($request->hasFile('re_imgupload13')){
        $photo=$request->file('re_imgupload13')->getClientOriginalName();
        $extension = $request->file('re_imgupload13')->getClientOriginalExtension();
        $filename = "re_imgupload13.".$extension;
        $request->file('re_imgupload13')->move($destinationPath,$filename);
        $pengajuan->img_13=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "re_imgupload_thumb_13.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_13=$imgThumbName.$filename;
        }
        /////////////////
        if($request->hasFile('re_imgupload14')){
        $photo=$request->file('re_imgupload14')->getClientOriginalName();
        $extension = $request->file('re_imgupload14')->getClientOriginalExtension();
        $filename = "re_imgupload14.".$extension;
        $request->file('re_imgupload14')->move($destinationPath,$filename);
        $pengajuan->img_14=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "re_imgupload_thumb_14.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_14=$imgThumbName.$filename;
        }
        /////////////////
        if($request->hasFile('re_imgupload15')){
        $photo=$request->file('re_imgupload15')->getClientOriginalName();
        $extension = $request->file('re_imgupload15')->getClientOriginalExtension();
        $filename = "re_imgupload15.".$extension;
        $request->file('re_imgupload15')->move($destinationPath,$filename);
        $pengajuan->img_15=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "re_imgupload_thumb_15.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_15=$imgThumbName.$filename;
        }
        $pengajuan->update();
        return response()->json([
          'status' => 'success',
          'data' => 'Pendaftaran berhasil'
        ]);
      }
    }

    public function cek_pengajuan(Request $request)
    {
      $nik = Auth::user()->nik;
      $jenis_layanan = 0;
      
        $query1=DB::connection('webpunten')
         ->select("SELECT COUNT(1) JUMLAH FROM PENGAJUANSKTS WHERE NIK = ".$nik."");
        if($query1[0]->jumlah > 0){
          $jenis_layanan = 1;
        }

        $query2=DB::connection('webpunten')
         ->select("SELECT COUNT(1) JUMLAH FROM PENGAJUANDATANG WHERE NIK = ".$nik."");
        if($query2[0]->jumlah > 0){
          $jenis_layanan = 2;
        }

        $query3=DB::connection('webpunten')
         ->select("SELECT COUNT(1) JUMLAH FROM PENGAJUANWNA WHERE NIK = ".$nik."");
        if($query3[0]->jumlah > 0){
          $jenis_layanan = 3;
        }

      if($jenis_layanan <> 0){
        if($jenis_layanan == 1){
          $query=DB::connection('webpunten')
         ->select("SELECT  ID ,NIK ,IMG_1 ,IMG_THUMB_1 ,IMG_2 ,IMG_THUMB_2 ,IMG_3 ,IMG_THUMB_3 ,IMG_4 ,IMG_THUMB_4 ,IMG_5 ,PROC_STAT ,STAT_BERKAS ,VERIFIED_AT ,VERIFIED_BY ,CREATED_AT ,UPDATED_AT ,DELETED_AT ,DELETED_BY ,NO_SKTS ,NAMA_LGKP ,TMPT_LHR 
            ,TO_CHAR(TGL_LHR ,'DD-MM-YYYY') TGL_LHR
            ,JENIS_KLMIN ,AGAMA ,STAT_KWN ,GOL_DRH ,PENDIDIKAN ,PEKERJAAN 
            , SRC_PROV ,SRC_KAB ,SRC_KEC ,SRC_KEL 
            , F5_GET_NAMA_PROVINSI(SRC_PROV) SRC_NAMA_PROV
            , F5_GET_NAMA_KABUPATEN(SRC_PROV,SRC_KAB) SRC_NAMA_KAB
            , F5_GET_NAMA_KECAMATAN(SRC_PROV,SRC_KAB ,SRC_KEC) SRC_NAMA_KEC
            , F5_GET_NAMA_KELURAHAN(SRC_PROV,SRC_KAB ,SRC_KEC ,SRC_KEL ) SRC_NAMA_KEL
            , SRC_ALAMAT ,SRC_RT ,SRC_RW ,KEC ,KEL 
            , F5_GET_NAMA_KECAMATAN(32,73,KEC) NAMA_KEC
            , F5_GET_NAMA_KELURAHAN(32,73,KEC ,KEL ) NAMA_KEL
            , UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(JENIS_KLMIN,7), 801)) JENIS_KLMIN_DESC
            , UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(AGAMA,7), 501)) AGAMA_DESC
            , UPPER(F5_GET_REF_WNI(PEKERJAAN, 201)) PEKERJAAN_DESC
            , UPPER(F5_GET_REF_WNI(PENDIDIKAN, 101)) PENDIDIKAN_DESC
            , UPPER(F5_GET_REF_WNI(STAT_KWN, 601)) STAT_KWN_DESC
            , UPPER(F5_GET_REF_WNI(GOL_DRH, 401)) GOL_DRH_DESC
            ,ALAMAT ,NO_RT ,NO_RW ,STATUS 
            ,TO_CHAR(TGL_BERLAKU,'DD-MM-YYYY') TGL_BERLAKU  ,JENIS_LAYANAN,
            CASE WHEN JENIS_LAYANAN = 1 THEN 'SKTS' WHEN JENIS_LAYANAN = 2 THEN 'PINDAH DATANG' ELSE 'WNA' END  JENIS_LAYANAN_DESC 
            FROM PENGAJUANSKTS WHERE NIK = ".$nik."");
        }else if($jenis_layanan == 2){
            $query=DB::connection('webpunten')
            ->select("SELECT ID ,NIK ,IMG_1 ,IMG_THUMB_1 ,IMG_2 ,IMG_THUMB_2 ,IMG_3 ,IMG_THUMB_3 ,IMG_4 ,IMG_THUMB_4 ,IMG_5 ,PROC_STAT ,STAT_BERKAS ,VERIFIED_AT ,VERIFIED_BY ,CREATED_AT ,UPDATED_AT ,DELETED_AT ,DELETED_BY ,NO_DATANG ,NAMA_LGKP ,TMPT_LHR 
            ,TO_CHAR(TGL_LHR ,'DD-MM-YYYY') TGL_LHR
            ,JENIS_KLMIN ,AGAMA ,STAT_KWN ,GOL_DRH ,PENDIDIKAN ,PEKERJAAN 
            , SRC_PROV ,SRC_KAB ,SRC_KEC ,SRC_KEL 
            , F5_GET_NAMA_PROVINSI(SRC_PROV) SRC_NAMA_PROV
            , F5_GET_NAMA_KABUPATEN(SRC_PROV,SRC_KAB) SRC_NAMA_KAB
            , F5_GET_NAMA_KECAMATAN(SRC_PROV,SRC_KAB ,SRC_KEC) SRC_NAMA_KEC
            , F5_GET_NAMA_KELURAHAN(SRC_PROV,SRC_KAB ,SRC_KEC ,SRC_KEL ) SRC_NAMA_KEL
            , SRC_ALAMAT ,SRC_RT ,SRC_RW ,KEC ,KEL 
            , F5_GET_NAMA_KECAMATAN(32,73,KEC) NAMA_KEC
            , F5_GET_NAMA_KELURAHAN(32,73,KEC ,KEL ) NAMA_KEL
            , UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(JENIS_KLMIN,7), 801)) JENIS_KLMIN_DESC
            , UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(AGAMA,7), 501)) AGAMA_DESC
            , UPPER(F5_GET_REF_WNI(PEKERJAAN, 201)) PEKERJAAN_DESC
            , UPPER(F5_GET_REF_WNI(PENDIDIKAN, 101)) PENDIDIKAN_DESC
            , UPPER(F5_GET_REF_WNI(STAT_KWN, 601)) STAT_KWN_DESC
            , UPPER(F5_GET_REF_WNI(GOL_DRH, 401)) GOL_DRH_DESC
            ,ALAMAT ,NO_RT ,NO_RW ,STATUS 
            ,TO_CHAR(TGL_BERLAKU,'DD-MM-YYYY') TGL_BERLAKU  ,JENIS_LAYANAN,
            CASE WHEN JENIS_LAYANAN = 1 THEN 'SKTS' WHEN JENIS_LAYANAN = 2 THEN 'PINDAH DATANG' ELSE 'WNA' END  JENIS_LAYANAN_DESC 
            FROM PENGAJUANDATANG WHERE NIK = ".$nik."");
        }else{
            $query=DB::connection('webpunten')
          ->select("SELECT ID ,NIK ,IMG_1 ,IMG_THUMB_1 ,IMG_2 ,IMG_THUMB_2 ,IMG_3 ,IMG_THUMB_3 ,IMG_4 ,IMG_THUMB_4 ,IMG_5 ,PROC_STAT ,STAT_BERKAS ,VERIFIED_AT ,VERIFIED_BY ,CREATED_AT ,UPDATED_AT ,DELETED_AT ,DELETED_BY ,NO_WNA,NAMA_LGKP ,TMPT_LHR 
            ,TO_CHAR(TGL_LHR ,'DD-MM-YYYY') TGL_LHR
            ,JENIS_KLMIN ,AGAMA ,STAT_KWN ,GOL_DRH ,PENDIDIKAN ,PEKERJAAN 
            , SRC_PROV ,SRC_KAB ,SRC_KEC ,SRC_KEL 
            , F5_GET_NAMA_PROVINSI(SRC_PROV) SRC_NAMA_PROV
            , F5_GET_NAMA_KABUPATEN(SRC_PROV,SRC_KAB) SRC_NAMA_KAB
            , F5_GET_NAMA_KECAMATAN(SRC_PROV,SRC_KAB ,SRC_KEC) SRC_NAMA_KEC
            , F5_GET_NAMA_KELURAHAN(SRC_PROV,SRC_KAB ,SRC_KEC ,SRC_KEL ) SRC_NAMA_KEL
            , SRC_ALAMAT ,SRC_RT ,SRC_RW ,KEC ,KEL 
            , F5_GET_NAMA_KECAMATAN(32,73,KEC) NAMA_KEC
            , F5_GET_NAMA_KELURAHAN(32,73,KEC ,KEL ) NAMA_KEL
            , UPPER(F5_GET_REF_WNA(F5_TO_NUMBER(JENIS_KLMIN,7), 801)) JENIS_KLMIN_DESC
            , UPPER(F5_GET_REF_WNA(F5_TO_NUMBER(AGAMA,7), 901)) AGAMA_DESC
            , UPPER(F5_GET_REF_WNA(PEKERJAAN, 201)) PEKERJAAN_DESC
            , UPPER(F5_GET_REF_WNA(PENDIDIKAN, 101)) PENDIDIKAN_DESC
            , UPPER(F5_GET_REF_WNA(STAT_KWN, 701)) STAT_KWN_DESC
            , UPPER(F5_GET_REF_WNA(GOL_DRH, 401)) GOL_DRH_DESC
            , UPPER(F5_GET_REF_NEGARA(KWRNGRN)) KWRNGRN
            ,ALAMAT ,NO_RT ,NO_RW ,STATUS 
            ,TO_CHAR(TGL_BERLAKU,'DD-MM-YYYY') TGL_BERLAKU  ,JENIS_LAYANAN,
           'WNA' JENIS_LAYANAN_DESC 
            FROM PENGAJUANWNA WHERE NIK = ".$nik."");
        }
          return response()->json([
           $query[0]
        ]);
      }else{
        return response()->json([
          'status' => 'error',
          'data' => 'NIK Tidak Ada'
        ]);
      }
    }
    public function get_kk_epunten(Request $request){
      $nik = Auth::user()->nik;
      $query=DB::connection('db222')
          ->select("SELECT SUBSTR(URL_DOKUMEN,20) URLONE, SUBSTR(URL_DOKUMEN,11,8) URLSECOND FROM BIODATA_WNI A INNER JOIN DATA_KELUARGA B ON A.NO_KK = B.NO_KK INNER JOIN 
              (
                SELECT 
                NO_DOC
                , CERT_STATUS
                , REQ_DATE
                , REQ_BY
                , SEQN_ID
                , URL_DOKUMEN
                , PEJABAT_PROCCESS_BY
                , PEJABAT_PROCESS_DATE  
              FROM 
              (
                SELECT 
                NO_DOC
                , CERT_STATUS
                , REQ_DATE
                , REQ_BY
                , SEQN_ID
                , URL_DOKUMEN
                , PEJABAT_PROCCESS_BY
                , PEJABAT_PROCESS_DATE 
                , RANK() OVER (PARTITION BY NO_DOC ORDER BY REQ_DATE DESC,SEQN_ID DESC) RNK FROM BSRE_KARTU_KELUARGA) 
                WHERE RNK = 1) C ON B.NO_KK = C.NO_DOC WHERE A.NIK = ".$nik."");
          if (count($query) > 0){
            $filePath = 'http://10.32.73.222:8080/Siak/cetak/main/view_pdf/'.$query[0]->urlone.'/'.$query[0]->urlsecond.'/cetak_kartu_keluarga.pdf';
            $fileContent = file_get_contents($filePath);
              $response = response($fileContent, 200, [
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'attachment; filename=kartu_keluarga_'.date("Ymd").'_'.$nik.'.pdf',
              ]);
                // return $filePath;
              return $response;
          }else{
            return response()->json([
              'status' => 'error',
              'data' => 'KK Tidak Ditemukan'
            ]);
          }
    }
    public static function pengajuan_path(){
      return str_replace('\public', '', base_path());
    }
}
