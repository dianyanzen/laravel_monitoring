<?php

namespace App\Http\Controllers;

use DB;
use Session;
use DateTime;
use PDF;
use Illuminate\Http\Request;
use App\Http\Controllers\SharedController as Shr;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class SearchController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function uktp(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 90;
		   $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kec != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$nik = rtrim($request->nik, ',');
			$r = $this->scr_cek_req_data_uktp($nik,$tgl_start,$tgl_end,$no_kec,$no_kel);
			// $j = $this->scr_cek_req_count_uktp($nik,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Request Cetak Ktp',
		 		"mtitle"=>'Request Cetak Ktp (Belum Cetak Without Filter)',
		 		"my_url"=>'uktp',
		 		"type_tgl"=>'Request',
		 		"data"=>$r,
		 		// "jumlah"=>$j,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'Request Cetak Ktp',
		 		"mtitle"=>'Request Cetak Ktp (Belum Cetak Without Filter)',
		 		"my_url"=>'uktp',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('Cekreq/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}

	public function dktp(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 91;
		   $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kec != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$nik = rtrim($request->nik, ',');
			$r = $this->scr_cek_req_data_dktp($nik,$tgl_start,$tgl_end,$no_kec,$no_kel);
			// $j = $this->scr_cek_req_count_dktp($nik,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Request Cetak Ktp',
		 		"mtitle"=>'Request Cetak Ktp (Sudah Cetak Without Filter)',
		 		"my_url"=>'dktp',
		 		"type_tgl"=>'Request',
		 		"data"=>$r,
		 		// "jumlah"=>$j,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
    		);
			}else{
			$data = array(
		 		"stitle"=>'Request Cetak Ktp',
		 		"mtitle"=>'Request Cetak Ktp (Sudah Cetak Without Filter)',
		 		"my_url"=>'dktp',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
    		);
    		}
			return view('Cekcetak/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function uktp_f(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 87;
		   $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kec != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$nik = rtrim($request->nik, ',');
			$r = $this->scr_cek_req_data_uktp_f($nik,$tgl_start,$tgl_end,$no_kec,$no_kel);
			// $j = $this->scr_cek_req_count_uktp_f($nik,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Request Cetak Ktp',
		 		"mtitle"=>'Request Cetak Ktp (Belum Cetak With Filter)',
		 		"my_url"=>'uktp_f',
		 		"type_tgl"=>'Request',
		 		"data"=>$r,
		 		// "jumlah"=>$j,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'Request Cetak Ktp',
		 		"mtitle"=>'Request Cetak Ktp (Belum Cetak With Filter)',
		 		"my_url"=>'uktp_f',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('Cekreq/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}

	public function dktp_f(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 88;
		   $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kec != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$nik = rtrim($request->nik, ',');
			$r = $this->scr_cek_req_data_dktp_f($nik,$tgl_start,$tgl_end,$no_kec,$no_kel);
			// $j = $this->scr_cek_req_count_dktp_f($nik,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Request Cetak Ktp',
		 		"mtitle"=>'Request Cetak Ktp (Sudah Cetak With Filter)',
		 		"my_url"=>'dktp_f',
		 		"type_tgl"=>'Request',
		 		"data"=>$r,
		 		// "jumlah"=>$j,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
    		);
			}else{
			$data = array(
		 		"stitle"=>'Request Cetak Ktp',
		 		"mtitle"=>'Request Cetak Ktp (Sudah Cetak With Filter)',
		 		"my_url"=>'dktp_f',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
    		);
    		}
			return view('Cekcetak/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}

	public function usuket(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 93;
		   $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kec != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$nik = rtrim($request->nik, ',');
			$r = $this->scr_cek_req_data_usuket($nik,$tgl_start,$tgl_end,$no_kec,$no_kel);
			// $j = $this->scr_cek_req_count_usuket($nik,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Request Cetak Suket',
		 		"mtitle"=>'Request Cetak Suket (Belum Cetak)',
		 		"my_url"=>'usuket',
		 		"type_tgl"=>'Request',
		 		"data"=>$r,
		 		// "jumlah"=>$j,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
    		);
			}else{
			$data = array(
		 		"stitle"=>'Request Cetak Suket',
		 		"mtitle"=>'Request Cetak Suket (Belum Cetak)',
		 		"my_url"=>'usuket',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
    		);
    		}
			return view('Cekreq/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}

	public function dsuket(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 94;
		   $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kec != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$nik = rtrim($request->nik, ',');
			$r = $this->scr_cek_req_data_dsuket($nik,$tgl_start,$tgl_end,$no_kec,$no_kel);
			// $j = $this->scr_cek_req_count_dsuket($nik,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Request Cetak Suket',
		 		"mtitle"=>'Request Cetak Suket (Sudah Cetak)',
		 		"my_url"=>'dsuket',
		 		"type_tgl"=>'Request',
		 		"data"=>$r,
		 		// "jumlah"=>$j,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
    		);
			}else{
			$data = array(
		 		"stitle"=>'Request Cetak Suket',
		 		"mtitle"=>'Request Cetak Suket (Sudah Cetak)',
		 		"my_url"=>'dsuket',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
    		);
    		}
			return view('Cekcetak/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}

	public function uttelhr(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 125;
		   $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$is_user = $request->is_user;
			$is_lahir = $request->is_lahir;
			$r = $this->scr_cek_req_data_uttelhr($is_user,$is_lahir,$tgl_start,$tgl_end);
			// $j = $this->scr_cek_req_count_usuket($is_user,$tgl_start,$tgl_end);
			$data = array(
		 		"stitle"=>'Request TTE Kelahiran',
		 		"mtitle"=>'Request TTE Kelahiran (Waiting Aproval)',
		 		"my_url"=>'Capil_Waiting',
		 		"type_tgl"=>'Request',
		 		"data"=>$r,
		 		// "jumlah"=>$j,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
    		);
			}else{
			$data = array(
		 		"stitle"=>'Request TTE Kelahiran',
		 		"mtitle"=>'Request TTE Kelahiran (Waiting Aproval)',
		 		"my_url"=>'Capil_Waiting',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
    		);
    		}
			return view('Cekreqttelhr/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}

	public function dttelhr(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 126;
		   $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$is_user = $request->is_user;
			$is_lahir = $request->is_lahir;
			$r = $this->scr_cek_req_data_dttelhr($is_user,$is_lahir,$tgl_start,$tgl_end);
			// $j = $this->scr_cek_req_count_dsuket($is_user,$tgl_start,$tgl_end);
			$data = array(
		 		"stitle"=>'Request TTE Kelahiran',
		 		"mtitle"=>'Request TTE Kelahiran (Aproved)',
		 		"my_url"=>'Capil_Done',
		 		"type_tgl"=>'Request',
		 		"data"=>$r,
		 		// "jumlah"=>$j,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
    		);
			}else{
			$data = array(
		 		"stitle"=>'Request TTE Kelahiran',
		 		"mtitle"=>'Request TTE Kelahiran (Aproved)',
		 		"my_url"=>'Capil_Done',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
    		);
    		}
			return view('Cekreqttelhr/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}

	public function uttekk(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 127;
		   $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$nik = rtrim($request->nik, ',');
			$r = $this->scr_cek_req_data_uttekk($tgl_start,$tgl_end,$no_kec,$no_kel);
			// $j = $this->scr_cek_req_count_usuket($is_user,$tgl_start,$tgl_end);
			$data = array(
		 		"stitle"=>'Request TTE Kartu Keluarga',
		 		"mtitle"=>'Request TTE Kartu Keluarga (Waiting Aproval)',
		 		"my_url"=>'Dafduk_Waiting',
		 		"type_tgl"=>'Request',
		 		"data"=>$r,
		 		// "jumlah"=>$j,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
    		);
			}else{
			$data = array(
		 		"stitle"=>'Request TTE Kartu Keluarga',
		 		"mtitle"=>'Request TTE Kartu Keluarga (Waiting Aproval)',
		 		"my_url"=>'Dafduk_Waiting',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
    		);
    		}
			return view('Cekreqttekk/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}

	public function dttekk(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 128;
		   $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$nik = rtrim($request->nik, ',');
			$r = $this->scr_cek_req_data_dttekk($tgl_start,$tgl_end,$no_kec,$no_kel);
			// $j = $this->scr_cek_req_count_dsuket($is_user,$tgl_start,$tgl_end);
			$data = array(
		 		"stitle"=>'Request TTE Kartu Keluarga',
		 		"mtitle"=>'Request TTE Kartu Keluarga (Aproved)',
		 		"my_url"=>'Dafduk_Done',
		 		"type_tgl"=>'Request',
		 		"data"=>$r,
		 		// "jumlah"=>$j,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
    		);
			}else{
			$data = array(
		 		"stitle"=>'Request TTE Kartu Keluarga',
		 		"mtitle"=>'Request TTE Kartu Keluarga (Aproved)',
		 		"my_url"=>'Dafduk_Done',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
    		);
    		}
			return view('Cekreqttekk/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function req_bio(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 85;
		   $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kec != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$nik = rtrim($request->nik, ',');
			$r = $this->scr_cek_req_data_bio($nik,$tgl_start,$tgl_end,$no_kec,$no_kel);
			// $j = $this->scr_cek_req_count_bio($nik,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Request Biometrik Kosong',
		 		"mtitle"=>'Request Biometrik Kosong',
		 		"my_url"=>'req_bio',
		 		"type_tgl"=>'Request',
		 		"data"=>$r,
		 		// "jumlah"=>$j,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
    		);
			}else{
			$data = array(
		 		"stitle"=>'Request Biometrik Kosong',
		 		"mtitle"=>'Request Biometrik Kosong',
		 		"my_url"=>'req_bio',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
    		);
    		}
			return view('Reqbio/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function delete_req(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 119;
		   $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->nik != null){
			$nik = rtrim($request->nik, ',');
			if (substr($nik, 0, 1) === ','){
				$nik = ltrim($nik, ',');
			}
			$r = $this->scr_del_req($nik,$request->session()->get('S_NO_KEC'));
			$data = array(
		 		"stitle"=>'Delete Pengajuan Bermasalah',
		 		"mtitle"=>'Delete Pengajuan Bermasalah',
		 		"my_url"=>'delete_req',
		 		"type_tgl"=>'Pengajuan',
		 		"data"=>$r,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
    		);
			}else{
			$data = array(
		 		"stitle"=>'Delete Pengajuan Bermasalah',
		 		"mtitle"=>'Delete Pengajuan Bermasalah',
		 		"my_url"=>'delete_req',
		 		"type_tgl"=>'Pengajuan',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
    		);
    		}
			return view('Deletereq/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function do_delete(Request $request)
	{
		   $menu_id = 119;
		   $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->niks != null){
			$nik = $request->niks;
			if (substr($nik, 0, 1) === ','){
				$nik = ltrim($nik, ',');
			}
				$this->scr_do_delete($nik);
				return redirect()->route('delete_req');
			}else{
				return redirect()->route('delete_req');
    		}    		
	}


	public function upkk(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 90;
		   $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kec != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$r = $this->scr_pengajuan_kk_wait($tgl_start,$tgl_end,$no_kec,$no_kel);
			// $j = $this->scr_cek_req_count_uktp($nik,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Request Pengajuan KK',
		 		"mtitle"=>'Request Pengajuan KK (Belum Pengajuan TTE)',
		 		"my_url"=>'Wait',
		 		"type_tgl"=>'Request',
		 		"data"=>$r,
		 		// "jumlah"=>$j,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'Request Pengajuan KK',
		 		"mtitle"=>'Request Pengajuan KK (Belum Pengajuan TTE)',
		 		"my_url"=>'Wait',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('cekcetakkk/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}

	public function dpkk(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 91;
		   $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kec != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$r = $this->scr_pengajuan_kk_done($tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Request Pengajuan KK',
		 		"mtitle"=>'Request Pengajuan KK (Sudah Pengajuan TTE)',
		 		"my_url"=>'Done',
		 		"type_tgl"=>'Request',
		 		"data"=>$r,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
    		);
			}else{
			$data = array(
		 		"stitle"=>'Request Pengajuan KK',
		 		"mtitle"=>'Request Pengajuan KK (Sudah Pengajuan TTE)',
		 		"my_url"=>'Done',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
    		);
    		}
			return view('cekcetakkk/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function req_ektp_perubahan(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 280;
		   $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Request Pencetakan Ektp (Perubahan Elemen)',
		 		"mtitle"=>'Request Pencetakan Ektp (Perubahan Elemen)',
		 		"my_url"=>'delete_req',
		 		"type_tgl"=>'Pengajuan',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
    		);
			return view('pengajuan_ektp_perubahan/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function req_ektp_rusak(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 279;
		   $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Request Pencetakan Ektp (Rusak)',
		 		"mtitle"=>'Request Pencetakan Ektp (Rusak)',
		 		"my_url"=>'delete_req',
		 		"type_tgl"=>'Pengajuan',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
    		);
			return view('pengajuan_ektp_rusak/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function req_ektp_hilang(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 278;
		   $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Request Pencetakan Ektp (Hilang)',
		 		"mtitle"=>'Request Pencetakan Ektp (Hilang)',
		 		"my_url"=>'delete_req',
		 		"type_tgl"=>'Pengajuan',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
    		);
			return view('pengajuan_ektp_hilang/main',$data);
		}else{
			return redirect()->route('logout');
			}
	}

	public function req_ektp_detail_hilang(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 281;
		   $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Detail Pencetakan Ektp (Hilang)',
		 		"mtitle"=>'Detail Pencetakan Ektp (Hilang)',
		 		"my_url"=>'InputKehilangan',
		 		"type_tgl"=>'Pengajuan',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
    		);
			return view('pengajuan_hilang_detail/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function list_pengajuan(Request $request)
     {
          $draw = intval($request->draw);
          $start = intval($request->start);
          $length = intval($request->length);
          if($request->nik != null){
			$nik = rtrim($request->nik, ',');
			if($request->nik != null){
				$nik = rtrim($request->nik, ',');
				if (substr($nik, 0, 1) === ','){
					$nik = ltrim($nik, ',');
				}
			$rekap_pengajuan = $this->scr_cek_list_pengajuan($nik,$request->session()->get('S_NO_KEC'));
			}
			
          $data = array();
          $i = 0;
          foreach($rekap_pengajuan as $r) {
          		$i ++;
               $data[] = array(
               		$i,
                    $r->no_kec,
                    $r->nik,
                    $r->no_kk,
                    $r->nama_lengkap,
                    $r->current_status_code,
                    $r->tmpt_lhr.',<br>'.$r->tgl_lhr,
                    $r->alamat.'<br><b>RT.</b> '.$r->rt.' <b>RW.</b> '.$r->rw.'<br><b>KDOE POS:</b>'.$r->kode_pos,
                    $r->req_date,
                    $r->req_by,
                    $r->printed_date,
                    $r->printed_by,
                    $r->alasan_pengajuan,
                    $r->status_pengajuan,
                    $r->keterangan_status,
                    $r->nama_kec,
                    $r->nama_kel,
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => count($rekap_pengajuan),
                 "recordsFiltered" => count($rekap_pengajuan),
                 "data" => $data
            );
          return $output;
          exit();
		}else{
			$rekap_pengajuan= [];
			 $data = array();
		      $i = 0;
          $output = array(
               "draw" => $draw,
                 "recordsTotal" => count($rekap_pengajuan),
                 "recordsFiltered" => count($rekap_pengajuan),
                 "data" => $data
            );
          return $output;
          exit();
		}
	}
	public function list_pengajuan_ditolak(Request $request)
     {
          $draw = intval($request->draw);
          $start = intval($request->start);
          $length = intval($request->length);
          if($request->nik != null){
			$nik = rtrim($request->nik, ',');
			if($request->nik != null){
				$nik = rtrim($request->nik, ',');
				if (substr($nik, 0, 1) === ','){
					$nik = ltrim($nik, ',');
				}
			$rekap_pengajuan = $this->scr_tolak_list_pengajuan($nik,$request->session()->get('S_NO_KEC'));
			}
			
          $data = array();
          $i = 0;
          foreach($rekap_pengajuan as $r) {
          		$i ++;
               $data[] = array(
               		$i,
                    $r->no_kec,
                    $r->nik,
                    $r->no_kk,
                    $r->nama_lengkap,
                    ($r->current_status_code == 'DATA REKAM KOSONG') ? '<i class="mdi mdi-biohazard text-white" style="float: none; color: red !important;"></i><span style="float: none; color: red !important;"> '.$r->current_status_code : '<i class="mdi mdi-fingerprint text-white" style="float: none; color: green !important;"></i><span style="float: none; color: green !important;"> '.$r->current_status_code,
                    $r->tmpt_lhr.',<br>'.$r->tgl_lhr,
                    $r->alamat.'<br><b>RT.</b> '.$r->rt.' <b>RW.</b> '.$r->rw.'<br><b>KDOE POS:</b>'.$r->kode_pos,
                    $r->req_date,
                    $r->req_by,
                    $r->printed_date,
                    $r->printed_by,
                    $r->alasan_pengajuan,
                    $r->status_pengajuan,
                    $r->keterangan_status,
                    $r->nama_kec,
                    $r->nama_kel,
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => count($rekap_pengajuan),
                 "recordsFiltered" => count($rekap_pengajuan),
                 "data" => $data
            );
          return $output;
          exit();
		}else{
			$rekap_pengajuan= [];
			 $data = array();
		      $i = 0;
          $output = array(
               "draw" => $draw,
                 "recordsTotal" => count($rekap_pengajuan),
                 "recordsFiltered" => count($rekap_pengajuan),
                 "data" => $data
            );
          return $output;
          exit();
		}
	}
	public function list_hilang(Request $request)
     {
          $draw = intval($request->draw);
          $start = intval($request->start);
          $length = intval($request->length);
          if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$rekap_pengajuan = $this->scr_cek_list_hilang($tgl_start,$tgl_end,$request->session()->get('S_NO_KEC'));
			
			
          $data = array();
          $i = 0;
          foreach($rekap_pengajuan as $r) {
          		$i ++;
               $data[] = array(
               		$i,
                    $r->nik,
                    $r->no_kk,
                    $r->nama_lengkap,
                    $r->req_date,
                    $r->req_by,
                    $r->alasan_pengajuan,
                    $r->no_kehilangan,
                    $r->nama_kec,
                    $r->nama_kel,
                     '<button type="button" class="btn btn-info btn-xs" id="btn-lihat" onclick="do_kehilangan('.$r->do_nik.');" style="margin-top: 5px;">Update <i class="mdi mdi-tooltip-edit fa-fw"></i></button>'
                 );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => count($rekap_pengajuan),
                 "recordsFiltered" => count($rekap_pengajuan),
                 "data" => $data
            );
          return $output;
          exit();
		}else{
			$rekap_pengajuan= [];
			 $data = array();
		      $i = 0;
          $output = array(
               "draw" => $draw,
                 "recordsTotal" => count($rekap_pengajuan),
                 "recordsFiltered" => count($rekap_pengajuan),
                 "data" => $data
            );
          return $output;
          exit();
		}
	}
	public function do_ajukan(Request $request)
	{
		   header("Content-Type: application/json", true);
			if($request->niks != null){
			$nik = $request->niks;
			$pengajuan = $request->reqstatus;
			if (substr($nik, 0, 1) === ','){
				$nik = ltrim($nik, ',');
			}
			if ($pengajuan == 3){
				$this->scr_do_ajukan_hilang($nik,$request->session()->get('S_USER_ID'));
				$data["success"] = TRUE;
				$data["message"] = "Data Berhasil Di Ajukan";
	       		return $data;
			}else if ($pengajuan == 2){
				$this->scr_do_ajukan_rusak($nik,$request->session()->get('S_USER_ID'));
				$data["success"] = TRUE;
				$data["message"] = "Data Berhasil Di Ajukan";
	       		return $data;
			}else{
				$this->scr_do_ajukan_perubahan_data($nik,$request->session()->get('S_USER_ID'));
				$data["success"] = TRUE;
				$data["message"] = "Data Berhasil Di Ajukan";
	       		return $data;
			}
				
			}else{
				$data["success"] = FALSE;
				$data["message"] = "Data Gagal Di Ajukan";
	       		return $data;
    		}    		
	}
	public function edit_kehilangan(Request $request)
    {
        if ($request->nik != null)
        {
            $nik = $request->nik;
            $no_kehilangan = $request->no_kehilangan;
            $pengajuan = $this->src_get_cek_pengajuan_hilang($nik);
            if (count($pengajuan) > 0)
            {
                $nik_hilang = $pengajuan[0]->nik;
                $this->src_update_pengajuan_hilang($nik_hilang, $no_kehilangan);
                $data["success"] = true;
                $data["is_save"] = 0;
                $data["message"] = "Nomor Kehilangan Dari Nik : " . $nik . " Berhasil Di Ubah";
                return $data;
            }
            else
            {
                $data["success"] = true;
                $data["is_save"] = 1;
                $data["message"] = "Mohon Maaf Nik : " . $nik . " Tidak Memiliki Pengajuan Ektp Hilang, Silahkan Cek SIAK";
                return $data;
            }
        }
        else
        {
            redirect('/', 'refresh');
        }
    }

	public function scr_cek_data_rekam($nik = '',$no_kk = '',$nama_lgkp = '',$tgl_lhr = '',$no_kec = 0,$no_kel = 0){
			$sql = "";
			$sql .= " SELECT 
            B.NIK
            , B.NO_KK
            , B.NAMA_LGKP
            , B.TMPT_LHR
            , TO_CHAR(B.TGL_LHR,'DD-MM-YYYY') AS TGL_LHR
            , CASE WHEN B.JENIS_KLMIN = 1 THEN 'LAKI-LAKI' ELSE 'PEREMPUAN' END AS JENIS_KLMIN
            , D.ALAMAT
            , LPAD(TO_CHAR(D.NO_RT), 3, '0') AS RT
            , LPAD(TO_CHAR(D.NO_RW), 3, '0') AS RW
            , CASE WHEN D.KODE_POS IS NULL THEN '-' ELSE TO_CHAR(D.KODE_POS) END KODE_POS
            ,(SELECT COUNT(1) AS JML FROM CARD_MANAGEMENT@DB2 WHERE NIK = B.NIK ) AS JUMLAH_CETAK
            , F5_GET_NAMA_KECAMATAN(B.NO_PROP,B.NO_KAB,B.NO_KEC) NAMA_KEC
            , F5_GET_NAMA_KELURAHAN(B.NO_PROP,B.NO_KAB,B.NO_KEC,B.NO_KEL) NAMA_KEL
            , UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(B.AGAMA,7), 501)) AGAMA
            , UPPER(F5_GET_REF_WNI(B.JENIS_PKRJN, 201)) JENIS_PKRJN
            , UPPER(F5_GET_REF_WNI(B.STAT_KWN, 601)) STAT_KWN
            , CASE WHEN A.CURRENT_STATUS_CODE IS NULL THEN '-' ELSE A.CURRENT_STATUS_CODE END AS CURRENT_STATUS_CODE
            , CASE WHEN F.PRINTED_BY IS NULL THEN '-' ELSE F.PRINTED_BY END AS SUKET_BY 
            , CASE WHEN TO_CHAR(F.PRINTED_DATE,'DD-MM-YYYY') IS NULL THEN '-' ELSE TO_CHAR(F.PRINTED_DATE,'DD-MM-YYYY') END AS SUKET_DT
            , CASE WHEN  TO_CHAR(G.LAST_UPDATE,'DD-MM-YYYY') IS NULL AND TO_CHAR(G.PERSONALIZED_DATE,'DD-MM-YYYY') IS NULL THEN '-' WHEN TO_CHAR(G.LAST_UPDATE,'DD-MM-YYYY') IS NULL AND TO_CHAR(G.PERSONALIZED_DATE,'DD-MM-YYYY') IS NOT NULL THEN TO_CHAR(G.PERSONALIZED_DATE,'DD-MM-YYYY') ELSE  TO_CHAR(G.LAST_UPDATE,'DD-MM-YYYY') END AS KTP_DT
            , CASE WHEN TO_CHAR(G.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(G.CREATED_USERNAME) IS NULL  THEN '-' WHEN TO_CHAR(G.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(G.CREATED_USERNAME) IS NOT NULL THEN TO_CHAR(G.CREATED_USERNAME) ELSE TO_CHAR(G.LAST_UPDATED_USERNAME) END AS KTP_BY
            , E.PATH
            , CASE WHEN TO_CHAR(H.REQ_DATE,'DD-MM-YYYY') IS NULL THEN '-' ELSE TO_CHAR(H.REQ_DATE,'DD-MM-YYYY') END REQ_DATE
            , CASE WHEN H.REQ_BY IS NULL THEN '-' ELSE H.REQ_BY END REQ_BY,CASE WHEN H.PROC_BY IS NULL THEN '-' ELSE H.PROC_BY END PROC_BY
            , CASE WHEN H.STEP_PROC IS NULL THEN '-' WHEN H.STEP_PROC = 'T' THEN 'Pengajuan Ditolak' WHEN H.STEP_PROC = 'A' THEN 'Masih Dalam Proses' WHEN H.STEP_PROC = 'C' THEN 'Sudah Tercetak' ELSE  H.STEP_PROC END STEP_PROC 
            FROM BIODATA_WNI B 
            LEFT JOIN DATA_KELUARGA D ON B.NO_KK = D.NO_KK 
            LEFT JOIN DEMOGRAPHICS_ALL@DB2 A  ON A.NIK = B.NIK  
            LEFT JOIN T5_FOTO E ON A.NIK = E.NIK 
            LEFT JOIN (SELECT NIK, PRINTED_DATE,PRINTED_BY,NO_DOKUMEN  FROM (SELECT NIK,PRINTED_DATE,PRINTED_BY,NO_DOKUMEN, RANK() OVER (PARTITION BY NIK ORDER BY PRINTED_DATE DESC,NO_DOKUMEN DESC) RNK FROM T7_HIST_SUKET)  WHERE RNK = 1) F ON F.NIK = B.NIK 
            LEFT JOIN (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME,LAST_UPDATE,LAST_UPDATE,CREATED FROM (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME,LAST_UPDATE,LAST_UPDATE,CREATED, RANK() OVER (PARTITION BY NIK ORDER BY  PERSONALIZED_DATE DESC, LAST_UPDATE DESC, CHIP_ID DESC) RNK FROM CARD_MANAGEMENT@DB2) WHERE RNK = 1) G ON G.NIK =B.NIK LEFT JOIN (SELECT NIK,REQ_DATE,REQ_BY,PROC_DATE,PROC_BY,STEP_PROC FROM (SELECT NIK,REQ_DATE,REQ_BY,PROC_DATE,PROC_BY,STEP_PROC, RANK() OVER (PARTITION BY NIK ORDER BY REQ_DATE,ID_CETAK DESC) RNK FROM SIAK_REQ_CETAK_KTP@YZDB) WHERE RNK = 1) H ON H.NIK =B.NIK WHERE 1=1 
				AND (B.FLAG_STATUS = '0' OR (B.FLAG_STATUS = '2' AND B.FLAG_PINDAH IN(1,2,3))) ";
			if($no_kec != 0 ){ 
				$sql .= " AND B.NO_KEC = $no_kec ";
			} 
			if($no_kel != 0 ){ 
				$sql .= " AND B.NO_KEL = $no_kel ";
			} 
			if($nik != '' || $nik != null || !empty($nik)){ 
				if(strlen($nik) <= 17){
				$sql .= " AND B.NIK = $nik ";	
				}else{
				$sql .= " AND B.NIK IN ($nik) ";	
				}
				
			} 
			if($no_kk != '' || $no_kk != null || !empty($no_kk)){ 
				if(strlen($no_kk) <= 17){
				$sql .= " AND B.NO_KK = $no_kk ";
				}else{
				$sql .= " AND B.NO_KK IN ($no_kk) ";
				}
			}
			if($nama_lgkp != '' || $nama_lgkp != null || !empty($nama_lgkp)){ 
				$sql .= " AND B.NAMA_LGKP LIKE UPPER('$nama_lgkp') ";
			}
			if($tgl_lhr != '' || $tgl_lhr != null || !empty($tgl_lhr)){ 
				$sql .= " AND TO_CHAR(B.TGL_LHR,'DD-MM-YYYY') = '$tgl_lhr' ";
			} 
			$sql .= " ORDER BY B.NO_KK, B.STAT_HBKEL ";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}
		public function scr_cek_kk($no_kk = '',$no_kec = 0){
			$sql = "";
			$sql .= " SELECT 
						  A.NO_KK
						  , A.NAMA_KEP
						  , A.ALAMAT
						  , A.NO_PROP
						  , A.NO_KAB
						  , A.NO_KEC
						  , A.NO_KEL
						  , F5_GET_BSRE_KK_JML(A.NO_KK) JML_PENGAJUAN
						  , F5_GET_NAMA_KECAMATAN(A.NO_PROP,A.NO_KAB,A.NO_KEC) NAMA_KEC
            			  , F5_GET_NAMA_KELURAHAN(A.NO_PROP,A.NO_KAB,A.NO_KEC,A.NO_KEL) NAMA_KEL
						  , LPAD(TO_CHAR(A.NO_RT), 3, '0') AS NO_RT
            			  , LPAD(TO_CHAR(A.NO_RW), 3, '0') AS NO_RW
						  , CASE WHEN A.KODE_POS IS NULL THEN '-' ELSE TO_CHAR(A.KODE_POS) END KODE_POS
						  , A.COUNT_KK
						  , CASE WHEN B.CERT_STATUS IS NULL THEN '-'  ELSE TO_CHAR(B.CERT_STATUS) END CERT_STATUS
						  , CASE WHEN B.REQ_DATE IS NULL THEN '-'  ELSE TO_CHAR(B.REQ_DATE,'DD-MM-YYYY') END REQ_DATE
						  , CASE WHEN B.REQ_BY IS NULL THEN '-'  ELSE TO_CHAR(B.REQ_BY) END REQ_BY
						  , CASE WHEN B.URL_DOKUMEN IS NULL THEN '-'  ELSE TO_CHAR(B.URL_DOKUMEN) END URL_DOKUMEN
						  , CASE WHEN B.URL_DOKUMEN IS NULL THEN '-'  ELSE SUBSTR(B.URL_DOKUMEN,20) END URL_KK
						  , CASE WHEN B.URL_DOKUMEN IS NULL THEN '-'  ELSE SUBSTR(B.URL_DOKUMEN,11,8) END URL_DATE
						  , CASE WHEN B.URL_DOKUMEN IS NULL THEN '-'  ELSE CONCAT(CONCAT(CONCAT(CONCAT('https://10.32.73.222/Siak/cetak/main/view_pdf/',SUBSTR(B.URL_DOKUMEN,20)),'/'),SUBSTR(B.URL_DOKUMEN,11,8)),'/cetak_kartu_keluarga.pdf') END URL_DOWNLOAD 
						  , CASE WHEN B.PEJABAT_PROCESS_DATE IS NULL THEN '-'  ELSE TO_CHAR(B.PEJABAT_PROCESS_DATE,'DD-MM-YYYY') END APROVE_DATE
						  , CASE WHEN C.PRINTED_DATE IS NULL THEN '-'  ELSE TO_CHAR(C.PRINTED_DATE,'DD-MM-YYYY') END PRINTED_DATE
						  , CASE WHEN C.PRINTED_BY IS NULL THEN '-'  ELSE TO_CHAR(C.PRINTED_BY) END PRINTED_BY
						FROM DATA_KELUARGA A 
						LEFT JOIN 
						  (
						    SELECT 
						    NO_DOC
						    , CERT_STATUS
						    , REQ_DATE
						    , REQ_BY
						    , SEQN_ID
						    , URL_DOKUMEN
						    , PEJABAT_PROCCESS_BY
						    , PEJABAT_PROCESS_DATE  
						  FROM 
						  (
						    SELECT 
						    NO_DOC
						    , CERT_STATUS
						    , REQ_DATE
						    , REQ_BY
						    , SEQN_ID
						    , URL_DOKUMEN
						    , PEJABAT_PROCCESS_BY
						    , PEJABAT_PROCESS_DATE 
						    , RANK() OVER (PARTITION BY NO_DOC ORDER BY REQ_DATE DESC,SEQN_ID DESC) RNK FROM BSRE_KARTU_KELUARGA) 
						    WHERE RNK = 1) B ON A.NO_KK = B.NO_DOC
						LEFT JOIN 
						   (
						     SELECT 
						     NO_KK
						     , PRINTED_DATE
						     , PRINTED_BY
						     , SEQN_ID  
						FROM 
						  (
						    SELECT 
						    NO_KK
						    , PRINTED_DATE
						    , PRINTED_BY
						    , SEQN_ID
						    , RANK() OVER (PARTITION BY NO_KK ORDER BY PRINTED_DATE DESC,SEQN_ID DESC) RNK FROM T5_SEQN_PRINT_KK) 
						    WHERE RNK = 1) C ON A.NO_KK = C.NO_KK 
						    WHERE 1=1 
   						 ";
					if($no_kec != 0 ){ 
						$sql .= " AND A.NO_KEC = $no_kec ";
					} 
					if($no_kk != '' || $no_kk != null || !empty($no_kk)){ 
						if(strlen($no_kk) <= 17){
						$sql .= " AND A.NO_KK = $no_kk ";
						}else{
						$sql .= " AND A.NO_KK IN ($no_kk) ";
						}
					}
					$sql .= " ORDER BY A.NO_KEC, A.NO_KEL, A.NO_RW, A.NO_RT,A.NO_KK ";
					$r = DB::connection('db222')->select($sql);
					return $r;
		}
		public function scr_cek_count_rekam($nik = '',$no_kk = '',$nama_lgkp = '',$no_kec= 0){
			$sql = "";
			$sql .= "SELECT COUNT(1) AS JML FROM BIODATA_WNI B WHERE 1=1 			
					AND (B.FLAG_STATUS = '0' OR (B.FLAG_STATUS = '2' AND B.FLAG_PINDAH IN(1,2,3))) ";
			if($no_kec != 0 && ($nama_lgkp != '' || $nama_lgkp != null || !empty($nama_lgkp))){ 
				$sql .= " AND B.NO_KEC = $no_kec ";
			} 
			if($nik != '' || $nik != null || !empty($nik)){ 
				$sql .= " AND B.NIK IN ($nik) ";
			} 
			if($no_kk != '' || $no_kk != null || !empty($no_kk)){ 
				$sql .= " AND B.NO_KK IN ($no_kk) ";
			}
			if($nama_lgkp != '' || $nama_lgkp != null || !empty($nama_lgkp)){ 
				$sql .= " AND B.NAMA_LGKP LIKE '$nama_lgkp' ";
			} 
			$sql .= " ORDER BY B.NO_KK, B.STAT_HBKEL ";
			$r = DB::connection('db222')->select($sql);
			return $r[0]->jml;
		}
		public function scr_export_data_rekam($nik = ''){
			$sql = "";
			$sql .= "SELECT B.NIK,B.NO_KK, B.NAMA_LGKP, B.TMPT_LHR,TO_CHAR(B.TGL_LHR,'DD-MM-YYYY') AS TGL_LHR,CASE WHEN B.JENIS_KLMIN = 1 THEN 'LAKI-LAKI' ELSE 'PEREMPUAN' END AS JENIS_KLMIN, D.ALAMAT,  LPAD(TO_CHAR(D.NO_RT), 3, '0') AS RT,LPAD(TO_CHAR(D.NO_RW), 3, '0') AS RW,(SELECT COUNT(1) AS JML FROM CARD_MANAGEMENT@DB2 WHERE NIK = B.NIK ) AS JUMLAH_CETAK, (SELECT C.NAMA_KEC FROM SETUP_KEC C WHERE C.NO_PROP = 32 AND C.NO_KAB = 73 AND B.NO_KEC = C.NO_KEC) AS NAMA_KEC, (SELECT C.NAMA_KEL FROM SETUP_KEL C WHERE C.NO_PROP = 32 AND C.NO_KAB = 73 AND B.NO_KEC = C.NO_KEC AND B.NO_KEL = C.NO_KEL) AS NAMA_KEL, UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(B.AGAMA,7), 501)) AGAMA, UPPER(F5_GET_REF_WNI(B.JENIS_PKRJN, 201)) JENIS_PKRJN, UPPER(F5_GET_REF_WNI(B.STAT_KWN, 601)) STAT_KWN, CASE WHEN A.CURRENT_STATUS_CODE IS NULL THEN '-' ELSE A.CURRENT_STATUS_CODE END AS CURRENT_STATUS_CODE, CASE WHEN F.PRINTED_BY IS NULL THEN '-' ELSE F.PRINTED_BY END AS SUKET_BY ,CASE WHEN TO_CHAR(F.PRINTED_DATE,'DD-MM-YYYY') IS NULL THEN '-' ELSE TO_CHAR(F.PRINTED_DATE,'DD-MM-YYYY') END AS SUKET_DT,CASE WHEN  TO_CHAR(G.LAST_UPDATE,'DD-MM-YYYY') IS NULL AND TO_CHAR(G.PERSONALIZED_DATE,'DD-MM-YYYY') IS NULL THEN '-' WHEN TO_CHAR(G.LAST_UPDATE,'DD-MM-YYYY') IS NULL AND TO_CHAR(G.PERSONALIZED_DATE,'DD-MM-YYYY') IS NOT NULL THEN TO_CHAR(G.PERSONALIZED_DATE,'DD-MM-YYYY') ELSE  TO_CHAR(G.LAST_UPDATE,'DD-MM-YYYY') END AS KTP_DT, CASE WHEN TO_CHAR(G.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(G.CREATED_USERNAME) IS NULL  THEN '-' WHEN TO_CHAR(G.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(G.CREATED_USERNAME) IS NOT NULL THEN TO_CHAR(G.CREATED_USERNAME) ELSE TO_CHAR(G.LAST_UPDATED_USERNAME) END AS KTP_BY, E.PATH, CASE WHEN TO_CHAR(H.REQ_DATE,'DD-MM-YYYY') IS NULL THEN '-' ELSE TO_CHAR(H.REQ_DATE,'DD-MM-YYYY') END REQ_DATE, CASE WHEN H.REQ_BY IS NULL THEN '-' ELSE H.REQ_BY END REQ_BY,CASE WHEN H.PROC_BY IS NULL THEN '-' ELSE H.PROC_BY END PROC_BY, CASE WHEN H.STEP_PROC IS NULL THEN '-' WHEN H.STEP_PROC = 'T' THEN 'Pengajuan Ditolak' WHEN H.STEP_PROC = 'A' THEN 'Masih Dalam Proses' WHEN H.STEP_PROC = 'C' THEN 'Sudah Tercetak' ELSE  H.STEP_PROC END STEP_PROC FROM BIODATA_WNI B LEFT JOIN DATA_KELUARGA D ON B.NO_KK = D.NO_KK LEFT JOIN DEMOGRAPHICS_ALL@DB2 A  ON A.NIK = B.NIK  LEFT JOIN T5_FOTO E ON A.NIK = E.NIK LEFT JOIN (SELECT NIK, PRINTED_DATE,PRINTED_BY,NO_DOKUMEN  FROM (SELECT NIK,PRINTED_DATE,PRINTED_BY,NO_DOKUMEN, RANK() OVER (PARTITION BY NIK ORDER BY PRINTED_DATE DESC,NO_DOKUMEN DESC) RNK FROM T7_HIST_SUKET)  WHERE RNK = 1) F ON F.NIK = B.NIK LEFT JOIN (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME,LAST_UPDATE,LAST_UPDATE,CREATED FROM (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME,LAST_UPDATE,LAST_UPDATE,CREATED, RANK() OVER (PARTITION BY NIK ORDER BY  PERSONALIZED_DATE DESC, LAST_UPDATE DESC, CHIP_ID DESC) RNK FROM CARD_MANAGEMENT@DB2) WHERE RNK = 1) G ON G.NIK =B.NIK LEFT JOIN (SELECT NIK,REQ_DATE,REQ_BY,PROC_DATE,PROC_BY,STEP_PROC FROM (SELECT NIK,REQ_DATE,REQ_BY,PROC_DATE,PROC_BY,STEP_PROC, RANK() OVER (PARTITION BY NIK ORDER BY REQ_DATE,ID_CETAK DESC) RNK FROM SIAK_REQ_CETAK_KTP@YZDB) WHERE RNK = 1) H ON H.NIK =B.NIK WHERE 1=1 
				AND (B.FLAG_STATUS = '0' OR (B.FLAG_STATUS = '2' AND B.FLAG_PINDAH IN(1,2,3))) ";
			if($nik != '' || $nik != null || !empty($nik)){ 
				$sql .= " AND B.NIK IN ($nik) ";
			} 
			$sql .= " ORDER BY B.NO_KEC, B.NO_KEL, B.NO_KK, D.NO_RW, D.NO_RT, B.STAT_HBKEL ";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}
		public function scr_cek_data_history($nik = '',$no_kec= 0){
			$sql = "";
			$sql .= "SELECT A.NIK, A.NO_KK, A.NAMA_LGKP, B.CHIP_ID, ,CASE WHEN TO_CHAR(B.LAST_UPDATE,'DD-MM-YYYY') IS NULL THEN TO_CHAR(B.PERSONALIZED_DATE,'DD-MM-YYYY') ELSE TO_CHAR(B.LAST_UPDATE,'DD-MM-YYYY') END AS PRINTED_DATE AS KTP_DT, B.CREATED_USERNAME AS KTP_BY
			, CASE WHEN TO_CHAR(B.LAST_UPDATE,'DD-MM-YYYY') IS NULL THEN '-' ELSE TO_CHAR(B.LAST_UPDATE,'DD-MM-YYYY') END AS UPDATED_DT
			, CASE WHEN B.LAST_UPDATED_USERNAME IS NULL THEN NULL ELSE B.LAST_UPDATED_USERNAME END AS UPDATED_BY
			,F5_GET_NAMA_KECAMATAN(32,73,A.NO_KEC) NAMA_KEC, F5_GET_NAMA_KELURAHAN(32,73,A.NO_KEC,A.NO_KEL) NAMA_KEL FROM BIODATA_WNI A INNER JOIN CARD_MANAGEMENT@DB2 B ON A.NIK = B.NIK WHERE 1=1 AND (A.TGL_LHR<=ADD_MONTHS(SYSDATE,-12*17) OR A.STAT_KWN<>1) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3))) ";
			if($no_kec != 0 ){ 
				$sql .= " AND A.NO_KEC = $no_kec ";
			} 
			$sql .= " AND A.NIK IN ($nik) ORDER BY B.PERSONALIZED_DATE DESC";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}
		public function scr_cek_count_history($nik = ''){
			$sql = "";
			$sql .= "SELECT COUNT(1) AS JML FROM BIODATA_WNI A INNER JOIN CARD_MANAGEMENT@DB2 B ON A.NIK = B.NIK WHERE 1=1 AND (A.TGL_LHR<=ADD_MONTHS(SYSDATE,-12*17) OR A.STAT_KWN<>1) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3))) AND A.NIK IN ($nik)";
			$r = DB::connection('db222')->select($sql);
			return $r[0]->jml;
		}
		public function scr_push_demogh221($NIK){
			$sql = "UPDATE DEMOGRAPHICS A SET A.CURRENT_STATUS_CODE = 'BIO_CAPTURED' WHERE A.CURRENT_STATUS_CODE IN (SELECT CODE FROM STATUS_ID WHERE ID_STATUS IN (3,4,5,6,7,8,9,10,11,12)) AND A.NIK IN ($NIK) AND EXISTS (SELECT 1 FROM BIODATA_WNI@DB222 B WHERE A.NIK = B.NIK)";
			$r = DB::connection('db221')->update($sql);
			return $r;
		}
		public function scr_push_demogh2($NIK){
			$sql = "UPDATE DEMOGRAPHICS A SET A.CURRENT_STATUS_CODE = 'BIO_CAPTURED' WHERE A.CURRENT_STATUS_CODE IN (SELECT CODE FROM STATUS_ID WHERE ID_STATUS IN (3,4,5,6,7,8,9,10,11,12)) AND A.NIK IN ($NIK) AND EXISTS (SELECT 1 FROM BIODATA_WNI@DB222 B WHERE A.NIK = B.NIK)";
			$r = DB::connection('db2')->update($sql);
			return $r;
		}
		public function scr_push_demoghall($NIK){
			$sql = "UPDATE DEMOGRAPHICS_ALL A SET A.CURRENT_STATUS_CODE = 'BIO_CAPTURED' WHERE A.CURRENT_STATUS_CODE IN (SELECT CODE FROM STATUS_ID WHERE ID_STATUS IN (3,4,5,6,7,8,9,10,11,12)) AND A.NIK IN ($NIK) AND EXISTS (SELECT 1 FROM BIODATA_WNI@DB222 B WHERE A.NIK = B.NIK)";
			$r = DB::connection('db2')->update($sql);
			return $r;
		}
		public function scr_push_data_kel($no_kk){
			$sql = "UPDATE DATA_KELUARGA A SET  A.CERT_STATUS = 1 WHERE EXISTS (SELECT 1 FROM BSRE_KARTU_KELUARGA B WHERE A.NO_KK = B.NO_DOC AND B.CERT_STATUS IN (2,3) AND A.NO_KK = $no_kk)";
			$r = DB::connection('db222')->update($sql);
			return $r;
		}
		public function scr_push_data_bsre($no_kk){
			$sql = "UPDATE BSRE_KARTU_KELUARGA SET 
					REQ_DATE = SYSDATE
					, PEJABAT_PROCESS_DATE = NULL
					, URL_DOKUMEN = null  
					, COUNT_PRINT = 0 
					, SENT_STATUS = 0
					, SENT_DATE = null
					, ACTIVE_STATUS = 0
					, ACTIVE_DATE = null
					, PEJABAT_PROCCESS_BY = null
					, CERT_STATUS = 1
					WHERE CERT_STATUS IN (2,3) AND NO_DOC = $no_kk";
			$r = DB::connection('db222')->update($sql);
			return $r;
		}
		
		public function scr_cek_data_duplicate($NIK){
			$sql = "SELECT  A.NIK
					, A.NO_PROP
					, A.NAMA_PROP
					, A.NO_KAB
					, A.NAMA_KAB
					, A.NO_KEC
					, A.NAMA_KEC
					, A.NO_KEL
					, A.NAMA_KEL
					, A.NAMA_LGKP
					, A.ALAMAT
					, A.CURRENT_STATUS_CODE
					, A.TGL_REKAM
					, A.NIK_SINGLE
					, A.NO_PROP_SINGLE
					, A.NAMA_PROP_SINGLE
					, A.NO_KAB_SINGLE
					, A.NAMA_KAB_SINGLE
					, A.NO_KEC_SINGLE
					, A.NAMA_KEC_SINGLE
					, A.NO_KEL_SINGLE
					, A.NAMA_KEL_SINGLE
					, A.NAMA_LGKP_SINGLE
					, A.ALAMAT_SINGLE
					, A.CURRENT_STATUS_CODE_SINGLE
					, A.TGL_REKAM_SINGLE FROM (SELECT  A.NIK
					, A.NO_PROP
					, F5_GET_NAMA_PROVINSI(A.NO_PROP) NAMA_PROP
					, A.NO_KAB
					, F5_GET_NAMA_KABUPATEN(A.NO_PROP,A.NO_KAB) NAMA_KAB
					, A.NO_KEC
					, F5_GET_NAMA_KECAMATAN(A.NO_PROP,A.NO_KAB,A.NO_KEC) NAMA_KEC
					, '0' NO_KEL
					, '-' NAMA_KEL
					, A.NAMA_LGKP
					, '-' ALAMAT
					, A.CURRENT_STATUS_CODE
					, TO_CHAR(A.TGL_REKAM,'DD-MM-YYYY') TGL_REKAM
					, TO_CHAR(A.NIK_SINGLE) NIK_SINGLE
					, TO_CHAR(A.NO_PROP_SINGLE) NO_PROP_SINGLE
					, F5_GET_NAMA_PROVINSI(A.NO_PROP_SINGLE) NAMA_PROP_SINGLE
					, TO_CHAR(A.NO_KAB_SINGLE) NO_KAB_SINGLE
					, F5_GET_NAMA_KABUPATEN(A.NO_PROP_SINGLE,A.NO_KAB_SINGLE) NAMA_KAB_SINGLE
					, TO_CHAR(A.NO_KEC_SINGLE) NO_KEC_SINGLE
					, F5_GET_NAMA_KECAMATAN(A.NO_PROP_SINGLE,A.NO_KAB_SINGLE,A.NO_KEC_SINGLE) NAMA_KEC_SINGLE
					, '0' NO_KEL_SINGLE
					, '-' NAMA_KEL_SINGLE
					, A.NAMA_LGKP_SINGLE
					, '-' ALAMAT_SINGLE
					, A.CURRENT_STATUS_CODE_SINGLE
					, TO_CHAR(A.TGL_REKAM_SINGLE,'DD-MM-YYYY') TGL_REKAM_SINGLE
          			FROM SIAK_DUPLICATE_FULL A
                UNION ALL
                SELECT  B.NIK
					, B.NO_PROP
					, B.NAMA_PROP
					, B.NO_KAB
					, B.NAMA_KAB
					, B.NO_KEC
					, B.NAMA_KEC
					, B.NO_KEL
					, B.NAMA_KEL
					, B.NAMA_LGKP
					, B.ALAMAT
					, B.CURRENT_STATUS_CODE
					, B.TGL_REKAM
					, B.NIK_SINGLE
					, B.NO_PROP_SINGLE
					, B.NAMA_PROP_SINGLE
					, B.NO_KAB_SINGLE
					, B.NAMA_KAB_SINGLE
					, B.NO_KEC_SINGLE
					, B.NAMA_KEC_SINGLE
					, B.NO_KEL_SINGLE
					, B.NAMA_KEL_SINGLE
					, B.NAMA_LGKP_SINGLE
					, B.ALAMAT_SINGLE
					, B.CURRENT_STATUS_CODE_SINGLE
					, B.TGL_REKAM_SINGLE FROM (SELECT  B.NIK
					, B.NO_PROP
					, F5_GET_NAMA_PROVINSI(B.NO_PROP) NAMA_PROP
					, B.NO_KAB
					,F5_GET_NAMA_KABUPATEN(B.NO_PROP,B.NO_KAB) NAMA_KAB
					, B.NO_KEC
					, F5_GET_NAMA_KECAMATAN(B.NO_PROP,B.NO_KAB,B.NO_KEC) NAMA_KEC
					, '0' NO_KEL
					, '-' NAMA_KEL
					, B.NAMA_LGKP
					, '-' ALAMAT
					, B.CURRENT_STATUS_CODE
					, TO_CHAR(B.CREATED,'DD-MM-YYYY') TGL_REKAM
					, TO_CHAR(C.DUPLICATE_NIK) NIK_SINGLE
					, '0' NO_PROP_SINGLE
					, '-' NAMA_PROP_SINGLE
					, '0' NO_KAB_SINGLE
					, '-' NAMA_KAB_SINGLE
					, '0' NO_KEC_SINGLE
					, '-' NAMA_KEC_SINGLE
					, '0' NO_KEL_SINGLE
					, '-' NAMA_KEL_SINGLE
					, '-' NAMA_LGKP_SINGLE
					, '-' ALAMAT_SINGLE
					, '-' CURRENT_STATUS_CODE_SINGLE
					, '-' TGL_REKAM_SINGLE 
          FROM DEMOGRAPHICS@DB221 B INNER JOIN  DUPLICATE_RESULTS@DB221 C ON B.NIK = C.NIK WHERE 
          (B.NIK IN ($NIK)) AND NOT EXISTS (SELECT 1 FROM SIAK_DUPLICATE_FULL A WHERE A.NIK = B.NIK)) B
                ) A
                WHERE A.NIK IN ($NIK) OR A.NIK_SINGLE IN ($NIK)";
			$r = DB::select($sql);
			return $r;
		}
		public function scr_cek_count_duplicate($NIK){
			$sql = "SELECT COUNT(1) JML FROM SIAK_DUPLICATE_FULL A WHERE A.NIK IN ($NIK) OR A.NIK_SINGLE IN ($NIK)";
			$r = DB::select($sql);
			return $r[0]->jml;
		}		

		public function scr_del_req($nik,$no_kec){
			$sql = "";
			$sql .= "SELECT 
					X.NO_KEC
					,X.NIK
					,X.NAMA_LGKP AS NAMA_LENGKAP, Y.ALAMAT, CASE WHEN TO_CHAR(Y.KODE_POS) IS NULL THEN '-' ELSE TO_CHAR(Y.KODE_POS) END KODE_POS, LPAD(TO_CHAR(Y.NO_RT), 3, '0') AS RT, LPAD(TO_CHAR(Y.NO_RW), 3, '0') AS RW,CASE WHEN B.CURRENT_STATUS_CODE IS NULL THEN '-' ELSE B.CURRENT_STATUS_CODE END AS CURRENT_STATUS_CODE
					,TO_CHAR(A.REQ_DATE,'YYYY-MM-DD') AS REQ_DATE
					,A.REQ_BY
					, CASE WHEN A.STEP_PROC = 'T' THEN 'DITOLAK' WHEN A.STEP_PROC = 'C' THEN 'DICETAK' ELSE 'PROSSESS' END AS STATUS_PENGAJUAN
					, CASE WHEN A.ALASAN_PENGAJUAN IS NOT NULL THEN A.ALASAN_PENGAJUAN WHEN A.STEP_PROC = 'T' THEN UPPER(A.PROC_BY) WHEN A.STEP_PROC = 'C' THEN 'SUDAH DICATAK' ELSE '-' END AS KETERANGAN_STATUS
					,CASE WHEN TO_CHAR(C.LAST_UPDATE,'DD-MM-YYYY') IS NULL THEN TO_CHAR(C.PERSONALIZED_DATE,'DD-MM-YYYY') ELSE TO_CHAR(C.LAST_UPDATE,'DD-MM-YYYY') END AS PRINTED_DATE
					, CASE WHEN TO_CHAR(C.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(C.CREATED_USERNAME) IS NULL  THEN '-' WHEN TO_CHAR(C.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(C.CREATED_USERNAME) IS NOT NULL THEN TO_CHAR(C.CREATED_USERNAME) ELSE TO_CHAR(C.LAST_UPDATED_USERNAME) END AS PRINTED_BY
					, F5_GET_NAMA_KECAMATAN(X.NO_PROP,X.NO_KAB,X.NO_KEC) NAMA_KEC
					, F5_GET_NAMA_KELURAHAN(X.NO_PROP,X.NO_KAB,X.NO_KEC,X.NO_KEL) NAMA_KEL
					, UPPER(F5_GET_REF_WNI(X.JENIS_PKRJN, 201)) JENIS_PKRJN
					, UPPER(F5_GET_REF_WNI(X.STAT_KWN, 601)) STAT_KWN
					FROM BIODATA_WNI X LEFT JOIN DATA_KELUARGA Y ON X.NO_KK = Y.NO_KK INNER JOIN (SELECT ID_CETAK,NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY,STEP_PROC,PROC_BY,ALASAN_PENGAJUAN FROM 
					(SELECT  ID_CETAK, NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY,STEP_PROC,PROC_BY,ALASAN_PENGAJUAN, RANK() OVER (PARTITION BY NIK ORDER BY ID_CETAK DESC) RNK FROM SIAK_REQ_CETAK_KTP@YZDB WHERE STEP_PROC <> 'C') WHERE RNK = 1) A ON A.NIK =X.NIK
					LEFT JOIN DEMOGRAPHICS_ALL@DB2 B ON A.NIK = B.NIK LEFT JOIN (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME,LAST_UPDATE FROM (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME,LAST_UPDATE, RANK() OVER (PARTITION BY NIK ORDER BY  PERSONALIZED_DATE DESC, LAST_UPDATE DESC, CHIP_ID DESC) RNK FROM CARD_MANAGEMENT@DB2) WHERE RNK = 1) C ON A.NIK = C.NIK  WHERE 1=1 AND (TRUNC(A.REQ_DATE+1) > CASE WHEN C.LAST_UPDATE IS NULL THEN TRUNC(C.PERSONALIZED_DATE) ELSE TRUNC(C.LAST_UPDATE) END OR C.PERSONALIZED_DATE IS NULL) ";
			if($no_kec != 0){
				$sql .= " AND X.NO_KEC = $no_kec";
			}
			if($nik != ""){
				$sql .= " AND X.NIK IN ($nik)";
			}
			$sql .= "ORDER BY C.PERSONALIZED_DATE";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}
		public function scr_do_delete($nik){
			$sql = "DELETE FROM SIAK_REQ_CETAK_KTP@YZDB WHERE STEP_PROC <> 'C' AND NIK IN ($nik) ";
			$r = DB::connection('db222')->delete($sql);
		}
		public function scr_cek_req_data_uktp($nik,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			$sql .= "SELECT 
					X.NO_KEC
					,X.NIK
					,X.NAMA_LGKP AS NAMA_LENGKAP, Y.ALAMAT, CASE WHEN TO_CHAR(Y.KODE_POS) IS NULL THEN '-' ELSE TO_CHAR(Y.KODE_POS) END KODE_POS, LPAD(TO_CHAR(Y.NO_RT), 3, '0') AS RT, LPAD(TO_CHAR(Y.NO_RW), 3, '0') AS RW,CASE WHEN B.CURRENT_STATUS_CODE IS NULL THEN '-' ELSE B.CURRENT_STATUS_CODE END AS CURRENT_STATUS_CODE
					,TO_CHAR(A.REQ_DATE,'YYYY-MM-DD') AS REQ_DATE
					,A.REQ_BY
					, CASE WHEN A.STEP_PROC = 'T' THEN 'DITOLAK' WHEN A.STEP_PROC = 'C' THEN 'DICETAK' ELSE 'PROSSESS' END AS STATUS_PENGAJUAN
					, CASE WHEN A.NO_KEHILANGAN IS NULL THEN '-' ELSE A.NO_KEHILANGAN END AS NO_KEHILANGAN
					, CASE WHEN A.ALASAN_PENGAJUAN IS NOT NULL THEN A.ALASAN_PENGAJUAN WHEN A.STEP_PROC = 'T' THEN UPPER(A.PROC_BY) WHEN A.STEP_PROC = 'C' THEN 'SUDAH DICATAK' ELSE '-' END AS KETERANGAN_STATUS
					,CASE WHEN TO_CHAR(C.LAST_UPDATE,'DD-MM-YYYY') IS NULL THEN TO_CHAR(C.PERSONALIZED_DATE,'DD-MM-YYYY') ELSE TO_CHAR(C.LAST_UPDATE,'DD-MM-YYYY') END AS PRINTED_DATE
					, CASE WHEN TO_CHAR(C.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(C.CREATED_USERNAME) IS NULL  THEN '-' WHEN TO_CHAR(C.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(C.CREATED_USERNAME) IS NOT NULL THEN TO_CHAR(C.CREATED_USERNAME) ELSE TO_CHAR(C.LAST_UPDATED_USERNAME) END AS PRINTED_BY
					, F5_GET_NAMA_KECAMATAN(X.NO_PROP,X.NO_KAB,X.NO_KEC) NAMA_KEC
					, F5_GET_NAMA_KELURAHAN(X.NO_PROP,X.NO_KAB,X.NO_KEC,X.NO_KEL) NAMA_KEL
					, UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(X.AGAMA,7), 501)) AGAMA
					, UPPER(F5_GET_REF_WNI(X.JENIS_PKRJN, 201)) JENIS_PKRJN
					, UPPER(F5_GET_REF_WNI(X.STAT_KWN, 601)) STAT_KWN
					FROM BIODATA_WNI X LEFT JOIN DATA_KELUARGA Y ON X.NO_KK = Y.NO_KK INNER JOIN (SELECT ID_CETAK,NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY,STEP_PROC,PROC_BY,ALASAN_PENGAJUAN, NO_KEHILANGAN FROM 
					(SELECT  ID_CETAK, NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY,STEP_PROC,PROC_BY,ALASAN_PENGAJUAN,NO_KEHILANGAN, RANK() OVER (PARTITION BY NIK ORDER BY ID_CETAK DESC) RNK FROM SIAK_REQ_CETAK_KTP@YZDB WHERE STEP_PROC <> 'C') WHERE RNK = 1) A ON A.NIK =X.NIK
					LEFT JOIN DEMOGRAPHICS_ALL@DB2 B ON A.NIK = B.NIK LEFT JOIN (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME,LAST_UPDATE FROM (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME,LAST_UPDATE, RANK() OVER (PARTITION BY NIK ORDER BY  PERSONALIZED_DATE DESC, LAST_UPDATE DESC, CHIP_ID DESC) RNK FROM CARD_MANAGEMENT@DB2) WHERE RNK = 1) C ON A.NIK = C.NIK  WHERE 1=1 AND (TRUNC(A.REQ_DATE) > CASE WHEN C.LAST_UPDATE IS NULL THEN TRUNC(C.PERSONALIZED_DATE) ELSE TRUNC(C.LAST_UPDATE) END OR C.PERSONALIZED_DATE IS NULL) ";
			if($no_kec != 0){
				$sql .= " AND X.NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND X.NO_KEL = $no_kel";
			} 
			if($nik != ""){
				$sql .= " AND X.NIK IN ($nik)";
			}
			$sql .= " AND A.REQ_DATE >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.REQ_DATE < TO_DATE('$tgl_end','DD-MM-YYYY') +1 ORDER BY C.PERSONALIZED_DATE";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}
		public function scr_cek_req_count_uktp($nik,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			$sql .= "SELECT COUNT(X.NIK) AS JML FROM BIODATA_WNI X LEFT JOIN DATA_KELUARGA Y ON X.NO_KK = Y.NO_KK INNER JOIN (SELECT ID_CETAK,NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY,STEP_PROC,PROC_BY FROM  
					(SELECT  ID_CETAK, NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY,STEP_PROC,PROC_BY, RANK() OVER (PARTITION BY NIK ORDER BY ID_CETAK DESC) RNK FROM SIAK_REQ_CETAK_KTP@YZDB WHERE STEP_PROC <> 'C') WHERE RNK = 1) A ON A.NIK =X.NIK
					LEFT JOIN DEMOGRAPHICS_ALL@DB2 B ON A.NIK = B.NIK LEFT JOIN (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME,LAST_UPDATE FROM (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME,LAST_UPDATE, RANK() OVER (PARTITION BY NIK ORDER BY  PERSONALIZED_DATE DESC, LAST_UPDATE DESC, CHIP_ID DESC) RNK FROM CARD_MANAGEMENT@DB2) WHERE RNK = 1) C ON A.NIK = C.NIK  WHERE 1=1 AND (TRUNC(A.REQ_DATE) > CASE WHEN C.LAST_UPDATE IS NULL THEN TRUNC(C.PERSONALIZED_DATE) ELSE TRUNC(C.LAST_UPDATE) END OR C.PERSONALIZED_DATE IS NULL)";
			if($no_kec != 0){
				$sql .= " AND X.NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND X.NO_KEL = $no_kel";
			} 
			if($nik != ""){
				$sql .= " AND X.NIK IN ($nik)";
			}
			$sql .= " AND A.REQ_DATE >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.REQ_DATE < TO_DATE('$tgl_end','DD-MM-YYYY') +1";
			$r = DB::connection('db222')->select($sql);
			return $r[0]->jml;
		}

		public function scr_cek_req_data_dktp($nik,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			$sql .= "SELECT X.NO_KEC,X.NIK,X.NAMA_LGKP AS NAMA_LENGKAP, Y.ALAMAT, CASE WHEN TO_CHAR(Y.KODE_POS) IS NULL THEN '-' ELSE TO_CHAR(Y.KODE_POS) END KODE_POS, LPAD(TO_CHAR(Y.NO_RT), 3, '0') AS RT, LPAD(TO_CHAR(Y.NO_RW), 3, '0') AS RW, B.CURRENT_STATUS_CODE,TO_CHAR(A.REQ_DATE,'DD-MM-YYYY') AS REQ_DATE,A.REQ_BY, CASE WHEN TO_CHAR(C.LAST_UPDATE,'DD-MM-YYYY') IS NULL THEN TO_CHAR(C.PERSONALIZED_DATE,'DD-MM-YYYY') ELSE TO_CHAR(C.LAST_UPDATE,'DD-MM-YYYY') END AS PRINTED_DATE,TO_CHAR(C.PERSONALIZED_DATE,'DD-MM-YYYY') AS PRINTED_DATE,CASE WHEN TO_CHAR(C.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(C.CREATED_USERNAME) IS NULL  THEN '-' WHEN TO_CHAR(C.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(C.CREATED_USERNAME) IS NOT NULL THEN TO_CHAR(C.CREATED_USERNAME) ELSE TO_CHAR(C.LAST_UPDATED_USERNAME) END AS PRINTED_BY, F5_GET_NAMA_KECAMATAN(32,73,A.NO_KEC) NAMA_KEC, F5_GET_NAMA_KELURAHAN(32,73,A.NO_KEC,A.NO_KEL) NAMA_KEL, UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(X.AGAMA,7), 501)) AGAMA, UPPER(F5_GET_REF_WNI(X.JENIS_PKRJN, 201)) JENIS_PKRJN, UPPER(F5_GET_REF_WNI(X.STAT_KWN, 601)) STAT_KWN FROM BIODATA_WNI X LEFT JOIN DATA_KELUARGA Y ON X.NO_KK = Y.NO_KK INNER JOIN (SELECT ID_CETAK,NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY FROM (SELECT  ID_CETAK, NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY, RANK() OVER (PARTITION BY NIK ORDER BY ID_CETAK DESC) RNK FROM SIAK_REQ_CETAK_KTP@YZDB  WHERE STEP_PROC = 'A') WHERE RNK = 1) A ON A.NIK = X.NIK LEFT JOIN DEMOGRAPHICS_ALL@DB2 B ON A.NIK = B.NIK LEFT JOIN (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME,LAST_UPDATE FROM (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME,LAST_UPDATE, RANK() OVER (PARTITION BY NIK ORDER BY  PERSONALIZED_DATE DESC, LAST_UPDATE DESC, CHIP_ID DESC) RNK FROM CARD_MANAGEMENT@DB2) WHERE RNK = 1) C ON A.NIK = C.NIK  WHERE 1=1  AND (A.REQ_DATE < C.PERSONALIZED_DATE) ";
			if($no_kec != 0){
				$sql .= " AND X.NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND X.NO_KEL = $no_kel";
			} 
			if($nik != ""){
				$sql .= " AND X.NIK IN ($nik)";
			}
			$sql .= " AND A.REQ_DATE >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.REQ_DATE < TO_DATE('$tgl_end','DD-MM-YYYY') +1";
	
			$r = DB::connection('db222')->select($sql);
			return $r;
		}

		public function scr_cek_req_count_dktp($nik,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			$sql .= "SELECT COUNT(X.NIK) AS JML FROM BIODATA_WNI X LEFT JOIN DATA_KELUARGA Y ON X.NO_KK = Y.NO_KK INNER JOIN (SELECT ID_CETAK,NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY FROM (SELECT  ID_CETAK, NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY, RANK() OVER (PARTITION BY NIK ORDER BY ID_CETAK DESC) RNK FROM SIAK_REQ_CETAK_KTP@YZDB  WHERE STEP_PROC = 'A') WHERE RNK = 1) A ON A.NIK = X.NIK LEFT JOIN DEMOGRAPHICS_ALL@DB2 B ON A.NIK = B.NIK LEFT JOIN (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME FROM (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME, RANK() OVER (PARTITION BY NIK ORDER BY  PERSONALIZED_DATE DESC, LAST_UPDATE DESC, CHIP_ID DESC) RNK FROM CARD_MANAGEMENT@DB2) WHERE RNK = 1) C ON A.NIK = C.NIK  WHERE 1=1 AND (A.REQ_DATE < C.PERSONALIZED_DATE)";
			if($no_kec != 0){
				$sql .= " AND X.NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND X.NO_KEL = $no_kel";
			} 
			if($nik != ""){
				$sql .= " AND X.NIK IN ($nik)";
			}
			$sql .= " AND A.REQ_DATE >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.REQ_DATE < TO_DATE('$tgl_end','DD-MM-YYYY') +1";
			$r = DB::connection('db222')->select($sql);
			return $r[0]->jml;
		}

		public function scr_cek_req_data_uktp_f($nik,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			$sql .= "SELECT X.NO_KEC,X.NIK,X.NAMA_LGKP AS NAMA_LENGKAP, Y.ALAMAT, CASE WHEN TO_CHAR(Y.KODE_POS) IS NULL THEN '-' ELSE TO_CHAR(Y.KODE_POS) END KODE_POS, LPAD(TO_CHAR(Y.NO_RT), 3, '0') AS RT, LPAD(TO_CHAR(Y.NO_RW), 3, '0') AS RW, CASE WHEN B.CURRENT_STATUS_CODE IS NULL THEN '-' ELSE B.CURRENT_STATUS_CODE END AS CURRENT_STATUS_CODE,TO_CHAR(A.REQ_DATE,'YYYY-MM-DD') AS REQ_DATE,A.REQ_BY,CASE WHEN TO_CHAR(C.LAST_UPDATE,'DD-MM-YYYY') IS NULL THEN TO_CHAR(C.PERSONALIZED_DATE,'DD-MM-YYYY') ELSE TO_CHAR(C.LAST_UPDATE,'DD-MM-YYYY') END AS PRINTED_DATE,CASE WHEN TO_CHAR(C.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(C.CREATED_USERNAME) IS NULL  THEN '-' WHEN TO_CHAR(C.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(C.CREATED_USERNAME) IS NOT NULL THEN TO_CHAR(C.CREATED_USERNAME) ELSE TO_CHAR(C.LAST_UPDATED_USERNAME) END AS PRINTED_BY, F5_GET_NAMA_KECAMATAN(X.NO_PROP,X.NO_KAB,X.NO_KEC) NAMA_KEC, F5_GET_NAMA_KELURAHAN(X.NO_PROP,X.NO_KAB,X.NO_KEC,X.NO_KEL) NAMA_KEL, UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(X.AGAMA,7), 501)) AGAMA, UPPER(F5_GET_REF_WNI(X.JENIS_PKRJN, 201)) JENIS_PKRJN, UPPER(F5_GET_REF_WNI(X.STAT_KWN, 601)) STAT_KWN FROM BIODATA_WNI X LEFT JOIN DATA_KELUARGA Y ON X.NO_KK = Y.NO_KK INNER JOIN (SELECT ID_CETAK,NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY FROM (SELECT  ID_CETAK, NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY, RANK() OVER (PARTITION BY NIK ORDER BY ID_CETAK DESC) RNK FROM SIAK_REQ_CETAK_KTP@YZDB  WHERE STEP_PROC = 'A') WHERE RNK = 1) A ON A.NIK = X.NIK LEFT JOIN DEMOGRAPHICS_ALL@DB2 B ON A.NIK = B.NIK LEFT JOIN (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME,LAST_UPDATE FROM (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME,LAST_UPDATE, RANK() OVER (PARTITION BY NIK ORDER BY  PERSONALIZED_DATE DESC, LAST_UPDATE DESC, CHIP_ID DESC) RNK FROM CARD_MANAGEMENT@DB2) WHERE RNK = 1) C ON A.NIK = C.NIK  WHERE 1=1 AND (B.CURRENT_STATUS_CODE LIKE '%CARD%' OR B.CURRENT_STATUS_CODE LIKE '%PRINT%') AND (TRUNC(A.REQ_DATE) > CASE WHEN C.LAST_UPDATE IS NULL THEN TRUNC(C.PERSONALIZED_DATE) ELSE TRUNC(C.LAST_UPDATE) END OR C.PERSONALIZED_DATE IS NULL)";
			if($no_kec != 0){
				$sql .= " AND X.NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND X.NO_KEL = $no_kel";
			} 
			if($nik != ""){
				$sql .= " AND X.NIK IN ($nik)";
			}
			$sql .= " AND A.REQ_DATE >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.REQ_DATE < TO_DATE('$tgl_end','DD-MM-YYYY') +1";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}

		public function scr_cek_req_count_uktp_f($nik,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			$sql .= "SELECT COUNT(X.NIK) AS JML FROM BIODATA_WNI X LEFT JOIN DATA_KELUARGA Y ON X.NO_KK = Y.NO_KK INNER JOIN (SELECT ID_CETAK,NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY FROM (SELECT  ID_CETAK, NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY, RANK() OVER (PARTITION BY NIK ORDER BY ID_CETAK DESC) RNK FROM SIAK_REQ_CETAK_KTP@YZDB  WHERE STEP_PROC = 'A') WHERE RNK = 1) A ON A.NIK = X.NIK LEFT JOIN DEMOGRAPHICS_ALL@DB2 B ON A.NIK = B.NIK LEFT JOIN (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME FROM (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME, RANK() OVER (PARTITION BY NIK ORDER BY  PERSONALIZED_DATE DESC, LAST_UPDATE DESC, CHIP_ID DESC) RNK FROM CARD_MANAGEMENT@DB2) WHERE RNK = 1) C ON A.NIK = C.NIK  WHERE 1=1 AND (B.CURRENT_STATUS_CODE LIKE '%CARD%' OR B.CURRENT_STATUS_CODE LIKE '%PRINT%') AND (TRUNC(A.REQ_DATE) > CASE WHEN C.LAST_UPDATE IS NULL THEN TRUNC(C.PERSONALIZED_DATE) ELSE TRUNC(C.LAST_UPDATE) END OR C.PERSONALIZED_DATE IS NULL)";
			if($no_kec != 0){
				$sql .= " AND X.NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND X.NO_KEL = $no_kel";
			} 
			if($nik != ""){
				$sql .= " AND X.NIK IN ($nik)";
			}
			$sql .= " AND A.REQ_DATE >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.REQ_DATE < TO_DATE('$tgl_end','DD-MM-YYYY') +1";
			$r = DB::connection('db222')->select($sql);
			return $r[0]->jml;
		}

		public function scr_cek_req_data_dktp_f($nik,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			$sql .= "SELECT X.NO_KEC,X.NIK,X.NAMA_LGKP AS NAMA_LENGKAP, Y.ALAMAT, CASE WHEN TO_CHAR(Y.KODE_POS) IS NULL THEN '-' ELSE TO_CHAR(Y.KODE_POS) END KODE_POS, LPAD(TO_CHAR(Y.NO_RT), 3, '0') AS RT, LPAD(TO_CHAR(Y.NO_RW), 3, '0') AS RW, B.CURRENT_STATUS_CODE,TO_CHAR(A.REQ_DATE,'DD-MM-YYYY') AS REQ_DATE,A.REQ_BY, CASE WHEN TO_CHAR(C.LAST_UPDATE,'DD-MM-YYYY') IS NULL THEN TO_CHAR(C.PERSONALIZED_DATE,'DD-MM-YYYY') ELSE TO_CHAR(C.LAST_UPDATE,'DD-MM-YYYY') END AS PRINTED_DATE,TO_CHAR(C.PERSONALIZED_DATE,'DD-MM-YYYY') AS PRINTED_DATE,CASE WHEN TO_CHAR(C.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(C.CREATED_USERNAME) IS NULL  THEN '-' WHEN TO_CHAR(C.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(C.CREATED_USERNAME) IS NOT NULL THEN TO_CHAR(C.CREATED_USERNAME) ELSE TO_CHAR(C.LAST_UPDATED_USERNAME) END AS PRINTED_BY, F5_GET_NAMA_KECAMATAN(X.NO_PROP,X.NO_KAB,X.NO_KEC) NAMA_KEC, F5_GET_NAMA_KELURAHAN(X.NO_PROP,X.NO_KAB,X.NO_KEC,X.NO_KEL) NAMA_KEL, UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(X.AGAMA,7), 501)) AGAMA, UPPER(F5_GET_REF_WNI(X.JENIS_PKRJN, 201)) JENIS_PKRJN, UPPER(F5_GET_REF_WNI(X.STAT_KWN, 601)) STAT_KWN FROM BIODATA_WNI X LEFT JOIN DATA_KELUARGA Y ON X.NO_KK = Y.NO_KK INNER JOIN (SELECT ID_CETAK,NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY FROM (SELECT  ID_CETAK, NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY, RANK() OVER (PARTITION BY NIK ORDER BY ID_CETAK DESC) RNK FROM SIAK_REQ_CETAK_KTP@YZDB  WHERE STEP_PROC = 'A') WHERE RNK = 1) A ON A.NIK = X.NIK LEFT JOIN DEMOGRAPHICS_ALL@DB2 B ON A.NIK = B.NIK LEFT JOIN (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME,LAST_UPDATE FROM (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME,LAST_UPDATE, RANK() OVER (PARTITION BY NIK ORDER BY  PERSONALIZED_DATE DESC, LAST_UPDATE DESC, CHIP_ID DESC) RNK FROM CARD_MANAGEMENT@DB2) WHERE RNK = 1) C ON A.NIK = C.NIK  WHERE 1=1  AND (B.CURRENT_STATUS_CODE LIKE '%CARD%' OR B.CURRENT_STATUS_CODE LIKE '%PRINT%') AND (A.REQ_DATE < C.PERSONALIZED_DATE)";
			if($no_kec != 0){
				$sql .= " AND X.NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND X.NO_KEL = $no_kel";
			} 
			if($nik != ""){
				$sql .= " AND X.NIK IN ($nik)";
			}
			$sql .= " AND A.REQ_DATE >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.REQ_DATE < TO_DATE('$tgl_end','DD-MM-YYYY') +1";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}

		public function scr_cek_req_count_dktp_f($nik,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			$sql .= "SELECT COUNT(X.NIK) AS JML FROM BIODATA_WNI X LEFT JOIN DATA_KELUARGA Y ON X.NO_KK = Y.NO_KK INNER JOIN (SELECT ID_CETAK,NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY FROM (SELECT  ID_CETAK, NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY, RANK() OVER (PARTITION BY NIK ORDER BY ID_CETAK DESC) RNK FROM SIAK_REQ_CETAK_KTP@YZDB  WHERE STEP_PROC = 'A') WHERE RNK = 1) A ON A.NIK = X.NIK LEFT JOIN DEMOGRAPHICS_ALL@DB2 B ON A.NIK = B.NIK LEFT JOIN (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME FROM (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME, RANK() OVER (PARTITION BY NIK ORDER BY  PERSONALIZED_DATE DESC, LAST_UPDATE DESC, CHIP_ID DESC) RNK FROM CARD_MANAGEMENT@DB2) WHERE RNK = 1) C ON A.NIK = C.NIK  WHERE 1=1 AND (B.CURRENT_STATUS_CODE LIKE '%CARD%' OR B.CURRENT_STATUS_CODE LIKE '%PRINT%') AND (A.REQ_DATE < C.PERSONALIZED_DATE)";
			if($no_kec != 0){
				$sql .= " AND X.NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND X.NO_KEL = $no_kel";
			} 
			if($nik != ""){
				$sql .= " AND X.NIK IN ($nik)";
			}
			$sql .= " AND A.REQ_DATE >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.REQ_DATE < TO_DATE('$tgl_end','DD-MM-YYYY') +1";
			$r = DB::connection('db222')->select($sql);
			return $r[0]->jml;
		}

		public function scr_cek_req_data_usuket($nik,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			$sql .= "SELECT X.NO_KEC,X.NIK,X.NAMA_LGKP AS NAMA_LENGKAP, Y.ALAMAT,UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(X.AGAMA,7), 501)) AGAMA, CASE WHEN TO_CHAR(Y.KODE_POS) IS NULL THEN '-' ELSE TO_CHAR(Y.KODE_POS) END KODE_POS, LPAD(TO_CHAR(Y.NO_RT), 3, '0') AS RT, LPAD(TO_CHAR(Y.NO_RW), 3, '0') AS RW, B.CURRENT_STATUS_CODE,TO_CHAR(A.REQ_DATE,'DD-MM-YYYY') AS REQ_DATE,A.REQ_BY, TO_CHAR(C.PRINTED_DATE,'DD-MM-YYYY') AS PRINTED_DATE,C.PRINTED_BY, F5_GET_NAMA_KECAMATAN(32,73,A.NO_KEC) NAMA_KEC, F5_GET_NAMA_KELURAHAN(32,73,A.NO_KEC,A.NO_KEL) NAMA_KEL, UPPER(F5_GET_REF_WNI(X.JENIS_PKRJN, 201)) JENIS_PKRJN, UPPER(F5_GET_REF_WNI(X.STAT_KWN, 601)) STAT_KWN FROM BIODATA_WNI X LEFT JOIN DATA_KELUARGA Y ON X.NO_KK = Y.NO_KK INNER JOIN (SELECT ID_CETAK,NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY FROM (SELECT  ID_CETAK, NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY, RANK() OVER (PARTITION BY NIK ORDER BY ID_CETAK DESC) RNK FROM SIAK_REQ_CETAK_KTP@YZDB WHERE STEP_PROC = 'A') WHERE RNK = 1) A ON A.NIK = X.NIK INNER JOIN DEMOGRAPHICS_ALL@DB2 B ON A.NIK = B.NIK LEFT JOIN (SELECT NIK, PRINTED_DATE,PRINTED_BY FROM (SELECT NIK,PRINTED_DATE,PRINTED_BY, RANK() OVER (PARTITION BY NIK ORDER BY PRINTED_DATE DESC) RNK FROM T7_HIST_SUKET) WHERE RNK = 1) C ON A.NIK = C.NIK  WHERE 1=1 AND (B.CURRENT_STATUS_CODE NOT LIKE '%CARD%' AND B.CURRENT_STATUS_CODE NOT LIKE '%PRINT%') AND (A.REQ_DATE > C.PRINTED_DATE OR C.PRINTED_DATE IS NULL)";
			if($no_kec != 0){
				$sql .= " AND X.NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND X.NO_KEL = $no_kel";
			} 
			if($nik != ""){
				$sql .= " AND X.NIK IN ($nik)";
			}
			$sql .= " AND A.REQ_DATE >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.REQ_DATE < TO_DATE('$tgl_end','DD-MM-YYYY') +1";

			$r = DB::connection('db222')->select($sql);
			return $r;
		}

		public function scr_cek_req_count_usuket($nik,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			$sql .= "SELECT COUNT(X.NIK) AS JML FROM BIODATA_WNI X LEFT JOIN DATA_KELUARGA Y ON X.NO_KK = Y.NO_KK INNER JOIN (SELECT ID_CETAK,NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY FROM (SELECT  ID_CETAK, NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY, RANK() OVER (PARTITION BY NIK ORDER BY ID_CETAK DESC) RNK FROM SIAK_REQ_CETAK_KTP@YZDB  WHERE STEP_PROC = 'A') WHERE RNK = 1) A ON A.NIK = X.NIK INNER JOIN DEMOGRAPHICS_ALL@DB2 B ON A.NIK = B.NIK LEFT JOIN (SELECT NIK, PRINTED_DATE,PRINTED_BY FROM (SELECT NIK,PRINTED_DATE,PRINTED_BY, RANK() OVER (PARTITION BY NIK ORDER BY PRINTED_DATE DESC) RNK FROM T7_HIST_SUKET) WHERE RNK = 1) C ON A.NIK = C.NIK  WHERE 1=1 AND (B.CURRENT_STATUS_CODE NOT LIKE '%CARD%' AND B.CURRENT_STATUS_CODE NOT LIKE '%PRINT%') AND (A.REQ_DATE > C.PRINTED_DATE OR C.PRINTED_DATE IS NULL)";
			if($no_kec != 0){
				$sql .= " AND X.NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND X.NO_KEL = $no_kel";
			} 
			if($nik != ""){
				$sql .= " AND X.NIK IN ($nik)";
			}
			$sql .= " AND A.REQ_DATE >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.REQ_DATE < TO_DATE('$tgl_end','DD-MM-YYYY') +1";
			$r = DB::connection('db222')->select($sql);
			return $r[0]->jml;
		}

		public function scr_cek_req_data_dsuket($nik,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			$sql .= "SELECT X.NO_KEC,X.NIK,X.NAMA_LGKP AS NAMA_LENGKAP, Y.ALAMAT,UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(X.AGAMA,7), 501)) AGAMA, CASE WHEN TO_CHAR(Y.KODE_POS) IS NULL THEN '-' ELSE TO_CHAR(Y.KODE_POS) END KODE_POS, LPAD(TO_CHAR(Y.NO_RT), 3, '0') AS RT, LPAD(TO_CHAR(Y.NO_RW), 3, '0') AS RW, B.CURRENT_STATUS_CODE,TO_CHAR(A.REQ_DATE,'DD-MM-YYYY') AS REQ_DATE,A.REQ_BY, TO_CHAR(C.PRINTED_DATE,'DD-MM-YYYY') AS PRINTED_DATE,C.PRINTED_BY, F5_GET_NAMA_KECAMATAN(32,73,A.NO_KEC) NAMA_KEC, F5_GET_NAMA_KELURAHAN(32,73,A.NO_KEC,A.NO_KEL) NAMA_KEL, UPPER(F5_GET_REF_WNI(X.JENIS_PKRJN, 201)) JENIS_PKRJN, UPPER(F5_GET_REF_WNI(X.STAT_KWN, 601)) STAT_KWN FROM BIODATA_WNI X LEFT JOIN DATA_KELUARGA Y ON X.NO_KK = Y.NO_KK INNER JOIN (SELECT ID_CETAK,NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY FROM (SELECT  ID_CETAK, NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY, RANK() OVER (PARTITION BY NIK ORDER BY ID_CETAK DESC) RNK FROM SIAK_REQ_CETAK_KTP@YZDB  WHERE STEP_PROC = 'A') WHERE RNK = 1) A ON A.NIK = X.NIK INNER JOIN DEMOGRAPHICS_ALL@DB2 B ON A.NIK = B.NIK LEFT JOIN (SELECT NIK, PRINTED_DATE,PRINTED_BY FROM (SELECT NIK,PRINTED_DATE,PRINTED_BY, RANK() OVER (PARTITION BY NIK ORDER BY PRINTED_DATE DESC) RNK FROM T7_HIST_SUKET) WHERE RNK = 1) C ON A.NIK = C.NIK  WHERE 1=1 AND (B.CURRENT_STATUS_CODE NOT LIKE '%CARD%' AND B.CURRENT_STATUS_CODE NOT LIKE '%PRINT%') AND (A.REQ_DATE < C.PRINTED_DATE)";
			if($no_kec != 0){
				$sql .= " AND X.NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND X.NO_KEL = $no_kel";
			} 
			if($nik != ""){
				$sql .= " AND X.NIK IN ($nik)";
			}
			$sql .= " AND A.REQ_DATE >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.REQ_DATE < TO_DATE('$tgl_end','DD-MM-YYYY') +1";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}

		public function scr_cek_req_count_dsuket($nik,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			$sql .= "SELECT COUNT(X.NIK) AS JML FROM BIODATA_WNI X LEFT JOIN DATA_KELUARGA Y ON X.NO_KK = Y.NO_KK INNER JOIN (SELECT ID_CETAK,NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY FROM (SELECT  ID_CETAK, NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY, RANK() OVER (PARTITION BY NIK ORDER BY ID_CETAK DESC) RNK FROM SIAK_REQ_CETAK_KTP@YZDB  WHERE STEP_PROC = 'A') WHERE RNK = 1) A ON A.NIK = X.NIK INNER JOIN DEMOGRAPHICS_ALL@DB2 B ON A.NIK = B.NIK LEFT JOIN (SELECT NIK, PRINTED_DATE,PRINTED_BY FROM (SELECT NIK,PRINTED_DATE,PRINTED_BY, RANK() OVER (PARTITION BY NIK ORDER BY PRINTED_DATE DESC) RNK FROM T7_HIST_SUKET) WHERE RNK = 1) C ON A.NIK = C.NIK  WHERE 1=1 AND (B.CURRENT_STATUS_CODE NOT LIKE '%CARD%' AND B.CURRENT_STATUS_CODE NOT LIKE '%PRINT%') AND (A.REQ_DATE < C.PRINTED_DATE)";
			if($no_kec != 0){
				$sql .= " AND X.NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND X.NO_KEL = $no_kel";
			} 
			if($nik != ""){
				$sql .= " AND X.NIK IN ($nik)";
			}
			$sql .= " AND A.REQ_DATE >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.REQ_DATE < TO_DATE('$tgl_end','DD-MM-YYYY') +1";
			$r = DB::connection('db222')->select($sql);
			return $r[0]->jml;
		}
		public function scr_cek_req_data_uttelhr($is_user,$is_lahir,$tgl_start,$tgl_end){
			$sql = "";
			$sql .= "SELECT 
					  D.ADM_AKTA_NO
					  , D.BAYI_NAMA_LGKP
            		  , A.NO_KEC
					  , F5_GET_NAMA_KECAMATAN(A.NO_PROV,A.NO_KAB,A.NO_KEC) NAMA_KEC
					  , A.NO_KEL
					  , F5_GET_NAMA_KELURAHAN(A.NO_PROV,A.NO_KAB,A.NO_KEC,A.NO_KEL) NAMA_KEL
					  , CASE WHEN TRUNC(A.REQ_DATE) > TRUNC(B.PRINTED_DATE) OR B.PRINTED_DATE  IS NULL THEN 'BELUM CETAK' ELSE 'SUDAH CETAK' END STATUS_CETAK
					  , A.REQ_BY
					  , TO_CHAR(A.REQ_DATE,'DD-MM-YYYY') REQ_DATE
					  , CASE WHEN B.PRINTED_DATE IS NULL THEN '-' ELSE TO_CHAR(B.PRINTED_DATE,'DD-MM-YYYY') END TANGGAL_CETAK
					  , CASE WHEN A.CERT_STATUS = 1 THEN 'TELAH DIAJUKAN' WHEN A.CERT_STATUS = 2 THEN 'PROSES PENERBITAN TTE' WHEN A.CERT_STATUS = 3 THEN 'SUDAH TERSERTIFIKASI'  WHEN A.CERT_STATUS = 9 THEN 'BELUM DI VERIVIKASI' ELSE 'DOKUMEN TELAH DIPUBLISH' END STATUS_TTE
					  FROM (SELECT NO_PROV, NO_KAB, NO_KEC, NO_KEL,NO_DOKUMEN,PEJABAT_PROCESS_DATE, CERT_STATUS, REQ_DATE,REQ_BY FROM (SELECT  NO_PROV, NO_KAB, NO_KEC, NO_KEL,NO_DOKUMEN, CERT_STATUS,PEJABAT_PROCESS_DATE, REQ_DATE,REQ_BY, RANK() OVER (PARTITION BY NO_DOKUMEN ORDER BY REQ_DATE DESC,SEQN_ID DESC) RNK FROM BSRE_KELAHIRAN ) WHERE RNK = 1) A INNER JOIN CAPIL_LAHIR D ON A.NO_DOKUMEN = D.ADM_AKTA_NO LEFT JOIN (SELECT  NO_DOKUMEN, PRINTED_DATE FROM (SELECT  NO_DOKUMEN, PRINTED_DATE, RANK() OVER (PARTITION BY NO_DOKUMEN ORDER BY PRINTED_DATE DESC, SEQN_ID DESC) RNK FROM T5_SEQN_PRINT_KELAHIRAN ) WHERE RNK = 1) B ON A.NO_DOKUMEN = B.NO_DOKUMEN WHERE A.PEJABAT_PROCESS_DATE IS NULL";
			if($is_user != '0'){
				$sql .= " AND LOWER(A.REQ_BY) = '$is_user'";
			}
			if($is_lahir != '0'){
				$sql .= " AND A.NO_DOKUMEN LIKE '%$is_lahir%'";
			}
			$sql .= " AND A.REQ_DATE >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.REQ_DATE < TO_DATE('$tgl_end','DD-MM-YYYY') +1
				ORDER BY TRUNC(A.REQ_DATE) DESC, A.NO_DOKUMEN 
			";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}

		public function scr_cek_req_data_dttelhr($is_user,$is_lahir,$tgl_start,$tgl_end){
			$sql = "";
			$sql .= "SELECT 
					  D.ADM_AKTA_NO
					  , D.BAYI_NAMA_LGKP
            		  , A.NO_KEC
					  , F5_GET_NAMA_KECAMATAN(A.NO_PROV,A.NO_KAB,A.NO_KEC) NAMA_KEC
					  , A.NO_KEL
					  , F5_GET_NAMA_KELURAHAN(A.NO_PROV,A.NO_KAB,A.NO_KEC,A.NO_KEL) NAMA_KEL
					  , CASE WHEN TRUNC(A.REQ_DATE) > TRUNC(B.PRINTED_DATE) OR B.PRINTED_DATE  IS NULL THEN 'BELUM CETAK' ELSE 'SUDAH CETAK' END STATUS_CETAK
					  , A.REQ_BY
					  , TO_CHAR(A.REQ_DATE,'DD-MM-YYYY') REQ_DATE
					  , CASE WHEN B.PRINTED_DATE IS NULL THEN '-' ELSE TO_CHAR(B.PRINTED_DATE,'DD-MM-YYYY') END TANGGAL_CETAK
					  , CASE WHEN A.CERT_STATUS = 1 THEN 'TELAH DIAJUKAN' WHEN A.CERT_STATUS = 2 THEN 'PROSES PENERBITAN TTE' WHEN A.CERT_STATUS = 3 THEN 'SUDAH TERSERTIFIKASI'  WHEN A.CERT_STATUS = 9 THEN 'BELUM DI VERIVIKASI' ELSE 'DOKUMEN TELAH DIPUBLISH' END STATUS_TTE
					  FROM (SELECT NO_PROV, NO_KAB, NO_KEC, NO_KEL,NO_DOKUMEN,PEJABAT_PROCESS_DATE, CERT_STATUS, REQ_DATE,REQ_BY FROM (SELECT  NO_PROV, NO_KAB, NO_KEC, NO_KEL,NO_DOKUMEN, CERT_STATUS,PEJABAT_PROCESS_DATE, REQ_DATE,REQ_BY, RANK() OVER (PARTITION BY NO_DOKUMEN ORDER BY REQ_DATE DESC,SEQN_ID DESC) RNK FROM BSRE_KELAHIRAN ) WHERE RNK = 1) A INNER JOIN CAPIL_LAHIR D ON A.NO_DOKUMEN = D.ADM_AKTA_NO LEFT JOIN (SELECT  NO_DOKUMEN, PRINTED_DATE FROM (SELECT  NO_DOKUMEN, PRINTED_DATE, RANK() OVER (PARTITION BY NO_DOKUMEN ORDER BY PRINTED_DATE DESC, SEQN_ID DESC) RNK FROM T5_SEQN_PRINT_KELAHIRAN ) WHERE RNK = 1) B ON A.NO_DOKUMEN = B.NO_DOKUMEN WHERE A.PEJABAT_PROCESS_DATE IS NOT NULL";
			if($is_user != '0'){
				$sql .= " AND LOWER(A.REQ_BY) = '$is_user'";
			}
			if($is_lahir != '0'){
				$sql .= " AND A.NO_DOKUMEN LIKE '%$is_lahir%'";
			}
			$sql .= " AND A.REQ_DATE >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.REQ_DATE < TO_DATE('$tgl_end','DD-MM-YYYY') +1
				ORDER BY TRUNC(A.REQ_DATE) DESC, A.NO_DOKUMEN 
			";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}
		public function scr_cek_req_data_uttekk($tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			$sql .= "SELECT 
					  D.NO_KK
					  , D.NAMA_KEP
					  , D.ALAMAT
					  , D.NO_KEC
					  , F5_GET_NAMA_KECAMATAN(D.NO_PROP,D.NO_KAB,D.NO_KEC) NAMA_KEC
					  , D.NO_KEL
					  , F5_GET_NAMA_KELURAHAN(D.NO_PROP,D.NO_KAB,D.NO_KEC,D.NO_KEL) NAMA_KEL
					  , CASE WHEN TRUNC(A.REQ_DATE) > TRUNC(B.PRINTED_DATE) OR B.PRINTED_DATE  IS NULL THEN 'BELUM CETAK' ELSE 'SUDAH CETAK' END STATUS_CETAK
					  , A.REQ_BY
					  , TO_CHAR(A.REQ_DATE,'DD-MM-YYYY') REQ_DATE
					  , CASE WHEN B.PRINTED_DATE IS NULL THEN '-' ELSE TO_CHAR(B.PRINTED_DATE,'DD-MM-YYYY') END TANGGAL_CETAK
					  , CASE WHEN A.CERT_STATUS = 1 THEN 'TELAH DIAJUKAN' WHEN A.CERT_STATUS = 2 THEN 'PROSES PENERBITAN TTE' WHEN A.CERT_STATUS = 3 THEN 'SUDAH TERSERTIFIKASI'  WHEN A.CERT_STATUS = 9 THEN 'BELUM DI VERIVIKASI' ELSE 'DOKUMEN TELAH DIPUBLISH' END STATUS_TTE
					  FROM (SELECT  NO_DOC,PEJABAT_PROCESS_DATE, CERT_STATUS, REQ_DATE,REQ_BY FROM (SELECT  NO_DOC, CERT_STATUS,PEJABAT_PROCESS_DATE, REQ_DATE,REQ_BY, RANK() OVER (PARTITION BY NO_DOC ORDER BY REQ_DATE DESC,SEQN_ID DESC) RNK FROM BSRE_KARTU_KELUARGA ) WHERE RNK = 1) A INNER JOIN DATA_KELUARGA D ON A.NO_DOC = D.NO_KK LEFT JOIN (SELECT  NO_KK, PRINTED_DATE FROM (SELECT  NO_KK, PRINTED_DATE, RANK() OVER (PARTITION BY NO_KK ORDER BY PRINTED_DATE DESC, SEQN_ID DESC) RNK FROM T5_SEQN_PRINT_KK ) WHERE RNK = 1) B ON A.NO_DOC = B.NO_KK WHERE A.PEJABAT_PROCESS_DATE IS NULL ";
			if($no_kec != 0){
				$sql .= " AND D.NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND D.NO_KEL = $no_kel";
			} 
			$sql .= " AND A.REQ_DATE >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.REQ_DATE < TO_DATE('$tgl_end','DD-MM-YYYY') +1
				ORDER BY D.NO_KEC, D.NO_KEL, A.REQ_DATE
			";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}

		public function scr_cek_req_data_dttekk($tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			$sql .= "SELECT 
					  D.NO_KK
					  , D.NAMA_KEP
					  , D.ALAMAT
					  , D.NO_KEC
					  , F5_GET_NAMA_KECAMATAN(D.NO_PROP,D.NO_KAB,D.NO_KEC) NAMA_KEC
					  , D.NO_KEL
					  , F5_GET_NAMA_KELURAHAN(D.NO_PROP,D.NO_KAB,D.NO_KEC,D.NO_KEL) NAMA_KEL
					  , CASE WHEN TRUNC(A.REQ_DATE) > TRUNC(B.PRINTED_DATE) OR B.PRINTED_DATE  IS NULL THEN 'BELUM CETAK' ELSE 'SUDAH CETAK' END STATUS_CETAK
					  , A.REQ_BY
					  , TO_CHAR(A.REQ_DATE,'DD-MM-YYYY') REQ_DATE
					  , CASE WHEN B.PRINTED_DATE IS NULL THEN '-' ELSE TO_CHAR(B.PRINTED_DATE,'DD-MM-YYYY') END TANGGAL_CETAK
					  , CASE WHEN A.CERT_STATUS = 1 THEN 'TELAH DIAJUKAN' WHEN A.CERT_STATUS = 2 THEN 'PROSES PENERBITAN TTE' WHEN A.CERT_STATUS = 3 THEN 'SUDAH TERSERTIFIKASI'  WHEN A.CERT_STATUS = 9 THEN 'BELUM DI VERIVIKASI' ELSE 'DOKUMEN TELAH DIPUBLISH' END STATUS_TTE
					  FROM (SELECT  NO_DOC,PEJABAT_PROCESS_DATE, CERT_STATUS, REQ_DATE,REQ_BY FROM (SELECT  NO_DOC, CERT_STATUS,PEJABAT_PROCESS_DATE, REQ_DATE,REQ_BY, RANK() OVER (PARTITION BY NO_DOC ORDER BY REQ_DATE DESC,SEQN_ID DESC) RNK FROM BSRE_KARTU_KELUARGA ) WHERE RNK = 1) A INNER JOIN DATA_KELUARGA D ON A.NO_DOC = D.NO_KK LEFT JOIN (SELECT  NO_KK, PRINTED_DATE FROM (SELECT  NO_KK, PRINTED_DATE, RANK() OVER (PARTITION BY NO_KK ORDER BY PRINTED_DATE DESC, SEQN_ID DESC) RNK FROM T5_SEQN_PRINT_KK ) WHERE RNK = 1) B ON A.NO_DOC = B.NO_KK WHERE A.PEJABAT_PROCESS_DATE IS NOT NULL ";
			if($no_kec != 0){
				$sql .= " AND D.NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND D.NO_KEL = $no_kel";
			} 
			$sql .= " AND A.REQ_DATE >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.REQ_DATE < TO_DATE('$tgl_end','DD-MM-YYYY') +1
				ORDER BY D.NO_KEC, D.NO_KEL, A.REQ_DATE
			";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}

		
		public function scr_pengajuan_kk_wait($tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			$sql .= "SELECT D.NO_KK
					  , D.NAMA_KEP
					  , D.ALAMAT
					  , D.NO_KEC
					  , F5_GET_NAMA_KECAMATAN(D.NO_PROP,D.NO_KAB,D.NO_KEC) NAMA_KEC
					  , D.NO_KEL
					  , F5_GET_NAMA_KELURAHAN(D.NO_PROP,D.NO_KAB,D.NO_KEC,D.NO_KEL) NAMA_KEL
					  , CASE WHEN TRUNC(X.REQ_DATE) > TRUNC(B.PRINTED_DATE) OR B.PRINTED_DATE  IS NULL THEN 'BELUM CETAK' ELSE 'SUDAH CETAK' END STATUS_CETAK
					  , X.REQ_BY
					  , TO_CHAR(X.REQ_DATE,'DD-MM-YYYY') REQ_DATE
           			  , CASE WHEN A.REQ_BY_TTE IS NULL THEN '-' ELSE A.REQ_BY_TTE END REQ_BY_TTE
					  , CASE WHEN TO_CHAR(A.REQ_DATE_TTE,'DD-MM-YYYY') IS NULL THEN '-' ELSE TO_CHAR(A.REQ_DATE_TTE,'DD-MM-YYYY') END REQ_DATE_TTE
					  , CASE WHEN B.PRINTED_DATE IS NULL THEN '-' ELSE TO_CHAR(B.PRINTED_DATE,'DD-MM-YYYY') END TANGGAL_CETAK
					  , CASE  WHEN A.CERT_STATUS IS NULL THEN 'BELUM DIAJUKAN' WHEN A.CERT_STATUS = 1 THEN 'TELAH DIAJUKAN' WHEN A.CERT_STATUS = 2 THEN 'PROSES PENERBITAN TTE' WHEN A.CERT_STATUS = 3 THEN 'SUDAH TERSERTIFIKASI'  WHEN A.CERT_STATUS = 9 THEN 'BELUM DI VERIVIKASI' ELSE 'DOKUMEN TELAH DIPUBLISH' END STATUS_TTE 
            FROM (SELECT  NO_KK,NO_KEC, NO_KEL, REQ_DATE,REQ_BY FROM (SELECT  NO_KK, NO_KEC,NO_KEL, REQ_DATE,REQ_BY, RANK() OVER (PARTITION BY NO_KK ORDER BY REQ_DATE DESC,ID_CETAK DESC) RNK FROM T5_REQ_CETAK_KK ) WHERE RNK = 1) X 
            INNER JOIN DATA_KELUARGA D ON X.NO_KK = D.NO_KK 
            LEFT JOIN (SELECT  NO_KK, PRINTED_DATE FROM (SELECT  NO_KK, PRINTED_DATE, RANK() OVER (PARTITION BY NO_KK ORDER BY PRINTED_DATE DESC, SEQN_ID DESC) RNK FROM T5_SEQN_PRINT_KK ) WHERE RNK = 1) B ON B.NO_KK = D.NO_KK
            LEFT JOIN (SELECT  NO_DOC,PEJABAT_PROCESS_DATE, CERT_STATUS, REQ_DATE REQ_DATE_TTE,REQ_BY REQ_BY_TTE FROM (SELECT  NO_DOC, CERT_STATUS,PEJABAT_PROCESS_DATE, REQ_DATE,REQ_BY, RANK() OVER (PARTITION BY NO_DOC ORDER BY REQ_DATE DESC,SEQN_ID DESC) RNK FROM BSRE_KARTU_KELUARGA ) WHERE RNK = 1) A ON X.NO_KK = A.NO_DOC 
            WHERE 
            (A.REQ_DATE_TTE IS NULL OR TRUNC(A.REQ_DATE_TTE) < TRUNC(X.REQ_DATE))
            ";
			if($no_kec != 0){
				$sql .= " AND D.NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND D.NO_KEL = $no_kel";
			} 
			$sql .= " AND X.REQ_DATE >= TO_DATE('$tgl_start','DD-MM-YYYY') AND X.REQ_DATE < TO_DATE('$tgl_end','DD-MM-YYYY') +1
				ORDER BY D.NO_KEC, D.NO_KEL, X.REQ_DATE DESC
			";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}

		public function scr_pengajuan_kk_done($tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			$sql .= "SELECT D.NO_KK
					  , D.NAMA_KEP
					  , D.ALAMAT
					  , D.NO_KEC
					  , F5_GET_NAMA_KECAMATAN(D.NO_PROP,D.NO_KAB,D.NO_KEC) NAMA_KEC
					  , D.NO_KEL
					  , F5_GET_NAMA_KELURAHAN(D.NO_PROP,D.NO_KAB,D.NO_KEC,D.NO_KEL) NAMA_KEL
					  , CASE WHEN TRUNC(X.REQ_DATE) > TRUNC(B.PRINTED_DATE) OR B.PRINTED_DATE  IS NULL THEN 'BELUM CETAK' ELSE 'SUDAH CETAK' END STATUS_CETAK
					  , X.REQ_BY
					  , TO_CHAR(X.REQ_DATE,'DD-MM-YYYY') REQ_DATE
           			  , CASE WHEN A.REQ_BY_TTE IS NULL THEN '-' ELSE A.REQ_BY_TTE END REQ_BY_TTE
					  , CASE WHEN TO_CHAR(A.REQ_DATE_TTE,'DD-MM-YYYY') IS NULL THEN '-' ELSE TO_CHAR(A.REQ_DATE_TTE,'DD-MM-YYYY') END REQ_DATE_TTE
					  , CASE WHEN B.PRINTED_DATE IS NULL THEN '-' ELSE TO_CHAR(B.PRINTED_DATE,'DD-MM-YYYY') END TANGGAL_CETAK
					  , CASE  WHEN A.CERT_STATUS IS NULL THEN 'BELUM DIAJUKAN' WHEN A.CERT_STATUS = 1 THEN 'TELAH DIAJUKAN' WHEN A.CERT_STATUS = 2 THEN 'PROSES PENERBITAN TTE' WHEN A.CERT_STATUS = 3 THEN 'SUDAH TERSERTIFIKASI'  WHEN A.CERT_STATUS = 9 THEN 'BELUM DI VERIVIKASI' ELSE 'DOKUMEN TELAH DIPUBLISH' END STATUS_TTE 
            FROM (SELECT  NO_KK,NO_KEC, NO_KEL, REQ_DATE,REQ_BY FROM (SELECT  NO_KK, NO_KEC,NO_KEL, REQ_DATE,REQ_BY, RANK() OVER (PARTITION BY NO_KK ORDER BY REQ_DATE DESC,ID_CETAK DESC) RNK FROM T5_REQ_CETAK_KK ) WHERE RNK = 1) X 
            INNER JOIN DATA_KELUARGA D ON X.NO_KK = D.NO_KK LEFT JOIN (SELECT  NO_KK, PRINTED_DATE FROM (SELECT  NO_KK, PRINTED_DATE, RANK() OVER (PARTITION BY NO_KK ORDER BY PRINTED_DATE DESC, SEQN_ID DESC) RNK FROM T5_SEQN_PRINT_KK ) WHERE RNK = 1) B ON B.NO_KK = D.NO_KK
            LEFT JOIN (SELECT  NO_DOC,PEJABAT_PROCESS_DATE, CERT_STATUS, REQ_DATE REQ_DATE_TTE,REQ_BY REQ_BY_TTE FROM (SELECT  NO_DOC, CERT_STATUS,PEJABAT_PROCESS_DATE, REQ_DATE,REQ_BY, RANK() OVER (PARTITION BY NO_DOC ORDER BY REQ_DATE DESC,SEQN_ID DESC) RNK FROM BSRE_KARTU_KELUARGA ) WHERE RNK = 1) A ON X.NO_KK = A.NO_DOC 
            WHERE 
            TRUNC(A.REQ_DATE_TTE) >= TRUNC(X.REQ_DATE)
            ";
			if($no_kec != 0){
				$sql .= " AND D.NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND D.NO_KEL = $no_kel";
			} 
			$sql .= " AND X.REQ_DATE >= TO_DATE('$tgl_start','DD-MM-YYYY') AND X.REQ_DATE < TO_DATE('$tgl_end','DD-MM-YYYY') +1
				ORDER BY D.NO_KEC, D.NO_KEL, X.REQ_DATE DESC
			";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}

		
		public function scr_cek_req_data_bio($nik,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			$sql .= "SELECT X.NO_KEC,X.NIK,X.NAMA_LGKP AS NAMA_LENGKAP, Y.ALAMAT, CASE WHEN TO_CHAR(Y.KODE_POS) IS NULL THEN '-' ELSE TO_CHAR(Y.KODE_POS) END KODE_POS, LPAD(TO_CHAR(Y.NO_RT), 3, '0') AS RT, LPAD(TO_CHAR(Y.NO_RW), 3, '0') AS RW,  CASE WHEN B.CURRENT_STATUS_CODE IS NULL THEN 'REQ BIO' ELSE B.CURRENT_STATUS_CODE END AS CURRENT_STATUS_CODE,TO_CHAR(A.REQ_DATE,'DD-MM-YYYY') AS REQ_DATE,A.REQ_BY, TO_CHAR(C.CREATED,'DD-MM-YYYY') AS PRINTED_DATE,C.CREATED_USERNAME AS PRINTED_BY FROM BIODATA_WNI X LEFT JOIN DATA_KELUARGA Y ON X.NO_KK = Y.NO_KK INNER JOIN (SELECT ID_CETAK,NO_KEC, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY FROM (SELECT  ID_CETAK, NO_KEC, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY, RANK() OVER (PARTITION BY NIK ORDER BY ID_CETAK DESC) RNK FROM SIAK_REQ_CETAK_KTP@YZDB WHERE STEP_PROC = 'A') WHERE RNK = 1) A ON A.NIK = X.NIK LEFT JOIN DEMOGRAPHICS_ALL@DB2 B ON A.NIK = B.NIK LEFT JOIN DEMOGRAPHICS@DB2 D ON A.NIK = D.NIK LEFT JOIN (SELECT NIK,CREATED,CREATED_USERNAME FROM (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME,LAST_UPDATE,LAST_UPDATE,CREATED, RANK() OVER (PARTITION BY NIK ORDER BY  PERSONALIZED_DATE DESC, LAST_UPDATE DESC, CHIP_ID DESC) RNK FROM CARD_MANAGEMENT@DB2) WHERE RNK = 1) C ON A.NIK = C.NIK  WHERE 1=1 AND ((B.CURRENT_STATUS_CODE IS NULL) AND (D.CURRENT_STATUS_CODE IS NULL))";
			if($no_kec != 0){
				$sql .= " AND X.NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND X.NO_KEL = $no_kel";
			} 
			if($nik != ""){
				$sql .= " AND X.NIK IN ($nik)";
			}
			$sql .= " AND A.REQ_DATE >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.REQ_DATE < TO_DATE('$tgl_end','DD-MM-YYYY') +1";			
			$r = DB::connection('db222')->select($sql);
			return $r;
		}

		public function scr_cek_req_count_bio($nik,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			$sql .= "SELECT COUNT(X.NIK) AS JML FROM BIODATA_WNI X INNER JOIN  (SELECT ID_CETAK,NO_KEC, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY FROM (SELECT  ID_CETAK, NO_KEC, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY, RANK() OVER (PARTITION BY NIK ORDER BY ID_CETAK DESC) RNK FROM SIAK_REQ_CETAK_KTP@YZDB WHERE STEP_PROC = 'A') WHERE RNK = 1) A ON A.NIK = X.NIK LEFT JOIN DEMOGRAPHICS_ALL@DB2 B ON A.NIK = B.NIK LEFT JOIN DEMOGRAPHICS@DB2 D ON A.NIK = D.NIK LEFT JOIN (SELECT NIK,CREATED,CREATED_USERNAME FROM (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME,LAST_UPDATE,LAST_UPDATE,CREATED, RANK() OVER (PARTITION BY NIK ORDER BY  PERSONALIZED_DATE DESC, LAST_UPDATE DESC, CHIP_ID DESC) RNK FROM CARD_MANAGEMENT@DB2) WHERE RNK = 1) C ON A.NIK = C.NIK  WHERE 1=1 AND ((B.CURRENT_STATUS_CODE IS NULL) AND (D.CURRENT_STATUS_CODE IS NULL))";
			if($no_kec != 0){
				$sql .= " AND X.NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND X.NO_KEL = $no_kel";
			} 
			if($nik != ""){
				$sql .= " AND X.NIK IN ($nik)";
			}
			$sql .= " AND A.REQ_DATE >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.REQ_DATE < TO_DATE('$tgl_end','DD-MM-YYYY') +1";
			$r = DB::connection('db222')->select($sql);
			return $r[0]->jml;
		}

		public function scr_cek_list_pengajuan($nik,$no_kec){
			$sql = "";
			$sql .= "SELECT 
					X.NO_KEC
					, X.NIK
					, CONCAT(',',X.NIK) NIK
					, CONCAT(',',X.NO_KK) NO_KK
					, X.NAMA_LGKP AS NAMA_LENGKAP
					, X.TMPT_LHR
					, TO_CHAR(X.TGL_LHR,'DD-MM-YYYY') TGL_LHR
                    , Y.ALAMAT
                    , CASE WHEN TO_CHAR(Y.KODE_POS) IS NULL THEN '-' ELSE TO_CHAR(Y.KODE_POS) END KODE_POS
                    , LPAD(TO_CHAR(Y.NO_RT), 3, '0') AS RT
                    , LPAD(TO_CHAR(Y.NO_RW), 3, '0') AS RW
                    , CASE WHEN B.CURRENT_STATUS_CODE IS NULL THEN 'DATA REKAM KOSONG' ELSE B.CURRENT_STATUS_CODE END AS CURRENT_STATUS_CODE
					, CASE WHEN TO_CHAR(A.REQ_DATE,'YYYY-MM-DD') IS NULL THEN '-' ELSE TO_CHAR(A.REQ_DATE,'YYYY-MM-DD')  END AS REQ_DATE
					, CASE WHEN A.REQ_BY IS NULL THEN '-' ELSE A.REQ_BY END REQ_BY
                    , CASE WHEN A.ALASAN_PENGAJUAN IS NULL THEN '-' ELSE A.ALASAN_PENGAJUAN END ALASAN_PENGAJUAN
					, CASE WHEN A.STEP_PROC = 'T' THEN 'DITOLAK' WHEN A.STEP_PROC = 'C' THEN 'DICETAK' WHEN A.STEP_PROC IS NULL THEN '-' ELSE 'PROSSESS' END AS STATUS_PENGAJUAN
					, CASE WHEN A.ALASAN_PENGAJUAN IS NOT NULL THEN A.ALASAN_PENGAJUAN WHEN A.STEP_PROC = 'T' THEN UPPER(A.PROC_BY) WHEN A.STEP_PROC = 'C' THEN 'SUDAH DICATAK' ELSE '-' END AS KETERANGAN_STATUS
					, CASE WHEN TO_CHAR(C.LAST_UPDATE,'DD-MM-YYYY') IS NULL AND TO_CHAR(C.PERSONALIZED_DATE,'DD-MM-YYYY') IS NULL THEN '-' WHEN TO_CHAR(C.LAST_UPDATE,'DD-MM-YYYY') IS NULL THEN TO_CHAR(C.PERSONALIZED_DATE,'DD-MM-YYYY') ELSE TO_CHAR(C.LAST_UPDATE,'DD-MM-YYYY') END AS PRINTED_DATE
					, CASE WHEN TO_CHAR(C.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(C.CREATED_USERNAME) IS NULL  THEN '-' WHEN TO_CHAR(C.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(C.CREATED_USERNAME) IS NOT NULL THEN TO_CHAR(C.CREATED_USERNAME) ELSE TO_CHAR(C.LAST_UPDATED_USERNAME) END AS PRINTED_BY
					, F5_GET_NAMA_KECAMATAN(X.NO_PROP,X.NO_KAB,X.NO_KEC) NAMA_KEC
					, F5_GET_NAMA_KELURAHAN(X.NO_PROP,X.NO_KAB,X.NO_KEC,X.NO_KEL) NAMA_KEL
					, UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(X.AGAMA,7), 501)) AGAMA
					, UPPER(F5_GET_REF_WNI(X.JENIS_PKRJN, 201)) JENIS_PKRJN
					, UPPER(F5_GET_REF_WNI(X.STAT_KWN, 601)) STAT_KWN
					FROM BIODATA_WNI X LEFT JOIN DATA_KELUARGA Y ON X.NO_KK = Y.NO_KK 
                    LEFT JOIN DEMOGRAPHICS_ALL@DB2 B ON X.NIK = B.NIK 
                    LEFT JOIN (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME,LAST_UPDATE FROM 
                    (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME,LAST_UPDATE, RANK() OVER (PARTITION BY NIK ORDER BY  PERSONALIZED_DATE DESC, LAST_UPDATE DESC, CHIP_ID DESC) RNK FROM 
                    CARD_MANAGEMENT@DB2) WHERE RNK = 1) C ON X.NIK = C.NIK  
                    LEFT JOIN (SELECT ID_CETAK,NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY,STEP_PROC,PROC_BY,ALASAN_PENGAJUAN FROM 
					(SELECT  ID_CETAK, NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY,STEP_PROC,PROC_BY,ALASAN_PENGAJUAN, RANK() OVER (PARTITION BY NIK ORDER BY ID_CETAK DESC) RNK FROM 
                    SIAK_REQ_CETAK_KTP@YZDB WHERE STEP_PROC <> 'C') WHERE RNK = 1) A ON A.NIK =X.NIK
                    WHERE 1=1  AND B.CURRENT_STATUS_CODE NOT IN ('UNREGISTERED' ,'BIO_CAPTURED' ,'ENROLL_FAILURE_AT_REGIONAL' ,'PACKET_RETRY' ,'RECEIVED_AT_CENTRAL' ,'PROCESSING' ,'SENT_FOR_ENROLLMENT' ,'ENROLL_FAILURE_AT_CENTRAL' ,'MISSING_BIOMETRIC_EXCEPTION' ,'INVALID_PACKET' ,'SEARCH_FAILURE_AT_CENTRAL' ,'ADJUDICATE_RECORD' ,'ADJUDICATE_IN_PROCESS' ,'SENT_FOR_DEDUP' ,'DUPLICATE_RECORD' ,'PRINT_READY_RECORD') AND B.CURRENT_STATUS_CODE IS NOT NULL";
			if($no_kec != 0){
				$sql .= " AND X.NO_KEC = $no_kec";
			}
			if($nik != ""){
				$sql .= " AND X.NIK IN ($nik)";
			}
				$sql .= " ORDER BY X.NO_KEC, X.NO_KEL, X.NO_KK, Y.NO_RW, Y.NO_RT, X.NAMA_LGKP";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}
		public function scr_tolak_list_pengajuan($nik,$no_kec){
			$sql = "";
			$sql .= "SELECT 
					X.NO_KEC
					, X.NIK
					, CONCAT(',',X.NIK) NIK
					, CONCAT(',',X.NO_KK) NO_KK
					, X.NAMA_LGKP AS NAMA_LENGKAP
					, X.TMPT_LHR
					, TO_CHAR(X.TGL_LHR,'DD-MM-YYYY') TGL_LHR
                    , Y.ALAMAT
                    , CASE WHEN TO_CHAR(Y.KODE_POS) IS NULL THEN '-' ELSE TO_CHAR(Y.KODE_POS) END KODE_POS
                    , LPAD(TO_CHAR(Y.NO_RT), 3, '0') AS RT
                    , LPAD(TO_CHAR(Y.NO_RW), 3, '0') AS RW
                    , CASE WHEN B.CURRENT_STATUS_CODE IS NULL THEN 'DATA REKAM KOSONG' ELSE B.CURRENT_STATUS_CODE END AS CURRENT_STATUS_CODE
					, CASE WHEN TO_CHAR(A.REQ_DATE,'YYYY-MM-DD') IS NULL THEN '-' ELSE TO_CHAR(A.REQ_DATE,'YYYY-MM-DD')  END AS REQ_DATE
					, CASE WHEN A.REQ_BY IS NULL THEN '-' ELSE A.REQ_BY END REQ_BY
                    , CASE WHEN A.ALASAN_PENGAJUAN IS NULL THEN '-' ELSE A.ALASAN_PENGAJUAN END ALASAN_PENGAJUAN
					, CASE WHEN A.STEP_PROC = 'T' THEN 'DITOLAK' WHEN A.STEP_PROC = 'C' THEN 'DICETAK' WHEN A.STEP_PROC IS NULL THEN '-' ELSE 'PROSSESS' END AS STATUS_PENGAJUAN
					, CASE WHEN A.ALASAN_PENGAJUAN IS NOT NULL THEN A.ALASAN_PENGAJUAN WHEN A.STEP_PROC = 'T' THEN UPPER(A.PROC_BY) WHEN A.STEP_PROC = 'C' THEN 'SUDAH DICATAK' ELSE '-' END AS KETERANGAN_STATUS
					, CASE WHEN TO_CHAR(C.LAST_UPDATE,'DD-MM-YYYY') IS NULL AND TO_CHAR(C.PERSONALIZED_DATE,'DD-MM-YYYY') IS NULL THEN '-' WHEN TO_CHAR(C.LAST_UPDATE,'DD-MM-YYYY') IS NULL THEN TO_CHAR(C.PERSONALIZED_DATE,'DD-MM-YYYY') ELSE TO_CHAR(C.LAST_UPDATE,'DD-MM-YYYY') END AS PRINTED_DATE
					, CASE WHEN TO_CHAR(C.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(C.CREATED_USERNAME) IS NULL  THEN '-' WHEN TO_CHAR(C.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(C.CREATED_USERNAME) IS NOT NULL THEN TO_CHAR(C.CREATED_USERNAME) ELSE TO_CHAR(C.LAST_UPDATED_USERNAME) END AS PRINTED_BY
					, F5_GET_NAMA_KECAMATAN(X.NO_PROP,X.NO_KAB,X.NO_KEC) NAMA_KEC
					, F5_GET_NAMA_KELURAHAN(X.NO_PROP,X.NO_KAB,X.NO_KEC,X.NO_KEL) NAMA_KEL
					, UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(X.AGAMA,7), 501)) AGAMA
					, UPPER(F5_GET_REF_WNI(X.JENIS_PKRJN, 201)) JENIS_PKRJN
					, UPPER(F5_GET_REF_WNI(X.STAT_KWN, 601)) STAT_KWN
					FROM BIODATA_WNI X LEFT JOIN DATA_KELUARGA Y ON X.NO_KK = Y.NO_KK 
                    LEFT JOIN DEMOGRAPHICS_ALL@DB2 B ON X.NIK = B.NIK 
                    LEFT JOIN (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME,LAST_UPDATE FROM 
                    (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME,LAST_UPDATE, RANK() OVER (PARTITION BY NIK ORDER BY  PERSONALIZED_DATE DESC, LAST_UPDATE DESC, CHIP_ID DESC) RNK FROM 
                    CARD_MANAGEMENT@DB2) WHERE RNK = 1) C ON X.NIK = C.NIK  
                    LEFT JOIN (SELECT ID_CETAK,NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY,STEP_PROC,PROC_BY,ALASAN_PENGAJUAN FROM 
					(SELECT  ID_CETAK, NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY,STEP_PROC,PROC_BY,ALASAN_PENGAJUAN, RANK() OVER (PARTITION BY NIK ORDER BY ID_CETAK DESC) RNK FROM 
                    SIAK_REQ_CETAK_KTP@YZDB WHERE STEP_PROC <> 'C') WHERE RNK = 1) A ON A.NIK =X.NIK
                    WHERE 1=1  AND (B.CURRENT_STATUS_CODE IN ('UNREGISTERED' ,'BIO_CAPTURED' ,'ENROLL_FAILURE_AT_REGIONAL' ,'PACKET_RETRY' ,'RECEIVED_AT_CENTRAL' ,'PROCESSING' ,'SENT_FOR_ENROLLMENT' ,'ENROLL_FAILURE_AT_CENTRAL' ,'MISSING_BIOMETRIC_EXCEPTION' ,'INVALID_PACKET' ,'SEARCH_FAILURE_AT_CENTRAL' ,'ADJUDICATE_RECORD' ,'ADJUDICATE_IN_PROCESS' ,'SENT_FOR_DEDUP' ,'DUPLICATE_RECORD' ,'PRINT_READY_RECORD') OR B.CURRENT_STATUS_CODE IS NULL)";
			if($no_kec != 0){
				$sql .= " AND X.NO_KEC = $no_kec";
			}
			if($nik != ""){
				$sql .= " AND X.NIK IN ($nik)";
			}
				$sql .= " ORDER BY X.NO_KEC, X.NO_KEL, X.NO_KK, Y.NO_RW, Y.NO_RT, X.NAMA_LGKP";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}
		public function scr_do_ajukan_perubahan_data($nik,$user_id){
			$sql = "INSERT INTO SIAK_REQ_CETAK_KTP ( NIK ,NO_KK ,NAMA_LENGKAP ,TGL_LAHIR ,TMP_LAHIR ,NO_PROP ,NO_KAB ,NO_KEC ,NO_KEL ,REQ_DATE ,REQ_BY ,PROC_DATE ,PROC_BY ,STEP_PROC ,SMS_COUNT ,ALAMAT ,NO_RT ,NO_RW ,DUSUN ,TELP ,KODE_POS ,JENIS_KLMIN ,ID_CETAK ,SMS_PHONE, ALASAN_PENGAJUAN) 
					SELECT B.NIK, B.NO_KK, B.NAMA_LGKP NAMA_LENGKAP, B.TGL_LHR TGL_LAHIR, B.TMPT_LHR TMP_LAHIR, B.NO_PROP, B.NO_KAB, B.NO_KEC, B.NO_KEL, SYSDATE REQ_DATE, '$user_id' REQ_BY, NULL PROC_DATE , NULL PROC_BY, 'A' STEP_PROC, 0 SMS_COUNT, C.ALAMAT, C.NO_RT, C.NO_RW, C.DUSUN, B.SMS_PHONE TELP, C.KODE_POS, B.JENIS_KLMIN, CONCAT(CONCAT(CONCAT(TRUNC(DBMS_RANDOM.VALUE(1000000000,9999999999)),'-'),TRUNC(DBMS_RANDOM.VALUE(1000000000,9999999999))),B.NIK) ID_CETAK, B.SMS_PHONE, 'PERUBAHAN ELEMEN' ALASAN_PENGAJUAN FROM BIODATA_WNI@DB222 B INNER JOIN DATA_KELUARGA C ON B.NO_KK = C.NO_KK  
					WHERE NIK IN ($nik) AND NOT EXISTS (SELECT 1 FROM SIAK_REQ_CETAK_KTP D WHERE B.NIK = D.NIK AND D.STEP_PROC = 'A')";
			$r = DB::insert($sql);
		}
		public function scr_do_ajukan_rusak($nik,$user_id){
			$sql = "INSERT INTO SIAK_REQ_CETAK_KTP ( NIK ,NO_KK ,NAMA_LENGKAP ,TGL_LAHIR ,TMP_LAHIR ,NO_PROP ,NO_KAB ,NO_KEC ,NO_KEL ,REQ_DATE ,REQ_BY ,PROC_DATE ,PROC_BY ,STEP_PROC ,SMS_COUNT ,ALAMAT ,NO_RT ,NO_RW ,DUSUN ,TELP ,KODE_POS ,JENIS_KLMIN ,ID_CETAK ,SMS_PHONE, ALASAN_PENGAJUAN) 
					SELECT B.NIK, B.NO_KK, B.NAMA_LGKP NAMA_LENGKAP, B.TGL_LHR TGL_LAHIR, B.TMPT_LHR TMP_LAHIR, B.NO_PROP, B.NO_KAB, B.NO_KEC, B.NO_KEL, SYSDATE REQ_DATE, '$user_id' REQ_BY, NULL PROC_DATE , NULL PROC_BY, 'A' STEP_PROC, 0 SMS_COUNT, C.ALAMAT, C.NO_RT, C.NO_RW, C.DUSUN, B.SMS_PHONE TELP, C.KODE_POS, B.JENIS_KLMIN, CONCAT(CONCAT(CONCAT(TRUNC(DBMS_RANDOM.VALUE(1000000000,9999999999)),'-'),TRUNC(DBMS_RANDOM.VALUE(1000000000,9999999999))),B.NIK) ID_CETAK, B.SMS_PHONE, 'RUSAK' ALASAN_PENGAJUAN FROM BIODATA_WNI@DB222 B INNER JOIN DATA_KELUARGA C ON B.NO_KK = C.NO_KK  
					WHERE NIK IN ($nik) AND NOT EXISTS (SELECT 1 FROM SIAK_REQ_CETAK_KTP D WHERE B.NIK = D.NIK AND D.STEP_PROC = 'A')";
			$r = DB::insert($sql);
		}
		public function scr_do_ajukan_hilang($nik,$user_id){
			$sql = "INSERT INTO SIAK_REQ_CETAK_KTP ( NIK ,NO_KK ,NAMA_LENGKAP ,TGL_LAHIR ,TMP_LAHIR ,NO_PROP ,NO_KAB ,NO_KEC ,NO_KEL ,REQ_DATE ,REQ_BY ,PROC_DATE ,PROC_BY ,STEP_PROC ,SMS_COUNT ,ALAMAT ,NO_RT ,NO_RW ,DUSUN ,TELP ,KODE_POS ,JENIS_KLMIN ,ID_CETAK ,SMS_PHONE, ALASAN_PENGAJUAN) 
					SELECT B.NIK, B.NO_KK, B.NAMA_LGKP NAMA_LENGKAP, B.TGL_LHR TGL_LAHIR, B.TMPT_LHR TMP_LAHIR, B.NO_PROP, B.NO_KAB, B.NO_KEC, B.NO_KEL, SYSDATE REQ_DATE, '$user_id' REQ_BY, NULL PROC_DATE , NULL PROC_BY, 'A' STEP_PROC, 0 SMS_COUNT, C.ALAMAT, C.NO_RT, C.NO_RW, C.DUSUN, B.SMS_PHONE TELP, C.KODE_POS, B.JENIS_KLMIN, CONCAT(CONCAT(CONCAT(TRUNC(DBMS_RANDOM.VALUE(1000000000,9999999999)),'-'),TRUNC(DBMS_RANDOM.VALUE(1000000000,9999999999))),B.NIK) ID_CETAK, B.SMS_PHONE, 'HILANG' ALASAN_PENGAJUAN FROM BIODATA_WNI@DB222 B INNER JOIN DATA_KELUARGA C ON B.NO_KK = C.NO_KK  
					WHERE NIK IN ($nik) AND NOT EXISTS (SELECT 1 FROM SIAK_REQ_CETAK_KTP D WHERE B.NIK = D.NIK AND D.STEP_PROC = 'A')";
			$r = DB::insert($sql);
		}
		public function scr_cek_list_hilang($tgl_start,$tgl_end,$no_kec){
			$sql = "";
			$sql .= "SELECT 
					X.NO_KEC
					, X.NIK DO_NIK
					, CONCAT(',',X.NIK) NIK
					, CONCAT(',',X.NO_KK) NO_KK
					, X.NAMA_LGKP AS NAMA_LENGKAP
					, X.TMPT_LHR
					, TO_CHAR(X.TGL_LHR,'DD-MM-YYYY') TGL_LHR
                    , Y.ALAMAT
                    , CASE WHEN TO_CHAR(Y.KODE_POS) IS NULL THEN '-' ELSE TO_CHAR(Y.KODE_POS) END KODE_POS
                    , LPAD(TO_CHAR(Y.NO_RT), 3, '0') AS RT
                    , LPAD(TO_CHAR(Y.NO_RW), 3, '0') AS RW
					, CASE WHEN TO_CHAR(A.REQ_DATE,'YYYY-MM-DD') IS NULL THEN '-' ELSE TO_CHAR(A.REQ_DATE,'YYYY-MM-DD')  END AS REQ_DATE
					, CASE WHEN A.REQ_BY IS NULL THEN '-' ELSE A.REQ_BY END REQ_BY
                    , CASE WHEN A.ALASAN_PENGAJUAN IS NULL THEN '-' ELSE A.ALASAN_PENGAJUAN END ALASAN_PENGAJUAN
                    , CASE WHEN A.NO_KEHILANGAN IS NULL THEN '-' ELSE A.NO_KEHILANGAN END NO_KEHILANGAN
					, F5_GET_NAMA_KECAMATAN(X.NO_PROP,X.NO_KAB,X.NO_KEC) NAMA_KEC
					, F5_GET_NAMA_KELURAHAN(X.NO_PROP,X.NO_KAB,X.NO_KEC,X.NO_KEL) NAMA_KEL
					FROM BIODATA_WNI X LEFT JOIN DATA_KELUARGA Y ON X.NO_KK = Y.NO_KK 
                    LEFT JOIN (SELECT ID_CETAK,NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY,STEP_PROC,PROC_BY,ALASAN_PENGAJUAN,NO_KEHILANGAN FROM 
					(SELECT  ID_CETAK, NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY,STEP_PROC,PROC_BY,ALASAN_PENGAJUAN,NO_KEHILANGAN, RANK() OVER (PARTITION BY NIK ORDER BY ID_CETAK DESC) RNK FROM 
                    SIAK_REQ_CETAK_KTP@YZDB WHERE STEP_PROC <> 'C') WHERE RNK = 1) A ON A.NIK =X.NIK
                    WHERE 1=1 AND A.ALASAN_PENGAJUAN = 'HILANG' ";
			if($no_kec != 0){
				$sql .= " AND X.NO_KEC = $no_kec";
			}
			$sql .= " AND A.REQ_DATE >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.REQ_DATE < TO_DATE('$tgl_end','DD-MM-YYYY') +1";			
			$r = DB::connection('db222')->select($sql);
			return $r;
		}
	
	public function src_get_cek_pengajuan_hilang($nik)
    {
        $sql = "SELECT A.NIK FROM SIAK_REQ_CETAK_KTP A WHERE A.NIK = $nik AND ALASAN_PENGAJUAN = 'HILANG' ";
        $r = DB::select($sql);
        return $r;
    }

    public function src_update_pengajuan_hilang($nik_hilang, $no_kehilangan)
    {
        $sql = "UPDATE SIAK_REQ_CETAK_KTP SET KEHILANGAN = 1, NO_KEHILANGAN = upper('$no_kehilangan') WHERE NIK = $nik_hilang AND ALASAN_PENGAJUAN = 'HILANG'";
        $r = DB::update($sql);
        return $r;
    }

}
